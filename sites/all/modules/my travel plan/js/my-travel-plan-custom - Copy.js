/*
* Company : Orange Mantra
* Author : Kumar.shiv@orangemantra.in
* Package : Myscore
* Module : My Travel Plan
* Creation Date : July , 2016
*/
var sk = jQuery.noConflict();
/* onSubmit: validation to check any blank entry for the month */
var addplan = true;
function submit_validation(obj){
		var p=0;
		jQuery(function() {
			sk.ajax({
				async: false,
				url: Drupal.settings.travelplan.hostname+'/my-travel-plan/getBlankPlan',
				type: 'post',
				data: 'mon='+'sd',
				success: function (response) {
					var data = JSON.parse(response);
					
					var fruits=[];
					var html ='<br/>';
					for(i=0;i<data.length;i++){
						if(data[i]['session']=='1' && data[i]['channel_partner_id']!='5'){
								
								var d = new Date(data[i]['visit_date']);
								html += d.getDate()+'-'+d.getMonth()+'-'+d.getFullYear()+'<br/>';
						}
						else if(data[i]['session']=='2' && data[i]['channel_partner_id']!='5'){
							
							var d = new Date(data[i]['visit_date']);
							html += d.getDate()+'-'+d.getMonth()+'-'+d.getFullYear()+'<br/>';
						}
					}
				
					
				}
			   });
		});
		if(p==0){
			return true;
		}
		else{	
			return false;
		}
}

sk(document).ready(function($) {
	
	/* last visit date */	
			sk(document).on('change','#edit-dealer-name',function(e){
				
				var dcode = sk(this).val();
				sk('body .last_visit_date').remove();
				if(dcode < 100){ return true; }
				
				$.ajax({
						url: Drupal.settings.travelplan.hostname+"/my-travel-plan/get-last-visit-date",
						type: "post",
						data: {dealer_code:dcode},
						success: function (response) {
							sk('#edit_dealer_name_chosen').after(response);
						}
				});			
	});
			
	/* add monthly visit plan */	
	$('#calendar').fullCalendar({
			
			firstDay: 1,
			header: {
				left: 'title',
				center: '',
				right: 'prev,next,today'
			},			
			defaultDate: ($.cookie("currentDate") != '' ? $.cookie("currentDate") : Drupal.settings.travelplan.curr_date),
			editable: true,
			eventLimit: true, 
			events: {
				url: Drupal.settings.travelplan.hostname+'/my-travel-plan/monthly-visit-plan-ajax',
				error: function() {
					$('#script-warning').show();
				}
			},
			loading: function(bool) {
				$('#loading').toggle(bool);
			},
			viewRender: function (view, element) {				
				
			},
			
			eventClick:  function(event, jsEvent, view) {
				var visit_str = '';
				$('#eventInfo').html('');
				var open_con='';
				/* if plan submited then hide delete and edit buttons */
				if(parseInt(event.submited) != 1 ){
					var open_con= '<div class="edit-plan"><span onclick="openBox('+event.visit_id+');">Edit</span>'+
					'<form method="post" class="none" action="'+Drupal.settings.travelplan.hostname+'/my-travel-plan/add-monthly-visit-plan" >'+
					'<input value="Delete" type="submit" class="noborderbutton" name="deleteplan-submit" />'+
					'<input type="hidden" value='+event.visit_id+' name="visit_plan_id" />'+
					'</form></div>';
				}
				/* if not dealer then hide last visit date */
				if(parseInt(event.dealer_id) > 8){
					visit_str = '<p><b>Visit Purpose : </b>'+event.visit_purpose+'<p/><p><b>Visit Details : </b>'+event.description+'<p/><p><b>Last Visit Date : </b>'+event.last_visit_date+'<p/>';
				}else{
					visit_str = '<p><b>Visit Purpose : </b>'+event.visit_purpose+'<p/><p><b>Visit Details : </b>'+event.description+'<p/>';
				}
					
				$.colorbox({					
					html:$('#eventInfo').html('<p><b>Date: </b>'+event.visit_date+'</p>'+event.title+'<br/>'+visit_str+open_con),
					onClosed:function(){
						if(!$('#eventContent').find('#eventInfo').length){
							$('#eventContent').append("<div id='eventInfo'></div>");
						}
					}
				});
			
			},
			 eventRender: function(event, element) {
				
				if(event.visit == 'Leave' || event.visit == 'Holiday'){
					element.addClass('leave');
				}				
				if(event.session == 1){
					element.addClass('first-half');
				}else if(event.session == 2){
					element.addClass('second-half');
				}else if(event.session == 3){
					element.addClass('full-day');
					
				}
				/* plans are submited for the month then remove add plan button*/				
				if(event.submited == 1){
					$('#calendar').addClass('plan_submited');
					
				}
				element.find('.fc-title').html($.parseHTML( event.title )); 
				
			},
			eventAfterRender: function (event, element, view) {		
				//$(element).css('height', '50px');
            },
			eventAfterAllRender: function (view) {		
				$(".fc-day-number").append("<a href='#addplan' class='addmoreplan' >+Add Plan</a>");
				var b = $('#calendar').fullCalendar('getDate');
				var cur_my = b.format('L');
				$('.getDate').val(cur_my);
				var splitter = cur_my.split('/');
				var MyDate = new Date();
				var month = ('0' + (MyDate.getMonth() + 1)).slice(-2);
				var year = MyDate.getFullYear();
				if(month + ' '+year != splitter[0]+' '+splitter[2]){
					$('.fc-myCustomButton-button').hide();
					
				}
				else{
					$('.fc-myCustomButton-button').show();
					
				}
				/* hide add plan button */
				setTimeout(function(){
				var show_submit = $('input[name="plan_modify_status"]').val();				
				
				console.log(parseInt(month) +'=='+ parseInt(splitter[0]));
				console.log('show_submit'+show_submit);
				
				if((parseInt(month) >= parseInt(splitter[0])) && (parseInt(show_submit) == 1)){
						$('.addmoreplan').hide();	
						addplan = false;
						
				}else{	
					$('.addmoreplan').show();
					addplan = true;
					
				}}, 1000);
				
            },
			
		});	/* open add  visit form */
		
		
		$(document).on('click', 'button.fc-prev-button', function() {
				setTimeout(function(){					
					$.cookie("currentDate", $('.getDate').val(),{ expires : 1, path: '/' });
				},
				2000);
			
		});

		$(document).on('click', 'button.fc-next-button', function() {
			setTimeout(function(){			
				$.cookie("currentDate", $('.getDate').val(),{ expires : 1, path: '/' });
			},
			2000);
		
		});
			
		$( ".fc-today-button" ).bind( "click", function() {		
		    $.cookie("currentDate", $('.currDate').val(),{ expires : 1, path: '/' });
		});
		
		/* custom button to submit visit plan */
		$('.fc-right').find('.fc-button-group').after("<form onsubmit=\"return myFunction(this)\" id=\'edit-submit-form\' method=\'post\' action=\'"+Drupal.settings.travelplan.hostname+"/my-travel-plan/add-monthly-visit-plan/\'><input class=\'current-month-submit fc-button fc-state-default fc-myCustomButton-button \' type=\'submit\' name=\'save-submit\' value=\'Submit\' /></form>");
		
		$('.fc-right').find('.fc-button-group').before("<a class=\'travel_guideline\' href=\'#\' >Travel Guideline</a>");
		
		/* onload check for show current month submit button */
		/* ERROR #1 */
		setTimeout(function(){
			
		var b = $('#calendar').fullCalendar('getDate');
		var cur_my = b.format('L');				
		var splitter = cur_my.split('/');
		var MyDate = new Date();
		var month = ('0' + (MyDate.getMonth() + 1)).slice(-2);


		if(parseInt(month) == parseInt(splitter[0])){
			var show_submit = $('input[name="plan_modify_status"]').val();
			//alert(show_submit);
			if(show_submit == 1){
				$('.fc-myCustomButton-button').val(' Submitted ');
				$('.fc-myCustomButton-button').attr('disabled','disabled');
			
			} else if(show_submit == 2){
				$('.fc-myCustomButton-button').val(' Expired ');
				$('.fc-myCustomButton-button').attr('disabled','disabled');	
				
			}	
		}},1000);
		
		$(document).on('change','.chosen-select', function(){
			if(parseInt($(this).val())==5){
				
				$('.edit-session').attr('disabled', 'disabled');
			}
			else{
				$('.visit-purpose').removeAttr('disabled');
				$('.edit-session').removeAttr('disabled');
			}
		});
		$(document).on('click','#calendar .fc-day-number:not(.fc-other-month)', function(e){
			e.preventDefault();
			if(!addplan){ return; }
			
			var selected_date = $('.getDate').val();		
				var sd = selected_date.split('/');
				sd = sd[1]+'-'+sd[2];
				
			if(parseInt($(this).text()) < 5 && !$(this).hasClass('fc-past')){
				var selected_dt = $(this).attr('data-date');
				
			
			var date = new Date(selected_dt);

			$('#add_visit_form_container h1 span#selected-date').text(date.getDate()+'-'+(date.getMonth() + 1)+'-'+date.getFullYear());	

			$('input[name=\"visit_date\"]').val(selected_dt);	
			var session = [];
			$('input[name='+selected_dt+']').each(function(){
				session.push(parseInt($(this).val()));
			});	
			
			$('select#edit-session option').show();
			if($.inArray(3, session) != -1) {
				$.colorbox({html:'You can\'t add more visit plans'});	
			}
			else if($.inArray(1, session)  != -1 && $.inArray(2, session)  != -1) {
				$.colorbox({html:'You can\'t add more visit plans'});	
			}
			else if($.inArray(1, session)  !=-1) {
				$('select#edit-session option[value=\"1\"]').hide();
				$('select#edit-session option[value=\"2\"]').show();
				$('select#edit-session option[value=\"3\"]').hide();
				$.colorbox({html:$('#add_visit_form_container').html()});
				 
			}
			else if($.inArray(2, session)  != -1) {
				$('select#edit-session option[value=\"1\"]').show();
				$('select#edit-session option[value=\"2\"]').hide();
				$('select#edit-session option[value=\"3\"]').hide();
				$.colorbox({html:$('#add_visit_form_container').html()});
				 
			}
			else{
				$.colorbox({html:$('#add_visit_form_container').html()});
				 		
			} 
			
			}
			else{
				var selected_dt = $(this).attr('data-date');
				var date = new Date(selected_dt);
				$('#add_visit_form_container h1 span#selected-date').text(date.getDate()+'-'+(date.getMonth() + 1)+'-'+date.getFullYear());
				$('input[name=\"visit_date\"]').val(selected_dt);
				$.colorbox({html:$('#add_visit_form_container').html()});	
				
			}
			$('#cboxContent .chosen-select').chosen({no_results_text: 'Oops, nothing found!',width: '95%'}); 
					
		});	
		
		
		
});
/* get tm plan in ASM view */
sk(document).ready(function($){
	$("#gettm-button").click(function(){
		var tm_id = $("#select-tm option:selected").val();
		$.ajax({
				url: Drupal.settings.travelplan.hostname+"/my-travel-plan/get-tm-Plan",
				type: "post",
				data: "tm_id="+tm_id,
				beforeSend:function(){
					$(".loadergif1").css("display","inline-block");					
				},
				complete:function(){
					$(".loadergif1").css("display","none");
				},
				success: function (response) {
						$("#result").html(response);
				}
		});
	});
});

/* actual submit */
sk(document).ready(function($) {	
			
			$(".save-actual-submit").click(function(){
				var select_var = $(this).attr("data-class");
				var actual_dealer_visit = $("select."+select_var+" option:selected").val();
				var remarks = $("textarea."+select_var+"").val();
				var today_date = $(".today-date").val();
				var planned_visit = $("input."+select_var).val();
				var user_id = $("input.user_id").val();
				var ele = $(this);
				jQuery(function($) {
					$.ajax({
						async: false,
						url: Drupal.settings.travelplan.hostname+"/my-travel-plan/saveActualVisit",
						type: "post",
						data: "actual_dealer_visit="+actual_dealer_visit+"&today_date="+today_date+"&visit_id="+select_var+"&planned_visit="+planned_visit+"&user_id="+user_id+"&remarks="+remarks,
						beforeSend:function(){
							 ele.next("span.loadergif").css("display","inline-block");
						},
						complete:function(){
							
						},
						success: function (response) {
							ele.next("span.loadergif").css("display","none");							
							//ele.attr("disabled", true);
							//ele.val("submitted");
							$.colorbox({html:$.parseHTML( response ),width:"600",height:"200",close:"Cancel",onLoad:function(){
								$('#cboxYes').remove();
								$('#cboxClose').before('<button type="button" id="cboxYes">Yes</button>');
							}});
							
						}
					});
				}); 
			});
			
			/* submit actual */
			sk(document).on('click','#cboxYes',function(e){
				e.preventDefault();
				var yesno = sk('#myscore_submit [name="submit_myscore"]:checked').val();
				if(yesno == 'yes' || yesno == 'no'){
					
					window.location.href = sk('#myscore_submit').data('href')+'&option='+yesno;	
				}else{
					alert('Please select option.');
					return;
				}
				
			});
			
			
			
}); 		

/* edit visit plan */
function openBox(visit_id){
	jQuery(function($) {
		$.colorbox.close();
			$.ajax({
				url: Drupal.settings.travelplan.hostname+'/my-travel-plan/edit-monthly-visit-plan-ajax',
				type: 'post',
				data: 'visit_id='+visit_id,
				beforeSend:function(){
					$('.loaders').show();
				},
				complete:function(){
					$('.loaders').hide();
				},
				success: function (response) {
					$.colorbox({html:$.parseHTML( response )});
					$('#cboxContent .chosen-select').chosen({no_results_text: 'Oops, nothing found!',width: '95%'}); 
					$('#cboxContent .visit-type-select').change(function(){
						if($(this).val()==1){
							$('#cboxContent .chosen-select').prop('disabled', true).trigger('chosen:updated');
							$('#cboxContent .visit-purpose').prop('disabled', true);
							$('#cboxContent .visit-purpose').addClass('disabled');
							$('#cboxContent .visit-details').prop('disabled', true);
							$('#cboxContent .visit-details').addClass('disabled');
						}
						else{
							$('#cboxContent .chosen-select').prop('disabled', false).trigger('chosen:updated');
							$('#cboxContent .visit-purpose').removeClass('disabled');
							$('#cboxContent .visit-purpose').prop('disabled', false);
							$('#cboxContent .visit-details').prop('disabled', false);
							$('#cboxContent .visit-details').removeClass('disabled');
						}
					});		
				}
	   });
});
	  
}	

/* date picker */
sk(document).ready(function() {	
	sk('#user_id').val(sk('#uid').val());
	
	sk('.select-month').datepicker({ 
		changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
		yearRange: '2015:+20',
        onClose: function(dateText, inst) { 
            sk(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
	}); 
	
	sk('.page-my-travel-plan-add-monthly-visit-plan #messages').click(function(){
				sk(this).hide();
	});
	
	if(sk('#messages').length > 0 && sk('#messages').text().indexOf('Warning') != -1 ){
		sk('#messages').hide();
		sk.colorbox({ html:sk('#messages').html(), });
	}
	
	/* open travel guideline */	
	sk(document).on('click','.travel_guideline',function(e){
			e.preventDefault();
			var emp_department = sk('[name="emp_department"]').val();
				
			if(emp_department == 'services'){
				sk.colorbox({href:Drupal.settings.travelplan.hostname+"/sites/default/files/travel-guideline-services.html"});
			}else if(emp_department == 'sales'){
				sk.colorbox({href:Drupal.settings.travelplan.hostname+"/sites/default/files/travel-guideline-sales.html"});
			}
	});
	
	
});
/*
* Company : Orange Mantra
* Author : Kumar.shiv@orangemantra.in
* Package : Myscore
* Module : My Travel Plan
* Creation Date : July , 2016
*/