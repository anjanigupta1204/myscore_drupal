-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 28, 2016 at 12:19 AM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seindia_myscore`
--

-- --------------------------------------------------------

--
-- Table structure for table `travel_plan`
--

CREATE TABLE IF NOT EXISTS `travel_plan` (
  `visit_plan_id` int(10) unsigned NOT NULL COMMENT 'Visit Plan Id.',
  `uid` int(10) unsigned NOT NULL COMMENT 'User Id',
  `channel_partner_id` int(10) unsigned NOT NULL COMMENT 'Channel Partner ID',
  `channel_partner` varchar(10) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Actual Submited',
  `visit_purpose` varchar(255) DEFAULT NULL COMMENT 'For which purpose',
  `visit_type` int(5) DEFAULT '0',
  `actual_dealer_visit` varchar(255) DEFAULT '0',
  `visit_date` varchar(255) DEFAULT '0' COMMENT 'visited date.',
  `month_year` varchar(6) DEFAULT NULL,
  `create_date` varchar(255) DEFAULT '0' COMMENT 'Visit created date.',
  `modified_date` varchar(255) DEFAULT NULL COMMENT 'Visit modified date.',
  `session` varchar(50) DEFAULT NULL COMMENT 'Visit session morning or evening',
  `visit_details` mediumtext COMMENT 'Visit Details',
  `remarks` text
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='Stores visit plan basic information.';

--
-- Dumping data for table `travel_plan`
--

INSERT INTO `travel_plan` (`visit_plan_id`, `uid`, `channel_partner_id`, `channel_partner`, `status`, `visit_purpose`, `visit_type`, `actual_dealer_visit`, `visit_date`, `month_year`, `create_date`, `modified_date`, `session`, `visit_details`, `remarks`) VALUES
(64, 453, 1, 'Leave', '0', '', 0, '0', '2016-09-03', '092016', '1473660226', NULL, '3', '', NULL),
(65, 453, 1, 'Leave', '0', '', 0, '0', '2016-09-01', '092016', '1473660239', NULL, '3', '', NULL),
(66, 453, 7, 'ZO', '0', '', 0, '0', '2016-09-02', '092016', '1473660249', NULL, '3', '', NULL),
(67, 1797, 10000, 'DLR', '1', '1', 0, '10000', '2016-09-01', '092016', '1473672063', NULL, '1', 'vgrg', '5heth'),
(68, 1797, 10000, 'DLR', '1', '', 0, '10000', '2016-09-02', '092016', '1473672079', NULL, '2', 'gnf', ''),
(69, 1797, 4, 'Training', '0', '2', 0, '0', '2016-09-05', '092016', '1473672099', NULL, '3', 'f s ', ''),
(70, 1797, 3, 'Plant', '0', '1', 0, '0', '2016-09-06', '092016', '1473672114', NULL, '3', 'hndgnh', 'dfgdfsg'),
(71, 1797, 4, 'Training', '0', '2', 0, '0', '2016-09-07', '092016', '1473672129', NULL, '3', 'geb', ''),
(72, 1797, 10000, 'DLR', '1', '2', 0, '10000', '2016-09-08', '092016', '1473672144', NULL, '3', 'trbb', ''),
(73, 1797, 11502, 'DLR', '1', '2', 0, '11502', '2016-09-09', '092016', '1473672160', NULL, '3', 'brbwr', 'dfgmsdfngkjdfsg'),
(74, 1797, 11502, 'DLR', '0', '2', 0, '0', '2016-09-12', '092016', '1473672176', NULL, '3', 'bfbg', ''),
(75, 1797, 29000, 'ASC', '1', '2', 0, '10000', '2016-09-13', '092016', '1473672450', NULL, '3', '6u767u', 'asdfasffsfsa'),
(76, 1797, 4, 'Training', '0', '2', 0, '0', '2016-09-14', '092016', '1473672464', NULL, '3', '6u5', ''),
(77, 1797, 11390, 'DLR', '1', '2', 0, '11390', '2016-09-15', '092016', '1473672478', NULL, '3', '67u7u', ''),
(78, 1797, 11501, 'DLR', '0', '2', 0, '0', '2016-09-16', '092016', '1473672496', NULL, '3', 'j65', ''),
(79, 1797, 11501, 'DLR', '1', '2', 0, '11501', '2016-09-19', '092016', '1473672680', NULL, '2', 'j655k5', ''),
(80, 1797, 29001, 'ARD', '0', '2', 0, '0', '2016-09-20', '092016', '1473672699', NULL, '3', 'jyujr', ''),
(81, 1797, 11390, 'DLR', '0', '2', 0, '0', '2016-09-21', '092016', '1473672714', NULL, '3', '75kk', ''),
(82, 1797, 3, 'Plant', '0', '2', 0, '0', '2016-09-22', '092016', '1473672727', NULL, '3', '7u67u', ''),
(83, 1797, 2, 'HO', '0', '', 0, '0', '2016-09-23', '092016', '1473672751', NULL, '3', '', ''),
(84, 1797, 3, 'Plant', '0', '2', 0, '0', '2016-09-26', '092016', '1473672766', NULL, '3', 'ujj', ''),
(85, 1797, 10760, 'DLR', '1', '1', 0, '10760', '2016-09-27', '092016', '1473672782', NULL, '3', 'liolio', ''),
(86, 1797, 11390, 'DLR', '1', '2', 0, '11390', '2016-09-28', '092016', '1473672797', NULL, '3', 'hnghf', ''),
(87, 1797, 11382, 'DLR', '0', '2', 0, '0', '2016-09-29', '092016', '1473672816', NULL, '3', '5t45t3', ''),
(88, 1797, 2, 'HO', '0', '', 0, '0', '2016-09-30', '092016', '1473672832', NULL, '3', '', ''),
(89, 1797, 3, 'Plant', '0', '2', 0, '0', '2016-09-01', '092016', '1473672884', NULL, '2', 'ttttttttn', ''),
(90, 1797, 6, 'AO', '0', '2', 0, '0', '2016-09-02', '092016', '1473672901', NULL, '1', '', ''),
(91, 1797, 4, 'Training', '0', '1', 0, '0', '2016-09-19', '092016', '1473672988', NULL, '1', '', ''),
(92, 1797, 10000, 'DLR', '0', '1', 0, '0', '2016-10-03', '102016', '1474003122', NULL, '1', 'Discuss action plan and evaluate network manager along with TM Service', NULL),
(93, 1797, 11501, 'DLR', '0', '2', 0, '0', '2016-10-03', '102016', '1474003163', NULL, '2', 'Network Sales Evaluation', NULL),
(94, 1797, 10000, 'DLR', '0', '1', 0, '0', '2016-10-04', '102016', '1474863504', NULL, '1', 'gfgffdhjg', NULL),
(95, 1797, 10000, 'DLR', '0', '1', 0, '0', '2016-10-05', '102016', '1474863566', NULL, '1', ',m,g,gk,', NULL),
(96, 1797, 10000, 'DLR', '0', '1', 0, '0', '2016-10-06', '102016', '1474863611', NULL, '1', 'ruddutut', NULL),
(97, 1797, 11502, 'DLR', '0', '1', 0, '0', '2016-10-07', '102016', '1474893174', NULL, '1', '', NULL),
(98, 1797, 10000, 'DLR', '0', '1', 0, '0', '2016-10-11', '102016', '1474894900', NULL, '2', '', NULL),
(99, 1797, 10000, 'DLR', '0', '1', 0, '0', '2016-10-12', '102016', '1475035317', NULL, '1', 'bgfhgfhf', NULL),
(100, 1797, 10760, 'DLR', '0', '1', 0, '0', '2016-10-13', '102016', '1475035924', NULL, '1', 'jygukt', NULL),
(101, 1797, 11390, 'DLR', '0', '1', 0, '0', '2016-10-14', '102016', '1475035973', NULL, '2', 'jygukt', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `travel_plan_submit`
--

CREATE TABLE IF NOT EXISTS `travel_plan_submit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `month_year` varchar(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_on` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `travel_plan_submit`
--

INSERT INTO `travel_plan_submit` (`id`, `user_id`, `month_year`, `status`, `created_on`, `modified_on`) VALUES
(3, 1797, '092016', 1, '2016-09-12 09:36:36', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `travel_plan`
--
ALTER TABLE `travel_plan`
  ADD PRIMARY KEY (`visit_plan_id`);

--
-- Indexes for table `travel_plan_submit`
--
ALTER TABLE `travel_plan_submit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `travel_plan`
--
ALTER TABLE `travel_plan`
  MODIFY `visit_plan_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Visit Plan Id.',AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `travel_plan_submit`
--
ALTER TABLE `travel_plan_submit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
