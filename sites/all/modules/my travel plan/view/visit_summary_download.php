<div class="download-report" >
<div class="col-right">
<form accept-charset="UTF-8" onsubmit="return setFormData();"id="submit-visit-summary-form" method="post" action="<?php echo $base_url.'/my-travel-plan/actuals-entry-post-visit/?str=visit-summary'; ?>">
	<div>
		<h3>Download visit summary for the month</h3>
		<label for="select-month">Select Month</label>  		  
		<div class="form-item form-type-textfield form-item-month">
			<input type="text"  value="<?php echo date('F y'); ?>" id="visit_summary_month" name="visit_summary_month" class="select-month">
		</div>		
		<input type="submit" class="form-submit" value="Generate Visit Summary Excel Sheet" id="excelbutton" name="excelbutton">
	</div>
	</form>
</div>
</div>
<script>
function setFormData(){
	var form = jQuery('#submit-visit-summary-form');
	form.attr('action',form.attr('action')+'&month='+jQuery('#visit_summary_month').val());
	return true;
}
</script>