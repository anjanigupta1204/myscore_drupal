<?php if(isset($all_tm_month_plan[1])): ?>

<?php

$session_options = session_options();	
$dealer_options = dealer_options();

?>
<hr style="clear:both;" />
<h3 style="clear:both;" class="overall-score"> TM's Plan For the <?php echo date('F, Y'); ?> <h3>
<table>
	<?php $tm = array_keys($all_tm_month_plan[1]); ?>
	<tr>
		<th class="tmlh_date"> Date: </th>
		<?php foreach($tm as $id): ?>
		<th><?php echo tm_name($id); ?></th>
		<?php endforeach; ?>
	</th>
	<?php foreach($all_tm_month_plan as $day=>$val): ?>
	<tr>
		<td class="tml_date"><?php echo date('d-m-Y',strtotime(date('Y-m-').$day)); ?></td>
		<?php foreach($all_tm_month_plan[$day] as $record ): ?>
		<td>			
		<?php if($record): ?>
			<?php foreach($record as $plan): ?>
			<?php							
				$session_name = $plan["session"]? $session_options[$plan["session"]]:'';
				$plan_for =  $dealer_options[$plan["channel_partner_id"]] ? $dealer_options[$plan["channel_partner_id"]] : "-";
			?>
				<span class="<?php echo str_replace(' ','-',strtolower($session_name)); echo(($plan_for == 'Leave' || $plan_for == 'Holiday' ) ? ' leave' : '' );?>" ><?php echo "Visit: ".$plan_for .' : '.$session_name; ?></span>
			<?php endforeach; ?>
		<?php else: ?>
				<?php echo " - "; ?>
		<?php endif; ?>
		</td>
		<?php endforeach; ?>
	</tr>
	<?php endforeach; ?>
</table>
<?php endif; ?>

