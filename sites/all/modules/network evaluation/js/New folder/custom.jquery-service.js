var sk = jQuery.noConflict();

var ard_service_form = 'network-evaluation-ard-service-display-form';
var asc_service_form = 'network-evaluation-asc-service-display-form';
var error_in_criteria = [];

sk(document).ready(function() {	


/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASC Service validaion start %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */



/* network-evaluation-asc-service-display-form validation*/

	 
	 
	sk( "#"+asc_service_form ).submit(function( event ) {

	  //console.log( "Handler for .submit() called." );		
		
		event.preventDefault();		
		
		
		
		/* Manpower field validation */
		sk('#'+asc_service_form+' [id^=edit-question-availabe-]').each(function(){ 
				
		  availabe = sk(this).val();
		  
		  if(availabe != '' ){
			  sk(this).removeClass('ard-validation-error');
		  }else{
			 sk(this).addClass('ard-validation-error');	 
		  }
		  
		  current_tr =  sk(this).closest('tr');
		  required =  current_tr.find('[name^="question\[required\]"]').val();
		  var trining =  current_tr.find('[id^="edit-question-score-trining-"]');
		  var score_trining = '';
		  
		 if(trining.length > 0){
			score_trining =  parseInt(trining.val());
			console.log(score_trining +'--'+ availabe);
		  }
		
			
			if(score_trining != '' && availabe != ''){
				
				if(parseInt(score_trining) > parseInt(availabe)){			
					trining.addClass('ard-validation-error');	
				}else{			
					trining.removeClass('ard-validation-error');	  
				}
			}
			
			/* Manpower field training validation */
			sk('#'+asc_service_form+' [id^=edit-question-score-trining-]').each(function(){ 
			
				if(sk(this).val() == ''){
					sk(this).addClass('ard-validation-error');	
				}
		
			});
			
			var tar_date =  current_tr.find('[name^="question\[date\]"]');
			var remark =  current_tr.find('[name^="question\[remark\]"]');
			
			if(parseInt(availabe) < parseInt(required)){
				
				if(tar_date.val() == "")
				tar_date.addClass('ard-validation-error');	
				else
				tar_date.removeClass('ard-validation-error');	
			
				if(remark.val() == "")
				remark.addClass('ard-validation-error');	
				else
				remark.removeClass('ard-validation-error');		
			}else{
				tar_date.removeClass('ard-validation-error');
				remark.removeClass('ard-validation-error');		
			}
			
			
	
		});
		
		
		/* KPI field training validation */
		sk('#'+asc_service_form+' [name^="question\[actual\]"]').each(function(){ 
			
			var actual = sk(this).val();
			var current_tr =  sk(this).closest('tr');			
			var actual_par =  current_tr.find('[id^=edit-question-actual-par-]');
			var remark =  current_tr.find('[id^=edit-question-remark-]');
			var tar_date =  current_tr.find('[id^=edit-question-date-]');
			if(actual_par.val() < 80){
				if(remark.val() == '')
				remark.addClass('ard-validation-error');
				if(tar_date.val() == '')			
				tar_date.addClass('ard-validation-error');	
			}else{
				remark.removeClass('ard-validation-error');			
				tar_date.removeClass('ard-validation-error');
			}
			
			
		});
	

	/* Description radio button validaion on submit */	  

	  sk('#'+asc_service_form+' fieldset tr').each(function(){		  

		var current_tr = sk(this);		

		

		var have_radio = current_tr.find('[type="radio"]').length;

		if(have_radio){

			var select_checked = current_tr.find('[type="radio"]:checked');

			if(select_checked.length){

				current_tr.removeClass('ard-validation-error');

				var _val = select_checked.val();

				

				/* find all radio buttons */

				var remark = current_tr.find('[id^=edit-question-remark-]');

				/* find all date fields */

				var dt = current_tr.find('[id^=edit-question-date-]');
				
				var score = current_tr.find('[id^=edit-question-score-]').val();
				
			
				if(_val == 2){

					if(sk('#'+remark.attr('id')).val() == ''){

					sk('#'+remark.attr('id')).addClass('ard-validation-error');	

					}else{

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');	
					}
					

					/* find all date fields */

					if(sk('#'+dt.attr('id')).val() == ''){

					sk('#'+dt.attr('id')).addClass('ard-validation-error');	
					}else{

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');
					}
					

				}else if(_val == 1){			

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');

				}	

			}

			else{

				current_tr.addClass('ard-validation-error');				

			}

		}

		

	  });
		
	   var errors = sk( "#"+asc_service_form+" .ard-validation-error" );

	   if(!errors.length){		

		sk.ajax({
		  url: Drupal.settings.network_evaluation.hostname+"/network-evaluation/network-evaluation-service-autosave",
		  beforeSend: function( xhr ) { sk.colorbox({html:"Please wait saving data....."});  },
		  method: "POST",		 
		  data: sk("#"+asc_service_form).serializeArray(),
		  dataType: "html",
		  
		}).done(function( data ) {
			 sk("#"+asc_service_form).unbind('submit').submit();
		});
				

	   }

	   else{
			error_in_criteria = [];
			var criteriaid = criteria = '';
			errors.closest('fieldset.collapsible').each(function(){
				 criteriaid = sk(this).data('criteriaid');
				 criteria = sk(this).data('criteria');
				if(criteria !== 'undefined' && criteriaid !== 'undefined' )
				error_in_criteria[criteriaid] = criteria;
			});
			//console.log(error_in_criteria);
			
			error_in_criteria = error_in_criteria.filter(function( element ) {
			   return !!element;
			});
				  
		   
		   sk.colorbox({html:"There are <span class='error-count'>"+errors.length+"</span> error(s) found!! <br> Please correct errors.<br> Errors are in the following criteria:<ul style=' color: red; list-style: inside disc;margin-left:-30px;margin-top: 0;'><li>"+error_in_criteria.join('<li>','</li>')+"</li></ul>"});

	   }

	  

	});

	



/* Description radio button validaion */

	sk(document).on('click','#'+asc_service_form+' [id^=edit-question-score-]:radio, #'+ard_service_form+' [id^=edit-question-score-]:radio',function(e){

		e.stopPropagation();

		var _val = sk(this).val();

		var remark = sk(this).closest("tr").find('[id^=edit-question-remark-]');
		var dt = sk(this).closest("tr").find('[id^=edit-question-date-]');
		
		if(_val == 2){

			if(sk('#'+remark.attr('id')).val() == '')

			sk('#'+remark.attr('id')).addClass('ard-validation-error');	

			else

			sk('#'+remark.attr('id')).removeClass('ard-validation-error');	

			

			/* find all date fields */

			if(sk('#'+dt.attr('id')).val() == '')

			sk('#'+dt.attr('id')).addClass('ard-validation-error');	

			else

			sk('#'+dt.attr('id')).removeClass('ard-validation-error');

		}else if(_val == 1){			

			sk('#'+remark.attr('id')).removeClass('ard-validation-error');

			sk('#'+dt.attr('id')).removeClass('ard-validation-error');

			}		
		sk(this).closest('tr').removeClass('ard-validation-error');
	});
	
	
	/* available and training */
	sk(document).on('keyup change','#'+ard_service_form+' [id^=edit-question-availabe-], #'+asc_service_form+' [id^=edit-question-availabe-]',function(){
	  number_validation_service(this);
	  
	  
	  
	  availabe = sk(this).val();
	  
	  if(availabe != '' ){
		  sk(this).removeClass('ard-validation-error');
	  }
	  
	  current_tr =  sk(this).closest('tr');
	  required =  current_tr.find('[name^="question\[required\]"]').val();
	  var trining =  current_tr.find('[name^="question\[score_trining\]"]');
	
	  score_trining =  trining.val();
	 
	
	if(score_trining != '' && availabe != ''){
		if(parseInt(score_trining) > parseInt(availabe)){			
			trining.addClass('ard-validation-error');	
		}
		else{			
			trining.removeClass('ard-validation-error');	  
		}
	}

	var tar_date =  current_tr.find('[name^="question\[date\]"]');
	var remark =  current_tr.find('[name^="question\[remark\]"]');
	
	if(parseInt(availabe) < parseInt(required)){
		
		if(tar_date.val() == "")
		tar_date.addClass('ard-validation-error');	
		else
		tar_date.removeClass('ard-validation-error');	
	
		if(remark.val() == "")
		remark.addClass('ard-validation-error');	
		else
		remark.removeClass('ard-validation-error');		
	}else{
		tar_date.removeClass('ard-validation-error');
		remark.removeClass('ard-validation-error');		
	}
	
	  
	});		

	
	/* available and training */
	var score_trining = '';
	sk(document).on('keyup change','#'+asc_service_form+' [id^=edit-question-score-trining-], #'+ard_service_form+' [id^=edit-question-score-trining-]',function(){
	   number_validation_service(this);
	  
	  score_trining = sk(this).val();
	  current_tr =  sk(this).closest('tr');
	  availabe =  current_tr.find('[name^="question\[availabe\]"]').val();
	/*
	console.log('training');
	console.log(availabe +'>'+ score_trining);
	*/
	if(score_trining != '' && availabe != ''){
		if(parseInt(score_trining) > parseInt(availabe)){			
			sk(this).addClass('ard-validation-error');	
		}
		else{			
			sk(this).removeClass('ard-validation-error');	  
		}
	}
	});
	
	
	
	
	
	
	
	
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ARD Service validaion start %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */



/* network-evaluation-ard-service-display-form validation*/
	 


	sk( "#"+ard_service_form ).submit(function( event ) {

	  //console.log( "Handler for .submit() called." );
		
		event.preventDefault();
		
		if(sk(this).attr('id') == 'save'){
			sk("#"+ard_service_form+ " #save_as").val('1');
		} else if(sk(this).attr('id') == 'save_draft'){
			sk("#"+ard_service_form+ " #save_as").val('0');
		}
		/* Manpower field validation */
		/* Manpower field validation */
		sk('#'+ard_service_form+' [id^=edit-question-availabe-]').each(function(){ 
				
		  availabe = sk(this).val();
		  
		  if(availabe != '' ){
			  sk(this).removeClass('ard-validation-error');
		  }else{
			   sk(this).addClass('ard-validation-error');
		  }
		  
		  current_tr =  sk(this).closest('tr');
		  required =  current_tr.find('[name^="question\[required\]"]').val();
		  var trining =  current_tr.find('[id^="edit-question-score-trining-"]');
		  var score_trining = '';
		  
		 if(trining.length > 0){
			score_trining =  parseInt(trining.val());
			
		  }
		
			
			if(score_trining != '' && availabe != ''){
				
				if(parseInt(score_trining) > parseInt(availabe)){			
					trining.addClass('ard-validation-error');	
				}else{			
					trining.removeClass('ard-validation-error');	  
				}
			}
			
			/* Manpower field training validation */
			sk('#'+asc_service_form+' [id^=edit-question-score-trining-]').each(function(){ 
			
				if(sk(this).val() == ''){
					sk(this).addClass('ard-validation-error');	
				}
		
			});
			
			var tar_date =  current_tr.find('[name^="question\[date\]"]');
			var remark =  current_tr.find('[name^="question\[remark\]"]');
			
			if(parseInt(availabe) < parseInt(required)){
				
				if(tar_date.val() == "")
				tar_date.addClass('ard-validation-error');	
				else
				tar_date.removeClass('ard-validation-error');	
			
				if(remark.val() == "")
				remark.addClass('ard-validation-error');	
				else
				remark.removeClass('ard-validation-error');		
			}else{
				tar_date.removeClass('ard-validation-error');
				remark.removeClass('ard-validation-error');		
			}
			
	
		});
		
	
		
		/* KPI field training validation */
		sk('#'+ard_service_form+' [name^="question\[actual\]"]').each(function(){
			
			var actual = sk(this).val();
			var current_tr =  sk(this).closest('tr');			
			var actual_par =  current_tr.find('[id^=edit-question-actual-par-]');
			var remark =  current_tr.find('[id^=edit-question-remark-]');
			var tar_date =  current_tr.find('[id^=edit-question-date-]');
			if(actual_par.val() < 80){
				if(remark.val() == '')
					remark.addClass('ard-validation-error');
				if(tar_date.val() == '')			
					tar_date.addClass('ard-validation-error');	
			}else{
				remark.removeClass('ard-validation-error');			
				tar_date.removeClass('ard-validation-error');
			}
			
			
	
		});
	
	
	/* Description radio button validaion on submit */	  

	  sk('#'+ard_service_form+' fieldset tr').each(function(){		  

		 var current_tr = sk(this);		

		

		var have_radio = current_tr.find('[type="radio"]').length;

		if(have_radio){

			var select_checked = current_tr.find('[type="radio"]:checked');

			if(select_checked.length){

				current_tr.removeClass('ard-validation-error');

				var _val = select_checked.val();

				

				/* find all radio buttons */

				var remark = current_tr.find('[id^=edit-question-remark-]');

				/* find all date fields */

				var dt = current_tr.find('[id^=edit-question-date-]');			
				
				
			
				if(_val == 2){

					if(sk('#'+remark.attr('id')).val() == '')

					sk('#'+remark.attr('id')).addClass('ard-validation-error');	

					else

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');	

					

					/* find all date fields */

					if(sk('#'+dt.attr('id')).val() == '')

					sk('#'+dt.attr('id')).addClass('ard-validation-error');	

					else

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');

					

				}else if(_val == 1){			

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');

				}	

			}

			else{

				current_tr.addClass('ard-validation-error');				

			}

		}

		

	  });

	   var errors = sk( "#"+ard_service_form+" .ard-validation-error" );

	  /* console.log(errors); */

	    if(!errors.length){		

		 sk.ajax({
		  url: Drupal.settings.network_evaluation.hostname+"/network-evaluation/network-evaluation-service-autosave",
		  beforeSend: function( xhr ) { sk.colorbox({html:"Please wait saving data....."});  },
		  method: "POST",		 
		  data: sk("#"+ard_service_form).serializeArray(),
		  dataType: "html",
		  
		}).done(function( data ) {
			 sk("#"+ard_service_form).unbind('submit').submit();
		});	

	   }

	   else{
			error_in_criteria = [];
			var criteriaid = criteria = '';
			errors.closest('fieldset.collapsible').each(function(){
				 criteriaid = sk(this).data('criteriaid');
				 criteria = sk(this).data('criteria');
				if(criteria !== 'undefined' && criteriaid !== 'undefined' )
				error_in_criteria[criteriaid] = criteria;
			});
			console.log(error_in_criteria);
			error_in_criteria = error_in_criteria.filter(function( element ) {
			   return !!element;
			});
				  
		   
		   sk.colorbox({html:"There are <span class='error-count'>"+errors.length+"</span> error(s) found!! <br> Please correct errors.<br> Errors are in the following criteria:<ul style=' color: red; list-style: inside disc;margin-left:-30px;margin-top: 0;'><li>"+error_in_criteria.join('<li>','</li>')+"</li></ul>"});

	   }

	  

	});

	sk('[id^=edit-question-criteria-date-]').datepicker({minDate:0,
		beforeShow: function(input, inst){
			inst.dpDiv.css({marginLeft: -input.offsetWidth + 'px'});
		},
		dateFormat: 'dd/mm/yy',
	});

	
	
	  
function number_validation_service(_this){
  
  var numbers = /^[0-9]+$/; 
  var replaceregx = /[^0-9]/g;		  

  var thisval = sk(_this).val();
  
  if(!thisval.match(numbers)) 
  {  
	sk(_this).val(thisval.replace(replaceregx,''));				
	return true;
  }
}

/* save as draft or final save */ 
 
sk( '#'+asc_service_form +' #save_draft, #'+asc_service_form +' #save').click(function(){
		if(sk(this).attr('id') == 'save'){
			sk("#save_as").val('1');		
		} else if(sk(this).attr('id') == 'save_draft'){
			sk("#save_as").val('0');		
		}
	 });
 
	sk( '#'+ard_service_form +' #save_draft, #'+ard_service_form +' #save').click(function(){
	if(sk(this).attr('id') == 'save'){
		sk("#save_as").val('1');	
	} else if(sk(this).attr('id') == 'save_draft'){
		sk("#save_as").val('0');	
	}
 });
	 

 if(!sk('#'+asc_service_form+' #save').length && sk('#'+asc_service_form).length == 1){
	sk('[id^=edit-question-availabe-],[id^=edit-question-actual-],[id^=edit-question-remark-],[id^=edit-question-date-],[id^=edit-question-score-],[id^=edit-question-score-trining-],#edit-tsm-feedback').attr('disabled','disabled');
}
sk('#'+asc_service_form+'  #edit-evaluation-details').find('input').attr('readonly','readonly');


if(!sk('#'+ard_service_form+' #save').length && sk('#'+ard_service_form).length == 1){
	sk('[id^=edit-question-availabe-],[id^=edit-question-actual-],[id^=edit-question-remark-],[id^=edit-question-date-],[id^=edit-question-score-],[id^=edit-question-score-trining-],#edit-tsm-feedback').attr('disabled','disabled');
}
sk('#'+ard_service_form+' #edit-evaluation-details').find('input').attr('readonly','readonly');


/* remark and target date */
sk(document).on('change','#'+ard_service_form+' [id^=edit-question-remark-], #'+asc_service_form+' [id^=edit-question-remark-]',function(){
if(sk(this).val() == '')
	sk(this).addClass('ard-validation-error');	
	else
	sk(this).removeClass('ard-validation-error');	
});

/* remark and target date */
sk(document).on('change','#'+ard_service_form+' [id^=edit-question-date-], #'+asc_service_form+' [id^=edit-question-date-]',function(){
	if(sk(this).val() == '')
	sk(this).addClass('ard-validation-error');	
	else
	sk(this).removeClass('ard-validation-error');	
});
 
		  
});