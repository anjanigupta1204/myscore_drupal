jQuery(document).ready(function($){
	$('#upload_photo_window').on('click',function(e){
		e.preventDefault();	
		$(this).after('<span class="loading-window"></span>');
		var url = Drupal.settings.network_evaluation.hostname+"/network-evaluation/attachement/upload";
		var path = window.location.pathname;
		url += '?str='+path.substr(path.indexOf('network-evaluation-'));
		
		var if_html = '<iframe height="400" width="768" src="'+url+'"></iframe>';
		sk.colorbox({html:if_html,
			onClosed:function(){
				parent.location.reload();
			}
		});
		//window.open(url, "Upload Window", "width=768,height=460").focus();
		
		$('.loading-window').delay(10000).remove();
	});	
});