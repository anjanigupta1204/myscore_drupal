<?php

// $dealer_code = 99990 ,$dealer_type = 'ASC-SALSE' ,$evaluation_by_staff = 9479 ,$evaluation_by_staff_name='AMIT',$evaluation_month = 'Jul-2016' Email/Customer-Id/Phone/User-name

function network_evaluation_asc_ard_service_score_pdf($dealer_code  ,$dealer_type  ,$evaluation_by_staff ,$evaluation_by_staff_name,$evaluation_month) { 
 
if((int)$dealer_code && (int)$evaluation_by_staff && $dealer_type && $evaluation_month){
		
	$query = db_select('evaluation_score', 'ev')
	->fields('ev', array())
	->condition('ev.asc_ard_code', $dealer_code)	
	->condition('ev.dealer_type', $dealer_type)	
	->condition('ev.eva_by_user', $evaluation_by_staff)
	->condition('ev.eva_month', $evaluation_month)
	->execute();

	
	$dealer_query = db_select('hero_dealer', 'hd')
	 ->fields('hd', array())
	  ->condition('hd.dealer_code', $dealer_code)	 
	  ->execute();  
	  $dealer_data = $dealer_query->fetchAssoc(); 
	
	//print_r($dealer_data);
	//die;
	$rowCount = $query->rowCount();

	if($rowCount){
		
		$score_result 		= $query->fetchAssoc();
		$qn['question'] = drupal_json_decode($score_result['eva_answer']);
		
		$marks_obtained = drupal_json_decode($score_result['marks_obtained']);
		
		
		foreach($marks_obtained as $ob){
			if($ob['criteria'] != 'manpower' ){
				
				$myscore += $ob['obtain_score'];
				$overall += $ob['overall_score'];	
				
			}else{
				if($ob['criteria'] == 'manpower' ){
					
					$myscore += ($ob['obtain_score']['available']+$ob['obtain_score']['training']);
					$overall += $ob['overall_score']['total'];						
				}			
			}			
		}
		
		
		$finalscore = 0;
		if($myscore && overall){
			$finalscore = round(($myscore*100)/$overall);
		}
		
				
		//print_r($qn['question']);
		$questions_id = implode(',',array_keys($qn['question']['score']));

		//print_r(array_keys($qn['question']['score']));
		
		$kpi_questions_id = array_keys($qn['question']['availabe']);
		$manpower_questions_id = array_keys($qn['question']['actual']);
		

		$qq = "SELECT (SELECT TRIM(name) FROM evaluation_category WHERE id = (SELECT parent_id FROM evaluation_category WHERE id = question_category)) category_name,(SELECT name FROM evaluation_category WHERE id = question_category) sub_category, question,sub_question , id qid FROM `evaluation_question` WHERE id in($questions_id,".implode(',',$manpower_questions_id).",".implode(',',$kpi_questions_id).") and status = 1 order by category_name,sub_category ASC";

		$result = db_query($qq);

			$results = $result->fetchAll(PDO::FETCH_ASSOC);
			//echo '<pre>';
			//print_R($results);
			
			$html = '';			
			$html .= '<div>';
			$html .= '<h3>Score '.$finalscore.'%</h3>';
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';
			$html .= '<tr style="background-color:#000;color:#fff">';
			$html .= '<td align="center" >Dealer Code</td>';
			$html .= '<td align="center" >Dealer Name</td>';
			$html .= '<td align="center" >Department</td>';
			$html .= '<td align="center" >Created By</td>';
			$html .= '<td align="center" >Visit Date</td>';
			$html .= '<td align="center" >Evalution Month</td>';
			$html .= '</tr>';			
			$html .= '<tr>';
			$html .= '<td align="center"   >'.$score_result['asc_ard_code'].'</td>';
			$html .= '<td align="center"   >'.$dealer_data['dealer_name'].'</td>';
			$html .= '<td align="center"   >'.str_replace(array('ASC-','ARD-'),'',$score_result['dealer_type']).'</td>';
			$html .= '<td align="center"   >'.$evaluation_by_staff_name.'</td>';
			$html .= '<td align="center"   >'.date('d-m-Y',strtotime($score_result['eva_date'])).'</td>';
			$html .= '<td align="center"   >'.$score_result['eva_month'].'</td>';
			$html .= '</tr>';
			$html .= '</table>';
			
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';			
			$category = '';
			
			foreach($results as $row){
			
				if(in_array(trim($row['category_name']),array('Manpower','KPI'))){
					continue;
				}				
				
				if($row['category_name'] != $category){				
					$html .= '<tr> ';							
					$html .= '<td colspan="5">&nbsp;</td>';						
					$html .= '</tr>';
					$html .= '<tr> ';							
					$html .= '<td colspan="5"><h2>'.$row['category_name'].'</h2></td>';				
					$html .= '</tr>';					
					$html .= '<tr style="background-color:#000;color:#fff">';
					$html .= '<th>Sub-Criteria</th>';		
					$html .= '<th>Parameter</th>';		
					$html .= '<th align="center" >Score(Scores to be given by TM)</th>';		
					$html .= '<th align="center" >Remarks/Action Plan</th>';		
					$html .= '<th align="center" >Target Date</th>';		
					$html .= '</tr>';					
					$category = $row['category_name'];
					
				}
				
				$score = $qn['question']['score'][$row['qid']];
				
				if($qn['question']['score'][$row['qid']] == 1){
					$score = 'Yes';
				}else if($qn['question']['score'][$row['qid']] == 2){
					$score = 'No';
				}				
				$html .= '<tr>';
				$html .= '<td>'.$row['sub_category'].'</td>';					
				$html .= '<td>'.$row['question'].'</td>';					
				$html .= '<td align="center"  >'.$score.'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['remark'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['date'][$row['qid']].'</td>';							
				$html .= '</tr>';					
			}
			$html .= '</table>';
			
			/* Manpower */
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';			
			$category = '';
			
			foreach($results as $row){
			
				if(in_array(trim($row['category_name']), array('Infrastructure','System & Process','KPI'))) {
					continue;
				}			
				
				if($row['category_name'] != $category){
				
					$html .= '<tr> ';							
					$html .= '<td colspan="6">&nbsp;</td>';						
					$html .= '</tr>';
					$html .= '<tr> ';							
					$html .= '<td colspan="6"><h2>'.$row['category_name'].'</h2></td>';				
					$html .= '</tr>';					
					$html .= '<tr style="background-color:#000;color:#fff">';
					$html .= '<th>Sub-Criteria</th>';		
					$html .= '<th>Parameter</th>';		
					$html .= '<th align="center" >Required</th>';		
					$html .= '<th align="center" >Available</th>';		
					$html .= '<th align="center" >Training</th>';							
					$html .= '<th align="center" >Remarks/Action Plan</th>';		
					$html .= '<th align="center" >Target Date</th>';		
					$html .= '</tr>';					
					$category = $row['category_name'];
					
				}								
								
				$html .= '<tr>';
				$html .= '<td>'.$row['sub_category'].'</td>';					
				$html .= '<td>'.$row['question'].'</td>';					
				$html .= '<td align="center"  >'.$qn['question']['required'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['availabe'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['score_trining'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['remark'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['date'][$row['qid']].'</td>';							
				$html .= '</tr>';					
			}
			$html .= '</table>';
			
			/* KPI */
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';			
			$category = '';
			
			
			foreach($results as $row){
			
				if(in_array(trim($row['category_name']), array('Infrastructure','System & Process','Manpower'))) {
					continue;
				}				
				
				
				if($row['category_name'] != $category){
				
					$html .= '<tr> ';							
					$html .= '<td colspan="8">&nbsp;</td>';						
					$html .= '</tr>';
					$html .= '<tr> ';							
					$html .= '<td colspan="8"><h2>'.$row['category_name'].'</h2></td>';				
					$html .= '</tr>';					
					$html .= '<tr style="background-color:#000;color:#fff">';
					$html .= '<th>Sub-Criteria</th>';		
					$html .= '<th>Parameter</th>';		
					$html .= '<th align="center" >Target</th>';		
					$html .= '<th align="center" >Actual</th>';		
					$html .= '<th align="center" >% Ach</th>';							
					$html .= '<th align="center" >% Growth from LY</th>';		
					$html .= '<th align="center" >Remarks/Action Plan</th>';		
					$html .= '<th align="center" >Target Date</th>';		
					$html .= '</tr>';
					
					$category = $row['category_name'];
					
				}
								
				$html .= '<tr>';
				$html .= '<td>'.$row['sub_category'].'</td>';					
				$html .= '<td>'.$row['question'].'-'.$row['sub_question'].'</td>';					
				$html .= '<td align="center"   >'.$qn['question']['target'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['actual'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['actual_par'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['last_year_growth'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['remark'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['date'][$row['qid']].'</td>';							
				$html .= '</tr>';		

				
			}
			$html .= '</table>';
			
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';					
			$html .= '<tr> <td colspan="5"><h2 style="margin-top: 20px; margin-bottom: 0px;">TSM Remark</h2></td></tr>';				
			$html .= '<tr> <td colspan="5">'.$score_result['tsm_feedback'].'</td></tr>';
			
			$html .= '<tr><td colspan="5"><h2 style="margin-top: 20px; margin-bottom: 0px;">ASM Remark</h2></td></tr>';				
			$html .= '<tr><td colspan="5">'.$score_result['asm_feedback'].'</td></tr>';		
			$html .= '</table>';
			$html .= '</div>';
			
			
		//echo $html;
		
		require_once DRUPAL_ROOT ."/sites/all/modules/print/lib/tcpdf/tcpdf.php";
		
		$filename = 'Network_Evaluation_'.$dealer_code.'-'.time().'.pdf';
		
		$pdf = new TCPDF();
		// write data
		$pdf->AddPage('P');
		$pdf->SetFont('helvetica', '', 8);
		$pdf->writeHTML($html,true, false, false, false, '');
		// end write
		$pdfpath = DRUPAL_ROOT.'/evaluation_pdf/'.$filename;
		//$pdf->Output($filename, 'F');
		//$pdf->Output($filename, 'D');
		$content = $pdf->Output($pdfpath,'F');
		return $pdfpath;
		//die($pdf->Output($filename, 'I'));
		
		}		
	}

}

/* $dealer_code = 99990 ;$dealer_type = 'ASC-SALES' ;$evaluation_by_staff = 9479 ;$evaluation_by_staff_name='AMIT';$evaluation_month = 'Jul-2016' ; */

function network_evaluation_asc_ard_sales_score_pdf($dealer_code ,$dealer_type, $evaluation_by_staff, $evaluation_by_staff_name ,$evaluation_month) { 

if((int)$dealer_code && (int)$evaluation_by_staff && $dealer_type != '' && $evaluation_month != '' ){	
		
	$query = db_select('evaluation_score', 'ev')
	->fields('ev', array())
	->condition('ev.asc_ard_code', $dealer_code)	
	->condition('ev.dealer_type', $dealer_type)	
	->condition('ev.eva_by_user', $evaluation_by_staff)
	->condition('ev.eva_month', $evaluation_month)
	->execute();
		  
	$dealer_query = db_select('hero_dealer', 'hd')
	 ->fields('hd', array())
	  ->condition('hd.dealer_code', $dealer_code)	 
	  ->execute();  
	  $dealer_data = $dealer_query->fetchAssoc(); 
	//print_r($query);

	$rowCount = $query->rowCount();

	if($rowCount){
		
		$score_result 		= $query->fetchAssoc();
		$qn['question'] = drupal_json_decode($score_result['eva_answer']);
		
		$marks_obtained = drupal_json_decode($score_result['marks_obtained']);
		
		
		foreach($marks_obtained as $ob){
			$myscore += $ob['myscore'];
			$overall += $ob['cscore'];			
		}
		$finalscore = 0;
		if($myscore && overall){
			$finalscore = round(($myscore*100)/$overall);
		}
		
		//print_r($qn['question']);
		$questions_id = implode(',',array_keys($qn['question']['score']));
		//print_r(array_keys($qn['question']['score']));		
	
		$qq = "SELECT (SELECT TRIM(name) FROM evaluation_category WHERE id = question_category) category_name, id qid, question  , max_mark FROM `evaluation_question` WHERE id in($questions_id) and status = 1 order by category_name ";
		
			$result = db_query($qq);
			
			$results = $result->fetchAll(PDO::FETCH_ASSOC);
			//echo '<pre>';
			//print_R($results);
			
			$html = '';
			
			$html .= '<div>';
			$html .= '<h3>Score '.$finalscore.'%</h3>';
			
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';
			$html .= '<tr style="background-color:#000;color:#fff">';
			$html .= '<td align="center" >Dealer Code</td>';
			$html .= '<td align="center" >Dealer Name</td>';
			$html .= '<td align="center" >Department</td>';
			$html .= '<td align="center" >Created By</td>';
			$html .= '<td align="center" >Visit Date</td>';
			$html .= '<td align="center" >Evalution Month</td>';
			$html .= '</tr>';			
			$html .= '<tr>';
			$html .= '<td align="center"   >'.$score_result['asc_ard_code'].'</td>';
			$html .= '<td align="center"   >'.$dealer_data['dealer_name'].'</td>';
			$html .= '<td align="center"   >'.str_replace(array('ASC-','ARD-'),'',$score_result['dealer_type']).'</td>';
			$html .= '<td align="center"   >'.$evaluation_by_staff_name.'</td>';
			$html .= '<td align="center"   >'.date('d-m-Y',strtotime($score_result['eva_date'])).'</td>';
			$html .= '<td align="center"   >'.$score_result['eva_month'].'</td>';
			$html .= '</tr>';
			$html .= '</table>';
			
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';			
			$category = '';
			
			foreach($results as $row){
				
				if($row['category_name'] != $category){
				
					$html .= '<tr> ';							
					$html .= '<td colspan="5">&nbsp;</td>';						
					$html .= '</tr>';
					$html .= '<tr> ';							
					$html .= '<td colspan="5"><h2>'.$row['category_name'].'</h2></td>';				
					$html .= '</tr>';
					
					$html .= '<tr style="background-color:#000;color:#fff">';
					$html .= '<th  >Parameter</th>';		
					$html .= '<th align="center" >Actual Value</th>';		
					$html .= '<th align="center" >Score(Scores to be given by TM)</th>';		
					$html .= '<th align="center" >Remarks/Action Plan</th>';		
					$html .= '<th align="center" >Target Date</th>';		
					$html .= '</tr>';
					
					$category = $row['category_name'];
					
				}
				
				$desc = $qn['question']['description'][$row['qid']];
				
				if($qn['question']['description'][$row['qid']] == 1){
					$desc = 'Yes';
				}else if($qn['question']['description'][$row['qid']] == 2){
					$desc = 'No';
				}				
				$html .= '<tr >';
				$html .= '<td  >'.$row['question'].'</td>';		
				$html .= '<td align="center"  >'.$desc.'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['score'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['remark'][$row['qid']].'</td>';		
				$html .= '<td align="center"  >'.$qn['question']['date'][$row['qid']].'</td>';							
				$html .= '</tr>';
					
			}
			$html .= '</table>';
			
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';					
			$html .= '<tr> <td colspan="5"  ><h2 style="margin-top: 20px; margin-bottom: 0px;">TSM Remark</h2></td></tr>';				
			$html .= '<tr> <td colspan="5"   >'.$score_result['tsm_feedback'].'</td></tr>';
			
			$html .= '<tr><td colspan="5"  ><h2 style="margin-top: 20px; margin-bottom: 0px;">ASM Remark</h2></td></tr>';				
			$html .= '<tr><td colspan="5"   >'.$score_result['asm_feedback'].'</td></tr>';		
			$html .= '</table>';
			$html .= '</div>';
			
			
		//echo $html;
		
		require_once DRUPAL_ROOT ."/sites/all/modules/print/lib/tcpdf/tcpdf.php";
		
		$filename = 'Network_Evaluation_'.$dealer_code.'-'.time().'.pdf';
		
		$pdf = new TCPDF();
		// write data
		$pdf->AddPage('P');
		$pdf->SetFont('helvetica', '', 8);
		$pdf->writeHTML($html,true, false, false, false, '');
		// end write
		$pdfpath = DRUPAL_ROOT.'/evaluation_pdf/'.$filename;
		//$pdf->Output($filename, 'F');
		//$pdf->Output($filename, 'D');
		$content = $pdf->Output($pdfpath,'F');
		
		return  $pdfpath;
		//die($pdf->Output($filename, 'I'));
		
		}		
	}

}