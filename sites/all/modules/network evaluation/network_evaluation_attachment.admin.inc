<?php
function get_image_facia(){
$image_facia = array(	
	1 => 'Front Fascia', 
	2 => 'Back Fascia',
	3 => 'Image #3',
	4 => 'Image #4',
	5 => 'Image #5',
	6 => 'Image #6',
	7 => 'Image #7',
	8 => 'Image #8',
	9 => 'Image #9',
	10 => 'Image #10',
);
return $image_facia;
}

function get_saved_ard_criteria_attachments($dealer_type = ''){
	$delear_code 	= $_POST['question']['ard_code'];
	$month 			= $_POST['question']['ard_evaluation_month'];
	$evaluation_by 	=  $_POST['question']['ard_evaluation_by'];	
	
	/* checking uploaded image already uploaded or not */
	$attachment_query = db_select('eval_criteria_attachment', 'a')
	->fields('a', array('dealer_type'))	  
	->condition('a.owner_id',$evaluation_by)		  
	->condition('a.asc_ard_code',$delear_code)		  
	->condition('a.dealer_type',$dealer_type)		  
	->condition('a.eva_month',$month)	
	->execute();
	
	return $attachment_query->rowCount();
}

function get_saved_asc_criteria_attachments($dealer_type = ''){
	
	$delear_code 	= $_POST['question']['asc_code'];
	$month 			= $_POST['question']['asc_evaluation_month'];
	$evaluation_by 	=  $_POST['question']['asc_evaluation_by'];	
	
	/* checking uploaded image already uploaded or not */
	$attachment_query = db_select('eval_criteria_attachment', 'a')
	->fields('a', array('dealer_type'))	  
	->condition('a.owner_id',$evaluation_by)		  
	->condition('a.asc_ard_code',$delear_code)		  
	->condition('a.dealer_type',$dealer_type)		  
	->condition('a.eva_month',$month)	
	->execute();
	
	return $attachment_query->rowCount();
}

function save_ard_criteria_attachments($dealer_type = 'ARD-SALES'){
	
	$temp = $_FILES['files']['tmp_name']['evaluation'];
	$file_name = $_FILES['files']['name']['evaluation'];

	if(!empty($file_name)){
			$path = 'public://survey_attachments_network/'.$file_name;
	}else{
			 $path = '';
	}
			
	move_uploaded_file($temp,$path);
	//print "<pre>"; print_r($form_state); exit;
	
	$evaluation_month = $_POST['question']['ard_evaluation_month'];
	$ard_evaluation_by = $_POST['question']['ard_evaluation_by'];
	$ard_code = $_POST['question']['ard_code'];
	
	if(!empty($path)){
		$attachment_submit= db_insert('eval_criteria_attachment')
		->fields(array(
		'attachment' => $path,
		'owner_id' => $ard_evaluation_by,
		'asc_ard_code' => $ard_code,
		'dealer_type' => $dealer_type,
		'eva_month' =>$evaluation_month,
		
		))
		//->condition('criteria_id',$key)
		->execute();
		}		
}

function save_asc_criteria_attachments($dealer_type = 'ASC-SALES'){
print "<pre>"; print_r($_FILES['files']); exit;
	$temp = $_FILES['files']['tmp_name']['evaluation'];
	$file_name = $_FILES['files']['name']['evaluation'];

	if(!empty($file_name)){
		$path = 'public://survey_attachments_network/'.$file_name;
	}else{
	 $path = '';
	}
		
	move_uploaded_file($temp,$path);

	
	$evaluation_month = $_POST['question']['asc_evaluation_month'];
	$asc_evaluation_by = $_POST['question']['asc_evaluation_by'];
	$asc_code = $_POST['question']['asc_code'];
	if(!empty($path)){
		$attachment_submit = db_insert('eval_criteria_attachment')
		->fields(array(
		'attachment' => $path,
		'owner_id' => $asc_evaluation_by,
		'asc_ard_code' => $asc_code,
		'dealer_type' => $dealer_type,
		'eva_month' =>$evaluation_month,
		
		))
		//->condition('criteria_id',$key)
		->execute();
		}		
}
