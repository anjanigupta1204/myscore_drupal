<?php
module_load_include('inc', 'network_evaluation', 'network_evaluation_attachment.admin');

function network_evaluation_attachement_upload(){

global $base_url;

$image_facia = get_image_facia();

	if( isset( $_POST['image_upload'] ) ){
		
	$dealer_type 	= $_POST['dealer_type'];
	$delear_code 	= $_POST['delear_code'];
	$month 			= $_POST['month'];
	$evaluation_by 	= $_POST['evaluation_by'];	
	$image_type 	= $_POST['image_type'];	
	
	/* checking uploaded image already uploaded or not */
	$attachment_query = db_select('eval_criteria_attachment', 'a')
	->fields('a', array('dealer_type'))	  
	->condition('a.owner_id',$evaluation_by)		  
	->condition('a.asc_ard_code',$delear_code)		  
	->condition('a.dealer_type',$dealer_type)		  
	->condition('a.eva_month',$month)		  
	->condition('a.image_type',$image_type)		  
	->execute();
	
	$attachmentCount = $attachment_query->rowCount();
			 
	$data = array();
	if(!$attachmentCount  && $image_type){
		
			if( isset( $_POST['image_upload'] ) && !empty( $_FILES['images'] )){
				
				$image = $_FILES['images'];
				$allowedExts = array("gif", "jpeg", "jpg", "png");
				
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
					$ip = $_SERVER['REMOTE_ADDR'];
				}
				
				
				//create directory if not exists
				if (!file_exists('sites/default/files/survey_attachments_network')) {
					mkdir('sites/default/files/survey_attachments_network', 0777, true);
				}
				$image_name = $image['name'];
				//get image extension
				$ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
				//assign unique name to image
				$name = time().'.'.$ext;				
				$image_size = $image["size"] / 1024;
				$image_flag = true;				
				$max_size = 512;
				if( in_array($ext, $allowedExts) && $image_size < $max_size ){
					$image_flag = true;
				} else {
					$image_flag = false;
					$data['error'] = 'Maybe '.$image_name. ' exceeds max '.$max_size.' KB size or incorrect file extension';
				} 
				
				if( $image["error"] > 0 ){
					$image_flag = false;
					$data['error'] = '';
					$data['error'].= '<br/> '.$image_name.' Image contains error - Error Code : '.$image["error"];
				}
				
				if($image_flag){
					move_uploaded_file($image["tmp_name"], "sites/default/files/survey_attachments_network/".$name);
					$src = "sites/default/files/survey_attachments_network/".$name;
					$dist = "sites/default/files/survey_attachments_network/thumbnail_".$name;
					
					$data['success'] = $thumbnail = 'thumbnail_'.$name;
					
					//$data['facia'] = $images[$image_type];					
					
					thumbnail($src, $dist, 200);
					
					if(!empty($name)){
					$path = 'public://survey_attachments_network/'.$name;
					}else{
							 $path = '';
					}
	
				if(!empty($path)){
						
						$attachment_submit= db_insert('eval_criteria_attachment')
						->fields(array(
						'attachment' => $path,
						'owner_id' => $evaluation_by,
						'asc_ard_code' => $delear_code,
						'dealer_type' => $dealer_type,
						'eva_month' => $month,
						'image_type' => $image_type,						
						))				
						->execute();
					}		
					
				}
				
			}else{
				$data['error'] = 'Please select image.';
			}		
		
	} else {		
		$data['error'] = $image_facia[$image_type].' image exists!!';
		if(!$image_type)
		$data['error'] = 'Please select image type.';
	}
	echo json_encode($data);
die;
}

include('network_evaluation_upload_photo_html.php');
return;
}




function thumbnail($src, $dist, $dis_width = 100 ){

	$img = '';
	$extension = strtolower(strrchr($src, '.'));
	switch($extension)
	{
		case '.jpg':
		case '.jpeg':
			$img = @imagecreatefromjpeg($src);
			break;
		case '.gif':
			$img = @imagecreatefromgif($src);
			break;
		case '.png':
			$img = @imagecreatefrompng($src);
			break;
	}
	$width = imagesx($img);
	$height = imagesy($img);




	$dis_height = $dis_width * ($height / $width);

	$new_image = imagecreatetruecolor($dis_width, $dis_height);
	imagecopyresampled($new_image, $img, 0, 0, 0, 0, $dis_width, $dis_height, $width, $height);


	$imageQuality = 100;

	switch($extension)
	{
		case '.jpg':
		case '.jpeg':
			if (imagetypes() & IMG_JPG) {
				imagejpeg($new_image, $dist, $imageQuality);
			}
			break;

		case '.gif':
			if (imagetypes() & IMG_GIF) {
				imagegif($new_image, $dist);
			}
			break;

		case '.png':
			$scaleQuality = round(($imageQuality/100) * 9);
			$invertScaleQuality = 9 - $scaleQuality;

			if (imagetypes() & IMG_PNG) {
				imagepng($new_image, $dist, $invertScaleQuality);
			}
			break;
	}
	imagedestroy($new_image);
}