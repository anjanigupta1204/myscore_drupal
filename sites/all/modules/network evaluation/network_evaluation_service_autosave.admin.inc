<?php

function network_evaluation_service_autosave(){	

	$dealer_type = '';
	$qtype = '';
	
	
	if(array_key_exists('ard_name',$_POST['question'])){

		$dealer_type = 'ARD-SERVICE';
		$qtype = 'ARD';

	}else if(array_key_exists('asc_name',$_POST['question'])){

		$dealer_type = 'ASC-SERVICE';
		$qtype = 'ASC';

	}
	
	echo $dealer_type;
	
	if($dealer_type != '' ){	

		if($dealer_type == 'ARD-SERVICE'){

		$ard_name = $_POST['question']['ard_name'];

		$ard_code = $_POST['question']['ard_code'];

		$evaluation_month = $_POST['question']['ard_evaluation_month'];
		
		$evaluation_by = $_POST['question']['ard_evaluation_by'];

		

		}else if($dealer_type == 'ASC-SERVICE'){			

		$ard_name = $_POST['question']['asc_name'];

		$ard_code = $_POST['question']['asc_code'];

		$evaluation_month = $_POST['question']['asc_evaluation_month'];

		$evaluation_by = $_POST['question']['asc_evaluation_by'];

		}

		

		$tsm_feedback = $_POST['tsm_feedback'];		

			

		$form_data = drupal_json_encode($_POST['question']);	

		
		$status = 0;
		
		$criteria_score = criteria_score($qtype);
		
		/* Status calculation */		
		/* qualification criteria 60% 60% ...  and overall 60% */		
		$isQualified = 1;
		
		if(!empty($criteria_score)){			
			foreach($criteria_score as $cscore){
				if($cscore['criteria'] != 'manpower' ){
					
					$myscore += $cscore['obtain_score'];
					$overall += $cscore['overall_score'];
					
					if($cscore['obtain_score'] != 0 && $cscore['overall_score'] != 0 ){
						 if((($cscore['obtain_score']*100)/$cscore['overall_score']) < 60){
							 $isQualified = 0;
						 }
					 }					
				}else{
					if($cscore['criteria'] == 'manpower' ){
						
						$myscore += ($cscore['obtain_score']['available']+$cscore['obtain_score']['training']);
						$overall += $cscore['overall_score']['total'];
						
						if(($cscore['obtain_score']['available']+$cscore['obtain_score']['training']) != 0 && $cscore['overall_score']['total'] != 0 ){
							 if(((($cscore['obtain_score']['available']+$cscore['obtain_score']['training'])*100)/$cscore['overall_score']['total']) < 60){
								 $isQualified = 0;
							 }
						 }
					}			
				}			
			}
		}
		
		/* Status Calculation */

		$query = db_select('evaluation_score', 'ev')

		 ->fields('ev', array())

		  ->condition('ev.asc_ard_code', $ard_code)
		  ->condition('ev.dealer_type', $dealer_type)	
		  ->condition('ev.eva_by_user', $evaluation_by)	
		  ->condition('ev.eva_month', $evaluation_month)	
		  ->execute();

		/* print_r($_POST['question']); */

		  $rowCount = $query->rowCount();

			if($rowCount){  
			
				$eval = $query->fetchAssoc();
				
				/* if auto save  enabled */
				
				if($eval['auto_save'] == 1){
					
					db_update('evaluation_score')

						->fields(array(			       

					  'total_marks' => '0',			  

					  'marks_obtained' => drupal_json_encode($criteria_score),   
					  'is_qualified' => $isQualified,      

					  'eva_answer' => $form_data,       

					  'tsm_feedback' => $tsm_feedback,       

					))

					->condition('asc_ard_code', $ard_code)
					->condition('dealer_type', $dealer_type)
					->condition('eva_by_user', $evaluation_by)	
					->condition('eva_month', $evaluation_month)	
					->execute();
					
					echo 'Auto save done!!!!';
				}
				
			}

			else{

			 db_insert('evaluation_score')

				->fields(array(      

			  'eva_by_user' => $evaluation_by,

			  'asc_ard_code' =>  $ard_code,      

			  'asc_ard_name' =>  $ard_name,      

			  'eva_month' =>  $evaluation_month,      

			  'total_marks' => '0',        

			  'eva_date' => date('Y-m-d H:i:s'),

			  'dealer_type' => $dealer_type,

			  'marks_obtained' => drupal_json_encode($criteria_score),       

			  'eva_status' => $status,      
			  'is_qualified' => $isQualified,      

			  'eva_answer' => $form_data,       

			  'tsm_feedback' => $tsm_feedback,       

			))->execute(); 
			
			echo 'Auto save done!!! New Entry';
		} 
		
		
	}
	
}

function criteria_score($qtype){
	
		
		/*score calculation */
		$criteria_score = array();
		$infra_obtain_score = array();
		
		/* Infrastructure   */
		$infra  = "SELECT id, max_mark, training_mark, question_type FROM `evaluation_question` WHERE question_type = '$qtype' AND question_category in (SELECT id FROM `evaluation_category` WHERE parent_id = 1) and status = '1'";	
		$infra_result = db_query($infra);
		
		
		
		/* system & process   */
		$system_obtain_score = array();
		$system_process  = "SELECT id, max_mark, training_mark, question_type FROM `evaluation_question` WHERE question_type = '$qtype' AND question_category in (SELECT id FROM `evaluation_category` WHERE parent_id = 2) and status = '1'";	
		$system_process_result = db_query($system_process);
		
		
		/* Manpower  */
		$manpower_obtain_score = array();
		$manpower  = "SELECT id, max_mark, training_mark, question_type FROM `evaluation_question` WHERE question_type = '$qtype' AND question_category in (SELECT id FROM `evaluation_category` WHERE parent_id = 15 ) and status = '1'";	
		$manpower_result = db_query($manpower);
		
		
		
		/* KPI   */
		$kpi_obtain_score = array();
		$kpi  = "SELECT id, max_mark, training_mark, question_type FROM `evaluation_question` WHERE question_type = '$qtype' AND question_category in (SELECT id FROM `evaluation_category` WHERE parent_id = 16) and status = '1'";	
		$kpi_result = db_query($kpi);
		
		$infra_score = 0;
		$infra_overall_score = 0;
		foreach($infra_result->fetchAll(PDO::FETCH_ASSOC) as $score){
			if(array_key_exists($score['id'],$_POST['question']['score']) && $_POST['question']['score'][$score['id']] == 1){
				$infra_score += $score['max_mark'];
			}
			$infra_overall_score += $score['max_mark'];
			
		}
		$infra_obtain_score['obtain_score'] = $infra_score;
		$infra_obtain_score['overall_score'] = $infra_overall_score;
		$infra_obtain_score['criteria'] = 'infra';
		
		
		
		$system_process_score = 0;
		$system_process_overall_score = 0;
		foreach($system_process_result->fetchAll(PDO::FETCH_ASSOC) as $score){
			if(array_key_exists($score['id'],$_POST['question']['score']) && $_POST['question']['score'][$score['id']] == 1){
				$system_process_score += $score['max_mark'];
			}
			$system_process_overall_score += $score['max_mark'];
		}
		
		
		$system_obtain_score['obtain_score'] = $system_process_score;
		$system_obtain_score['overall_score'] = $system_process_overall_score;
		$system_obtain_score['criteria'] = 'system_process';
		
		/* echo '<br>';
		echo '$infra_score: '.$infra_score;
		echo '<br>';
		echo '$infra_overall_score: '.$infra_overall_score;
		echo '<br>';
		
		echo '$system_process_score: '.$system_process_score;
		echo '<br>';
		echo '$system_process_overall_score: '.$system_process_overall_score;
		echo '<br>'; */
		
		$_required = $_POST['question']['required'];
		$available = $_POST['question']['availabe'];
		$score_trining = $_POST['question']['score_trining'];
		$actual_par = $_POST['question']['actual_par'];
		
		//p_r($actual_par);
		
		$perc_available = array();
		$perc_score_trining = array();
		
		
		
		foreach($_required as $qid => $required){
			$perc_available[$qid] = 0;
			
			if($available[$qid] != '' && $required != '' ){
				if($available[$qid] != '0')
					$perc_available[$qid] = ($available[$qid]*100)/$required;
				else
					$perc_available[$qid] = 100;
			}
		}
		
		foreach($score_trining as $qid => $score){
			$perc_score_trining[$qid] = 0;
			if($available[$qid] != '0')
			$perc_score_trining[$qid] = ($score*100)/$available[$qid];
			else
			$perc_score_trining[$qid] = 100;
		}
		
		$perc_available_score = 0;
		$perc_available_overall_score = 0;
		
		$score_trining_score = 0;
		$score_trining_overall_score = 0;
		
		foreach($manpower_result->fetchAll(PDO::FETCH_ASSOC) as $score){
			if(array_key_exists($score['id'],$perc_available) && $perc_available[$score['id']] != '' ){
				if($perc_available[$score['id']] >= 100){
					$perc_available_score += $score['max_mark'];
				}else{
					$perc_available_score += ($score['max_mark'] * $perc_available[$score['id']])/100;
				}
				
			}		
			$perc_available_overall_score += $score['max_mark'];
			
			if(array_key_exists($score['id'],$perc_score_trining) && $perc_score_trining[$score['id']] != '' ){
				if($perc_score_trining[$score['id']] >= 100){
					$score_trining_score += $score['training_mark'];
				}else{
					$score_trining_score += ($score['training_mark'] * $perc_score_trining[$score['id']])/100;
				}
			}
			$score_trining_overall_score += $score['training_mark'];
		}
		
		
		
		/* echo '<pre>';
		echo '$perc_available_score: '.$perc_available_score;
		echo '<br>';
		echo '$perc_available_overall_score: '.$perc_available_overall_score;
		echo '<br>';
		
		echo '$score_trining_score: '.$score_trining_score;
		echo '<br>';
		echo '$score_trining_overall_score: '.$score_trining_overall_score;
		echo '<br>'; */
		
		
		$kpi_score = 0;
		$kpi_overall_score = 0;
		
		foreach($kpi_result->fetchAll(PDO::FETCH_ASSOC) as $score){
			if(array_key_exists($score['id'],$actual_par) && $actual_par[$score['id']]){
				if($actual_par[$score['id']] >= 100){
					$kpi_score += $score['max_mark'];
				}else{
					$kpi_score += ($score['max_mark'] * $actual_par[$score['id']])/100;
				}
			}
			$kpi_overall_score += $score['max_mark'];
		}
		
		$manpower_obtain_score['obtain_score'] = array(
			'available' => round($perc_available_score), 
			'training' => round($score_trining_score) , 
			'total' => round($perc_available_score + $score_trining_score)
		);
		
		$manpower_obtain_score['overall_score'] = array(
			'available' => $perc_available_overall_score ,
			'training' => $score_trining_overall_score, 
			'total' => $score_trining_overall_score + $perc_available_overall_score
		);
		
		$manpower_obtain_score['criteria'] = 'manpower';
		
		
		$kpi_obtain_score['obtain_score'] = $kpi_score;
		$kpi_obtain_score['overall_score'] = $kpi_overall_score;
		$kpi_obtain_score['criteria'] = 'kpi';
		
		
		$criteria_score = array($infra_obtain_score,$system_obtain_score,$manpower_obtain_score,$kpi_obtain_score);
		
		/* 
		echo '$kpi_score: '.$kpi_score;
		echo '<br>';
		echo '$kpi_overall_score: '.$kpi_overall_score; 
		*/
		
		//echo '<br>';
		
		
		//p_r($criteria_score);
		
		//die;
		/*score calculation */
		return $criteria_score;
		
		
}

function p_r($arr){
echo '<pre>';print_r($arr);echo '</pre>';
}