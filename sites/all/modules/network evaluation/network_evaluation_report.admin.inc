<?php
//module_load_include('inc', 'score', 'dealer_mapping.admin');

function network_evaluation_report_download_start() { 
 
$path = menu_get_item();
// print_R( $path['map'] );
/* 
[0] => network-evaluation
[1] => asc
[2] => 99990
[3] => sales
[4] => report
[5] => Jul-2016
[6] => download
[7] => 9479 
*/

$dealer_type 		= $path['map'][1];
$dealer_code 		= $path['map'][2];
$dealer_dept 		= $path['map'][3];
$evaluation_month 	= $path['map'][5];
$evaluation_by_staff = $path['map'][7];

if((int)$dealer_code && $dealer_dept && (int)$evaluation_by_staff ){
	
$query = db_select('evaluation_score', 'ev')
->fields('ev', array())
->condition('ev.asc_ard_code', $dealer_code)	
->condition('ev.dealer_type', "$dealer_type-$dealer_dept")	
->condition('ev.eva_by_user', $evaluation_by_staff)
->condition('ev.eva_month', $evaluation_month)
->execute();
	  


$rowCount = $query->rowCount();

if($rowCount){
	
	$result 		= $query->fetchAssoc();
	$qn['question'] = drupal_json_decode($result['eva_answer']);
	//print_r($qn['question']);
	//die;
	$questions_id = implode(',',array_keys($qn['question']['score']));
	
	

	//print_r(array_keys($qn['question']['score']));
	
	if($dealer_dept == 'sales'){
		
			$qq = "SELECT (SELECT name FROM evaluation_category WHERE id = question_category) category_name,question ,max_mark, id actual_value, id score, id remark, id target_date, guidelines FROM `evaluation_question` WHERE id in($questions_id) order by category_name";
			
			
			$result = db_query($qq);
			
			$header[0] = array('Dealer Code',$dealer_code,'','','Month:'.$evaluation_month,'','','');
			$header[1] = array('','','','','','','','');
			$header[2] = array(
			'Criteria Name',
			'Parameter',
			'Max Marks',
			'Actual Value',
			'(Scores to be given by TM)',
			'Remarks/Action Plan',
			'Target Date',
			'Guide Lines'
			);
			
			$results = $result->fetchAll(PDO::FETCH_ASSOC);
			$filename =  "Network-Evaluation-$dealer_code-$evaluation_month.xls";
			header("Content-Disposition: attachment; filename=$filename");
			header("Content-Type: application/vnd.ms-excel;");
			header("Pragma: no-cache");
			header("Expires: 0");
			$out = fopen("php://output", 'w');
			
			foreach ($header as $data)
			{
				fputcsv($out, $data,"\t");
			}
		
			foreach ($results as $data)
			{	
				unset($data['sub_question']);
				$data['actual_value'] = $qn['question']['description'][$data['actual_value']];
				
				if($qn['question']['description'][$data['actual_value']] == 1)
				$data['actual_value'] 	= 'Yes';
				else if($qn['question']['description'][$data['actual_value']] == 2)
				$data['actual_value'] 	= 'No';
				
				$data['score'] 			= $qn['question']['score'][$data['score']];
				$data['remark'] 		= $qn['question']['remark'][$data['remark']];
				$data['target_date'] 	= $qn['question']['date'][$data['target_date']];
				$data['guidelines'] 	= str_replace(array('<br/>','<br>'),'',$data['guidelines']);
			
				fputcsv($out, $data,"\t");
			}
			fclose($out);

		}else if($dealer_dept == 'service'){
			/* Download for servie */
			
			$availabe_questions_id = array_keys($qn['question']['availabe']);
			$actual_questions_id = array_keys($qn['question']['actual']);
			
			
			
			$qq = "SELECT (SELECT name FROM evaluation_category WHERE id = (SELECT parent_id FROM evaluation_category WHERE id = question_category)) category_name,(SELECT name FROM evaluation_category WHERE id = question_category) sub_category,question,sub_question , id score, id remark, id target_date FROM `evaluation_question` WHERE id in($questions_id,".implode(',',$availabe_questions_id).",".implode(',',$actual_questions_id).") order by category_name";
		
			
			$result = db_query($qq);
			
			$header[0] = array('Dealer Code',$dealer_code,'Month:'.$evaluation_month);
			$header[1] = array('','','','','','');
			$header[2] = array(
			'Criteria Name',
			'Subcriteria Name',
			'Parameter',
			'(Scores to be given by TM)',
			'Remarks/Action Plan',
			'Target Date',		
			); 
			
			$results = $result->fetchAll(PDO::FETCH_ASSOC);
			$filename =  "Network-Evaluation-$dealer_code-$evaluation_month.xls";
			header("Content-Disposition: attachment; filename=$filename");
			header("Content-Type: application/vnd.ms-excel;");
			header("Pragma: no-cache");
			header("Expires: 0");
			 
			$out = fopen("php://output", 'w');
			
			foreach ($header as $data)
			{
				fputcsv($out, $data,"\t");
			} 
		
			/* adding data for Infrastructure','System & Process */
			
			foreach ($results as $data)
			{	
				if(in_array(trim($data['category_name']) ,array('Manpower','KPI'))){
					continue;
				}
				unset($data['sub_question']);
				$data['category_name'] 		= trim($data['category_name']);
				$data['sub_category'] 		= trim($data['sub_category']);
				
				$data['question'] 		= trim($data['question']);
				
				if($qn['question']['score'][$data['score']] == 1)
				$data['score'] 	= 'Yes';
				else if($qn['question']['score'][$data['score']] == 2)
				$data['score'] 	= 'No';
						
				$data['remark'] 		= $qn['question']['remark'][$data['remark']];
				$data['target_date'] 	= $qn['question']['date'][$data['target_date']];
				
			
				fputcsv($out, $data,"\t");
			}
			 
			/* adding data for Manpower */
			$header = array();
			$header[0] = array('','','','','','','','');
			$header[1] = array('','','','','','','','');
			$header[2] = array(
			'Criteria Name',
			'Subcriteria Name',
			'Parameter',
			'Required',
			'Available',
			'Training',
			'Remark/Action Plan',
			'Target Date',		
			);
			
			foreach ($header as $data)
			{
				fputcsv($out, $data,"\t");
			}
			 
			
			
			 foreach ($results as $data)
			{	
				if(in_array(trim($data['category_name']), array('Infrastructure','System & Process','KPI'))) {
					continue;					
				}				
							
				$data['category_name'] = trim($data['category_name']);
				$data['sub_category'] = trim($data['sub_category']);				
				$data['question'] = trim($data['question']);
				$remark =  $data['remark'];
				$td =  $data['target_date'];
				
				unset($data['target_date']);
				unset($data['remark']);
				unset($data['score']);
				unset($data['sub_question']);
				
				$data['required'] = ($qn['question']['required'][$remark] != '') ? $qn['question']['required'][$remark] : '-';
				$data['availabe'] = ($qn['question']['availabe'][$remark] != '') ? $qn['question']['availabe'][$remark] : '-';
				$data['score_trining'] = ($qn['question']['score_trining'][$remark] != '') ? $qn['question']['score_trining'][$remark] : '-';
				
				$data['remark'] = $qn['question']['remark'][$remark];
				$data['target_date'] = $qn['question']['date'][$td];
				
				
			
			
				fputcsv($out, $data,"\t");
				
			} 
			
			
			/* adding data for KPI */
			
		 	$header = array();
			$header[0] = array('','','','','','','','','');
			$header[1] = array('','','','','','','','','');
			$header[2] = array(
			'Criteria Name',
			'Sub-Criteria Name',
			'Parameter',
			'Sub-Parameter',
			'Target',
			'Actual',
			'% Ach',
			'% Growth from LY',
			'Remarks/Action Plan',
			'Target Date',		
			);
			
			foreach ($header as $data)
			{
				fputcsv($out, $data,"\t");
			} 
			
			
		
			foreach ($results as $data)
			{	
				if(in_array(trim($data['category_name']), array('Infrastructure','System & Process','Manpower'))) {
					continue;	
					
				}
				
				$remark =  $data['remark'];
				//echo $remark;
				//echo '<br>';
				//continue;
				
				$td =  $data['target_date'];
				
				unset($data['target_date']);
				unset($data['remark']);
				unset($data['score']);
				
							
				$data['category_name'] 	= trim($data['category_name']);
				$data['sub_category'] 	= trim($data['sub_category']);				
				$data['question'] 		= trim($data['question']);														
				$data['sub_question']	= trim($data['sub_question']);														
				$data['target'] 		= ($qn['question']['target'][$remark] != '') ? $qn['question']['target'][$remark] : '-';
				$data['actual'] 		= ($qn['question']['actual'][$remark] != '') ? $qn['question']['actual'][$remark]  : '-';
				$data['actual_par'] 	= ($qn['question']['actual_par'][$remark] != '') ? $qn['question']['actual_par'][$remark]  : '-';
				$data['last_year_growth'] = ($qn['question']['last_year_growth'][$remark] != '') ? $qn['question']['last_year_growth'][$remark] : '-';			
				$data['remark'] 		= $qn['question']['remark'][$remark];
				$data['target_date'] 	= $qn['question']['date'][$td];
				
			
				fputcsv($out, $data,"\t");
				
			} 
			
			fclose($out);
		
		}
	}
}else{
	die('No data found!!');
}

}





