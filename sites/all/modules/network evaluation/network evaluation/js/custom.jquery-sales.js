/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ARD validaion start %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* network-evaluation-ARD-sales-display-form validation */


var sk = jQuery.noConflict();

var ard_form = 'network-evaluation-ard-sales-display-form';
var asc_form = 'network-evaluation-asc-sales-display-form';
var error_in_criteria = [];
sk(document).ready(function() {	

	function score_validation(form_id){
	 sk('#'+form_id+' [id^=edit-question-score-]:not([readonly="readonly"])').each(function(){  

		  var numbers = /^[0-9]+$/;  
		  var score = sk(this).val();
		  
		  if(!sk(this).val().match(numbers)) {
			  sk(this).addClass('ard-validation-error');
		  } else{
			  sk(this).removeClass('ard-validation-error');
		  }
		  
		 /* START : score value is less then 50% and data or remark field is empty then show error message */
		  var max = sk(this).data('maxmark');
				
				var is_score_gt = false;
				if(score != '' && sk.isNumeric(max) && sk.isNumeric(score)){				
						var value_ok	= (parseInt(score)*100)/parseInt(max);
						if(Math.round(value_ok) >= 50){
							is_score_gt = true;
						}
				}		
				
				var current_tr = sk(this).closest('tr');	
				var remark = current_tr.find('[id^=edit-question-remark-]');
				var dt = current_tr.find('[id^=edit-question-date-]');
				
				/* console.log('is_score_gt' + is_score_gt);*/
				
				if(is_score_gt){					
					remark.removeClass('ard-validation-error');				
					dt.removeClass('ard-validation-error');					
				}else{
					if(remark.val() == '' ){
					remark.addClass('ard-validation-error');
					}else{
					remark.removeClass('ard-validation-error');
					}
				
					if(dt.val() == '' ){
					dt.addClass('ard-validation-error');
					}else{
					dt.removeClass('ard-validation-error');
					}
				}
				/* END: score value is less then 50% and data or remark field is empty then show error message */
	  });
	}

	sk( "#"+ard_form ).submit(function( event ) {

	  /*console.log( "Handler for .submit() called." );*/

	  event.preventDefault();

	  /* score validation */	  
	  score_validation(ard_form);
	 
	 
	

	/* Description radio button validaion on submit */	  

	  sk('#'+ard_form+' fieldset tr').each(function(){		  

		 var current_tr = sk(this);		
		
		

		var have_radio = current_tr.find('[type="radio"]').length;
		/* actual value validation */
		var qdesc = current_tr.find('[id^=edit-question-description-][type="text"],[id^=edit-question-description-]textarea');
		if(qdesc.length){
			if(qdesc.val() == ''){
				qdesc.addClass('ard-validation-error');
			}else{
				qdesc.removeClass('ard-validation-error');
			}
		}
		/* actual value validation */
		
		if(have_radio){

			var select_checked = current_tr.find('[type="radio"]:checked');

			if(select_checked.length){

				current_tr.removeClass('ard-validation-error');

				var _val = select_checked.val();

				

				var remark = current_tr.find('[id^=edit-question-remark-]');

				var dt = current_tr.find('[id^=edit-question-date-]');
				
				var score = current_tr.find('[id^=edit-question-score-]').val();
				var max = current_tr.find('[id^=edit-question-score-]').data('maxmark');
				
				var is_score_gt = false;
				if(score != '' && max && sk.isNumeric(score)){				
						var value_ok	= (parseInt(score)*100)/max;
						if(Math.round(value_ok) >= 50){
							is_score_gt = true;
						}
				}			
				

				if(_val == 2){

					if(sk('#'+remark.attr('id')).val() == '' && !is_score_gt )

					sk('#'+remark.attr('id')).addClass('ard-validation-error');	

					else

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');	

					

					/* find all date fields */

					if(sk('#'+dt.attr('id')).val() == '' && !is_score_gt )

					sk('#'+dt.attr('id')).addClass('ard-validation-error');	

					else

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');

				}else if(_val == 1){			

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');

				}	

			}

			else{

				current_tr.addClass('ard-validation-error');
				
			}

		}	
		
		/* criteria date validation */
		
		var cdt = sk(this).closest('fieldset').find('[id^=edit-question-criteria-date-]');
		var rk = sk(this).closest('fieldset').find('[id^=remark-]').text();

		if(cdt.val() == '' && rk == 'Disqualify' ){
			cdt.addClass('ard-validation-error');
		}else{
			cdt.removeClass('ard-validation-error');
		}
		
		/* criteria date validation */
		

	  });

	   var errors = sk( "#"+ard_form+" .ard-validation-error" );

	   /* console.log(errors); */

	   if(!errors.length){		
		 
		 sk.ajax({
		  url: Drupal.settings.network_evaluation.hostname+"/network-evaluation/network-evaluation-autosave",
		  beforeSend: function( xhr ) { sk.colorbox({html:"Please wait saving data....."});  },
		  method: "POST",		 
		  data: sk("#"+ard_form).serializeArray(),
		  dataType: "html",		  
		}).done(function( data ) {			
			sk("#"+ard_form).unbind('submit').submit();
		});
		
		 		

	   }

	   else{
			error_in_criteria = [];
			var criteriaid = criteria = '';
			errors.closest('fieldset').each(function(){
				 criteriaid = sk(this).data('criteriaid');
				 criteria = sk(this).data('criteria');
				if(criteria !== 'undefined' && criteriaid !== 'undefined' )
				error_in_criteria[criteriaid] = criteria;
			});
			error_in_criteria = error_in_criteria.filter(function( element ) {
			   return !!element;
			});
				  
		   
		   sk.colorbox({html:"There are <span class='error-count'>"+errors.length+"</span> error(s) found!! <br> Please correct errors.<br> Errors are in the following criteria:<ul style=' color: red; list-style: inside disc;margin-left:-30px;margin-top: 0;'><li>"+error_in_criteria.join('<li>','</li>')+"</li></ul>"});

	   }

		

	});

	/* *********************************************end form submition validaion ***************************************** */

	
	
	

	/* score validation */

	sk(document).on('keyup change','#'+ard_form+' [id^=edit-question-score-]',function(){

		 var numbers = /^[0-9]+$/;  	

		  var thisval = sk(this).val();
			
		 /* number validation */
		  number_validation(this);
		  
		  
		   var lg_array , range_array ;
			
			var markrange = sk(this).data('markrange');
			
			/* value vadation */
			/* console.log(markrange);
			 console.log(thisval); */
			 /* this or that value */
			 if(markrange.indexOf("<") != -1 ){
			 
				lg_array = markrange.split("<");
				var len = lg_array.length;
				
				if( len == 2 ){
					if(!(parseInt(thisval) == parseInt(lg_array[0]) || parseInt(thisval) == parseInt(lg_array[1]))){
						
						
						if(thisval != 1){
						sk(this).val('');
							sk.colorbox({html:'Only allowed digits '+lg_array[0]+' , '+lg_array[1]});
						}
						
					}
				}else if( len == 3){
					
					if(!((parseInt(thisval) == lg_array[0]) || (parseInt(thisval) ==  parseInt(lg_array[1])) || (parseInt(thisval) == parseInt(lg_array[2]))))
					{
						if(thisval != 1){
						sk(this).val('');
						sk.colorbox({html:'Only allowed digits '+lg_array[0]+' , '+lg_array[1]+' , '+lg_array[2]});
						}
					}

					/* console.log(lg_array);
					//console.log('sk-'+parseInt(thisval));
					*/
				}
				
				
			 }else if(markrange.indexOf("-") != -1){
				range_array = markrange.split("-");					 
				var len = range_array.length;
				
				if( len == 2 ){
					if(parseInt(thisval) < range_array[0]){
						
						sk.colorbox({html:'Only allowed digits '+range_array[0]+' to '+range_array[1]});
						sk(this).val('');
					}
					else if(parseInt(thisval) > range_array[1]){						
						sk.colorbox({html:'Only allowed digits '+range_array[0]+' to '+range_array[1]});
						sk(this).val('');
					}
					
				}
				
			 
			 }
			 
			/* ********************************** ARD score calculatin for each criteria ******************************************** */				
			criteria_score(this);				
			/* ********************************** ARD score calculatin for each criteria ******************************************** */
			/* ********************************** ARD score calculatin final ******************************************** */
			/* final score */
			final_score(ard_form);
			/* final score */

			/* ********************************** ARD score calculatin final ******************************************** */

			 /* score is less 70% then the date and action plan is mandatory  */
				
				var sval = sk(this).val();
				var maxmark = sk(this).data('maxmark');
				
				if(maxmark && sk.isNumeric(sval) && sval != '0' ){					
					
					var parcent = (parseInt(sval)*100)/maxmark;
					
					var remark = sk(this).closest("tr").find('[id^=edit-question-remark-]');
					var dt = sk(this).closest("tr").find('[id^=edit-question-date-]');
					
					if(Math.round(parcent) < 50)
					sk('#'+remark.attr('id')).addClass('ard-validation-error');	
					else
					sk('#'+remark.attr('id')).removeClass('ard-validation-error');
					
					if(Math.round(parcent) < 50)
					sk('#'+dt.attr('id')).addClass('ard-validation-error');
					else
					sk('#'+dt.attr('id')).removeClass('ard-validation-error');				
					
				}
			 /* score is less 70% then the date and action plan is mandatory  */
				  

	});

	/* Description radio button validaion */

	sk(document).on('click','#'+ard_form+' [id^=edit-question-description-]:radio',function(e){

		e.stopPropagation();

		var _val = sk(this).val();

		var remark = sk(this).closest("tr").find('[id^=edit-question-remark-]');

		var dt = sk(this).closest("tr").find('[id^=edit-question-date-]');
		var score = sk(this).closest("tr").find('[id^=edit-question-score-]');
		var markrange = score.data('markrange');

		//console.log(score);
		
		/* set value in score field */
		/* set value in score field */
		
		 var lg_array , range_array ;		
			
			/* value vadation */
			 //console.log(markrange);
			 //console.log(_val);
			 /* this or that value */
			 if(markrange.indexOf("<") != -1 ){
			 
				lg_array = markrange.split("<");
				var len = lg_array.length;
				
				if( len == 2 ){
					//console.log(lg_array);
					if(_val == 2)
					score.val(lg_array[0]);
					else if(_val == 1)
					score.val(lg_array[1]);
				}		
				
			 }else if(markrange.indexOf("-") != -1){
				range_array = markrange.split("-");					 
				var len = range_array.length;
				
				if( len == 2 ){
					//console.log(range_array);
					if(_val == 2)
					score.val(range_array[0]);
					else if(_val == 1)
					score.val(range_array[1]);
					
				}		
			 
			 }
		
		/* set value in score field */
		/* set value in score field */
		
		
		

		if(_val == 2){

			if(sk('#'+remark.attr('id')).val() == '')

			sk('#'+remark.attr('id')).addClass('ard-validation-error');	

			else

			sk('#'+remark.attr('id')).removeClass('ard-validation-error');	

			

			/* find all date fields */

			if(sk('#'+dt.attr('id')).val() == '')

			sk('#'+dt.attr('id')).addClass('ard-validation-error');	

			else

			sk('#'+dt.attr('id')).removeClass('ard-validation-error');

		}else if(_val == 1){			

			sk('#'+remark.attr('id')).removeClass('ard-validation-error');

			sk('#'+dt.attr('id')).removeClass('ard-validation-error');

			}
			
			/* ***** ARD score calculatin for each criteria *** */
				criteria_score(this);
				/* **** ARD score calculatin for each criteria ** */
				
				/* ********************************** ard score calculatin final ******************************************** */
				/* final score */
					final_score(ard_form);
				/* final score */				
				/* ********************************** ard score calculatin final ******************************************** */
				
		

	});

	

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASC validaion start %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */



/* network-evaluation-asc-sales-display-form validation*/
	 


	sk( "#"+asc_form ).submit(function( event ) {

	  //console.log( "Handler for .submit() called." );

		event.preventDefault();

	  /* score validation */	  
		score_validation(asc_form);

	  

	

	/* Description radio button validaion on submit */	  

	  sk('#'+asc_form+' fieldset tr').each(function(){		  

		var current_tr = sk(this);		
		
		/* actual value validation */
		var qdesc = current_tr.find('[id^=edit-question-description-][type="text"],[id^=edit-question-description-]textarea');
		if(qdesc.length){
			if(qdesc.val() == ''){
				qdesc.addClass('ard-validation-error');
			}else{
				qdesc.removeClass('ard-validation-error');
			}
		}
		/* actual value validation */

		var have_radio = current_tr.find('[type="radio"]').length;

		if(have_radio){

			var select_checked = current_tr.find('[type="radio"]:checked');

			if(select_checked.length){

				current_tr.removeClass('ard-validation-error');

				var _val = select_checked.val();

				

				/* find all radio buttons */

				var remark = current_tr.find('[id^=edit-question-remark-]');

				/* find all date fields */

				var dt = current_tr.find('[id^=edit-question-date-]');
				
				var score = current_tr.find('[id^=edit-question-score-]').val();
				var max = current_tr.find('[id^=edit-question-score-]').data('maxmark');
				
				var is_score_gt = false;
				if(score != '' && max && sk.isNumeric(score)){				
						var value_ok	= (parseInt(score)*100)/max;
						if(Math.round(value_ok) >= 50){
							is_score_gt = true;
						}
				}	
				
				if(_val == 2){

					if(sk('#'+remark.attr('id')).val() == '' && !is_score_gt)

					sk('#'+remark.attr('id')).addClass('ard-validation-error');	

					else

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');	

					

					/* find all date fields */

					if(sk('#'+dt.attr('id')).val() == '' && !is_score_gt)

					sk('#'+dt.attr('id')).addClass('ard-validation-error');	

					else

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');

					

				}else if(_val == 1){			

					sk('#'+remark.attr('id')).removeClass('ard-validation-error');

					sk('#'+dt.attr('id')).removeClass('ard-validation-error');

				}	

			}

			else{

				current_tr.addClass('ard-validation-error');				

			}

		}
		
		/* criteria date validation */
		
		var cdt = sk(this).closest('fieldset').find('[id^=edit-question-criteria-date-]');
		var rk = sk(this).closest('fieldset').find('[id^=remark-]').text();

		if(cdt.val() == '' && rk == 'Disqualify' ){
			cdt.addClass('ard-validation-error');
		}else{
			cdt.removeClass('ard-validation-error');
		}
		
		/* criteria date validation */
		

	  });

	   var errors = sk( "#"+asc_form+" .ard-validation-error" );

	  /* console.log(errors); */

	   if(!errors.length){	
			
			 sk.ajax({
			  url: Drupal.settings.network_evaluation.hostname+"/network-evaluation/network-evaluation-autosave",
			  beforeSend: function( xhr ) {  sk.colorbox({html:"Please wait saving data....."}); },
			  method: "POST",		 
			  data: sk("#"+asc_form).serializeArray(),
			  dataType: "html",		  
			}).done(function( data ) {				
				sk("#"+asc_form).unbind('submit').submit();
			});  

	   } else{

		    error_in_criteria = [];
			var criteriaid = criteria = '';
			errors.closest('fieldset').each(function(){
				 criteriaid = sk(this).data('criteriaid');
				 criteria = sk(this).data('criteria');
				if(criteria !== 'undefined' && criteriaid !== 'undefined' )
				error_in_criteria[criteriaid] = criteria;
			});
			error_in_criteria = error_in_criteria.filter(function( element ) {
			   return !!element;
			});
				  
		   
		   sk.colorbox({html:"There are <span class='error-count'>"+errors.length+"</span> error(s) found!! <br> Please correct errors.<br> Errors are in the following criteria:<ul style=' color: red; list-style:inside disc;margin-left:-30px;margin-top: 0;'><li>"+error_in_criteria.join('<li>','</li>')+"</li></ul>"});
	   }

	  

	});

	

	/* *********************************************end form submition validaion ***************************************** */

		

	/* score validation */

	sk(document).on('keyup change','#'+asc_form+' [id^=edit-question-score-]',function(){

		 var numbers = /^[0-9]+$/;  	

		  var thisval = sk(this).val();
		 
		 /* number validation */
		  number_validation(this);
		  
		   var lg_array , range_array ;
			
			var markrange = sk(this).data('markrange');
			
			/* value vadation */
			 //console.log(markrange);
			 //console.log(thisval);
			 /* this or that value */
			 if(markrange.indexOf("<") != -1 ){
			 
				lg_array = markrange.split("<");
				var len = lg_array.length;
				
				if( len == 2 ){
					if(!(parseInt(thisval) == parseInt(lg_array[0]) || parseInt(thisval) == parseInt(lg_array[1]))){
						
						if(thisval != 1){
						sk(this).val('');						
						sk.colorbox({html:'Only allowed digits '+lg_array[0]+' , '+lg_array[1]});
						}
					}
				}else if( len == 3){
					
					if(!((parseInt(thisval) == lg_array[0]) || (parseInt(thisval) ==  parseInt(lg_array[1])) || (parseInt(thisval) == parseInt(lg_array[2]))))
					{
						
						if(thisval != 1){
						sk(this).val('');						
						sk.colorbox({html:'Only allowed digits '+lg_array[0]+' , '+lg_array[1]+' , '+lg_array[2]});
						}
						
					}

					/* console.log(lg_array);
					//console.log('sk-'+parseInt(thisval));
					*/
				}
				
				
			 }else if(markrange.indexOf("-") != -1){
				range_array = markrange.split("-");					 
				var len = range_array.length;
				
				if( len == 2 ){
					if(parseInt(thisval) < range_array[0]){
						
						sk.colorbox({html:'Only allowed digits '+range_array[0]+' to '+range_array[1]});
						sk(this).val('');
					}
					else if(parseInt(thisval) > range_array[1]){

						sk.colorbox({html:'Only allowed digits '+range_array[0]+' to '+range_array[1]});
						sk(this).val('');
					}
					
				}
				
			 
			 }

				/* ***** ASC score calculatin for each criteria *** */
				criteria_score(this);
				/* **** ASC score calculatin for each criteria ** */
				/* ********************************** ASC score calculatin final ******************************************** */
				/* final score */
				final_score(asc_form);
				/* final score */
				
				/* ********************************** ASC score calculatin final ******************************************** */
				
				/* score is less 50% then the date and action plan is mandatory  */
				
				var sval = sk(this).val();
				var maxmark = sk(this).data('maxmark');
				
				if(maxmark && sk.isNumeric(sval) && sval != '0' ){					
					
					var parcent = (parseInt(sval)*100)/maxmark;
					
					var remark = sk(this).closest("tr").find('[id^=edit-question-remark-]');
					var dt = sk(this).closest("tr").find('[id^=edit-question-date-]');
					
					if(Math.round(parcent) < 50)
					sk('#'+remark.attr('id')).addClass('ard-validation-error');	
					else
					sk('#'+remark.attr('id')).removeClass('ard-validation-error');
					
					if(Math.round(parcent) < 50)
					sk('#'+dt.attr('id')).addClass('ard-validation-error');
					else
					sk('#'+dt.attr('id')).removeClass('ard-validation-error');				
					
				}
			 /* score is less 70% then the date and action plan is mandatory  */

	});

	/* Description radio button validaion */

	sk(document).on('click','#'+asc_form+' [id^=edit-question-description-]:radio',function(e){

		e.stopPropagation();

		var _val = sk(this).val();

		var remark = sk(this).closest("tr").find('[id^=edit-question-remark-]');

		var dt = sk(this).closest("tr").find('[id^=edit-question-date-]');

		var score = sk(this).closest("tr").find('[id^=edit-question-score-]');
		var markrange = score.data('markrange');

		//console.log(score);
		
		/* set value in score field */
		/* set value in score field */
		
		 var lg_array , range_array ;		
			
			/* value vadation */
			 //console.log(markrange);
			 //console.log(_val);
			 /* this or that value */
			 if(markrange.indexOf("<") != -1 ){
			 
				lg_array = markrange.split("<");
				var len = lg_array.length;
				
				if( len == 2 ){
					//console.log(lg_array);
					if(_val == 2)
					score.val(lg_array[0]);
					else if(_val == 1)
					score.val(lg_array[1]);
				}		
				
			 }else if(markrange.indexOf("-") != -1){
				range_array = markrange.split("-");					 
				var len = range_array.length;
				
				if( len == 2 ){
					//console.log(range_array);
					if(_val == 2)
					score.val(range_array[0]);
					else if(_val == 1)
					score.val(range_array[1]);
					
				}		
			 
			 }
		
		/* set value in score field */
		/* set value in score field */
		
		

		if(_val == 2){

			

			if(sk('#'+remark.attr('id')).val() == '')

			sk('#'+remark.attr('id')).addClass('ard-validation-error');	

			else

			sk('#'+remark.attr('id')).removeClass('ard-validation-error');	

			

			/* find all date fields */

			if(sk('#'+dt.attr('id')).val() == '')

			sk('#'+dt.attr('id')).addClass('ard-validation-error');	

			else

			sk('#'+dt.attr('id')).removeClass('ard-validation-error');

		

		}else if(_val == 1){			

			sk('#'+remark.attr('id')).removeClass('ard-validation-error');

			sk('#'+dt.attr('id')).removeClass('ard-validation-error');

		}	

				/* ***** ASC score calculatin for each criteria *** */
				criteria_score(this);
				/* **** ASC score calculatin for each criteria ** */
				/* final score */
				final_score(asc_form);
				/* final score */
				
						

	});

	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ASC validaion end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

	/* *********************************************end form submition validaion ***************************************** */
	
/* show status and date on each criteria heading */
	sk('#'+asc_form+' [id^=edit-fieldset-] a.fieldset-title, #'+ard_form+' [id^=edit-fieldset-] a.fieldset-title').click(function(){
	var remark = cdt = '';
		sk('#'+asc_form+' [id^=edit-fieldset-],#'+ard_form+' [id^=edit-fieldset-]').each(function(){
			remark = cdt = '';
		
			remark = sk(this).find('[id^=remark-]').text();
			cdt = sk(this).closest('[id^=edit-fieldset-]').find('[id^=edit-question-criteria-date-]').val();			
			sk(this).closest('[id^=edit-fieldset-]').find('legend > .status-and-date').html('<span class="remark">'+remark +'</span><span class="cdt">'+ cdt+'</span>');
		});
	});
/* show status and date on each criteria heading */
	
	
	sk(document).on('keyup','#'+asc_form+' [id^=edit-question-description-]',function(){
		number_validation(this);
	});

	sk(document).on('keyup','#'+ard_form+' [id^=edit-question-description-]',function(){
		number_validation(this);
	});	
	
	sk(document).on('keypress click','#'+asc_form+' [id^=edit-question-score-]',function(){
		if(sk(this).closest('tr').find('[type="radio"]').length)
		{
			sk(this).addClass('sreadyonly').blur();
			return;
		}
		
		
		
	});

	sk(document).on('keypress click','#'+ard_form+' [id^=edit-question-score-]',function(){
		
		if(sk(this).closest('tr').find('[type="radio"]').length)
		{
			sk(this).addClass('sreadyonly').blur();
			return;
		}
	});
		  
		  
		  
		  
		  function number_validation(_this){
			  
			  var vgex = sk(_this).data('vgex');
			 
			  
			  var numbers = /^[0-9]+$/; 
			  var replaceregx = /[^0-9]/g;
			  
			  if(vgex == 'alphnumspace'){
				numbers = /^[A-Za-z0-9 ]+$/;
				replaceregx = /[^A-Za-z0-9 ]/g;
			  }else if(vgex == 'alphspace'){
				numbers = /^[A-Za-z ]+$/;
				replaceregx = /[^A-Za-z ]/g;
			  }else if(vgex == 'num+-'){
				  numbers = /^[0-9-]+$/;
				  replaceregx = /[^0-9-]/g;
			  }
			  
			  var thisval = sk(_this).val();
			  
			  if(!thisval.match(numbers)) 
			  {  
				sk(_this).val(thisval.replace(replaceregx,''));				
				//sk(_this).addClass('ard-validation-error');
				return true;
			  }
			  
			  
			 
		  }
		  
		  /* ********************************** Criteria score calculatin start ******************************************** */
		  
		function criteria_score(_this){
			
				var totalscoreotained = score = max = 0;
				sk(_this).closest('fieldset').find('[id^=edit-question-score-]').each(function(){
					
					score = sk(this).val();
					max = sk(this).data('maxmark');
					
					if( score != '' && parseInt(max))
					{
					  totalscoreotained += parseInt(score)
					}
				});
				
				sk(_this).closest('fieldset').find('[id^=total-criteria-score-]').text(totalscoreotained);
				var total = sk(_this).closest('fieldset').find('[id^=total-score-] center').text();
				if(totalscoreotained && total ){
					
					var percent = (totalscoreotained * 100)/total;
					
					/* console.log(percent+'%'); */
					
					var q = 'Disqualify';
					
					if(Math.round(percent) >= 50){
						q = 'Qualify';
						sk(_this).closest('fieldset').find('[id^=edit-question-criteria-date-]').removeClass('ard-validation-error');
					} else{	
						
						var cdt = sk(_this).closest('fieldset').find('[id^=edit-question-criteria-date-]');
			
						if(cdt.val() == ''){
							cdt.addClass('ard-validation-error');
						}
					}
					
					sk(_this).closest('fieldset').find('[id^=remark-]').text(q);
				}
		
		}
		    /* ********************************** Criteria score calculatin end ******************************************** */
		  /* ********************************** ASC score calculatin final ******************************************** */
		function final_score(form_id){
		
		
				var eachcriteria = score =  0;
				
				sk('#'+form_id).find('[id^=total-criteria-score-]').each(function(){
					score = sk(this).text();
					if(score != '')
					eachcriteria += parseInt(score);
					
				});
				var echtotalScore = 0;
				sk('#'+form_id).find('[id^=total-score-] center').each(function(){
										
					score = sk(this).text();
					if(score != '')
					echtotalScore += parseInt(score);
					
				});			
				
				/* console.log('eachcriteria && echtotalScore' + eachcriteria +'::'+ echtotalScore); */
				
				if(eachcriteria && echtotalScore ){
					
					var percentoverall = (eachcriteria * 100)/echtotalScore;
					
					/* console.log(percentoverall+'%'); */
					
					var q = eachcriteria+ ' Disqualify';
					
					if(Math.round(percentoverall) >= 70)
					q = eachcriteria+ ' Qualify';
					
					sk('#yourscore').html(q);
				}
		}
		/* ********************************** ASC score calculatin final ************************************* */
	/* score validation */
	/* show final status and data on criteria heading */
	var cdt = cstatus = '';
	 sk('#'+asc_form+' fieldset').each(function(){
		 
		cdt = sk(this).find('[id^=edit-question-criteria-date-]').val();
		cstatus = sk(this).find('[id^=remark-]').text();
		
		sk(this).find('legend').append('<div class="status-and-date"></div>');
		if(cdt || cstatus)
		sk(this).find('legend > .status-and-date').append('<span class="remark">'+cstatus +'</span><span class="cdt">'+ cdt+'</span>');
		 		 
	 });
	  sk('#'+ard_form+' fieldset').each(function(){
		cdt = sk(this).find('[id^=edit-question-criteria-date-]').val();
		
		cstatus = sk(this).find('[id^=remark-]').text();
		sk(this).find('legend').append('<div class="status-and-date"></div>');
		if(cdt || cstatus)
		sk(this).find('.status-and-date').append('<span class="remark">'+cstatus +'</span><span class="cdt">'+ cdt+'</span>');
		 		 
	 });
	 
	 
	 
	if(!sk('#'+asc_form+' #save_draft').length && sk('#'+asc_form).length == 1){
		sk('[id^=edit-question-date-],[id^=edit-question-remark-],[id^=edit-question-criteria-date-],[id^=edit-question-score-],[id^=edit-question-description-],#edit-tsm-feedback').attr('readonly','readonly');
	}
	sk('#'+asc_form+'  #edit-evaluation-details').find('input').attr('readonly','readonly');
	
	
	if(!sk('#'+ard_form+' #save_draft').length && sk('#'+ard_form).length == 1){
		sk('[id^=edit-question-date-],[id^=edit-question-remark-],[id^=edit-question-criteria-date-],[id^=edit-question-score-],[id^=edit-question-description-],#edit-tsm-feedback').attr('readonly','readonly');
	}
	sk('#'+ard_form+' #edit-evaluation-details').find('input').attr('readonly','readonly');
	
	
	/* save as draft or final save */ 
 
sk( '#'+asc_form +' #save_draft, #'+asc_form +' #save').click(function(){
		if(sk(this).attr('id') == 'save'){
			sk("#save_as").val('1');		
		} else if(sk(this).attr('id') == 'save_draft'){
			sk("#save_as").val('0');		
		}
	 });
 
	sk( '#'+ard_form +' #save_draft, #'+ard_form +' #save').click(function(){
	if(sk(this).attr('id') == 'save'){
		sk("#save_as").val('1');	
	} else if(sk(this).attr('id') == 'save_draft'){
		sk("#save_as").val('0');	
	}
 });

});

sk(document).ready(function() {
     
	sk('.prev-evaluation').click(function(){
			sk(this).next().slideToggle();
	});
	sk('.moreinfo').click(function(e){
			e.preventDefault();
			sk.colorbox({href:Drupal.settings.network_evaluation.hostname+"/financial-note.html"});
	});
	
	
		
	sk('#edit-question-start-date').datepicker({ minDate: 0,
		beforeShow: function(input, inst){
		inst.dpDiv.css({marginLeft: -input.offsetWidth + 'px'});
	},
		dateFormat: 'dd/mm/yy',
	});
	
	/* disable fields */
	
});