<?php


function network_evaluation_autosave(){	



	$dealer_type = '';
	$qtype = '';
	
	if(array_key_exists('ard_name',$_POST['question'])){

		$dealer_type = 'ARD-SALES';
		$qtype = 'ARD-SALES';

	}else if(array_key_exists('asc_name',$_POST['question'])){

		$dealer_type = 'ASC-SALES';
		$qtype = 'ASC-SALES';

	}
	echo $dealer_type;
	
	if($dealer_type != '' ){	

		if($dealer_type == 'ARD-SALES'){

		$ard_name = $_POST['question']['ard_name'];

		$ard_code = $_POST['question']['ard_code'];

		$evaluation_month = $_POST['question']['ard_evaluation_month'];

		$evaluation_by = $_POST['question']['ard_evaluation_by'];

		}else if($dealer_type == 'ASC-SALES'){			

		$ard_name = $_POST['question']['asc_name'];

		$ard_code = $_POST['question']['asc_code'];

		$evaluation_month = $_POST['question']['asc_evaluation_month'];
		
		$evaluation_by = $_POST['question']['asc_evaluation_by'];			

		}

		

		$tsm_feedback = $_POST['tsm_feedback'];		

			

		$form_data = drupal_json_encode($_POST['question']);	

		
		$status = 0;
		
		
		/* getting question criteria wise scores */
		$qq = "SELECT question_category qcategory, GROUP_CONCAT(id SEPARATOR ',') sales_qids, sum(max_mark) as cscore,(SELECT name FROM {evaluation_category} WHERE id = qcategory ) cname FROM {evaluation_question} WHERE question_type = '$qtype' AND status='1' GROUP BY question_category";
		
		$cresult = db_query($qq);
		$cqs = array();
		if($cresult->rowCount()){
		
			$myscores = array();
			foreach($cresult->fetchAll(PDO::FETCH_ASSOC) as $rows){				
				$myscore = 0;
				foreach(explode(',',$rows['sales_qids']) as $qid) {
					/* exclude score for ids */
					if(!in_array($qid, array(141,142,169,168)))
					$myscore += $_POST['question']['score'][$qid];						
				}
				$myscores[$rows['qcategory']] = array('myscore'=> $myscore,'cscore'=> $rows['cscore'], 'cname'=> $rows['cname']);
			}		
		}
		
		/* Status Calculation */
		/* qualification criteria 50% 50% ...  and overall 70% */		
		$isQualified = 1;
		if(!empty($myscores)){
			$myscore = $overall = '';
			foreach($myscores as $cscore){
				$myscore += $cscore['myscore'];
				$overall += $cscore['cscore'];
				 if($cscore['myscore'] != 0 && $cscore['cscore'] != 0 ){
					 if((($cscore['myscore']*100)/$cscore['cscore']) < 50){
						 $isQualified = 0;
					 }
				 }
			}
			
			
			if($myscore != 0  && $overall != 0 && $isQualified){
				$finalscore = round(($myscore*100)/$overall);
				if($finalscore < 70){
					$isQualified = 0;
				}				
			}
		}
		
		/* Status Calculation */
		

		$query = db_select('evaluation_score', 'ev')

		 ->fields('ev', array())

		  ->condition('ev.asc_ard_code', $ard_code)	

		  ->condition('ev.dealer_type', $dealer_type)	
		  ->condition('ev.eva_by_user', $evaluation_by)	
		  ->condition('ev.eva_month', $evaluation_month)
		  ->execute();
		

		global $user; 

		  $rowCount = $query->rowCount();

			if($rowCount){  
				
				$eval = $query->fetchAssoc();
				
				/* if auto save  enabled */
				
				if($eval['auto_save'] == 1){
					echo 'Auto saved done- start!!!!';
					db_update('evaluation_score')

						->fields(array(			       

					  'total_marks' => '0',			  

					  'marks_obtained' => drupal_json_encode($myscores),				       

					   'is_qualified' => $isQualified,      

					  'eva_answer' => $form_data,       

					  'tsm_feedback' => $tsm_feedback,       

					))

					->condition('asc_ard_code', $ard_code)
					->condition('eva_month', $evaluation_month)
					->condition('dealer_type', $dealer_type)
					->condition('eva_by_user', $evaluation_by)	
					->execute();
					
					echo 'Auto saved done!!!!';

				}
				
				}

			else{

			 db_insert('evaluation_score')

				->fields(array(      

			  'eva_by_user' => $evaluation_by,

			  'asc_ard_code' =>  $ard_code,      

			  'asc_ard_name' =>  $ard_name,      

			  'eva_month' =>  $evaluation_month,      

			  'total_marks' => '0',        

			  'eva_date' => date('Y-m-d H:i:s'),

			  'dealer_type' => $dealer_type,

			  'marks_obtained' => drupal_json_encode($myscores),       

			  'eva_status' => $status,      

			   'is_qualified' => $isQualified,      

			  'eva_answer' => $form_data,       

			  'tsm_feedback' => $tsm_feedback,       

			))->execute(); 
			echo 'Auto saved done!!! New entry';
		} 
	
	}
}