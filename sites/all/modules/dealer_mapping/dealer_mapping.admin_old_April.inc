<?php

/**
 * Menu callback for admin/structure/dealer_mapping.
 *
 * @param $theme
 *   The theme to display the administration page for. If not provided, defaults
 *   to the currently used theme.
 */
function dealer_mapping_admin_display($theme = NULL, $dept) {
  $department = $dept;
  // Fetch and sort dealer_mappings.
  $dealer_detailed_list = get_dealer_list($dept);
  return drupal_get_form('dealer_mapping_admin_display_form', $dealer_detailed_list, $department);
}

function _dependent_dropdown_callback1($form, $form_state) {
  return $form['portal1']['c_acc_no1'];
}

function _get_second_dropdown_options1($key = '') {
  if (!empty($key)) {
	$output['0'] = t('All');  
    $result = db_select('hero_area_office_info','haoi')
               ->fields('haoi')
               ->condition('haoi.parent_zone_code',$key,'=')
               ->execute()
               ->fetchAll();
         foreach($result as $val) {
			 $output[$val->area_office_code] = $val->area_office_name;
		 }
		 return $output;	       
  }
  else {
    return array(0 => t("All"));
  }
}

function dealer_mapping_admin_display_form($form, &$form_state, $dealer_detailed_list, $dept, $dealer_mapping_staff_person_name_list = NULL) {
  if (!isset($dealer_mapping_staff_person_name_list)) {
    $array = staff_person_list($dept);
    if(!empty($array)) {
     foreach($array as $key => $val) {
	   $val = (array) $val;
	   $dealer_mapping_staff_person_name_list[$val['emp_code']] = $val['emp_name'];	
	 }	
    }
    else{
		$dealer_mapping_staff_person_name_list['--'] = t('None');
	}	
  }
  global $theme_key;
  drupal_theme_initialize();
  if (!isset($theme)) {
    // If theme is not specifically set, rehash for the current theme.
    $theme = $theme_key;
  }
    $cust_group = (isset($_SESSION['ord_status1']['c_group1']) ? $_SESSION['ord_status1']['c_group1'] : 0);
	$cust_name = (isset($_SESSION['ord_status1']['c_acc_no1']) ? $_SESSION['ord_status1']['c_acc_no1'] : 0);
	$selected = (isset($form_state['values']['portal1']['c_group1']) ? $form_state['values']['portal1']['c_group1'] : (isset($_SESSION['ord_status1']['c_group1']) ? $_SESSION['ord_status1']['c_group1'] : '0'));
    $option_name = _get_second_dropdown_options1($selected); 
    
	 $beginning = array('0' => t('All'));
	 $end = all_zone_list();
	 $zone = array_merge((array)$beginning, (array)$end);    
      
  $form['portal1'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Search by Area Office'), 
	  '#prefix' => '<div class="portal-form">',
      '#suffix' => '</div>',
	  '#weight' => -55, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => FALSE,
  );	
   $form['portal1']['c_group1'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Zone'),
	   '#options' => $zone,
	   '#default_value' => $cust_group,
	   '#ajax' => array(
          'callback' => '_dependent_dropdown_callback1',
          'wrapper' => 'dropdown-second-replace1',
       ),
   );
   
  $form['portal1']['c_acc_no1'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Area Office'),
	   '#default_value' => $cust_name,
	   '#prefix' => '<div id="dropdown-second-replace1">',
       '#suffix' => '</div>',
       '#options' => $option_name,
   );
   $form['portal1']['search'] = array(
	  '#type' =>'submit', 
	  '#submit' => array('_dealer_submit_mapping'),
	  '#value' => t('Search'), 
	  '#weight' => 19,
	);
  
  
  $weight_delta = round(count($dealer_detailed_list) / 2);
  // Build the form tree.
  $form['edited_theme'] = array(
    '#type' => 'value',
    '#value' => $theme,
  );
  $form['staff_dept'] = array(
    '#type' => 'value',
    '#value' => $dept,
  );
  $form['dealer_mapping_staff_person'] = array(
    '#type' => 'value',
    // Add a last region for disabled dealer_mappings.
    '#value' => $dealer_mapping_staff_person_name_list + array(dealer_mapping_dealer_none_to_attach => dealer_mapping_dealer_none_to_attach),
  );
  $form['dealer_mappings'] = array();
  $form['#tree'] = TRUE;
 
 //print "<pre>"; print_r($dealer_detailed_list); exit;
 
  foreach ($dealer_detailed_list as $i => $dealer_mapping) {
	$dealer_mapping = (array) $dealer_mapping; // print "<pre>"; print_r($dealer_mapping);exit;
    $key = $dealer_mapping['dealer_code'] . '_' . $dealer_mapping['area_office_code'];
    $form['dealer_mappings'][$key]['dealer_code'] = array(
      '#type' => 'value',
      '#value' => $dealer_mapping['dealer_code'],
    );
    $form['dealer_mappings'][$key]['area_office_code'] = array(
      '#type' => 'value',
      '#value' => $dealer_mapping['area_office_code'],
    );
    $form['dealer_mappings'][$key]['dealer_name'] = array(
      '#markup' => check_plain($dealer_mapping['dealer_Name']),
    );
    $form['dealer_mappings'][$key]['dealer_location'] = array(
      '#markup' => check_plain($dealer_mapping['dealer_location']),
    );
    $form['dealer_mappings'][$key]['theme'] = array(
      '#type' => 'hidden',
      '#value' => $theme,
    );
    $form['dealer_mappings'][$key]['dealer_weight'] = array(
      '#type' => 'value',
      '#default_value' => $dealer_mapping['dealer_weight'],
      '#delta' => $weight_delta,
      '#title_display' => 'invisible',
      '#title' => t('Weight for @dealer_mapping dealer_mapping', array('@dealer_mapping' => $dealer_mapping['dealer_code'])),
    );
    $form['dealer_mappings'][$key]['dealer_mapped_to_staff_person_code'] = array(
      '#type' => 'select',
      '#default_value' => $dealer_mapping['dealer_mapped_to_staff_person_code'] != dealer_mapping_dealer_none_to_attach ? $dealer_mapping['dealer_mapped_to_staff_person_code'] : NULL,
      '#empty_value' => dealer_mapping_dealer_none_to_attach,
      '#title_display' => 'invisible',
      '#title' => t('Region for @dealer_mapping dealer_mapping', array('@dealer_mapping' => $dealer_mapping['dealer_code'])),
      '#options' => $dealer_mapping_staff_person_name_list,
    );
  }
  $form['actions'] = array(
    '#tree' => TRUE,
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Dealer Mappings'),
  );
 return $form;
}

function _dealer_submit_mapping($form, &$form_state) {
	//print "<pre>"; print_r($form_state['values']); exit;
	foreach($form_state['values']['portal1'] as $key => $value) {
		$_SESSION['ord_status1'][$key] = $value;
	}	
}	

function dealer_mapping_admin_display_form_submit($form, &$form_state) {
	$update = $form_state['values']['dealer_mappings'];
	foreach($update as $key => $value){	
					
		 $updated = db_update('hero_dealer_mapping_status') 
		  ->fields(array(
			'dealer_mapped_to_staff_person_code' => $value['dealer_mapped_to_staff_person_code'],
			'is_dealer_mapped' => '1',
		  ))
		   ->condition('dealer_code', $value['dealer_code'], '=')
		   ->condition('dealer_mapped_to_staff_person_department_code', $form_state['values']['staff_dept'], '=')
		  ->execute();	
		 
		}	
	
  drupal_set_message(t('Record has been successfully Updated.'));
}	


function theme_dealer_mapping_admin_display_form($variables) {
 $form = $variables['form'];
 //print "<pre>"; print_r($form); exit;
  $rows = array();
  foreach (element_children($form['dealer_mappings']) as $id) {
    $form['dealer_mappings'][$id]['weight']['#attributes']['class'] = array('dealer-mappings-weight');
    $rows[] = array(
      'data' => array(
        // Add our 'name' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_name']),
        // Add our 'description' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_location']),
        // Add our 'staff person' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_mapped_to_staff_person_code']),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array(t('Dealer Name'), t('Location'),  t('Staff Person'));
  $table_id = 'dealer-mappings-table';
  //$output = drupal_render_children($form);
  // We can render our tabledrag table for output.
  $output = drupal_render($form['portal1']);
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));
  $output .= theme('pager'); 
  $output .= drupal_render($form['actions']);
  $output .= drupal_render_children($form);
  return $output;
}  


function get_dealer_list($dept) {
  if(isset($_SESSION['ord_status1']['c_acc_no1']) && $_SESSION['ord_status1']['c_group1'] != '0') {
	  if($_SESSION['ord_status1']['c_acc_no1'] == '0') {
		  $result = db_select('hero_dealer_mapping_status', 'hc')
			->fields('hc')
			->condition('hc.dealer_mapped_to_staff_person_department_code',$dept,'=')
			->extend('PagerDefault')
			->limit(5)
			->execute()
			->fetchAll();		  
	  }
	  else {
		  $result = db_select('hero_dealer_mapping_status', 'hc')
			->fields('hc')
			->condition('hc.dealer_mapped_to_staff_person_department_code',$dept,'=')
			->condition('hc.area_office_code',$_SESSION['ord_status1']['c_acc_no1'],'=')
			->extend('PagerDefault')
			->limit(5)
			->execute()
			->fetchAll();		  
	  }	
	 return $result;    	    
  }	
  else {  
		$result = db_select('hero_dealer_mapping_status', 'hc')
			->fields('hc')
			->condition('hc.dealer_mapped_to_staff_person_department_code',$dept,'=')
			->extend('PagerDefault')
			->limit(5)
			->execute()
			->fetchAll();
   return $result;
  } 
}	

function staff_person_list($dept) {
  if(isset($_SESSION['ord_status1']['c_acc_no1']) && $_SESSION['ord_status1']['c_group1'] != '0') {
	  if($_SESSION['ord_status1']['c_acc_no1'] == '0') {
			  $result = db_select('hero_hmcl_staff', 'hhs')
				->fields('hhs',array('emp_name','emp_code'))
				->condition('hhs.emp_designation_type','tsm','=') 
				->condition('hhs.emp_department',$dept,'=') 
				->execute()
				->fetchAll(); 		  
	  }
	  else {
			  $result = db_select('hero_hmcl_staff', 'hhs')
				->fields('hhs',array('emp_name','emp_code'))
				->condition('hhs.emp_designation_type','tsm','=') 
				->condition('hhs.emp_department',$dept,'=') 
				->condition('hhs.emp_area_office_code',$_SESSION['ord_status1']['c_acc_no1'],'=') 
				->execute()
				->fetchAll(); 	  
	  }	
	 return $result;    	    
  }	
  else {  
  $result = db_select('hero_hmcl_staff', 'hhs')
    ->fields('hhs',array('emp_name','emp_code'))
    ->condition('hhs.emp_designation_type','tsm','=') 
    ->condition('hhs.emp_department',$dept,'=') 
    ->execute()
    ->fetchAll();    
   return $result;
  }  
}	
