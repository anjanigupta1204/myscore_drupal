 <?php //prd($rows); ?>
<table>
   <tbody>
      <tr>
         <th colspan="2">
            <center>Business KPI's</center>
         </th>
         <th colspan="4">
            <center>YTD</center>
         </th>
         <th rowspan="2">
            <center>Action Plan/Remark(TM)</center>
         </th>
         <th rowspan="2">
            <center>Action Plan/Remark(AM)</center>
         </th>
         <th rowspan="2">
            <center>Action Plan/Remark(ZM)</center>
         </th>
         <th rowspan="2">
            <center>Target Date</center>
         </th>
      </tr>
      <tr>
         <th>Parameter</th>
         <th>Sub-Parameter</th>
         <th>
            <center>Target</center>
         </th>
         <th>
            <center>Actual</center>
         </th>
         <th>
            <center>% Ach</center>
         </th>
         <th>
            <center>% Growth from LY</center>
         </th>
      </tr>
      
	<?php foreach($rows as $res): ?>
	  <tr>
         <td rowspan=""> 
			<?php echo $res['question']; ?>
         </td>
         <td rowspan=""> 
			<?php echo $res['sub_question']; ?>
         </td>
         <td>
            <?php echo $res['target']; ?>
         </td>
         <td>
            <?php echo $res['actual']; ?>
         </td>
         <td>
            <?php echo $res['present_actual']; ?>
         </td>
         <td>
            <?php echo $res['present_growth']; ?>
         </td>
         <td>
            <?php echo $res['remark_tm']; ?>
         </td>
         <td>
            <?php echo $res['remark_asm']; ?>
         </td>
         <td>
			<?php echo $res['remark_zm']; ?>
         </td>
         <td>
			<?php echo $res['remark_date']; ?>
         </td> 
      </tr>
	<?php endforeach; ?>
 
  </tbody>
</table>