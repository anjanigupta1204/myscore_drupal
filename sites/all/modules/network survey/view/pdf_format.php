
<?php 
// prd($submited_survey); 		
$results = submited_survey_data();
if($results){
	foreach($results as $row){
		$response_val[$row['question_id']] = $row['response'];
		$remark_tm_val[$row['question_id']] = $row['action_plan_tm'];
		$remark_am_val[$row['question_id']] = $row['action_plan_am'];
		$remark_zm_val[$row['question_id']] = $row['action_plan_zm'];
		$remark_date_val[$row['question_id']] = $row['target_date'];  
		  
		$required_val[$row['question_id']] = $row['required'];  
		$available_val[$row['question_id']] = $row['available'];  
		$training_val[$row['question_id']] = $row['training'];   
		 
		$target_val[$row['question_id']] = $row['target'];  
		$actual_val[$row['question_id']] = $row['actual'];  
		$present_actual_val[$row['question_id']] = $row['pr_actual'];  
		$present_growth_val[$row['question_id']] = $row['pr_growth']; 
	}    
}
?>
<div>
   <h2>Score: <?php echo submited_survey_markes_prsentage($submited_survey['id']); ?> %</h2>
   	   <table style="border-collapse: collapse;" cellpadding="1" border="1">
		  <tbody>  
			 <tr style="background-color:#000;color:#fff"> 
				<td align="center">Name</td>
				<td align="center">Code</td>
				<td align="center">Department</td>
				<td align="center">Created By</td>
				<td align="center">Visit Date</td> 
				<td align="center">Evalution Month</td>
			 </tr> 
			 <tr>  
				<?php
					$path = menu_get_item(); 
					$survey_by_staff = $path['map'][6];
					$survey_month = $path['map'][5];
					$dealer_code = (int)$path['map'][4];
					$department =  ($path['map'][8]==3)?"Service":"Sales";

						/* getting dealer information */
					$dealer_query = db_select('hero_dealer', 'h')
					->fields('h', array('dealer_code','dealer_name','parent_dealer_code','dealer_email_id'))	  
					->condition('h.dealer_code',$dealer_code)		  
					->execute(); 
					 
					
					$dealer = $dealer_query->fetchAssoc();   
				?>
				<td align="center"><?php echo ucfirst($dealer['dealer_name']); ?></td>
				<td align="center"><?php echo $dealer_code; ?></td>
				<td align="center"><?php echo $department; ?></td> 
				<td align="center"><?php global $user;echo ucfirst($user->name);?></td> 
				<td align="center"><?php echo isset($submited_survey['start_date'])?$submited_survey['start_date']:null; ?></td> 
				<td align="center"><?php echo $survey_month; ?></td> 
			 </tr> 
		</tbody>  
		</table> <br/><br/><br/><br/>
   
   
   
   
   
   
   <?php foreach($service_question as $key=>$cat_val): ?> 
	<?php 
		$cat = explode('@@',$key);
		$cat_name= $cat[0];
		$cat_id = $cat[1];	
	?>
	<h2><?php echo ucfirst($cat_name); ?></h2>
		<?php  $first =  true; foreach($cat_val as $sub_key=>$sub_cat_val): ?>  
	   <table style="border-collapse: collapse;" cellpadding="1" border="1">
		  <tbody> 
			<?php
				switch($cat_id) {
					case 64: //Manpower
						$colspan = 8;
						break; 
					case 65: //KPI 
						$colspan = 10;
						break; 
					default:  
						$colspan = 6;
				}
			
			?>
			<?php if(!$first):    ?>
			 <tr>
				<td colspan="<?php echo $colspan; ?>"> &nbsp;</td>
			 </tr>
			 <?php endif; $first = false; ?>
			 
			 <tr>
				<td colspan="<?php echo $colspan; ?>">
					<?php 
						$sub_cat = explode('@@',$sub_key);
						$sub_cat_name = $sub_cat[0];
						$sub_cat_id = $sub_cat[1];
					?>
				   <span><b><?php echo ucfirst($sub_cat_name); ?></b></span>
				</td>
			 </tr>
 
			<?php switch ($cat_id) { case 64:  //manpower?>
			 <tr style="background-color:#000;color:#fff"> 
				<td align="center">Parameter</td>
				<td align="center">Required</td>
				<td align="center">Available</td>
				<td align="center">Training</td>
				<td align="center">Action Plan/Remark(TM)</td>
				<td align="center">Action Plan/Remark(ASM)</td>
				<td align="center">Action Plan/Remark(ZM)</td>
				<td align="center">Target Date</td>
			 </tr>
			<?php break; case 65: //KPI ?>
			 <tr style="background-color:#000;color:#fff"> 
				<td align="center">Parameter</td>
				<td align="center">Sub-<br>Parameter</td>
				<td align="center">Target</td>
				<td align="center">Actual</td>
				<td align="center">% Ach</td>
				<td align="center">% Growth <br>from LY</td>
				<td align="center">Action Plan<br>Remark(TM)</td>
				<td align="center">Action Plan<br>Remark(ASM)</td>
				<td align="center">Action Plan<br>Remark(ZM)</td>
				<td align="center">Target<br> Date</td>
			 </tr>
			<?php break; default: ?>
			 <tr style="background-color:#000;color:#fff"> 
				<td align="center">Parameter</td>
				<td align="center">Response</td>
				<td align="center">Action Plan/Remark(TM)</td>
				<td align="center">Action Plan/Remark(ASM)</td>
				<td align="center">Action Plan/Remark(ZM)</td>
				<td align="center">Target Date</td>
			 </tr>
			<?php }//end switch ?>
			 <?php foreach($sub_cat_val as $res): ?> 
			<?php switch ($cat_id) { case 64:  //manpower?>
			 <tr>  
				<td align="center"><?php echo $res['question']; ?></td> 
				<td align="center"><?php echo isset($required_val[$res["id"]])?$required_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($available_val[$res["id"]])?$available_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($training_val[$res["id"]])?$training_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_tm_val[$res["id"]])?$remark_tm_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_am_val[$res["id"]])?$remark_am_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_zm_val[$res["id"]])?$remark_zm_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_date_val[$res["id"]])?$remark_date_val[$res["id"]]:''; ?></td>
			 </tr>
			<?php break; case 65: //KPI ?>
			 <tr>  
				<td align="center"><?php echo $res['question']; ?></td>
				<td align="center"><?php echo $res['sub_question']; ?></td>
				<td align="center"><?php echo isset($target_val[$res["id"]])?$target_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($actual_val[$res["id"]])?$actual_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($present_actual_val[$res["id"]])?$present_actual_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($present_growth_val[$res["id"]])?$present_growth_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_tm_val[$res["id"]])?$remark_tm_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_am_val[$res["id"]])?$remark_am_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_zm_val[$res["id"]])?$remark_zm_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_date_val[$res["id"]])?$remark_date_val[$res["id"]]:''; ?></td>
			 </tr>
			<?php break; default: 			
				$response = isset($response_val[$res["id"]])?$response_val[$res["id"]]:false;
				if($res['is_boolean']==1){
					$response =  ($response == 1)?"Yes":"No";
				}
			
			?>
			 <tr> 
				<td align="center"><?php echo $res['question']; ?></td>
				<td align="center"><?php echo $response; ?></td>
				<td align="center"><?php echo isset($remark_tm_val[$res["id"]])?$remark_tm_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_am_val[$res["id"]])?$remark_am_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_zm_val[$res["id"]])?$remark_zm_val[$res["id"]]:''; ?></td>
				<td align="center"><?php echo isset($remark_date_val[$res["id"]])?$remark_date_val[$res["id"]]:''; ?></td>
			 </tr> 
			<?php }//end switch ?> 
			 <?php endforeach; ?>
		  </tbody>
	   </table>
   <?php endforeach; ?>
   <br/><br/><br/><br/>
   <?php endforeach; ?>
   
   
   
   <table style="border-collapse: collapse;" cellpadding="1" border="1">
      <tbody>         
		<tr>
            <td colspan="5">
               <h2 style="margin-top: 20px; margin-bottom: 0px;">TSM Remark</h2>
            </td>
         </tr>
         <tr>
            <td colspan="5"><?php echo isset($submited_survey['tm_feedback'])?$submited_survey['tm_feedback']:null; ?></td>
         </tr>
         <tr>
            <td colspan="5">
               <h2 style="margin-top: 20px; margin-bottom: 0px;">ASM Remark</h2>
            </td>
         </tr>
         <tr>
            <td colspan="5"><?php echo isset($submited_survey['am_feedback'])?$submited_survey['am_feedback']:null; ?></td>
         </tr>

         <tr>
            <td colspan="5">
               <h2 style="margin-top: 20px; margin-bottom: 0px;">ZM Remark</h2>
            </td>
         </tr>
         <tr>
            <td colspan="5"><?php echo isset($submited_survey['zm_feedback'])?$submited_survey['zm_feedback']:null; ?></td>
         </tr>
      </tbody>
   </table>
</div>

