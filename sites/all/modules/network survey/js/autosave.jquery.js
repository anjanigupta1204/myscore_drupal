var sk = jQuery.noConflict();

sk(window).load(function() {
	
	var currentRequest = null;
	var ard_service = "#network-survey-ard-service-display-form";
	var asc_service = "#network-survey-asc-service-display-form";
	var ard_sales 	= "#network-survey-ard-sales-display-form";
	var asc_sales 	= "#network-survey-asc-sales-display-form";
	function isTm(){
		if(sk('#login_as').length){
			return true;
		}
	}
	/* *****************Start ARD Sales autosave ****************************** */
	sk(ard_sales+' fieldset a.fieldset-title').click(function(){
		 if(!isTm()){ return; }
		 currentRequest = sk.ajax({
		  url: Drupal.settings.network_survey.hostname+"/network-survey/network-survey-sales-autosave",
		  beforeSend: function(  ) {   if(currentRequest != null) { currentRequest.abort(); }
			sk(ard_sales).find('[type="submit"]').attr('disabled','disabled');
		},
		  method: "POST",		 
		  data: sk(ard_sales).serializeArray(),
		  dataType: "html",
		  
		}).done(function( data ) {
			sk(ard_sales).find('[type="submit"]').removeAttr('disabled');
		  });
	});
	/* *****************end Sales autosave ****************************** */
	
	/* *****************Start ASC Sales autosave ****************************** */
	sk(asc_sales+' fieldset a.fieldset-title').click(function(){
		if(!isTm()){ return; }
		currentRequest = sk.ajax({
		  url: Drupal.settings.network_survey.hostname+"/network-survey/network-survey-sales-autosave",
		  beforeSend: function(  ) {  
			if(currentRequest != null) { currentRequest.abort(); }
			sk(asc_sales).find('[type="submit"]').attr('disabled','disabled');
		  },
		  method: "POST",		 
		  data: sk(asc_sales).serializeArray(),
		  dataType: "html",
		  
		}).done(function( data ) {		
			sk(asc_sales).find('[type="submit"]').removeAttr('disabled');
		  });
	});
	/* *****************end Sales autosave ****************************** */
	
	
	/* *****************Start ARD SERVICE autosave ****************************** */
	sk(ard_service+' fieldset a.fieldset-title').click(function(){
		if(!isTm()){ return; }
		currentRequest = sk.ajax({
		  url: Drupal.settings.network_survey.hostname+"/network-survey/network-survey-service-autosave",
		  beforeSend: function(  ) {   if(currentRequest != null) { currentRequest.abort(); }
		  sk(ard_service).find('[type="submit"]').attr('disabled','disabled');
		  },
		  method: "POST",		 
		  data: sk(ard_service).serializeArray(),
		  dataType: "html",
		  
		}).done(function( data ) {
			sk(ard_service).find('[type="submit"]').removeAttr('disabled');
		  });
	});
	/* *****************end SERVICE autosave ****************************** */
	
	/* *****************Start ASC SERVICE autosave ****************************** */
	sk(asc_service+' fieldset a.fieldset-title').click(function(){
		if(!isTm()){ return; }
		currentRequest =	sk.ajax({
			  url: Drupal.settings.network_survey.hostname+"/network-survey/network-survey-service-autosave",
			  beforeSend: function(  ) {  if(currentRequest != null) { currentRequest.abort(); }
				sk(asc_service).find('[type="submit"]').attr('disabled','disabled');
			},
			  method: "POST",		 
			  data: sk(asc_service).serializeArray(),
			  dataType: "html",
			  
			}).done(function( data ) { sk(asc_service).find('[type="submit"]').removeAttr('disabled'); });
	});
	/* *****************end SERVICE autosave ****************************** */
	
	
	
	
});
