/* network-evaluation-ARD-service-display-form validation  */
var sk = jQuery.noConflict();
var s_asc_form = 'network-survey-asc-service-display-form';
var s_ard_form = 'network-survey-ard-service-display-form';

var error_in_criteria = [];

sk(document).ready(function(){
   function isTm(){
		if(sk('#login_as').length){
			return true;
		}
	}
	sk('[data-setdate=""],[data-setdate="0"]').datepicker({ maxDate: 0,
		beforeShow: function(input, inst){
		inst.dpDiv.css({marginLeft: -input.offsetWidth + 'px'});
	},
		dateFormat: 'dd-mm-yy',
	}); 
	sk('[name^=remark_date]').datepicker({ minDate: 0,
		beforeShow: function(input, inst){
		inst.dpDiv.css({marginLeft: -input.offsetWidth + 'px'});
	},
		dateFormat: 'dd-mm-yy',
	}); 
	
	/* disable fields */

	
	
	
/* form validation */ 
sk(document).on('click','#'+s_asc_form+' [name^=question\\[response\\]]:radio',function(e){  
	
	if(!isTm()){ return true; }
	
	e.stopPropagation();

	var _val = sk(this).val();
	var id = sk(this).attr('id');

	var remark_tm = sk(this).closest("tr").find('[name^=remark_tm]');
	var date = sk(this).closest("tr").find('[name^=remark_date]');
	
	/* if no */
	if(_val == 2){ 
			$remark_tm_val = sk(this).closest("tr").find('[name^=remark_tm]').val(); 
			if($remark_tm_val == ''){ 
				sk(this).closest("tr").find('[name^=remark_tm]').addClass('ard-validation-error');	
			}
			else{
				sk(this).closest("tr").find('[name^=remark_tm]').removeClass('ard-validation-error');	
			}	

			/* find all date fields */

			$remark_tm_val = sk(this).closest("tr").find('[name^=remark_date]').val(); 
			if($remark_tm_val == ''){ 
				sk(this).closest("tr").find('[name^=remark_date]').addClass('ard-validation-error');	
			}
			else{
				sk(this).closest("tr").find('[name^=remark_date]').removeClass('ard-validation-error');	
			}
		
		
			/* if Yes */		
	}else if(_val == 1){	 
		sk(this).closest("tr").find('[name^=remark_tm]').removeClass('ard-validation-error');		
		sk(this).closest("tr").find('[name^=remark_date]').removeClass('ard-validation-error');	
	}	
	

});

/* ASC validateion */

/* START:: Manpower validation */ 
sk(document).on('keyup change','#'+s_asc_form+' [name^=required\\[]:text',function(e){  
	var _this = sk(this);
	if(!isTm()){ return true; }
	console.log('log.......');	
	var _val = _this.val();
	field_validation(_this);	
	
});
 sk(document).on('keyup change','#'+s_asc_form+' [name^=available\\[]:text',function(e){  
	var _this = sk(this);
	if(!isTm()){ return true; }
	console.log('log.......');	
	var _val =_this.val();	
	field_validation(_this);
	
	if(!_this.hasClass('survey-validation-error')){
		var training = sk(this).closest("tr").find('[name^=training\\[]');
		var training_val = training.val();	
		if(sk.isNumeric( _val) && sk.isNumeric(training_val)){
			if(training_val > _val){
				training.addClass('survey-validation-error');
			}else{
				training.removeClass('survey-validation-error');
			}
		}
	}	
});
sk(document).on('keyup change','#'+s_asc_form+' [name^=training\\[]:text',function(e){  
	var _this = sk(this);
	if(!isTm()){ return true; }
	console.log('log.......');
	var _val =_this.val();	
	field_validation(_this);
	if(!_this.hasClass('survey-validation-error')){
		var available = sk(this).closest("tr").find('[name^=available\\[]').val();	
		if(sk.isNumeric( _val) && sk.isNumeric(available)){
			if(_val > available){
				_this.addClass('survey-validation-error');
			}else{
				_this.removeClass('survey-validation-error');
			}
		}
	}
	
}); 

/* END:: Manpower validation */ 

/* detect submit button */
sk( '#'+s_asc_form +' #save_draft, #'+s_asc_form +' #save').click(function(){
	if(sk(this).attr('id') == 'save'){
		sk("#save_as").val('1');	
	} else if(sk(this).attr('id') == 'save_draft'){
		sk("#save_as").val('0');	
	}

});
	
/* Submit sales survey form */
sk("#"+s_asc_form ).submit(function( event ) {
	
	if(!isTm()){ return true; }
	
	event.preventDefault();	
    var submit_type = sk('#save_as').val();
	
	if(submit_type == '0'){	 
		sk("#"+s_asc_form).unbind('submit').submit();
		return true;
	}

	 sk('#'+s_asc_form+' fieldset tr').each(function(){
		var current_tr = sk(this);
		var have_radio = current_tr.find('[type="radio"]').length;

		if(have_radio){
			var select_checked = current_tr.find('[type="radio"]:checked');
			if(select_checked.length){
				current_tr.removeClass('survey-validation-error');
				var _val = select_checked.val();				
				/* find all radio buttons */
				var remark = current_tr.find('textarea[name^=remark_tm]');
				/* find all date fields */
				var dt = current_tr.find('[name^=remark_date]');			
				if(_val == 2){
					if(remark.val() == '')
					remark.addClass('survey-validation-error');	
					else
					remark.removeClass('survey-validation-error');				
					/* find all date fields */
					if(dt.val() == '')
					dt.addClass('survey-validation-error');	
					else
					dt.removeClass('survey-validation-error');					

				}else if(_val == 1){
					remark.removeClass('survey-validation-error');
					dt.removeClass('survey-validation-error');
				}
			}else{
				current_tr.addClass('survey-validation-error');
			}		
		}
		/* manpower  validation */
		current_tr.find('[name^=required\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		current_tr.find('[name^=available\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		current_tr.find('[name^=training\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		/* kpi validation */
		current_tr.find('[name^=target\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		current_tr.find('[name^=actual\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		

		
	  });
		/* add error class in respose field */
		 sk('#'+s_asc_form+' [name^=question\\[response\\]][type=text]').each(function(){
			var response = sk(this).val();
			if(response == ''){
				sk(this).addClass('survey-validation-error');	
			}else{
				sk(this).removeClass('survey-validation-error');					
			}
		});
		
		

	/* show error messages */
	
	var errors = sk( "#"+s_asc_form+" .survey-validation-error" );
	
	error_in_criteria = [];
	var criteriaid = criteria = '';
	errors.closest('fieldset.collapsible').each(function(){
		 criteriaid = sk(this).data('criteriaid');
		 criteria = sk(this).data('criteria');
		if(criteria !== 'undefined' && criteriaid !== 'undefined' )
		error_in_criteria[criteriaid] = criteria;
	});
	
	console.log(error_in_criteria);
	
	error_in_criteria = error_in_criteria.filter(function( element ) {
	   return !!element;
	});
	
	if(!errors.length){		
		sk("#"+s_asc_form).unbind('submit').submit();
	}else{
	sk.colorbox({html:"There are <span class='error-count'>"+errors.length+"</span> error(s) found!! <br> Please correct errors.<br> Errors are in the following criteria:<ul style=' color: red; list-style: inside disc;margin-left:-30px;margin-top: 0;'><li>"+error_in_criteria.join('<li>','</li>')+"</li></ul>"});
	}
	
	
});

/* ARD validateion */

/* SATRT:: kpi */
	/* KPI */
sk(document).on('keyup change','#'+s_ard_form+' [name^=actual\\[], #'+s_ard_form+' [name^=target\\[]',function(){
	if(!isTm()){ return true; }
	var _this = sk(this);
	field_validation(_this);
	if(!_this.hasClass('survey-validation-error')){
		current_tr =  _this.closest('tr');
		var target =  parseInt(current_tr.find('[name^=target\\[]').val());
		var actual =  parseInt(current_tr.find('[name^=actual\\[]').val());
		var actual_par =  current_tr.find('[name^=present_actual\\[]');

		if((target != '' && sk.isNumeric(target)) && (actual != '' && sk.isNumeric(actual) )){
			actual_par.val(parseFloat(((actual*100)/target).toFixed(2)));
		}else{
			actual_par.val(0);
		}
	}
});
/* KPI */
sk(document).on('keyup change','#'+s_asc_form+' [name^=actual\\[], #'+s_asc_form+' [name^=target\\[]',function(){
	  if(!isTm()){ return true; }
	  var _this = sk(this);
	  field_validation(_this);
	  if(!_this.hasClass('survey-validation-error')){
		  current_tr =  _this.closest('tr');
		  var target =  parseInt(current_tr.find('[name^=target\\[]').val());
		  var actual =  parseInt(current_tr.find('[name^=actual\\[]').val());
		  var actual_par =  current_tr.find('[name^=present_actual\\[]');
			if((target != '' && sk.isNumeric(target)) && (actual != '' && sk.isNumeric(actual) )){
				actual_par.val(parseFloat(((actual*100)/target).toFixed(2)));
			}else{
				actual_par.val(0);
			}
	  }
});

/* END:: kpi */

/* Manpower validation */ 
sk(document).on('keyup change','#'+s_ard_form+' [name^=required\\[]:text',function(e){  
	
	if(!isTm()){ return true; }
	var _this = sk(this);
	console.log('log.......');	
	var _val = _this.val();
	field_validation(_this);	
	
});
sk(document).on('keyup change','#'+s_ard_form+' [name^=available\\[]:text',function(e){  

	if(!isTm()){ return true; }
	var _this = sk(this);
	console.log('log.......');	
	var _val =_this.val();	
	field_validation(_this);
	if(!_this.hasClass('survey-validation-error')){
		var training = _this.closest("tr").find('[name^=training\\[]');
		var training_val = training.val();	
		if(sk.isNumeric( _val) && sk.isNumeric(training_val)){
			if(training_val > _val){
				training.addClass('survey-validation-error');
			}else{
				training.removeClass('survey-validation-error');
			}
		}
	}	
});
sk(document).on('keyup change','#'+s_ard_form+' [name^=training\\[]:text',function(e){  

	if(!isTm()){ return true; }
	
	var _this = sk(this);
	console.log('log.......');
	
	var _val =_this.val();	
	field_validation(_this);
	if(!_this.hasClass('survey-validation-error')){
		var available = sk(this).closest("tr").find('[name^=available\\[]').val();	
		if(sk.isNumeric( _val) && sk.isNumeric(available)){
			if(_val > available){
				_this.addClass('survey-validation-error');
			}else{
				_this.removeClass('survey-validation-error');
			}
		}
	}
	
});

/* form validation */ 
sk(document).on('click','#'+s_ard_form+' [name^=question\\[response\\]]:radio',function(e){  
	
	if(!isTm()){ return true; }
	
	e.stopPropagation();

	var _val = sk(this).val();
	var id = sk(this).attr('id');

	var remark_tm = sk(this).closest("tr").find('[name^=remark_tm]');
	var date = sk(this).closest("tr").find('[name^=remark_date]');
	
	/* if no */
	if(_val == 2){ 
			$remark_tm_val = sk(this).closest("tr").find('[name^=remark_tm]').val(); 
			if($remark_tm_val == ''){ 
				sk(this).closest("tr").find('[name^=remark_tm]').addClass('ard-validation-error');	
			}
			else{
				sk(this).closest("tr").find('[name^=remark_tm]').removeClass('ard-validation-error');	
			}	

			/* find all date fields */

			$remark_tm_val = sk(this).closest("tr").find('[name^=remark_date]').val(); 
			if($remark_tm_val == ''){ 
				sk(this).closest("tr").find('[name^=remark_date]').addClass('ard-validation-error');	
			}
			else{
				sk(this).closest("tr").find('[name^=remark_date]').removeClass('ard-validation-error');	
			}
		
		
			/* if Yes */		
	}else if(_val == 1){	 
		sk(this).closest("tr").find('[name^=remark_tm]').removeClass('ard-validation-error');		
		sk(this).closest("tr").find('[name^=remark_date]').removeClass('ard-validation-error');	
	}	
	

});

/* detect submit button */
sk( '#'+s_ard_form +' #save_draft, #'+s_ard_form +' #save').click(function(){
	if(sk(this).attr('id') == 'save'){
		sk("#save_as").val('1');	
	} else if(sk(this).attr('id') == 'save_draft'){
		sk("#save_as").val('0');	
	}

});
	
/* Submit sales survey form */
sk("#"+s_ard_form ).submit(function( event ) {
	
	if(!isTm()){ return true; }
	
	
	event.preventDefault();	
    var submit_type = sk('#save_as').val();
	
	if(submit_type == '0'){	 
		sk("#"+s_ard_form).unbind('submit').submit();
		return true;
	}

	 sk('#'+s_ard_form+' fieldset tr').each(function(){
		var current_tr = sk(this);
		var have_radio = current_tr.find('[type="radio"]').length;

		if(have_radio){
			var select_checked = current_tr.find('[type="radio"]:checked');
			if(select_checked.length){
				current_tr.removeClass('survey-validation-error');
				var _val = select_checked.val();				
				/* find all radio buttons */
				var remark = current_tr.find('textarea[name^=remark_tm]');
				/* find all date fields */
				var dt = current_tr.find('[name^=remark_date]');			
				if(_val == 2){
					if(remark.val() == '')
					remark.addClass('survey-validation-error');	
					else
					remark.removeClass('survey-validation-error');				
					/* find all date fields */
					if(dt.val() == '')
					dt.addClass('survey-validation-error');	
					else
					dt.removeClass('survey-validation-error');					

				}else if(_val == 1){
					remark.removeClass('survey-validation-error');
					dt.removeClass('survey-validation-error');
				}
			}else{
				current_tr.addClass('survey-validation-error');
			}		
		}
		/* manpower  validation */
		current_tr.find('[name^=required\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		current_tr.find('[name^=training\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		
		current_tr.find('[name^=available\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		
		/* kpi validation */
		current_tr.find('[name^=target\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		current_tr.find('[name^=actual\\[][type=text]').each(function(){
			if(sk(this).val() == ''){
				sk(this).addClass('survey-validation-error');
			}else{
				sk(this).removeClass('survey-validation-error');
			}
		});
		
		
	  });
		/* add error class in respose field */
		 sk('#'+s_ard_form+' [name^=question\\[response\\]][type=text]').each(function(){
			var response = sk(this).val();
			if(response == ''){
				sk(this).addClass('survey-validation-error');	
			}else{
				sk(this).removeClass('survey-validation-error');					
			}
		});
		
		

	/* show error messages */
	
	var errors = sk( "#"+s_ard_form+" .survey-validation-error" );
	
	error_in_criteria = [];
	var criteriaid = criteria = '';
	errors.closest('fieldset.collapsible').each(function(){
		 criteriaid = sk(this).data('criteriaid');
		 criteria = sk(this).data('criteria');
		if(criteria !== 'undefined' && criteriaid !== 'undefined' )
		error_in_criteria[criteriaid] = criteria;
	});
	
	console.log(error_in_criteria);
	
	error_in_criteria = error_in_criteria.filter(function( element ) {
	   return !!element;
	});
	
	if(!errors.length){		
		sk("#"+s_ard_form).unbind('submit').submit();
	}else{
	sk.colorbox({html:"There are <span class='error-count'>"+errors.length+"</span> error(s) found!! <br> Please correct errors.<br> Errors are in the following criteria:<ul style=' color: red; list-style: inside disc;margin-left:-30px;margin-top: 0;'><li>"+error_in_criteria.join('<li>','</li>')+"</li></ul>"});
	}
});


/* ARD validateion */
function field_validation(_this){
			  
		var vgex = sk(_this).data('vgex');
		var regexp = /^[0-9]+$/; 
		var replaceregx = /[^0-9]/g;

		if(vgex == 'alphnumspace'){
		regexp = /^[A-Za-z0-9 ]+$/;
		replaceregx = /[^A-Za-z0-9 ]/g;
		}else if(vgex == 'alphspace'){
		regexp = /^[A-Za-z ]+$/;
		replaceregx = /[^A-Za-z ]/g;
		}else if(vgex == 'num+-'){
		  regexp = /^[0-9-]+$/;
		  replaceregx = /[^0-9-]/g;
		}

		var thisval = sk(_this).val();

		if(!thisval.match(regexp)){  						
			sk(_this).addClass('survey-validation-error');
			return true;
		}else{
			sk(_this).removeClass('survey-validation-error');
		}
	}


}); // document ready end

sk(document).ready(function(){  
	if(sk("#save").hasClass("form-button-disabled")){
		sk('input').attr('disabled','disabled');
		sk('textarea').attr('disabled','disabled');
	}
}); 