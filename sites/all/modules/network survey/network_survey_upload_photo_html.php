<?php 

global $base_url ;

$image_facia = get_image_facia();

drupal_add_js(array('network_evaluation' => array('hostname' => $base_url,)),'setting');	
$moduel_path 	= $base_url.'/'.drupal_get_path('module', 'network_evaluation');
/* exploading url for get evaluation infomation */
$str 			= explode("/",$_GET['str']);
/* Array
(
    [0] => network-evaluation-asc
    [1] => sales
    [2] => start
    [3] => 29000
    [4] => Sep-2016
    [5] => 9479
) */

$dealer_type 	= strtoupper(substr($str[0],-3,3).'-'.$str[1]);
$delear_code 	= $str[3];
$month 			= $str[4];
$evaluation_by 	= $str[5];


/* checking uploaded image already uploaded or not */
$attachment_query = db_select('eval_criteria_attachment', 'a')
->fields('a', array('dealer_type','attachment','image_type','id','created_at'))	  
->condition('a.owner_id',$evaluation_by)		  
->condition('a.asc_ard_code',$delear_code)		  
->condition('a.eva_month',$month)	
->condition('a.dealer_type',$dealer_type)	
->execute();

$rows = $attachment_query->rowCount();
	
?>
<!DOCTYPE html> 
<html>
<head>
	<title>Image Upload</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php  echo $moduel_path ?>/css/style.css" >		
	<script>var host_name = '<?php echo $base_url; ?>';</script>
</head>
<body>
	<div class="container" style="border: 1px solid #ddd;
    margin: 40px auto;
    padding:20px;
    width: 650px;">
		<h1 class="page-title" >Upload photo attachment for  Dealer Code #<?php echo $delear_code; ?>, "<?php echo $month;?>"</h1>
		<div class="form-container">
			<form enctype="multipart/form-data" name='imageform' role="form" id="imageform" method="post" action="">
				<div class="form-group">
					
					<select class="" name="image_type" id="image_type">
					<option value="">Image Type</option>
					<?php
					foreach($image_facia as $k=>$v){
					?>
						<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
					<?php
					}
					?>					
					</select>
					<div class="clearfix"></div>
					<p>
					Choose Image: <input class='file' type="file" class="form-control" name="images" id="images" placeholder="Please choose your image"> Image max size: 300KB</p>
					<span class="help-block"></span>
				</div>
				<div id="loader" style="display: none;">
					Please wait image uploading to server....
				</div>
				<input type="hidden"  name="dealer_type" value="<?php echo $dealer_type;  ?>" />
				<input type="hidden"  name="delear_code" value="<?php echo $delear_code;  ?>" />
				<input type="hidden"  name="month" value="<?php echo $month;  ?>" />
				<input type="hidden"  name="evaluation_by" value="<?php echo $evaluation_by;  ?>" />
				<input type="submit" value="Upload" name="image_upload" id="image_upload" class="btn"/>				
			</form>
		</div>
		<div class="clearfix"></div>
		<div id="uploaded_images" class="uploaded-images">
			<div id="error_div">
			</div>
			<div id="message">
			</div>
			<div id="success_div">
			</div>
			<div class="clearfix"></div>
			
			<?php if($rows){ ?>
			<div id="pre_uploaded_images">
				<p>Previous uploaded images</p>
				<ul>
				<?php
				while($result = $attachment_query->fetchAssoc()){ ?>
						<li id="img_<?php echo $result['id']; ?>">
						
					<?php if(date('dmy',strtotime($result['created_at']) == date('dmy'))){ ?>
						<span class="remove_image" data-imageid="<?php echo $result['id']; ?>">x</span>
					<?php } ?>
					
					<?php if(in_array(end(explode('.', $result['attachment'])),array('pdf','docx'))){ ?>
						<p> <a href="<?php echo $base_url; ?>/sites/default/files/survey_attachments_network/<?php echo end(explode('/',$result['attachment'])); ?>" target="_blank"><?php echo strtoupper(end(explode('.', $result['attachment']))).': '; echo $image_facia[$result['image_type']]; ?></a></p>
					<?php } else{ ?>
					<img style="width:50px" src="<?php echo $base_url; ?>/sites/default/files/survey_attachments_network/thumbnail_<?php echo end(explode('/',$result['attachment'])); ?>">	
					<p><?php echo $image_facia[$result['image_type']]; ?></p>					
					<?php } ?>
					
					</li>				
				<?php }?>
				</ul><div class="clearfix"></div><br>
			</div>
			<?php } ?>
			
			<div class="clearfix"></div><br>
		
		</div>		
	</div>
	<input type="hidden" id='base_path' value="<?php echo $base_url; ?>/sites/default/files/survey_attachments_network/">
	<script src="<?php  echo $moduel_path ?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php  echo $moduel_path ?>/js/jquery.form.js"></script>
    <script src="<?php  echo $moduel_path ?>/js/photo-upload-script.js"></script>
</body>
</html>
