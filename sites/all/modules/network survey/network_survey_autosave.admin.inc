<?php
module_load_include('inc', 'network_survey', 'network_survey_global');

function network_survey_sales_autosave(){
	
	if(isset($_POST['form_id']) && $_POST['form_id'] == 'network_survey_asc_sales_display_form' ){
		network_survey_sales_asc_autosave();
	}else if(isset($_POST['form_id']) && $_POST['form_id'] == 'network_survey_ard_sales_display_form' ){
		network_survey_sales_ard_autosave();
	}
}

function network_survey_service_autosave(){
	if(isset($_POST['form_id']) && $_POST['form_id'] == 'network_survey_ard_service_display_form' ){
		network_survey_service_ard_autosave();
	}else if(isset($_POST['form_id']) && $_POST['form_id'] == 'network_survey_asc_service_display_form' ){
		network_survey_service_asc_autosave();
	}
}

/* ******************************************************************  */
/* ******************************************************************  */

/* ard sales  auto save */
function network_survey_sales_ard_autosave(){

	$user_role_meta = user_role_meta(); 
	$role = $user_role_meta['emp_designation_type'];
	$is_tm = (strtolower($role)=='tsm')?true:false;
	
	/*  for tm submit	 */
	if($is_tm){
		
	// check serve already exist or not 
	$query = db_select('network_survey', 'ns');
	$query->fields('ns', array());
	$query->condition('ns.dealer_code', $_POST['question']['dealer_code']);
	$query->condition('ns.survey_month', $_POST['question']['survey_month']);	
	$query->condition('ns.survey_by_staff',$_POST['question']['survey_by_staff']);	   
	 
	$network_survey_exist =  $query->execute();	  
 
	
		$submit_type = 0;
		 /* network survey data */
		$network_survey = array(
			'dealer_code'=>$_POST['question']['dealer_code'],
			'survey_by_staff'=>$_POST['question']['survey_by_staff'],
			'survey_month'=>$_POST['question']['survey_month'],
			'status'=>$submit_type,
			'start_date'=>$_POST['question']['start_date'], 
			'total_marks'=>array_sum($_POST['max_mark']),
			'obtained_marks'=>array_sum(array_intersect_key($_POST['max_mark'],array_flip(array_keys($_POST['question']['response'])))),
			'last_submited_date' => date('Y-m-d H:i:s')
		);
		
		
		$network_survey['tm_feedback'] = $_POST['tm_feedback']; 

		 
		//if already not exist
		if($network_survey_exist->rowCount()){	//update
				$serve = $network_survey_exist->fetchAssoc();  
				$serve_id = $serve['id'];
				db_update('network_survey')
				->fields($network_survey)
				->condition('id', $serve_id)	   
				->execute();   
		}else{//insert	 
			 $serve_id =  db_insert('network_survey')->fields($network_survey)->execute();  
		} 
		
		// question mark update 
		foreach($_POST['max_mark'] as $key=>$res){
				$response =  $_POST['question']['response'][$key] ;			 
				$questions_score['survey_score_id']= $serve_id;
				$questions_score['question_id']=$key; 
				$questions_score['response']=$response; 
				$questions_score['score']= obtain_markes_sales($key);
				$questions_score['action_plan_tm']=$_POST['remark_tm'][$key];
				$questions_score['target_date']=$_POST['remark_date'][$key];
 
			 
			
			if($network_survey_exist->rowCount()){	//update   
					db_update('network_survey_questions_score')
					->fields($questions_score)
					->condition('survey_score_id', $serve_id)	   
					->condition('question_id', $key)	   
					->execute();   
			}else{//insert	  
				  db_insert('network_survey_questions_score')->fields($questions_score)->execute();  
			} 	
		
		}
		echo 'Data saved!'; exit;
	}
	  
}
/* ******************************************************************  */
/* ******************************************************************  */
/* **** ard service auto save  ***** */

function network_survey_service_ard_autosave(){

	$user_role_meta = user_role_meta(); 
	$role = $user_role_meta['emp_designation_type'];
	$is_tm = (strtolower($role)=='tsm')?true:false;
	if($is_tm){
		
	$query = db_select('network_survey', 'ns');
	$query->fields('ns', array());
	
	$query->condition('ns.dealer_code', $_POST['question']['dealer_code']);
	$query->condition('ns.survey_month', $_POST['question']['survey_month']); 
	$query->condition('ns.survey_by_staff',$_POST['question']['survey_by_staff']);	   
	 
	$network_survey_exist =  $query->execute();	  
 
	$serve = $network_survey_exist->fetchAssoc();  
	$serve_id = $serve['id'];	
		
		/*  for tm submit	 */
		
		$submit_type = 0;
		 /* network survey data */
		 
		$obtained_marks = (isset($_POST['max_mark']) and isset($_POST['question']['response']) )?array_sum(array_intersect_key($_POST['max_mark'],array_flip(array_keys($_POST['question']['response'])))):0;		
		
		$network_survey = array(
			'dealer_code'=>$_POST['question']['dealer_code'],
			'survey_by_staff'=>$_POST['question']['survey_by_staff'],
			'survey_month'=>$_POST['question']['survey_month'],
			'status'=>$submit_type,
			'start_date'=>$_POST['question']['start_date'], 
			'total_marks'=>array_sum($_POST['max_mark']),
			'obtained_marks'=>$obtained_marks,
			'last_submited_date' => date('Y-m-d H:i:s')
		);
		
		
		$network_survey['tm_feedback'] = $_POST['tm_feedback']; 

		 
		//if already not exist
		if($network_survey_exist->rowCount()){	//update
				$serve = $network_survey_exist->fetchAssoc();
				
				$serve_id = $serve['id'];
				db_update('network_survey')
				->fields($network_survey)
				->condition('id', $serve_id)	   
				->execute();   
		}else{//insert	 
			 $serve_id =  db_insert('network_survey')->fields($network_survey)->execute();  
		}		
		
		// question mark update 
		
		foreach($_POST['max_mark'] as $key=>$res){
				$question_id = $key; 
				
				$questions_score['survey_score_id']= $serve_id;
				$questions_score['question_id']=$key; 
				
				$questions_score['score']= obtain_markes_service($key);
				$questions_score['action_plan_tm']=$_POST['remark_tm'][$key];
				$questions_score['target_date']=$_POST['remark_date'][$key]; 
				
				
				$cat_id = $_POST['formula_id'][$question_id]; 
				switch($cat_id) {
					case 64: //Manpower
					// var_dump($_POST['required'][$question_id]);die;
						$questions_score['required'] =  (int)$_POST['required'][$question_id];
						$questions_score['available'] =  (int)$_POST['available'][$question_id];
						$questions_score['training'] =  (int)$_POST['training'][$question_id];
						break; 
					case 65: //KPI  
						$questions_score['actual'] = $actual_input =  (int)$_POST['actual'][$question_id];
						$questions_score['target'] =  $target_input = (int)$_POST['target'][$question_id];
						$questions_score['pr_actual']  = (float)($actual_input>0 and $target_input>0)?($actual_input/$target_input)*100:0;  
						
						$last_year_growth = 50; // will change
						$pr_growth =  $questions_score['pr_actual'] - $last_year_growth; 
						$questions_score['pr_growth'] = (float)($pr_growth>0)?$pr_growth:0; 
						break;   
					default: 
						$questions_score['response']= $_POST['question']['response'][$key]; 
				}
	 
				  
				if($network_survey_exist->rowCount()){	//update   
						db_update('network_survey_questions_score')
						->fields($questions_score)
						->condition('survey_score_id', $serve_id)	   
						->condition('question_id', $key)	   
						->execute();   
				}else{//insert	  
					  db_insert('network_survey_questions_score')->fields($questions_score)->execute();  
					}   
			} 	
			echo 'Data saved!'; exit;
		}
	
}
/* ******************************************************************  */
/* ******************************************************************  */
/* **** asc service auto save  ***** */
function network_survey_service_asc_autosave(){
	
	$user_role_meta = user_role_meta(); 
	$role = $user_role_meta['emp_designation_type'];
	$is_tm = (strtolower($role)=='tsm')?true:false;
	
	if($is_tm){
	// check serve already exist or not 
	$query = db_select('network_survey', 'ns');
	$query->fields('ns', array());
	$query->condition('ns.dealer_code', $_POST['question']['dealer_code']);
	$query->condition('ns.survey_month', $_POST['question']['survey_month']);     
	$query->condition('ns.survey_by_staff',$_POST['question']['survey_by_staff']);	   
	
	$network_survey_exist =  $query->execute();	   
 
	/*  for tm submit	 */
	
	
		$submit_type = 0;
		 /* network survey data */
		$network_survey = array(
			'dealer_code'=>$_POST['question']['dealer_code'],
			'survey_by_staff'=>$_POST['question']['survey_by_staff'],
			'survey_month'=>$_POST['question']['survey_month'],
			'status'=>$submit_type,
			'start_date'=>$_POST['question']['start_date'], 
			'total_marks'=>array_sum($_POST['max_mark']),
			'obtained_marks'=>array_sum(array_intersect_key($_POST['max_mark'],array_flip(array_keys($_POST['question']['response'])))),
			'last_submited_date' => date('Y-m-d H:i:s')
		);
		
		
		$network_survey['tm_feedback'] = $_POST['tm_feedback']; 

		 
		//if already not exist
		if($network_survey_exist->rowCount()){	//update
				$serve = $network_survey_exist->fetchAssoc();  
				$serve_id = $serve['id'];
				db_update('network_survey')
				->fields($network_survey)
				->condition('id', $serve_id)	   
				->execute();   
		}else{//insert	 
			 $serve_id =  db_insert('network_survey')->fields($network_survey)->execute();  
		} 
		
		
		foreach($_POST['max_mark'] as $key=>$res){
				$question_id = $key; 
				
				$questions_score['survey_score_id']= $serve_id;
				$questions_score['question_id']=$key; 
				
				$questions_score['score']= round(obtain_markes_service($key));
				$questions_score['action_plan_tm']=$_POST['remark_tm'][$key];
				$questions_score['target_date']=$_POST['remark_date'][$key]; 
				
				
				$cat_id = $_POST['formula_id'][$question_id]; 
				switch($cat_id) {
					case 64: //Manpower
					// var_dump($_POST['required'][$question_id]);die;
						$questions_score['required'] =  (int)$_POST['required'][$question_id];
						$questions_score['available'] =  (int)$_POST['available'][$question_id];
						$questions_score['training'] =  (int)$_POST['training'][$question_id];
						break; 
					case 65: //KPI  
						$questions_score['actual'] = $actual_input =  (int)$_POST['actual'][$question_id];
						$questions_score['target'] =  $target_input = (int)$_POST['target'][$question_id];
						$questions_score['pr_actual']  = (float)($actual_input>0 and $target_input>0)?($actual_input/$target_input)*100:0;  
						
						$last_year_growth = 50; // will change
						$pr_growth =  $questions_score['pr_actual'] - $last_year_growth; 
						$questions_score['pr_growth'] = (float)($pr_growth>0)?$pr_growth:0; 
						break;   
					default: 
						$questions_score['response']= $_POST['question']['response'][$key]; 
				}
			  
		  	if($network_survey_exist->rowCount()){	//update   
					db_update('network_survey_questions_score')
					->fields($questions_score)
					->condition('survey_score_id', $serve_id)	   
					->condition('question_id', $key)	   
					->execute();   
			}else{//insert	  
				  db_insert('network_survey_questions_score')->fields($questions_score)->execute();  
			}   
		} 
		echo 'Data saved!'; exit;
	}

}


/* ******************************************************************  */
/* ******************************************************************  */

/* asc sales auto save */
function network_survey_sales_asc_autosave(){
	
	$user_role_meta = user_role_meta(); 
	$role = $user_role_meta['emp_designation_type'];
	$is_tm = (strtolower($role)=='tsm')?true:false;
	
	if($is_tm){
		
	// check serve already exist or not 
	$query = db_select('network_survey', 'ns');
	$query->fields('ns', array());
	$query->condition('ns.dealer_code', $_POST['question']['dealer_code']);
	$query->condition('ns.survey_month', $_POST['question']['survey_month']); 
	$query->condition('ns.survey_by_staff',$_POST['question']['survey_by_staff']);	   
	
	$network_survey_exist =  $query->execute();	  
 
	/*  for tm submit	 */
	
		$submit_type = 0;
		 /* network survey data */
		$network_survey = array(
			'dealer_code'=>$_POST['question']['dealer_code'],
			'survey_by_staff'=>$_POST['question']['survey_by_staff'],
			'survey_month'=>$_POST['question']['survey_month'],
			'status'=>$submit_type,
			'start_date'=>$_POST['question']['start_date'], 
			'total_marks'=>array_sum($_POST['max_mark']),
			'obtained_marks'=>array_sum(array_intersect_key($_POST['max_mark'],array_flip(array_keys($_POST['question']['response'])))),
			'last_submited_date' => date('Y-m-d H:i:s')
		);
		
		
		$network_survey['tm_feedback'] = $_POST['tm_feedback']; 

		 
		//if already not exist
		if($network_survey_exist->rowCount()){	//update
				$serve = $network_survey_exist->fetchAssoc();  
				$serve_id = $serve['id'];
				db_update('network_survey')
				->fields($network_survey)
				->condition('id', $serve_id)	   
				->execute();   
		}else{//insert	 
			 $serve_id =  db_insert('network_survey')->fields($network_survey)->execute();  
		} 
		
		// question mark update 
		foreach($_POST['max_mark'] as $key=>$res){
				$response =  $_POST['question']['response'][$key] ;
			 
			 
				$questions_score['survey_score_id']= $serve_id;
				$questions_score['question_id']=$key; 
				$questions_score['response']=$response; 
				$questions_score['score']= obtain_markes_sales($key);
				$questions_score['action_plan_tm']=$_POST['remark_tm'][$key];
				$questions_score['target_date']=$_POST['remark_date'][$key];
 
			 
			
			if($network_survey_exist->rowCount()){	//update   
					db_update('network_survey_questions_score')
					->fields($questions_score)
					->condition('survey_score_id', $serve_id)	   
					->condition('question_id', $key)	   
					->execute();   
			}else{//insert	  
				  db_insert('network_survey_questions_score')->fields($questions_score)->execute();  
			} 
		}
					
		echo 'Data saved!'; exit;
	}
	  

}