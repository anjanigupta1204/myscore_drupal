<?php


/**
 * @file
 * Module file for location.
 */

/**
 * Implements hook_permission().
 *
 * Since the access to our new custom pages will be granted based on
 * special permissions, we need to define what those permissions are here.
 * This ensures that they are available to enable on the user role
 * administration pages.
 */
function location_permission() {
  return array(
        'access location category page' => array(
      'title' => t('Access location category page'),
      'description' => t('Allow admin to create/add and update location category page'),
    ),
            'access location aplicability category page' => array(
      'title' => t('Access location aplicability category page'),
      'description' => t('Allow admin to create/add and update aplicability category page'),
    ),
            'access location criteria category page' => array(
      'title' => t('Access location criteria category page'),
      'description' => t('Allow admin to create/add and update criteria category page'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 */
function location_menu() {

 $items['location-admin-settings'] = array(
    'title' => t('Area Office'),
    'description' => t('Settings for location Evaluation System'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_area_office'),
    'access arguments' => array('access location aplicability category page'),
    );
   $items['admin/location/area-office'] = array(
    'title' => t('Area Office'),
    'description' => t('Configure Area office Settings for Dealer Evaluation Survey.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_area_office'),
    'access arguments' => array('access location aplicability category page'),
    );
   $items['admin/location/zone'] = array(
    'title' => t('Zone'),
    'description' => t('Configure Zone Settings for Dealer Evaluation Survey.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_zone'),
    'access arguments' => array('access location criteria category page'),
   );
  $items['location-admin-settings/questions'] = array(
    'title' => 'Area office',
    'description' => t('Settings for location Types'),
     'type' => MENU_DEFAULT_LOCAL_TASK,
     'weight' => 0,
  );
 $items['location-admin-settings/zone'] = array(
    'title' => t('Zone'),
    'description' => t('Settings for location Evaluation System'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_zone'),
    'access arguments' => array('access location criteria category page'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
   );
  $items['zone/%ctools_js/%/edit'] = array(
      'title' => t('Edit Zone'),
      'page callback' => 'edit_zone',
      'page arguments' => array(1,2),
      'access arguments' => array('access location criteria category page'),
  );
  $items['areaoffice/%ctools_js/%/edit'] = array(
      'title' => t('Edit Area Office'),
      'page callback' => 'edit_area_office',
      'page arguments' => array(1,2),
      'access arguments' => array('access location criteria category page'),
  );
  return $items;
}

/**
 * Constructs a descriptive page.
 *
 */
function form_location($form, $form_state) {
  $items[] = l(t('Add New Location'), 'admin/settings/structure/taxonomy/location/add', array('attributes' => array('title' => t('Add New Location.'), 'class' =>'addplussign')));
  $form['links'] = array('#markup' => theme('item_list', array('items' => $items,'attributes' => array('class' => 'add-plus-sign'))));	
  return $form;
}

/**
 * Constructs a descriptive page.
 *
 */
function form_area_office($form, $form_state) {
   $form['area_office'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Click here to add new Area office'), 
	  '#prefix' => '<div class="dealer-form">',
      '#suffix' => '</div>',
	  '#weight' => -55, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => TRUE,
  );
  $form['area_office']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Area Office Name'),  
	  '#size' => 30,  
	  '#required' => TRUE,
  );
  $form['area_office']['code'] = array(
	  '#type' =>'textfield', 
	  '#title' => t('Area Office Code'), 
	  '#size' => 30,
	  '#required' => TRUE,
  );
  $form['area_office']['zone'] = array(
	  '#type' =>'select', 
	  '#options'=> all_zone_list(),
	  '#title' => t('Zone list'), 
	  '#required' => TRUE,
  );
  $form['area_office']['add'] = array(
	  '#type' =>'submit', 
	  '#value' => t('Add'), 
	  '#weight' => 19,
  );
  $form['admin'] = area_office_list(); 
  return $form;
}

function form_area_office_validate($form, &$form_state) {
	$tree = db_query("SELECT * FROM {hero_area_office_info}")->fetchAll();
	foreach($tree as $record){
		if($form_state['values']['name'] == $record->area_office_name) {
		form_set_error('name', t('Area Office Name Already Exists.'));
		}
	}	
	if(!is_numeric($form_state['values']['code'])) {
		form_set_error('code', t('Only Numeric Value is allowed.'));
	}		
}

function form_area_office_submit($form, &$form_state) {	
  if(!empty($form_state['values']['name'])) {
  $result= db_insert('hero_area_office_info')
	   ->fields(array(
		'area_office_name' => $form_state['values']['name'],
		'area_office_code' => $form_state['values']['code'],
		'parent_zone_code' => $form_state['values']['zone'],
	 ))
	 ->execute();
	drupal_set_message(t('New Area office is added to database.'));
  }
}

/**
 * implementation of all zone list
 * 
 */ 
function all_zone_list() {
	$output = db_select('hero_zone_info','hz')
		 ->fields('hz')
		 ->execute()
		 ->fetchAll();
	foreach($output as $key => $val) {
	    $result[$val->zone_code] = $val->zone_name;
    }	
    return $result;	 
}	


function area_office_list() {
  // Include the CTools tools that we need.
  ctools_include('ajax');
  ctools_include('modal');

  // Add CTools' javascript to the page.
  ctools_modal_add_js();
  
    // Create our own javascript that will be used to theme a modal.
  $sample_style = array(
    'ctools-sample-style' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 1100,
        'height' => 500,
        'addWidth' => 20,
        'addHeight' => 15,
      ),
      'modalOptions' => array(
        'opacity' => .5,
        'background-color' => '#000',
      ),
      'animation' => 'fadeIn',
      'modalTheme' => 'CToolsSampleModal',
      'throbber' => theme('image', array('path' => ctools_image_path('wait.gif', 'dealer_mapping'), 'alt' => t('Loading...'), 'title' => t('Loading'))),
    ),
  );

  drupal_add_js($sample_style, 'setting');

  // Since we have our js, css and images in well-known named directories,
  // CTools makes it easy for us to just use them without worrying about
  // using drupal_get_path() and all that ugliness.
  ctools_add_js('ctools-ajax-sample', 'dealer_mapping');
  ctools_add_css('ctools-ajax-sample', 'dealer_mapping');	
	
	
  // Check if there is sorting request
  if(isset($_GET['sort']) && isset($_GET['order'])){
    // Sort it Ascending or Descending?
    if($_GET['sort'] == 'asc')
      $sort = 'ASC';
    else
      $sort = 'DESC';
    

// Which column will be sorted
    switch($_GET['order']){
      case 'Area Office Name':
        $order = 'area_office_name';
        break;
      case 'Area Office Code':
        $order = 'area_office_code';
        break;
      default:
        $order = 'area_office_name';
    }
  }
  else{
    // Default sort
    $sort = 'ASC';
    $order = ' area_office_name';
  }
  

$output = "";
  

// Select table
  $query = db_select("hero_area_office_info", "hz");
  // Selected fields
  $query->fields("hz", array("aid","area_office_name", "area_office_code", "parent_zone_code"));
  // Set order by
  $query->orderBy($order, $sort);
  // Pagination
  $query = $query->extend('TableSort')->extend('PagerDefault')->limit(5);
  

// Execute query
  $result = $query->execute();
  

// Prepare table header
  $header = array(
    array(
      "data" => t('Area Office Name'),
      "field" => "area_office_name"
    ),
    array(
      "data" => t('Area Office Code'),
      "field" => "area_office_code"
    ),
    array(
      "data" => t('Operation'),
    )
  );
  

$rows = array();
   $a = 1; 

// Looping for filling the table rows
  while($data = $result->fetchObject()){
    // Fill the table rows
    $rows[] = array(
      t($data->area_office_name),
      t($data->area_office_code),
      t(ctools_modal_text_button(t('Edit'), ('areaoffice/nojs/'.$data->aid.'/edit'), t('Edit'),  'ctools-modal-ctools-sample-style')),      
      //l(t('Edit'), 'admin/applicability/'.$data->area_office_code.'/edit', array('attributes' => array('title' => t('Edit.'), 'class' =>'addplussig'), 'query' => drupal_get_destination())),                 
    );
     $a++;
  }
  //print "<pre>"; print_r($rows); exit;

// Output of table with the paging
  $build['pager_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no date formats found in the db'),
  );

  // attach the pager theme
  $build['pager_pager'] = array('#theme' => 'pager');
  
  return $build;	
}	

/**
 * Constructs a descriptive page.
 *
 */
function form_zone($form, $form_state) {
     $form['zone'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Click here to add new Zone'), 
	  '#prefix' => '<div class="dealer-form">',
      '#suffix' => '</div>',
	  '#weight' => -55, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => TRUE,
  );
  $form['zone']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Zone Name'),  
	  '#size' => 30,  
	  '#required' => TRUE,
  );
  $form['zone']['code'] = array(
	  '#type' =>'textfield', 
	  '#title' => t('Zone Code'), 
	  '#size' => 30,
	  '#required' => TRUE,
  );
  $form['zone']['add'] = array(
	  '#type' =>'submit', 
	  '#value' => t('Add'), 
	  '#weight' => 19,
  );
  $form['admin'] = zone_list();
  return $form;
}
//Zone validation for duplicacy
function form_zone_validate($form, &$form_state) {
$tree = db_query("SELECT * FROM {hero_zone_info}")->fetchAll();
//echo "<pre>";print_r($tree);exit;
	foreach($tree as $record){
		if($form_state['values']['name'] == $record->zone_name) {
		form_set_error('name', t('Zonet Name Already Exists.'));
		}
		if($form_state['values']['code'] == $record->zone_code) {
		form_set_error('code', t('Zone Code Already Exists.'));
		}
	}	
}
function form_zone_submit($form, &$form_state) {
  if(!empty($form_state['values']['name'])) {
  $result= db_insert('hero_zone_info')
	   ->fields(array(
		'zone_name' => $form_state['values']['name'],
		'zone_code' => $form_state['values']['code'],
	 ))
	 ->execute();
	drupal_set_message(t('New Zone is added to database.'));
  }
}	



/**
 * implementation of zone list
 * 
 */ 

function zone_list() {
  // Include the CTools tools that we need.
  ctools_include('ajax');
  ctools_include('modal');

  // Add CTools' javascript to the page.
  ctools_modal_add_js();
  
    // Create our own javascript that will be used to theme a modal.
  $sample_style = array(
    'ctools-sample-style' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 1100,
        'height' => 500,
        'addWidth' => 20,
        'addHeight' => 15,
      ),
      'modalOptions' => array(
        'opacity' => .5,
        'background-color' => '#000',
      ),
      'animation' => 'fadeIn',
      'modalTheme' => 'CToolsSampleModal',
      'throbber' => theme('image', array('path' => ctools_image_path('wait.gif', 'dealer_mapping'), 'alt' => t('Loading...'), 'title' => t('Loading'))),
    ),
  );

  drupal_add_js($sample_style, 'setting');

  // Since we have our js, css and images in well-known named directories,
  // CTools makes it easy for us to just use them without worrying about
  // using drupal_get_path() and all that ugliness.
  ctools_add_js('ctools-ajax-sample', 'dealer_mapping');
  ctools_add_css('ctools-ajax-sample', 'dealer_mapping');
  	
	
$tree = db_select('hero_zone_info','hz')
		 ->fields('hz')
		 ->execute()
		 ->fetchAll();
if(!empty($tree)){    
  $header = array(
    array('data' => t('S.No.')),
    array('data' => t('Zone')),
    array('data' => t('Operation')),
  );
  $a = 1;
  foreach($tree as $key => $val) {
	  $val = (array)$val;
	  $rows[] = array( 
	    array('data' => t($a.'.')),
		array('data' => t($val['zone_name'])),
		//array('data' => l('Edit','zone/nojs/'.$val['zone_code'].'/edit'),array('attributes' => array('title' => t('Edit.'), 'class' =>'addplussig'), 'query' => drupal_get_destination()))   
		array('data' => t(ctools_modal_text_button(t('Edit'), ('zone/nojs/'.$val['zone_code'].'/edit'), t('Edit'),  'ctools-modal-ctools-sample-style'))),      
	  ); $a++;
  }
  $form['portal2'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Zone list'), 
	  '#weight' => 5, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => FALSE,
  );
  // build the table for the nice output.
  $form['portal2']['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
} 
else {
  $form['portal2'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Result'), 
	  '#weight' => 5, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => FALSE,
  );
    $form['portal2']['result'] = array(
	  '#markup' => t('Zone is not available.'), 
  );
}	 
  return $form;	
            
}	

function edit_zone($js = NULL, $val ) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('form_zone_edit');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Edit Zone'),
    'ajax' => TRUE,
  );
  $form_state['build_info']['args'] = array('zone_id' => $val );
  $output = ctools_modal_form_wrapper('form_zone_edit', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    $output[] = ctools_modal_command_display('Zone', '<h1 style="text-align:center">Zone is updated to database.</h1>');
  }
  print ajax_render($output);
  exit;
}

/**
 * Constructs a descriptive page.
 *
 */
function form_zone_edit($form, $form_state) {
//print "<pre>"; 	print_r($form_state);exit;

	$tree = db_select('hero_zone_info','hz')
		 ->fields('hz')
		 ->condition('hz.zone_code',$form_state['build_info']['args']['zone_id'],'=')
		 ->execute()
		 ->fetchAssoc();

//print "<pre>"; 	print_r($tree);exit;		
  $form['zone'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t(''), 
  );
  $form['zone']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Zone Name'),  
	  '#size' => 30, 
	  '#default_value' => $tree['zone_name'], 
	  '#required' => TRUE,
  );
  $form['zone']['code'] = array(
	  '#type' =>'textfield', 
	  '#title' => t('Zone Code'), 
	  '#size' => 30,
	  '#default_value' => $tree['zone_code'], 	  
	  '#required' => TRUE,
  );
  $form['zone']['add'] = array(
	  '#type' =>'submit', 
	  '#value' => t('Update'), 
	  '#weight' => 19,
  );
  return $form;
}

function form_zone_edit_validate($form, &$form_state) {
  if(!is_numeric($form_state['values']['code'])) {
		form_set_error('code', t('Zone code Should be Numeric.'));
	}
}	

function form_zone_edit_submit($form, &$form_state) {
  $result= db_update('hero_zone_info')
	   ->fields(array(
		'zone_name' => $form_state['values']['name'],
		'zone_code' => $form_state['values']['code'],
	 ))
	 ->condition('zone_code',$form_state['values']['code'],'=')
	 ->execute();
}



/***** Area office ***********/


function edit_area_office($js = NULL, $val ) {
  // Fall back if $js is not set.
  if (!$js) {
    return drupal_get_form('form_area_office_edit');
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Edit Area Office'),
    'ajax' => TRUE,
  );
  $form_state['build_info']['args'] = array('aid' => $val);
  $output = ctools_modal_form_wrapper('form_area_office_edit', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    $output[] = ctools_modal_command_display('Area Office', '<h1 style="text-align:center">Area Office is updated to database.</h1>');
  }
  print ajax_render($output);
  exit;
}

/**
 * Constructs a descriptive page.
 *
 */
function form_area_office_edit($form, $form_state) {	
	$tree = db_select('hero_area_office_info','hz')
		 ->fields('hz')
		 ->condition('hz.aid',$form_state['build_info']['args']['aid'],'=')
		 ->execute()
		 ->fetchAssoc();		
  $form['area_office'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t(''), 
  );
  $form['area_office']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Area Office Name'),  
	  '#size' => 30, 
	  '#default_value' => $tree['area_office_name'], 
	  '#required' => TRUE,
  );
  $form['area_office']['aid']= array('#type' => 'value', '#value' => $tree['aid']);
  
  $form['area_office']['area_code'] = array(
	  '#type' =>'textfield', 
	  '#title' => t('Area Office Code'), 
	  '#size' => 30,
	  '#default_value' => $tree['area_office_code'], 	  
	  '#required' => TRUE,
  );
  $form['area_office']['zone'] = array(
	  '#type' =>'select', 
	  '#options'=> all_zone_list(),
	  '#title' => t('Comes Under Zone'),
	  '#default_value' => $tree['parent_zone_code'], 
	  '#required' => TRUE,
  );
  $form['area_office']['add'] = array(
	  '#type' =>'submit', 
	  '#value' => t('Update'), 
	  '#weight' => 19,
  );
  return $form;
}

function form_area_office_edit_validate($form, &$form_state) {
  if(!is_numeric($form_state['values']['area_code'])) {
		form_set_error('code', t('Area Office code Should be Numeric.'));
	}
}	

function form_area_office_edit_submit($form, &$form_state) {
  $result= db_update('hero_area_office_info')
	   ->fields(array(
		'area_office_name' => $form_state['values']['name'],
		'area_office_code' => $form_state['values']['area_code'],
		'parent_zone_code' => $form_state['values']['zone'],
	 ))
	 ->condition('aid',$form_state['values']['aid'],'=')
	 ->execute();
}
