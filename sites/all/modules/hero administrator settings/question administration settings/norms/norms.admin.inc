<?php

/**
 * @file
 * Administrative page code for the hero survey norms module.
 *
 */

function sales_norms_detail($form, &$form_state) {
  $items[] = l(t('Add New Sales Norms'), 'admin/structure/add/sales_manpower', array('attributes' => array('title' => t('Add New sales norms.'), 'class' =>'addplussign'), 'query' => drupal_get_destination()));
  //$form['links'] = array('#markup' => theme('item_list', array('items' => $items,'attributes' => array('class' => 'add-plus-sign'))));
  $form['admin'] = view_sales_norms();
  return $form;	
}

function service_norms_detail($form, &$form_state) {
  $items[] = l(t('Add New Service Norms'), 'admin/structure/add/service_manpower', array('attributes' => array('title' => t('Add New Service Norms.'), 'class' =>'addplussign'), 'query' => drupal_get_destination()));
 // $form['links'] = array('#markup' => theme('item_list', array('items' => $items,'attributes' => array('class' => 'add-plus-sign'))));
  $form['admin'] = view_service_norms();
  return $form;	
}	

/**
 * Constructs a descriptive page.
 *
 * Our menu maps this function to the path 'examples/sales'.
 *
 */
function view_sales_norms() {
   // We are going to output the results in a table with a nice header.
  $header = array(
    // The header gives the table the information it needs in order to make
    // the query calls for ordering. TableSort uses the field information
    // to know what database column to sort by.
    array('data' => t('Applicability Criteria')),
    array('data' => t('GM')),
    array('data' => t('Showroom Manager')),
    array('data' => t('Receptionist & Hostess')),
    array('data' => t('Sales Executive')),
    array('data' => t('CRM Executive')),
    array('data' => t('Marketing Executive')),
    array('data' => t('Rural Support-sales**')),
    array('data' => t('Network Executive')),
    array('data' => t('Accoutant')),
    array('data' => t('Cashier')),
    array('data' => t('Delivery Executive')),
   // array('data' => t('Operation')),    
  );
  $sales_norms = db_select('hero_sales_manpower_norms','hsmn')
          ->fields('hsmn')
          ->execute()
          ->fetchAll();
      
  
  foreach($sales_norms as $key => $val) {
	    //print "<pre>"; print_r($val); exit;
	  $rows[] = array( 
		array('data' => t($val->appliacbility_criteria.'-'.$val->appliacbility_criteria_max)),  
		array('data' => t($val->gm)), 
		array('data' => t($val->Showroom_manager)),
		array('data' => t($val->receptionist_hostess)),  
		array('data' => t($val->Sales_executive)), 
		array('data' => t($val->crm_executive)),
		array('data' => t($val->marketing_executive)),  
		array('data' => t($val->rural_support_sales)), 
		array('data' => t($val->network_executive)),
		array('data' => t($val->accountant)),  
		array('data' => t($val->cashier)),
		array('data' => t($val->delivery_executive)),
		//array('data' => t(l('Edit', 'edit')))
	  ); 
  }
  $form['portal2'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Sales Norms List'), 
	  '#weight' => 5, 
	  '#collapsible' => FALSE, 
	  '#collapsed' => FALSE,
  );
  // build the table for the nice output.
  $form['portal2']['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $form;
}


/**
 * Administrative settings.
 *
 * @return
 *   An array containing form items to place on the module settings page.
 */
function sales_norms_admin_settings() {
	
  $form['hero'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Manage Sales Manpower'),
  );
  $form['hero']['job_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Applicability Criteria'),
    '#size' => 10,
    '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['gm'] = array(
    '#type'          => 'textfield',
    '#title'         => t('GM'),
    '#size' => 10,
    '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['showroom_manager'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Showroom Manager'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['rec_host'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Receptionist & Hostess'),
     '#size' => 10,
  );
    $form['hero']['sales_exe'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Sales Executive'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['crm_exe'] = array(
    '#type'          => 'textfield',
    '#title'         => t('CRM Executive'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['mrk_exe'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Marketing Executive'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['rural_support'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Rural Support-sales**'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['net_exe'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Network Executive'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['acc'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Accoutant'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['cash'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Cashier'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
    $form['hero']['delivery'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Delivery Executive'),
     '#size' => 10,
     '#element_validate' => array('_check_data_type'),
  );
  return system_settings_form($form);
}

function _check_data_type($element, &$form_state, $form) {
   if (empty($element['#value'])) {
	 form_error($element, t($element['#title'].' field is required.'));
   }
   elseif(!is_numeric($element['#value'])) {
     form_error($element, t($element['#title']. ' field accept only numeric value.'));
   }
}

/**
 * Constructs a descriptive page.
 *
 * Our menu maps this function to the path 'examples/sales'.
 *
 */
function view_service_norms() {

   // We are going to output the results in a table with a nice header.
  $header = array(
    // The header gives the table the information it needs in order to make
    // the query calls for ordering. TableSort uses the field information
    // to know what database column to sort by.
    array('data' => t('Designation')),
    array('data' => t('Requirement Guidelines')),
    array('data' => t('Formula')),
    array('data' => t('Program Norms(Only if following are attended)')),
    array('data' => t('Training Score')),    
    //array('data' => t('Operation')),
  );
  $service_norms = db_select('hero_service_manpower_norms','hsmn')
          ->fields('hsmn')
          ->execute()
          ->fetchAll();
  foreach($service_norms as $key => $val) {
	  $rows[] = array(  
		array('data' => t($val->designation)), 
		array('data' => t($val->require_guideline)),
		array('data' => t($val->formula)),  
		array('data' => t($val->program_norms)), 
		array('data' => t($val->score)),
		//array('data' => t(l('Edit', 'edit')))
	  ); 
  }
  $form['portal2'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Service Norms List'), 
	  '#weight' => 5, 
	  '#collapsible' => FALSE, 
	  '#collapsed' => FALSE,
  );
  // build the table for the nice output.
  $form['portal2']['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $form;
}


/**
 * Administrative settings.
 *
 * @return
 *   An array containing form items to place on the module settings page.
 */
function service_norms_admin_settings() {
  $form['hero'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Manage Sales Manpower'),
  );
  $form['hero']['designation'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Designation'),
    '#size' => 10,
  );
    $form['hero']['require_guideline'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Require Guideline'),
    '#size' => 10,
  );
    $form['hero']['formula'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Formula'),
     '#size' => 10,
  );
    $form['hero']['program_norms'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Program Norms'),
     '#size' => 10,
  );
    $form['hero']['score'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Sales Executive'),
     '#size' => 10,
  );
  return system_settings_form($form);
}

/**
 * Set suvey different different status
 * 
 */ 
function survey_status_detail($form, $form_state) {
  $form['survey_states'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Click here to add new Survey Status'), 
	  '#prefix' => '<div class="dealer-form">',
      '#suffix' => '</div>',
	  '#weight' => -55, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => TRUE,
  );
  $form['survey_states']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Survey Status Name'),  
	  '#size' => 60,  
	  '#required' => TRUE,
  );
  $form['survey_states']['add'] = array(
	  '#type' =>'submit', 
	  '#value' => t('Add'), 
	  '#weight' => 19,
  );
  $form['admin'] = survey_states_list();
  return $form;	
}	

function survey_status_detail_validate($form, &$form_state) {
	if(empty($form_state['values']['name'])) {
		form_set_error('name', t('Survey Status Name can not be empty.'));
	}	
}	
/**
 * Submit function for form_example_tutorial_7().
 *
 * Adds a submit handler/function to our form to send a successful
 * completion message to the screen.
 */
function survey_status_detail_submit($form, &$form_state) {

  if(!empty($form_state['values']['name'])) {
  $result= db_insert('survey_status_type')
	   ->fields(array(
		'survey_status_type' => $form_state['values']['name'],
	 ))
	 ->execute();
	drupal_set_message(t('New Survey Status is added to database.'));
  }	  
}

function survey_states_list() {	
  $tree = db_select('survey_status_type','ha')
             ->fields('ha')
             ->orderBy('survey_status_id','ASC')
             ->execute()
             ->fetchAll();
  $header = array(
    array('data' => t('S.No.')),
    array('data' => t('Survey Status Name')),
    array('data' => t('Operation')),
  );
  $a = 1;
  $rows = array();
  foreach($tree as $key => $val) {
	  $val = (array)$val;
	  $rows[] = array( 
	    array('data' => t($a.'.')),
		array('data' => t($val['survey_status_type'])),   
		array('data' => l(t('Edit'), 'survey/status/'.$val['survey_status_id'].'/edit', array('attributes' => array('title' => t('Edit.'), 'class' =>'addplussig'), 'query' => drupal_get_destination()))),      
	  ); $a++;      
  }
  $form['portal2'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Survey Status list'), 
	  '#weight' => 5, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => FALSE,
  );
  // build the table for the nice output.
  $form['portal2']['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Record Found.'),
  );
  return $form;	
}	
 
function survey_status_edit($form, &$form_state, $val = NULL) {
  $result = db_select('survey_status_type','sst')
               ->fields('sst')
               ->condition('survey_status_id',$val,'=')
               ->execute()
               ->fetchAssoc();
  //print "<pre>"; print_r($result); exit;            	
	
  $form['survey_states_code'] = array('#type' => 'value', '#value' => $result['survey_status_id']);
  $form['survey_states'] = array(
	  '#type' => 'fieldset', 
	  '#prefix' => '<div class="dealer-form">',
      '#suffix' => '</div>',
	  '#weight' => -55, 
  );
  $form['survey_states']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Survey Status Name'),  
	  '#size' => 60,  
	  '#default_value' => $result['survey_status_type'],
	  '#required' => TRUE,
  );
  $form['survey_states']['add'] = array(
	  '#type' =>'submit', 
	  '#value' => t('Update'), 
	  '#weight' => 19,
  );
  return $form;	
}	

function survey_status_edit_validate($form, &$form_state) {
	if(empty($form_state['values']['name'])) {
		form_set_error('name', t('Survey Status Name can not be empty.'));
	}	
}	
/**
 * Submit function for form_example_tutorial_7().
 *
 * Adds a submit handler/function to our form to send a successful
 * completion message to the screen.
 */
function survey_status_edit_submit($form, &$form_state) {

  if(!empty($form_state['values']['name'])) {
  $result= db_update('survey_status_type')
	   ->fields(array(
		'survey_status_type' => $form_state['values']['name'],
	    ))
	   ->condition('survey_status_id',$form_state['values']['survey_states_code'],'=')
	 ->execute();
	drupal_set_message(t('Survey Status has been updated.'));
  }	  
}
