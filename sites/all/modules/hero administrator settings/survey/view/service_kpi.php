<?php 

?>

<?php
	// remark and target field value
		foreach($survice_kpi_saved_data as $record){
	
		$remark[$record['question_key']] =   $record['remark'];
		$target_date[$record['question_key']] =   $record['target_date'];
	}
?>

<table id="business-kpi-table" class="sticky-enabled tableheader-processed sticky-table" cellspacing="0" cellpadding="1" border="1">
   <thead>
      <tr>
         <th colspan="2">
            <div align="center"> Business KPI's </div>
         </th>
         <th colspan="6">
            <div align="center"> YTD </div>
         </th>
         <th colspan="1">
            <div align="center"> Remark/Action Plan </div>
         </th>
         <th colspan="1">
            <div align="center"> Target Date </div>
         </th>
      </tr>
   </thead>
   <tbody>
      <tr class="odd">
         <td><b>Parameter</b></td>
         <td><b>Sub-Parameter</b></td>
         <td><b>Target</b></td>
         <td><b>Actual</b></td>
         <td><b>% Ach</b></td>
         <td><b>% Growth from LY</b></td>
         <td><b>Score</b></td>
         <td><b>Max Score</b></td>
         <td><b>&nbsp;</b></td>
         <td><b>&nbsp;</b></td>
      </tr>
	  
	  <?php $num = 1; $desc_arr = array(); ?>
	  <?php foreach($get_service_kpi_perm as $key => $kpi_perm): ?>
	  <?php if($kpi_perm['status'] == 1): ?>
	  <?php 
		$alter_class = ($num%2==0)?'odd':'even'; $num++;
	    $desc = (in_array($kpi_perm['desc'], $desc_arr))?"":$kpi_perm['desc']; 
		$desc_arr[] = $kpi_perm['desc'];
	  ?>
	  
      <tr class="<?php echo $alter_class; ?>">
         <td>
            <div> <?php echo $desc; ?> </div>
         </td>
         <td>
            <div> <?php echo $kpi_perm['desc2']; ?> </div>
         </td>
         <td>	
			<?php
			$target =  isset($get_service_kpi_target[$key])?$get_service_kpi_target[$key]:0;
			
			$actual =  isset($get_service_kpi_actual[$key])?$get_service_kpi_actual[$key]:0;
			
			$last_year_actual =  isset($get_service_kpi_actual_last_year[$key])?$get_service_kpi_actual_last_year[$key]:0;
			
			$prsent_ach_count = ($actual/$target)*100;
			//$prsent_ach = ($prsent_ach_count>100)?100:$prsent_ach_count;
			$prsent_ach = $prsent_ach_count;
			
			//$growth_from_ly = (($actual - $last_year_actual)/$last_year_actual)*100;
			$growth_from_ly = (($actual - $last_year_actual)/50)*100;
			
			$prsent_score = ($prsent_ach/100)*$kpi_perm['marks'];
			
			if($prsent_ach >= 100){
				$prsent_score = $kpi_perm['marks'];
			}
			?>
            <div>
				<?php echo $target; ?>
			</div>
         </td>
         <td>
            <div> <?php echo $actual; ?></div>
         </td>
		 <?php
		 ?>
		 
		 
         <td>
            <div> <?php echo round($prsent_ach,2); ?></div>
         </td>
         <td>
            <div> <?php echo round($growth_from_ly,2); ?></div>
         </td>
         <td>
            <div> <?php echo round($prsent_score,2); ?></div>
         </td>
         <td>
            <div> <?php echo $kpi_perm['marks']; ?></div>
         </td>
		 
         <td>
            <div> 
				<?php $is_pdf = isset($pdf)?true:false; ?>
				<?php if(!$is_pdf): ?>
				<textarea name="remark[<?php echo $key; ?>]" id="int_remark" cols="20" row="3" ><?php echo $remark[$key]; ?></textarea>
				
				<?php else: ?>
					<?php echo $remark[$key]; ?>
				<?php endif; ?>
			</div>
         </td>
         <td>
            <div>  
				<?php if(!$is_pdf): ?>
<input name="target_date[<?php echo $key; ?>]"  id="target_date[<?php echo $key; ?>]" class="target_date_datepicker" type="text" value =<?php echo $target_date[$key];?> >
					<script>
						jQuery(document).ready(function() {
							jQuery(".target_date_datepicker").datepicker({
								dateFormat: 'dd-mm-yy',
								 minDate: 0,
								changeMonth: true,
								changeYear: true,
							});
						});
					</script> 
				<?php else: ?>
					<?php echo date("d-m-Y", strtotime($target_date[$key])); ?>
				<?php endif; ?>
			</div>


         </td>
      </tr>	  
	  <?php endif; ?>
	  <?php endforeach; ?>
	</tbody>

	
	
</table>
