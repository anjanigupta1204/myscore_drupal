<?php

function dealer_score_area_office_wise() {
    global $user;
    $employee = $_SESSION[$user->name];

    $emp_code = $employee['emp_code'];
    $multiRole = explode(',', $employee['emp_designation_type']);
    if($multiRole['0']=='zo' || $multiRole['0']=='asm' || $multiRole['0']=='asmview'|| $multiRole['0']=='zoview'|| $multiRole['0']=='hoview'){
    $output = drupal_get_form('get_dealer_score_area_office_wise');
    return $output;
    }else{
        return "Requested page is only for ZO.";
    }
}

module_load_include('inc', 'excel_report', 'report_page_3');
module_load_include('inc', 'excel_report', 'report_page_4');
module_load_include('inc', 'excel_report', 'report_page_2');
function get_dealer_score_area_office_wise($form, &$form_state) {

    $form['#id'] = 'get_dealer_score_area_office_wise';
    $form['#tree'] = TRUE;
global $user;
    $employee = $_SESSION[$user->name];
    
    $multiRole = explode(',', $employee['emp_designation_type']);
    $role = $multiRole['0'];

    $list = array();
    $list['0'] = t('All');
    //echo $employee['emp_code'];exit;

    $year_list = array('2014' => '2014', '2015' => '2015','2016' => '2016');
    $month_list = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
    $depts = get_depts();
    $form['dealer_ao']['month'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Month:',
        '#options' => $month_list,
    );

    $form['dealer_ao']['excel_year'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Select Year',
        '#options' => $year_list,
    );
if($role=='asmview' || $role=='zoview'|| $role=='hoview'){
    $form['dealer_ao']['dept'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Stream',
        '#options' => $depts,
    );
}
    $form['dealer_ao']['excel_actions'] = array('#type' => 'actions');
    $form['dealer_ao']['excel_actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Export'),
        '#submit' => array('report_dealer_officearea_wise_submit'),
    );

    return $form;
}

function report_dealer_officearea_wise_submit($form, &$form_state) {
    global $user;
    $selected_month = $form_state['values']['dealer_ao']['month'];
    $selected_year = $form_state['values']['dealer_ao']['excel_year'];
    $selected_dept = $form_state['values']['dealer_ao']['dept'];

    if ($selected_dept == '1') {
        $dept = 'sales';
    } else {
        $dept = 'services';
    }

    $employee = $_SESSION[$user->name];
//print_r($employee); die;
    $emp_code = $employee['emp_code'];
    $emp_dept = $employee['emp_department'];   
    $zone_code = $employee['emp_zone_code'];
    $multiRole = explode(',', $employee['emp_designation_type']);
	//echo  $emp_dept;
	if($emp_dept == 'sales'){
		$dept_code = 1;
	}else{
		$dept_code = 2;
	}
    $officearea_code = '';
     $header[] = 'Criteria';
         $header[] = 'Question';
         
    if ($multiRole['0'] == 'zoview') {
        $officearea_codes_qry = "SELECT DISTINCT emp_area_office_code AS office_area_code FROM hero_hmcl_staff WHERE emp_zone_code  = '$zone_code'";
        
        $officearea_codes_qry_fire = db_query($officearea_codes_qry);
        while ($officearea_codes_row = $officearea_codes_qry_fire->fetchAssoc()) {
       
        $header[] = $officearea_codes_row['office_area_code'];
        $myheader[$officearea_codes_row['office_area_code']] = $officearea_codes_row['office_area_code'];
           
           $officearea_code .= $officearea_codes_row['office_area_code'] . ',';  

    }
    }else if ($multiRole['0'] == 'zo') {
        $officearea_codes_qry = "SELECT DISTINCT emp_area_office_code AS office_area_code FROM hero_hmcl_staff WHERE emp_department = '$emp_dept' AND  emp_zone_code  = '$zone_code'";
        
        $officearea_codes_qry_fire = db_query($officearea_codes_qry);
        while ($officearea_codes_row = $officearea_codes_qry_fire->fetchAssoc()) {
       
        $header[] = $officearea_codes_row['office_area_code'];
        $myheader[$officearea_codes_row['office_area_code']] = $officearea_codes_row['office_area_code'];
           
           $officearea_code .= $officearea_codes_row['office_area_code'] . ',';  

    }
    }else if($multiRole['0'] == 'asm'){
        $ao = $employee['emp_area_office_code']; 
        $header[] = $ao;
        $myheader[$ao] = $ao;
           
           $officearea_code .= $ao . ',';  
        
    }else if($multiRole['0'] == 'asmview'){
        $ao = $employee['emp_area_office_code']; 
        $header[] = $ao;
        $myheader[$ao] = $ao;
           
           $officearea_code .= $ao . ',';  
        
    }else if($multiRole['0'] == 'hoview'){
	
       $officearea_codes_qry = "SELECT DISTINCT emp_area_office_code AS office_area_code FROM hero_hmcl_staff WHERE emp_area_office_code !='NULL' and emp_area_office_code !='' and emp_department='$dept' ORDER BY emp_area_office_code ";
        
        $officearea_codes_qry_fire = db_query($officearea_codes_qry);
        while ($officearea_codes_row = $officearea_codes_qry_fire->fetchAssoc()) {
       
        //$header[] = ///$officearea_codes_row['office_area_code'];
        $myheader[$officearea_codes_row['office_area_code']] = $officearea_codes_row['office_area_code'];
           
            $officearea_code .= $officearea_codes_row['office_area_code'] . ',';  
        
      }
    }
    
  
     $officearea_code = rtrim($officearea_code, ',');
    
        if($multiRole['0']=='tsm' || $multiRole['0']=='asm' || $multiRole['0']=='zo'){
    if($emp_dept=='sales'){
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($emp_dept=='services'){
        $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
		}
	}
	else if($multiRole['0']=='hoview'){
	
    if($dept=='sales'){
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($dept=='services'){
        $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
		}
	}
	elseif($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){

     if($dept=='sales'){
       
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($dept=='services'){
        $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
    }   
    } 
    $ques_query_fire = db_query($ques_query);
    $ques_query_data = $ques_query_fire->fetchAll();
  if($multiRole['0'] == 'asmview' || $multiRole['0'] == 'zoview'){  
    $dealer_qry = "SELECT dms.area_office_code, dms.dealer_code, sbi.sid
                FROM hero_dealer_mapping_status AS dms
                JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code)
                WHERE dms.area_office_code IN ($officearea_code)
                AND sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year' AND dms.dealer_mapped_to_staff_person_department_code = '$dept'";
  }else if($multiRole['0'] == 'asm' || $multiRole['0'] == 'zo'){
  //echo $officearea_code; exit;
  
  
      $dealer_qry = "SELECT dms.area_office_code, dms.dealer_code, sbi.sid
                FROM hero_dealer_mapping_status AS dms
                JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code)
                WHERE dms.area_office_code IN ($officearea_code)
                AND sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year' AND dms.dealer_mapped_to_staff_person_department_code = '$emp_dept'";
  }else if($multiRole['0'] == 'hoview'){
  //echo $officearea_code; exit;
       if($dept == 'sales'){
	    $depcode = '1';
	   }else {
	    $depcode = '2';
	   }
           $dealer_qry = "SELECT  dea.dealer_area_office_code, sbi.asso_dealer_code, sbi.sid
                FROM survey_basic_info AS sbi
                JOIN hero_dealer AS dea ON(sbi.asso_dealer_code = dea.dealer_code)
                WHERE  sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year' and sbi.survey_for_dept_code = '$depcode' and dea.dealer_area_office_code !=''  order by dealer_area_office_code";
				
				  
  
  }
  
    $dealer_query_fire = db_query($dealer_qry);
    $dealer_query_data = $dealer_query_fire->fetchAll();
	//echo "<pre>";
//print_r($ques_query_data);
   // print_r($dealer_query_data); exit;
   // print_r($dealer_query_data); exit;
    $dataset1 = db_query($dealer_qry);
   

    foreach ($ques_query_data as $key => $value) {
        foreach ($dealer_query_data as $key1 => $value1) {
		
            $criteira[$value->question_criteria_id] = $value->criteria_name;
            $questions[$value->question_id] = $value->question_name;
			
			if($multiRole['0'] == 'hoview')
			{
			 $acode = $value1->dealer_area_office_code; 
			 $dcode = $value1->asso_dealer_code;
			  $q_query = "SELECT question_response FROM survey_question_list 
                        WHERE  S_id = '$value1->sid' AND 
						       Q_id = '$value->question_id' order by S_id "; 
		     }
			 else 
			 {
			
			 $acode = $value1->area_office_code;
			 $dcode = $value1->dealer_code;
			 $q_query = "SELECT question_response FROM survey_question_list 
                        WHERE  S_id = '$value1->sid' AND 
						       Q_id = '$value->question_id' ";
			 }
			
			 //print_r($value->question_id);exit;
		
    $questions_respose_set = db_query($q_query);
	//$res = array();
	$result = $questions_respose_set->rowCount();
	//echo $value1->dealer_code; exit;
	$header[] = $acode;
	
	if($result == 0 )
	{
	   
       $question_response[$value->question_id][$acode][$dcode] = '';
       
	}else{
	
   while ($question_res = $questions_respose_set->fetchAssoc()) {
       
		
		$question_response[$value->question_id][$acode][$dcode] = $question_res['question_response'];
		
      //print_r($question_response); exit;  
		}
           
        }
     }
   }
  $header = array_unique($header); 
  
  $header[]="Avg Zone";
  // print_r($header); exit;
   //print_r($question_response); exit;
    $header_count = sizeof($header);
    echo 'Year - '.$selected_year."\n";
    echo 'Month - '.get_month_name($selected_month)."\n";
    if($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){
        echo 'Department -'.$selected_dept."\n";
    }else if($multiRole['0']=='asm' || $multiRole['0']=='tsm'|| $multiRole['0']=='zo'|| $multiRole['0']=='hoview'){
        echo 'Department -'.$emp_dept."\n";
    }
	foreach($header as $hd){
	
	  echo $hd. "\t";
	}
   
	
    print("\n");
   
    foreach ($question_response as $key => $value) {
        
     //print_r($value);  
    // print_r($key);  
        
         
        
        //$cri_q_arr = explode("-", $key);
        //$selected_cri = $cri_q_arr[0];
        //$selected_q = $cri_q_arr[1];
        $query_q_c = "SELECT hc.criteria_name, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id) WHERE  hq.question_id = '$key'";
        
		$q_c_set = db_query($query_q_c);
        $q_c_data = $q_c_set->fetchAll();
		
        echo $q_c_data[0]->criteria_name . "\t";
        echo $q_c_data[0]->question_name . "\t";
    
        foreach($myheader as $keym=>$valuem){
        foreach ($value as $key12 => $value12) {
           
		            
            if($keym!=$key12){
                $countm[$keym]=$keym;
            }else{
                $count_actual[$keym]=$keym;
            }
        }
		
        }
		
        $count = sizeof($count_actual); 
        $total = 0; 
        $space_count =  sizeof($header)-sizeof($value)-3;
       
		foreach ($value as $key1 => $value1) {
           
			
               $sum = array_sum($value1);
			   echo $sum;
			   $total +=$sum;
			   echo "\t";
			   
			 }
          
       
        for($i=0; $i<$space_count; $i++){
            echo "\t";
        }
      
        
        $avg = $total/$count;
        echo round($avg, 2);
        print("\n");
    }
    // exit;
    $date = date("d-m-Y");
    $filename = 'AO_Dealers-'.$date.'.xls';

  
  header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename");
    exit;
}
