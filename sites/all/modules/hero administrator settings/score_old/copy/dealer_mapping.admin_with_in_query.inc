<?php

/**
 * Menu callback for admin/structure/dealer_mapping.
 *
 * @param $theme
 *   The theme to display the administration page for. If not provided, defaults
 *   to the currently used theme.
 */
function dealer_mapping_admin_display_1($theme = NULL, $dept) {
  $department = $dept;
  // Fetch and sort dealer_mappings.
  $dealer_detailed_list = get_dealer_list_1($dept);
  return drupal_get_form('dealer_mapping_admin_display_form_1', $dealer_detailed_list, $department);
}

function _dependent_dropdown_callback1_1($form, $form_state) {
  return $form['portal1']['c_acc_no1'];
}
function _dependent_dropdown_callback1_2($form, $form_state) {
  return $form['portal1']['c_month'];
}
function _get_second_dropdown_options1_1($key = '') {
  if (!empty($key)) {
	$output['0'] = t('Select');  
    $result = db_select('hero_area_office_info','haoi')
               ->fields('haoi')
               ->condition('haoi.parent_zone_code',$key,'=')
               ->execute()
               ->fetchAll();
         foreach($result as $val) {
			 $output[$val->area_office_code] = $val->area_office_name;
		 }
		 return $output;	       
  }
  else {
    return array(0 => t("Select"));
  }
}
function _get_second_dropdown_options1_2($key = '') {
  //$years = array('Select','2013','2014');
  
    $starting_year  = 2013;
    $ending_year    = date('Y');
	$years = array();
	$years[0] = 'Select';
    for($starting_year; $starting_year <= $ending_year; $starting_year++) 
	{
       $years[] = $starting_year;
    }
  $output = array();

  if (!empty($key)) {
	$output['0'] = t('Select'); 
    	
    if($years[$key]<date('Y'))
	{
		$output = array('Select','January','February','March','April','May','June','July','August','September','October','November','December');
	}
	else if($years[$key]==date('Y'))
	{
	  $current_month_num = (int)date("m");
	  for($i=1;$i<=$current_month_num;$i++)
	      $output[] = get_month_name_by_code($i); 
	}
	return $output;	       
  }
  else {
    return array(0 => t("Select"));
  }
}
function dealer_mapping_admin_display_form_1($form, &$form_state, $dealer_detailed_list, $dept, $dealer_mapping_staff_person_name_list = NULL) {
  if (!isset($dealer_mapping_staff_person_name_list)) {
    $array = staff_person_list_1($dept);
    if(!empty($array)) {
     foreach($array as $key => $val) {
	   $val = (array) $val;
	   $dealer_mapping_staff_person_name_list[$val['emp_code']] = $val['emp_name'];	
	 }	
    }
    else{
		$dealer_mapping_staff_person_name_list['--'] = t('None');
	}	
  }
  global $user,$theme_key;
  $employee = $_SESSION[$user->name];
  //$multiRole = explode(',',$);
  drupal_theme_initialize();
  if (!isset($theme)) {
    // If theme is not specifically set, rehash for the current theme.
    $theme = $theme_key;
  }
    $cust_group = (isset($_SESSION['ord_status1']['c_group1']) ? $_SESSION['ord_status1']['c_group1'] : 0);
	$cust_name = (isset($_SESSION['ord_status1']['c_acc_no1']) ? $_SESSION['ord_status1']['c_acc_no1'] : 0);
	$selected = (isset($form_state['values']['portal1']['c_group1']) ? $form_state['values']['portal1']['c_group1']  : (isset($_SESSION['ord_status1']['c_group1']) ? $_SESSION['ord_status1']['c_group1'] : '0'));
    $selected_year = (isset($form_state['values']['portal1']['c_year']) ? $form_state['values']['portal1']['c_year'] : (isset($_SESSION['ord_status1']['c_year']) ? $_SESSION['ord_status1']['c_year'] : '0'));
	$cust_month     =  (isset($_SESSION['ord_status1']['c_month']) ? $_SESSION['ord_status1']['c_month'] : 0);
	$cust_depart     =  (isset($_SESSION['ord_status1']['c_type']) ? $_SESSION['ord_status1']['c_type'] : 0);
	$option_name = _get_second_dropdown_options1_1($selected); 
    
	 $beginning = array('0' => t('Select'));
	 $end = all_zone_list();
	 $zone = array_merge((array)$beginning, (array)$end);    
      
  $form['portal1'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Search by Area Office'), 
	  '#prefix' => '<div class="portal-form">',
      '#suffix' => '</div>',
	  '#weight' => -55, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => FALSE,
  );

   $form['portal1']['c_group1'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Zone'),
	   '#options' => $zone,
	   '#default_value' => $cust_group,
	   '#ajax' => array(
          'callback' => '_dependent_dropdown_callback1_1',
          'wrapper' => 'dropdown-second-replace1',
       ),
   );
    
  $form['portal1']['c_acc_no1'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Area Office'),
	   '#default_value' => $cust_name,
	   '#prefix' => '<div id="dropdown-second-replace1">',
       '#suffix' => '</div>',
       '#options' => $option_name,
   );
   $type = array('Select','sales','services');
   $form['portal1']['c_type'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Type'),
	   '#options' => $type,
	   
	   '#default_value' => $cust_depart,
	  
   );
   
    $starting_year  = 2013;
    $ending_year    = date('Y');
	$years = array();
	$years[0] = 'Select';
    for($starting_year; $starting_year <= $ending_year; $starting_year++) 
	{
       $years[] = $starting_year;
    }
   
   $form['portal1']['c_year'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Year'),
	   '#options' => $years,
	   '#ajax' => array(
        'callback' => '_dependent_dropdown_callback1_2',
        'wrapper' => 'dropdown-second-replace2',
       ),
	   '#default_value' => $selected_year,
	  
   );
   
   $months = _get_second_dropdown_options1_2($selected_year); 
     
   $form['portal1']['c_month'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Month'),
	   '#prefix' => '<div id="dropdown-second-replace2">',
       '#suffix' => '</div>',
	   '#options' => $months,
	   '#default_value' => $cust_month,
   );
  
   
   $form['portal1']['search'] = array(
	  '#type' =>'submit', 
	  '#submit' => array('_dealer_submit_mapping_1'),
	  '#value' => t('Search'), 
	  '#weight' => 19,
	);
  
  
  $weight_delta = round(count($dealer_detailed_list) / 2);
  // Build the form tree.
  $form['edited_theme'] = array(
    '#type' => 'value',
    '#value' => $theme,
  );
  $form['staff_dept'] = array(
    '#type' => 'value',
    '#value' => $dept,
  );
  $form['dealer_mapping_staff_person'] = array(
    '#type' => 'value',
    // Add a last region for disabled dealer_mappings.
    '#value' => $dealer_mapping_staff_person_name_list + array(dealer_mapping_dealer_none_to_attach => dealer_mapping_dealer_none_to_attach),
  );
  $form['dealer_mappings'] = array();
  $form['#tree'] = TRUE;
 
 //print "<pre>"; print_r($dealer_detailed_list); exit;
 
  foreach ($dealer_detailed_list as $i => $dealer_mapping) {
	$dealer_mapping = (array) $dealer_mapping; // print "<pre>"; print_r($dealer_mapping);exit;
    $key = $dealer_mapping['dealer_code'] . '_' . $dealer_mapping['area_office_code'];
    $form['dealer_mappings'][$key]['dealer_code'] = array(
      '#type' => 'value',
      '#value' => $dealer_mapping['dealer_code'],
    );
    $form['dealer_mappings'][$key]['area_office_code'] = array(
      '#type' => 'value',
      '#value' => $dealer_mapping['area_office_code'],
    );
    $form['dealer_mappings'][$key]['dealer_name'] = array(
      '#markup' => check_plain($dealer_mapping['dealer_Name']),
    );
    $form['dealer_mappings'][$key]['dealer_location'] = array(
      '#markup' => check_plain($dealer_mapping['dealer_location']),
    );
    $form['dealer_mappings'][$key]['theme'] = array(
      '#type' => 'hidden',
      '#value' => $theme,
    );
    $form['dealer_mappings'][$key]['dealer_weight'] = array(
      '#type' => 'value',
      '#default_value' => $dealer_mapping['dealer_weight'],
      '#delta' => $weight_delta,
      '#title_display' => 'invisible',
      '#title' => t('Weight for @dealer_mapping dealer_mapping', array('@dealer_mapping' => $dealer_mapping['dealer_code'])),
    );
    $form['dealer_mappings'][$key]['dealer_mapped_to_staff_person_code'] = array(
      '#type' => 'select',
      '#default_value' => $dealer_mapping['dealer_mapped_to_staff_person_code'] != dealer_mapping_dealer_none_to_attach ? $dealer_mapping['dealer_mapped_to_staff_person_code'] : NULL,
      '#empty_value' => dealer_mapping_dealer_none_to_attach,
      '#title_display' => 'invisible',
      '#title' => t('Region for @dealer_mapping dealer_mapping', array('@dealer_mapping' => $dealer_mapping['dealer_code'])),
      '#options' => $dealer_mapping_staff_person_name_list,
    );
  }
  $form['actions'] = array(
    '#tree' => TRUE,
    '#type' => 'actions',
  );
 /* $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Dealer Mappings'),
  );*/
  
  
  $form['admin'] = order_status_portal_search_result1();
 return $form;
}

function _dealer_submit_mapping_1($form, &$form_state) {
	//print "<pre>"; print_r($form_state['values']); exit;
	//echo $form_state['value']['portal1']['c_month'];
	foreach($form_state['values']['portal1'] as $key => $value) {
	    //echo "Rohit".$form_state['values']['portal1']['c_month']."Kumar";exit();
		$_SESSION['ord_status1'][$key] = $value;
	}	
}	

function dealer_mapping_admin_display_form_submit_1($form, &$form_state) {
	$update = $form_state['values']['dealer_mappings'];
	foreach($update as $key => $value){	
					
		 $updated = db_update('hero_dealer_mapping_status') 
		  ->fields(array(
			'dealer_mapped_to_staff_person_code' => $value['dealer_mapped_to_staff_person_code'],
			'is_dealer_mapped' => '1',
		  ))
		   ->condition('dealer_code', $value['dealer_code'], '=')
		   ->condition('dealer_mapped_to_staff_person_department_code', $form_state['values']['staff_dept'], '=')
		  ->execute();	
		 
		}	
	
  drupal_set_message(t('Record has been successfully Updated.'));
}	


function theme_dealer_mapping_admin_display_form_1($variables) {
 $form = $variables['form'];
 //print "<pre>"; print_r($form); exit;
  $rows = array();
  foreach (element_children($form['dealer_mappings']) as $id) {
    $form['dealer_mappings'][$id]['weight']['#attributes']['class'] = array('dealer-mappings-weight');
    $rows[] = array(
      'data' => array(
        // Add our 'name' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_name']),
        // Add our 'description' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_location']),
        // Add our 'staff person' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_mapped_to_staff_person_code']),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array(t('Dealer Name'), t('Location'),  t('Staff Person'));
  $table_id = 'dealer-mappings-table';
  //$output = drupal_render_children($form);
  // We can render our tabledrag table for output.
  $output = drupal_render($form['portal1']);
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));
  $output .= theme('pager'); 
  $output .= drupal_render($form['actions']);
  $output .= drupal_render_children($form);
  return $output;
}  


function get_dealer_list_1($dept) {
  if(isset($_SESSION['ord_status1']['c_acc_no1']) && $_SESSION['ord_status1']['c_group1'] != '0') {
	  if($_SESSION['ord_status1']['c_acc_no1'] == '0') {
		  $result = db_select('hero_dealer_mapping_status', 'hc')
			->fields('hc')
			->condition('hc.dealer_mapped_to_staff_person_department_code',$dept,'=')
			->extend('PagerDefault')
			->limit(5)
			->execute()
			->fetchAll();	
					
	  }
	  else {
		  $result = db_select('hero_dealer_mapping_status', 'hc')
			->fields('hc')
			->condition('hc.dealer_mapped_to_staff_person_department_code',$dept,'=')
			->condition('hc.area_office_code',$_SESSION['ord_status1']['c_acc_no1'],'=')
			->extend('PagerDefault')
			->limit(5)
			->execute()
			->fetchAll();		  
	  }	
	  	
	 return $result;    	    
  }	
  else {  
		$result = db_select('hero_dealer_mapping_status', 'hc')
			->fields('hc')
			->condition('hc.dealer_mapped_to_staff_person_department_code',$dept,'=')
			->extend('PagerDefault')
			->limit(5)
			->execute()
			->fetchAll();
			
		
		
   return $result;
  } 
}	

function staff_person_list_1($dept) {
  if(isset($_SESSION['ord_status1']['c_acc_no1']) && $_SESSION['ord_status1']['c_group1'] != '0') {
	  if($_SESSION['ord_status1']['c_acc_no1'] == '0') {
			  $result = db_select('hero_hmcl_staff', 'hhs')
				->fields('hhs',array('emp_name','emp_code'))
				->condition('hhs.emp_designation_type','tsm','=') 
				->condition('hhs.emp_department',$dept,'=') 
				->execute()
				->fetchAll(); 		  
	  }
	  else {
			  $result = db_select('hero_hmcl_staff', 'hhs')
				->fields('hhs',array('emp_name','emp_code'))
				->condition('hhs.emp_designation_type','tsm','=') 
				->condition('hhs.emp_department',$dept,'=') 
				->condition('hhs.emp_area_office_code',$_SESSION['ord_status1']['c_acc_no1'],'=') 
				->execute()
				->fetchAll(); 	  
	  }	
	  
	 return $result;    	    
  }	
  else {  
  $result = db_select('hero_hmcl_staff', 'hhs')
    ->fields('hhs',array('emp_name','emp_code'))
    ->condition('hhs.emp_designation_type','tsm','=') 
    ->condition('hhs.emp_department',$dept,'=') 
    ->execute()
    ->fetchAll();   
	
   return $result;
  }  
}	

function get_month_name_by_code($month_code)
{

if(isset($month_code)){
  switch($month_code)
  {
   case 1: return 'January';
   break;
   case 2: return 'February';
   break;
   case 3: return 'March';
   break;
   case 4: return 'April';
   break;
   case 5: return 'May';
   break;
   case 6: return 'June';
   break;
   case 7: return 'July';
   break;
   case 8: return 'August';
   break;
   case 9: return 'September';
   break; 
   case 10: return 'October';
   break; 
   case 11: return 'November';
   break; 
   case 12: return 'December';
   break;    
   default :return 'None';
  }
  }else{
     return;
  }
}

function get_month_code_by_name($month_name)
{

if(isset($month_name)){
  switch($month_name)
  {
   case 'January'  : return 1;
   break;
   case 'February'  : return 2;
   break;
   case 'March'    : return 3;
   break;
   case 'April'    : return 4;
   break;
   case 'May'      : return 5;
   break;
   case 'June'     : return 6;
   break;
   case 'July'     : return 7;
   break;
   case 'August'   : return 8;
   break;
   case 'September': return 9;
   break; 
   case 'October'  : return 10;
   break; 
   case 'November' : return 11;
   break; 
   case 'December' : return 12;
   break;    
   default         : return  0;
  }
  }else{
     return;
   }
}


function get_year_for_prev_month($prev_month,$current_year)
{
	$previous_year = '';
	if($prev_month==12)
	{
		$previous_year = $current_year-1;
	}
	else{
		$previous_year = $current_year;
	}
	return $previous_year;
}
function get_month_for_next_to_prev_month($prev_month)
{
	$next_to_prev_month ='';
	if($prev_month==1)
	{
		$next_to_prev_month = 12;
	}else{
		$next_to_prev_month = $prev_month-1;
	}
	return $next_to_prev_month;
}
function get_year_for_next_to_prev_month($prev_month,$current_year)
{
	$next_to_prev_year ='';
	if($prev_month==1 || $prev_month==12)
	{
		$next_to_prev_year = $current_year-1;
	}else{
		$next_to_prev_year = $current_year;
	}
	return $next_to_prev_year;
}
function get_year_by_code($year_code)
{
  $years = array('0','2013','2014','2015','2016');
  return $years[$year_code];
}

function getAllTSMList($emp_code)
{
	$query=db_select('hero_hmcl_staff','hhs');
	$query->condition('hhs.emp_report_to_designation_code',$emp_code)
	->fields('hhs',array('emp_login_id'));
	$tsm_list = array();
	//echo $query;
	$result= $query->execute();
	foreach($result as $key=>$val)
	{
	  if (!empty($val->emp_login_id)){
		$tsm_list[$key] = $val->emp_login_id;
	   }
	}
		
	return $tsm_list;
}
function getAllTSMListForZO($emp_code)
{
	$tsm_list = array();
	/**$query_string = "SELECT * FROM hero_hmcl_staff WHERE emp_report_to_designation_code IN (
	SELECT emp_code FROM  hero_hmcl_staff h2 WHERE h2.emp_report_to_designation_code =".$emp_code.")";
	$result= db_query($query_string);
	foreach($result as $key=>$val)
	{    
	     if (!empty($val->emp_login_id)){
		$tsm_list[$key] = $val->emp_login_id;
		}
	}****/
	
	$query_string = "SELECT * FROM hero_hmcl_staff h1 WHERE emp_report_to_designation_code IN (
	SELECT emp_code FROM  hero_hmcl_staff h2 WHERE h2.emp_report_to_designation_code =".$emp_code.")";
	$result = db_query($query_string);
	foreach($result as $key=>$val)
	{
		if (!empty($val->emp_login_id)){
		$tsm_list[] = $val->emp_login_id;
		}
	}
	$result2 = db_query("SELECT * FROM hero_hmcl_staff h1 WHERE h1.emp_report_to_designation_code =".$emp_code." AND FIND_IN_SET('tsm', emp_designation_type) ");
	foreach($result2 as $key=>$val)
	{
		if (!empty($val->emp_login_id)){
		$tsm_list[] = $val->emp_login_id;
		}
	}
	$result3 = db_query("SELECT * FROM hero_hmcl_staff h1 WHERE h1.emp_code = ".$emp_code." AND FIND_IN_SET('tsm', emp_designation_type) ");
	foreach($result3 as $key=>$val)
	{
		if (!empty($val->emp_login_id)){
		$tsm_list[] = $val->emp_login_id;
		}
	}
	return $tsm_list;
}
// piyush 07-03-14
/**********************/
function getAllTSMListForAsmView($emp_code)
{
//echo $emp_code;exit;
	/*$query_string = "SELECT * FROM hero_hmcl_staff h1 WHERE h1.emp_report_to_designation_code IN (
	SELECT h2.emp_code FROM  hero_hmcl_staff h2 WHERE h2.report_to_am_view = ".$emp_code.")";*/
	$query_string = "SELECT * FROM hero_hmcl_staff h1 WHERE h1.emp_report_to_designation_code IN (
	SELECT h2.emp_code FROM hero_hmcl_staff h2 WHERE FIND_IN_SET('".$emp_code."', h2.report_to_am_view))";
	$tsm_list = array();
	//echo $query;
	$result= db_query($query_string);
	foreach($result as $key=>$val)
	{
	    if (!empty($val->emp_login_id)){
		$tsm_list[$key] = $val->emp_login_id;
		}
	}
	return $tsm_list;
}
function getAllTSMListForZoView($emp_code)
{
	$tsm_list = array();
	/***$query_string = "SELECT * FROM hero_hmcl_staff h1 WHERE h1.emp_report_to_designation_code IN (
	SELECT h2.emp_code FROM  hero_hmcl_staff h2 WHERE h2.emp_report_to_designation_code IN (
	SELECT h3.emp_code FROM  hero_hmcl_staff h3 WHERE h3.report_to_zo_view = ".$emp_code."))";**/
	
	$query_string = "SELECT * FROM hero_hmcl_staff h1 WHERE h1.emp_report_to_designation_code IN (
	SELECT h2.emp_code FROM  hero_hmcl_staff h2 WHERE h2.emp_report_to_designation_code IN (
	SELECT h3.emp_code FROM  hero_hmcl_staff h3 WHERE FIND_IN_SET('".$emp_code."', h3.report_to_zo_view)))";
	$result= db_query($query_string);
	foreach($result as $key=>$val)
	{
	 if (!empty($val->emp_login_id)){
	  $tsm_list[$key] = $val->emp_login_id;
	  }
	}
	return $tsm_list;
}
function getAllTSMListForHoView()
{
	$tsm_list = array();
	$query_string = "SELECT * FROM hero_hmcl_staff ";
	//$query_string = db_select('hero_hmcl_staff','hhs');
	//$query_string->join('survey_basic_info','sbi','sbi.survey_owner_emp_code = hhs.emp_code');
	//$result= $query_string->execute();
	$result= db_query($query_string);
	foreach($result as $key => $val)
	{
	   if (!empty($val->emp_login_id)){
	  $tsm_list[$key] = $val->emp_login_id;
	  }
	}
	return $tsm_list;
}
/*************/

function isSurveyExists($emp_code)
{
global $user;
$designation = $_SESSION[$user->name]['emp_designation_type'];
	$multiRole = explode(',',$designation);
	//var_dump($multiRole);exit;
	if($emp_code != null)
	{
	//  For read only 
	  if(in_array('hoview',$multiRole))
		{
			$query_string = "SELECT COUNT(sid) AS sbi_count FROM survey_basic_info";
		}else if(in_array('asmview',$multiRole))
		{
			$query_string = "SELECT COUNT(sid) AS sbi_count FROM survey_basic_info WHERE 
			survey_owner_emp_code IN (SELECT e1.emp_code FROM hero_hmcl_staff e1 WHERE 
			e1.emp_report_to_designation_code IN (SELECT e2.emp_code FROM hero_hmcl_staff e2 WHERE 
			FIND_IN_SET('".$emp_code."', e2.report_to_am_view )))";
		}else if(in_array('zoview',$multiRole))
		{
			$query_string = "SELECT COUNT(sid) AS sbi_count FROM survey_basic_info WHERE 
			survey_owner_emp_code IN (SELECT e1.emp_code FROM hero_hmcl_staff e1 WHERE 
			e1.emp_report_to_designation_code IN (SELECT e2.emp_code FROM hero_hmcl_staff e2 WHERE 
			e2.emp_report_to_designation_code IN (SELECT e3.emp_code FROM hero_hmcl_staff e3 WHERE 
			FIND_IN_SET('".$emp_code."', e3.report_to_zo_view))))";
		}else if(in_array('zo',$multiRole))
		{
			$query_string ="SELECT COUNT(sid) AS sbi_count FROM survey_basic_info WHERE 
			survey_owner_emp_code IN (SELECT e1.emp_code FROM hero_hmcl_staff e1 WHERE 
			e1.emp_report_to_designation_code IN (SELECT e2.emp_code FROM hero_hmcl_staff e2 WHERE 
			e2.emp_report_to_designation_code = ".$emp_code."))";
		}else if(in_array('asm',$multiRole))
		{
		//var_dump($multiRole);exit;
			$query_string ="SELECT COUNT(sid) AS sbi_count FROM survey_basic_info WHERE 
			survey_owner_emp_code IN (SELECT e1.emp_code FROM hero_hmcl_staff e1 WHERE e1.emp_report_to_designation_code = ".$emp_code.")";
		}
		else if(in_array('tsm',$multiRole))
		{
			$query_string = "SELECT COUNT(sid) as sbi_count FROM survey_basic_info WHERE survey_owner_emp_code =".$emp_code;
		}
	}
	else{
		   $query_string = "SELECT COUNT(sid) as sbi_count FROM survey_basic_info" ;
	}
	$result= db_query($query_string);
	$sbi_list =array();
	
	foreach($result as $key =>$val)
	{
		$sbi_list[$key] = $val->sbi_count;
	}
	if($sbi_list[$key])
	{
	 return true;
	}
	else{
		return false;
	}
}
/*************/

function get_survey_id($dealer_code,$survey_for_dept_code,$survey_month,$surver_year)
{
	$sid_query = db_select('survey_basic_info','sbi');
	$sid_query ->fields('sbi',array('sid'));
	$sid_query->condition('sbi.asso_dealer_code',$dealer_code);
	$sid_query->condition('sbi.survey_for_dept_code',$survey_for_dept_code);
	$sid_query->condition('sbi.survey_month',$survey_month);
	$sid_query->condition('sbi.survey_year',$surver_year);
	$result=$sid_query->execute();
	$sid='null';
	foreach($result as $key=>$val)
	{
		$sid = $val->sid;
	}
	return $sid;
}
function get_department_id($department_name)
{
$department_id =0;
 if(isset($department_name))
 {
   if($department_name =='sales')
   {
     $department_id = 1;  
   }
   else if($department_name =='services')
   {
     $department_id = 2;
   }
  return $department_id;
 }
 else{
   return;
 }
}
function get_previous_month($current_month)
{
 $prev_month = 0;
 if(isset($current_month) && $current_month <= 12)
 {
   if(1==(int)$current_month)
   {
     $prev_month = 12;
   }
   else
   {
     $prev_month = $current_month-1;
   }
  } 
   return $prev_month;
}