<?php
header('Content-Type: text/plain');


require('php-excel-reader/excel_reader2.php');
require('SpreadsheetReader.php');
/**
 * Menu callback for admin/structure/dealer_mapping.
 *
 * @param $theme
 *   The theme to display the administration page for. If not provided, defaults
 *   to the currently used theme.
 */
function dealer_mapping_admin_import($theme = NULL, $dept) {
 
  return drupal_get_form('dealer_mapping_import_form');
}

function dealer_mapping_import_form($form, &$form_state) {
  
  global $theme_key;
  
 $form = array();
 
  // Use the #managed_file FAPI element to upload a file.
  $form['file_fid'] = array(
    '#title' => t('File'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded file will be processed.'),
    '#default_value' => variable_get('file_fid', ''),
    '#upload_validators' => array(
    'file_validate_extensions' => array('xlsx'),
    ),
  '#upload_location' => 'public://docs/',
  '#process' => array('import_my_file_element_process')
);
 
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
	'#submit' => array('_dealer_mapping_import_into_table'),
  );
 
  return $form;
}

function import_my_file_element_process($element, &$form_state, $form) {
  $element = file_managed_file_process($element, $form_state, $form);
  $element['upload_button']['#access'] = FALSE;
 
return $element;
}

function _dealer_mapping_import_into_table($form, &$form_state) {
	
  $file = file_load($form_state['values']['file_fid']);
  // Change status to permanent.
  $file->status = FILE_STATUS_PERMANENT;
  // Save.
  $uploaded = file_save($file);
  if ($uploaded == TRUE) {

try
{
$file_path = $file->uri;
    $dir = dirname($file_path);
	
	$Spreadsheet = new SpreadsheetReader(drupal_realpath($file_path));
	//$BaseMem = memory_get_usage();
    	
	$Sheets = $Spreadsheet -> Sheets();
    //echo '<pre>'; print_r($Sheets);exit;
	foreach ($Sheets as $Index => $Name)
	{	
		$Spreadsheet -> ChangeSheet($Index);

		foreach ($Spreadsheet as $Key => $Row)
		{
			//echo '<pre>'; print_r($Row);exit;
		  if ($Row)
		  {
			 if($Row[38] == 'motorcycle'){
			  $productId = 1;
			  }else if($Row[38] == 'scooter'){
			  $productId = 2;
			  }else if($Row[38] == 'service'){
			  $productId = 4;
			  }else if($Row[38] == 'parts'){
			  $productId = 5;
			  }
			 $nid = db_insert('sale_business_parameter')
		      ->fields(array(
              'dealer_code' => $Row[0],
			  'dealer_name' => $Row[1],
			  'LY_april' => $Row[2],
			  'LY_may' => $Row[3],
			  'LY_june' => $Row[4],
			  'LY_july' => $Row[5],
			  'LY_aug' => $Row[6],
			  'LY_sept' => $Row[7],
			  'LY_oct' => $Row[8],
			  'LY_nov' => $Row[9],
			  'LY_dec' => $Row[10],
			  'LY_jan' => $Row[11],
			  'LY_fab' => $Row[12],
			  'LY_march' => $Row[13],
			  'TY_april' => $Row[14],
			  'TY_may' => $Row[15],
			  'TY_june' => $Row[16],
			  'TY_july' => $Row[17],
			  'TY_aug' => $Row[18],
			  'TY_sept' => $Row[19],
			  'TY_oct' => $Row[20],
			  'TY_nov' => $Row[21],
			  'TY_dec' => $Row[22],
			  'TY_jan' => $Row[23],
			  'TY_fab' => $Row[24],
			  'TY_march' => $Row[25],
			  'TY_achieved_april' => $Row[26],
			  'TY_achieved_may' => $Row[27],
			  'TY_achieved_june' => $Row[28],
			  'TY_achieved_july' => $Row[29],
			  'TY_achieved_aug' => $Row[30],
			  'TY_achieved_sept' => $Row[31],
			  'TY_achieved_oct' => $Row[32],
			  'TY_achieved_nov' => $Row[33],
			  'TY_achieved_dec' => $Row[34],
			  'TY_achieved_jan' => $Row[35],
			  'TY_achieved_fab' => $Row[36],
			  'TY_achieved_march' => $Row[37],
			  'product_id' => $productId,
			   ))
			   ->execute();
			//echo 'Insert';exit;
		  }
		}
	}
 drupal_set_message(t('The file has been uploaded.'));	

 }catch (Exception $E){
   //echo 'exception';exit;
	echo $E -> getMessage();
}
 //end try-catch block  
  }
  else {
    drupal_set_message(t('The file could not be uploaded. Please contact the site administrator.'), 'error');
  }
 
	
}	


