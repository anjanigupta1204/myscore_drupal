<?php

function parts_dealer_evaluation_report_download() { 

$path = menu_get_item();
//print_R( $path['map'] );
//die;
/* Array
(
    [0] => parts-dealer-evaluation
    [1] => report
    [2] => download
    [3] => 30001
    [4] => Aug-2016
    [5] => 9479
) */

$report = $path['map'][1];
$pd_code = $path['map'][3];
$evaluation_month = $path['map'][4];
$evaluation_by_staff = $path['map'][5];


if((int)$pd_code && (int)$evaluation_by_staff && $evaluation_month != '' && $report == 'report' ){	
		
	$score_query = db_select('pd_evalution_score', 'score')
		  ->fields('score')	  
		  ->condition('score.pd_code', $pd_code)		  
		  ->condition('score.evalution_by_staff', $evaluation_by_staff)		  
		  ->condition('score.evalution_month', $evaluation_month)		  
		  ->execute();
		  
	
	$rowCount = $score_query->rowCount();

	if($rowCount){		
		
		$score_result = $score_query->fetchAssoc();			
		
		$score_questions = db_query("SELECT 
				(SELECT name from pd_question_category WHERE id = (SELECT parent_id from pd_question_category WHERE id = q.category_id)) parent_category_name,
				(SELECT name from pd_question_category WHERE id = q.category_id) category_name,				
				q.question,
				sq.*,				
				q.question_type							
				FROM `pd_evalution_score_questions`sq LEFT JOIN pd_question q on sq.question_id = q.id WHERE pd_score_id = {$score_result[id]} order by parent_category_name DESC,category_name ASC")->fetchAll(PDO::FETCH_ASSOC);
			/* 	
			echo '<pre>';
			print_R($score_questions);			
			die; */
		
		
			$header[0] = array('Distributor Code',$pd_code,'','','Month:'.$evaluation_month,'','','');
			$header[1] = array('','','','','','','','');
			$header[2] = array(
			'Category',
			'Sub Category',
			'Parameter',	
			'Target',
			'Actual',
			'% Ach',
			'Score',
			'Remarks/Action Plan',
			'Target Date',			
			);
						
			$filename =  "Distributor-Evaluation-$pd_code-$evaluation_month.xls";
			header("Content-Disposition: attachment; filename=$filename");
			header("Content-Type: application/vnd.ms-excel;");
			header("Pragma: no-cache");
			header("Expires: 0");
			$out = fopen("php://output", 'w');
			
			foreach ($header as $data)
			{
				fputcsv($out, $data,"\t");
			}
		
			foreach ($score_questions as $data)
			{	
				
				//print_r($data);
				//die;
				
				unset($data['id']);
				unset($data['question_id']);
				unset($data['pd_score_id']);						
								
				if($data['question_type'] == 1){
					if($data['target'] == 1){
						$data['target'] = 'Yes';
					}else{
						$data['target'] = 'No';
					}						
					$data['actual'] = '';
					$data['actual_par'] = '';
				}
			
				
				unset($data['question_type']);
				unset($data['parent_category_id']);
			
				fputcsv($out, $data,"\t");
			}
			fclose($out);
	
	
	}

}
}