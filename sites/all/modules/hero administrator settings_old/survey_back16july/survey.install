<?php

/**
 * @file
 * Install, update and uninstall functions for the packages module.
 */

/**
 * Implements hook_schema().
 */
function survey_schema() {
 $schema = array();

  $schema['survey_basic_info'] = array(
    'description' => 'Stores survey basic information.',
    'fields' => array(
      'sid' => array(
        'description' => 'Survey id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'survey_name' => array(
        'description' => 'name of survey.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'survey_status_id' => array(
        'description' => 'survey status id.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'asso_dealer_code' => array(
        'description' => 'Associated Dealer name.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'survey_for_dept_code' => array(
        'description' => 'Survey for department code.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'survey_owner_emp_code' => array(
        'description' => 'owner of this survey.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'survey_created_date' => array(
        'description' => 'Survey created date.',
        'type' => 'varchar',
        'length' => 255,
        'default' => '0',
      ), 
      'survey_visited_date' => array(
        'description' => 'Survey visited date.',
        'type' => 'varchar',
        'length' => 255,
        'default' => '0',
      ), 
     'survey_modified_date' => array(
        'description' => 'Survey modified date.',
        'type' => 'varchar',
        'length' => 255,
      ),
     'survey_month' => array(
        'description' => 'Survey month.',
        'type' => 'varchar',
        'length' => 255,
      ),  
      'survey_year' => array(
        'description' => 'Survey year.',
        'type' => 'varchar',
        'length' => 255,
      ),          
      'survey_remark' => array(
        'description' => 'survey remark.',
        'type' => 'text',
        'size' => 'medium',
      ),          
    ),
    'indexes' => array(
      'survey_status_id' => array('survey_status_id',
          'survey_for_dept_code',
          'survey_owner_emp_code'
       ),
    ),
    'primary key' => array('sid'),
  );
  
   $schema['survey_criteria_list'] = array(
    'description' => 'Stores all criteria list associated to  a survey.',
    'fields' => array(
      'criteria_id' => array(
        'description' => 'criteria id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => 'survey id.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'criteria_name' => array(
        'description' => 'criteria name.',
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'indexes' => array(
      'criteria_name' => array('criteria_name'),
    ),
    'primary key' => array('criteria_id'),
  ); 
  
  $schema['survey_question_list'] = array(
    'description' => 'Stores all question list associated to a survey.',
    'fields' => array(
      'question_id' => array(
        'description' => 'question id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'criteria_id' => array(
        'description' => 'criteria id.',
        'type' => 'int',
        'not null' => TRUE,
      ),
     'question_name' => array(
        'description' => 'criteria name.',
        'type' => 'varchar',
        'length' => 255,
      ),
     'question_response' => array(
        'description' => 'question response.',
        'type' => 'int',
      ),
     'question_action_plan' => array(
        'description' => 'question action plan.',
        'type' => 'text',
        'size' => 'medium',
      ),
      'question_action_plan_date' => array(
        'description' => 'question action plan target date.',
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'primary key' => array('question_id'),
    'indexes' => array(
      'criteria_id' => array('criteria_id'),
    ),
  );
  
  $schema['survey_criteria_attachment'] = array(
    'description' => 'Stores all attactment of criteria list.',
    'fields' => array(
      'criteria_id' => array(
        'description' => 'criteria id.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'attachment_id' => array(
        'description' => 'attachment id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'attachment' => array(
        'description' => 'attachment name.',
        'type' => 'varchar',
        'length' => 255,
      ),
     'attachment_owner_id' => array(
        'description' => 'attachment owner emp code.',
        'type' => 'varchar',
        'length' => 255,
      )
    ),
    'primary key' => array('attachment_id'),
    'indexes' => array(
      'criteria_id' => array('criteria_id','attachment_owner_id'),
    ),
  );
  
   $schema['survey_question_list_history'] = array(
    'description' => 'Stores survey question history.',
    'fields' => array(
      'question_id' => array(
        'description' => 'criteria id.',
        'type' => 'int',
        'not null' => TRUE,
      ),
     'question_name' => array(
        'description' => 'criteria name.',
        'type' => 'varchar',
        'length' => 255,
      ),
     'question_response' => array(
        'description' => 'question response.',
        'type' => 'int',
        'not null' => TRUE,
      ),
     'question_action_plan' => array(
        'description' => 'question action plan.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'question_answer_modified_by' => array(
        'description' => 'question modified by.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'question_answer_modified_date' => array(
        'description' => 'question modified date/time.',
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'indexes' => array(
      'question_id' => array('question_id'),
    ),
  );
   $schema['survey_criteria_attachment_history'] = array(
    'description' => 'Stores all attactment of criteria list.',
    'fields' => array(
      'attachment_id' => array(
        'description' => 'attachment id.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'attachment' => array(
        'description' => 'attachment name.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'attachment_modified_by_emp_id' => array(
        'description' => 'Attachment modified Person Emp Id.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'attachment_modified_date' => array(
        'description' => 'attachment date/time.',
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'indexes' => array(
      'attachment_id' => array('attachment_id'),
    ),
  );
  
   $schema['survey_status_type'] = array(
    'description' => 'Stores Survey status type that would used for survey workflow.',
    'fields' => array(
      'survey_status_id' => array(
        'description' => 'Survey status id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'survey_status_type' => array(
        'description' => 'Store Survey Status Type.',
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'indexes' => array(
      'survey_status_type' => array('survey_status_type'),
    ),
    'primary key' => array('survey_status_id'),    
  );
  
  $schema['survey_norms'] = array(
    'description' => 'Stores all norms associated to a survey.',
    'fields' => array(
      'survey_id' => array(
        'description' => 'survey id.',
        'type' => 'int',
        'not null' => TRUE,
      ),
     'designation' => array(
        'description' => 'Designation.',
        'type' => 'varchar',
        'length' => 255,
      ),      
     'require_value' => array(
        'description' => 'Requited Value.',
        'type' => 'varchar',
        'length' => 255,
      ),
     'actual_value' => array(
        'description' => 'Actual Value.',
        'type' => 'varchar',
        'length' => 255,
      ),
     'program_norms' => array(
        'description' => 'Program Norms.',
        'type' => 'varchar',
        'length' => 255,
      ),
     'trained_value' => array(
        'description' => 'Trained Value.',
        'type' => 'varchar',
        'length' => 255,
      ),      
    ),
    'indexes' => array(
      'survey_id' => array('survey_id'),
    ),
  );  
  
  return $schema;
}	
