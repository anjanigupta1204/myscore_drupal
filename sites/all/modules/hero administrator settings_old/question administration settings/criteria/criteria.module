<?php


/**
 * @file
 * Module file for criteria.
 */

/**
 * Implements hook_permission().
 *
 * Since the access to our new custom pages will be granted based on
 * special permissions, we need to define what those permissions are here.
 * This ensures that they are available to enable on the user role
 * administration pages.
 */
function criteria_permission() {
  return array(
        'access criteria category page' => array(
      'title' => t('Access Criteria category page'),
      'description' => t('Allow admin to create/add and update criteria page'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 */
function criteria_menu() {

  // This is the minimum information you can provide for a menu item. This menu
  // item will be created in the default menu, usually Navigation.
    $items['question-admin-settings/criteria'] = array(
    'title' => 'Criteria',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_criteria'),
    'access arguments' => array('access criteria category page'),    
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
    $items['admin/question/criteria'] = array(
    'title' => 'Criteria',
    'description' => t('Configure Criteria for Questions'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_criteria'),
    'access arguments' => array('access criteria category page'),    
  );
   $items['admin/question/criteria/%/edit'] = array(
      'title' => t('Edit Criteria'),
      'page callback' => 'edit_criteria',
      'page arguments' => array(3),
      'access arguments' => array('access criteria category page'),
  );
  
  $items['criteria/%ctools_js/%/edit'] = array(
      'title' => t('Edit Criteria'),
      'page callback' => 'edit_criteria',
      'page arguments' => array(1,2),
      'access arguments' => array('access criteria category page'),
  );  
  return $items;
}

function _load_info_for_criteria() {
	$result = array();
	// Read all fields from the hero_frequency.
    $result['frequency'] = db_select('hero_frequency', 'hf')
       ->fields('hf')
       ->orderBy('frequency_id','ASC')
       ->execute()
       ->fetchAll();
       
       
   // Read all fields from the hero_department.
    $result['department'] = db_select('hero_department', 'hd')
       ->fields('hd')
       ->orderBy('department_id','ASC')
       ->execute()
       ->fetchAll(); 
     
  // Read all fields from the hero_applicability.
   $result['applicability'] = db_select('hero_applicability', 'ha')
       ->fields('ha')
       ->orderBy('applicability_id','ASC')
       ->execute()
       ->fetchAll(); 
   return $result;             
}	

/**
 * Constructs a descriptive page.
 *
 * Our menu maps this function to the path 'account_summary'.
 *
 */
function form_criteria($form, $form_state) {
  // Load frequency, department and applicability to form
  $entries = _load_info_for_criteria();
      
  if(empty($entries['frequency']) || empty($entries['applicability']) || empty($entries['department'])) {
  $form['criteria'] = array(
	'#type' => 'fieldset', 
	'#title' => t('Result'), 
	'#prefix' => '<div class="dealer-form">',
	'#suffix' => '</div>',
	'#weight' => -55, 
	'#collapsible' => FALSE, 
	'#collapsed' => FALSE,
  );  
    $form['criteria']['markup'] = array(
	'#markup' => 'One of field value is empty.', 
  );    
  }      
 else {
  foreach($entries['frequency'] as $key => $val) {
	 $frequency[$val->frequency_id] = $val->frequency_name;
  }	
  foreach($entries['department'] as $key => $val) {
	 $department[$val->department_id] = $val->department_name;
  }	
  foreach($entries['applicability'] as $key => $val) {
	   $applicability[$val->applicability_id] = $val->applicability_name;
  }	     
  $form['criteria'] = array(
	'#type' => 'fieldset', 
	'#title' => t('Click here to add new Criteria'), 
	'#prefix' => '<div class="dealer-form">',
	'#suffix' => '</div>',
	'#weight' => -55, 
	'#collapsible' => TRUE, 
	'#collapsed' => TRUE,
  );
  $form['criteria']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Criteria'),  
	  '#size' => 60,  
	  '#required' => TRUE,
  );
  $form['criteria']['applicability'] = array(
      '#type' => 'checkboxes',
      '#options' => $applicability, 
	  '#title' => t('Applicability'),  
	  '#required' => TRUE,
  );  
  $form['criteria']['department'] = array(
      '#type' => 'checkboxes',
      '#options' => $department, 
	  '#title' => t('Department'),  
	  '#required' => TRUE,
  );  
  $form['criteria']['frequency'] = array(
       '#type' => 'select',
       '#options' =>$frequency, 
       '#default_value' => 1,
	  '#title' => t('Frequency'),    
	  '#required' => TRUE,
  );
  $form['criteria']['add'] = array(
	  '#type' =>'submit', 
	  '#value' => t('Add'), 
	  '#weight' => 19,
  );
}
   $form['admin'] = criteria_list();
  return $form;
}

function form_criteria_validate($form, &$form_state) {
	if(empty($form_state['values']['name'])) {
		form_set_error('term', t('Criteria can not be empty.'));
	}	
}	
/**
 * Submit function for form_example_tutorial_7().
 *
 * Adds a submit handler/function to our form to send a successful
 * completion message to the screen.
 */
function form_criteria_submit($form, &$form_state) {

  if(!empty($form_state['values']['name'])) {
  $criteria_id = db_insert('hero_criteria')
	   ->fields(array(
		'criteria_name' => $form_state['values']['name'],
	 ))
	 ->execute();
  // insert record to hero_criteria_applicability table	
  foreach($form_state['values']['applicability'] as $key => $val) { 
     if($val != '0') {
		 db_insert('hero_criteria_applicability')
		   ->fields(array(
			'criteria_id' => $criteria_id,
			'applicability_id' => $val,
		 ))
		 ->execute();
	}	 
  } 
  // insert record to hero_criteria_department table
 foreach($form_state['values']['department'] as $key => $val) {    
    if($val != '0') { 
     db_insert('hero_criteria_department')
	     ->fields(array(
		  'criteria_id' => $criteria_id,
		  'department_id' => $val,
	 ))
	 ->execute();
	} 
  }	 
  // insert record to hero_criteria_frequency table	 
   	 db_insert('hero_criteria_frequency')
	   ->fields(array(
		'criteria_id' => $criteria_id,
		'frequency_id' => $form_state['values']['frequency'],
	 ))
	 ->execute();
	drupal_set_message(t('New Criteria is added to database.'));
  }
}

function criteria_list() {	
  // Include the CTools tools that we need.
  ctools_include('ajax');
  ctools_include('modal');

  // Add CTools' javascript to the page.
  ctools_modal_add_js();
  
    // Create our own javascript that will be used to theme a modal.
  $sample_style = array(
    'ctools-sample-style' => array(
      'modalSize' => array(
        'type' => 'fixed',
        'width' => 1100,
        'height' => 500,
        'addWidth' => 20,
        'addHeight' => 15,
      ),
      'modalOptions' => array(
        'opacity' => .5,
        'background-color' => '#000',
      ),
      'animation' => 'fadeIn',
      'modalTheme' => 'CToolsSampleModal',
      'throbber' => theme('image', array('path' => ctools_image_path('wait.gif', 'dealer_mapping'), 'alt' => t('Loading...'), 'title' => t('Loading'))),
    ),
  );

  drupal_add_js($sample_style, 'setting');

  // Since we have our js, css and images in well-known named directories,
  // CTools makes it easy for us to just use them without worrying about
  // using drupal_get_path() and all that ugliness.
  ctools_add_js('ctools-ajax-sample', 'dealer_mapping');
  ctools_add_css('ctools-ajax-sample', 'dealer_mapping');    

  // Check if there is sorting request
  if(isset($_GET['sort']) && isset($_GET['order'])){
    // Sort it Ascending or Descending?
    if($_GET['sort'] == 'asc')
      $sort = 'ASC';
    else
      $sort = 'DESC';
    

// Which column will be sorted
    switch($_GET['order']){
      case 'Criteria Name':
        $order = 'criteria_name';
        break;
      default:
        $order = 'criteria_name';
    }
  }
  else{
    // Default sort
    $sort = 'ASC';
    $order = ' criteria_name';
  }	
    
    $tree = db_select('hero_criteria','ha')
             ->fields('ha');
   // Set order by
  $tree->orderBy($order, $sort);
  // Pagination
  $tree = $tree->extend('TableSort')->extend('PagerDefault')->limit(10);    
  $result = $tree->execute();	
  $header = array(
    array('data' => t('S.No.')),
    array('data' => t('Criteria Name'), "field" => "criteria_name"),
    array('data' => t('Operation')),
  );
  $operations = array();
  $operations['edit'] = array(
	'title' => t('Edit'),
	'href' => 'edit',
  );
  $a = 1;
  foreach($result as $key => $val) {
	  $val = (array)$val; 
	  $rows[] = array( 
	    array('data' => t($a.'.')),
		array('data' => t($val['criteria_name'])), 
		array('data' => t(l('Edit',('admin/question/criteria/'.$val['criteria_id'].'/edit')))),
		//array('data' => t(ctools_modal_text_button(t('Edit'), ('criteria/nojs/'.$val['criteria_id'].'/edit'), t('Edit'),  'ctools-modal-ctools-sample-style'))),      
		//array('data' => array('#theme' => 'links__node_operations','#links' => $operations,'#attributes' => array('class' => array('links', 'inline')))),      
	  ); $a++;
  }
  $build['pager_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no date formats found in the db'),
  );

  // attach the pager theme
  $build['pager_pager'] = array('#theme' => 'pager');
  
  return $build;	
}	

function edit_criteria($val) {
	//print $val; exit;
  // Fall back if $js is not set.

    return drupal_get_form('form_criteria_edit', $val);


  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
    'title' => t('Edit Criteria'),
    'ajax' => TRUE,
  );
  $form_state['build_info']['args'] = array('criteria_id' => $val );
  $output = ctools_modal_form_wrapper('form_criteria_edit', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    $output[] = ctools_modal_command_display('Criteria', '<h1 style="text-align:center">Criteria is updated to database.</h1>');
  }
  print ajax_render($output);
  exit;
}

/**
 * Constructs a descriptive page.
 *
 */
function form_criteria_edit($form, $form_state, $val = NULL) {
  /************* get detail of associated terms ***************/
  $data = (isset($val)) ? $val : $form_state['build_info']['args']['criteria_id'];
  
  	$default_applicability = db_select('hero_criteria_applicability','hca')
		 ->fields('hca',array('applicability_id'))
		 ->condition('hca.criteria_id',$data,'=')
		 ->execute()
		 ->fetchAll();
	foreach($default_applicability as $record) {
		$default_applicability_array[] = $record->applicability_id;
	}
		 
	$default_department = db_select('hero_criteria_department','hcd')
	 ->fields('hcd',array('department_id'))
	 ->condition('hcd.criteria_id',$data,'=')
	 ->execute()
	 ->fetchAll();
	foreach($default_department as $record) {
		$default_department_array[] = $record->department_id;
	} 
	 
  	$default_frequency = db_select('hero_criteria_frequency','hcf')
		 ->fields('hcf',array('frequency_id'))
		 ->condition('hcf.criteria_id',$data,'=')
		 ->execute()
		 ->fetchAssoc();		 	 

  	$criteria_name = db_select('hero_criteria','hc')
		 ->fields('hc',array('criteria_name','criteria_id'))
		 ->condition('hc.criteria_id',$data,'=')
		 ->execute()
		 ->fetchAssoc();

  // Load frequency, department and applicability to form
  $entries = _load_info_for_criteria();
      
  if(empty($entries['frequency']) || empty($entries['applicability']) || empty($entries['department'])) {
  $form['criteria'] = array(
	'#type' => 'fieldset', 
	'#title' => t('Result'), 
	'#prefix' => '<div class="dealer-form">',
	'#suffix' => '</div>',
	'#weight' => -55, 
	'#collapsible' => FALSE, 
	'#collapsed' => FALSE,
  );  
    $form['criteria']['markup'] = array(
	'#markup' => 'One of field value is empty.', 
  );    
  }      
 else {
  foreach($entries['frequency'] as $key => $val) {
	 $frequency[$val->frequency_id] = $val->frequency_name;
  }	
  foreach($entries['department'] as $key => $val) {
	 $department[$val->department_id] = $val->department_name;
  }	
  foreach($entries['applicability'] as $key => $val) {
	   $applicability[$val->applicability_id] = $val->applicability_name;
  }	     
  $form['criteria'] = array(
	'#type' => 'fieldset',
	'#title' => t('Edit '.$criteria_name['criteria_name']), 
	'#prefix' => '<div class="dealer-form">',
	'#suffix' => '</div>',
    '#weight' => -55, 
	'#collapsible' => TRUE, 
	'#collapsed' => FALSE,
  );
  $form['criteria_id'] = array('#type' => 'value', '#value' => $criteria_name['criteria_id']);
  $form['criteria']['name'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Criteria'),  
	  '#size' => 60,  
	  '#default_value' => $criteria_name['criteria_name'],
	  '#required' => TRUE,
  );
  $form['criteria']['applicability'] = array(
      '#type' => 'checkboxes',
      '#options' => $applicability, 
	  '#title' => t('Applicability'), 
	  '#default_value' => $default_applicability_array, 
	  '#required' => TRUE,
	  '#disabled' => TRUE,
  );  
  $form['criteria']['department'] = array(
      '#type' => 'checkboxes',
      '#options' => $department,
      '#default_value' => $default_department_array,  
	  '#title' => t('Department'),  
	  '#required' => TRUE,
	  '#disabled' => TRUE,
  );  
  $form['criteria']['frequency'] = array(
      '#type' => 'select',
      '#options' =>$frequency, 
      '#default_value' => $default_frequency['frequency_id'], 
	  '#title' => t('Frequency'),    
	  '#required' => TRUE,
	  '#disabled' => TRUE,
  );
	$form['criteria']['actions'] = array('#type' => 'actions', '#weight' => 100);
	$form['criteria']['actions']['submit'] = array(
		'#type' => 'submit',
		'#submit' => array('_update_survey_criteria'),
		'#value' => t('Update'),
		'#weight' => 5,
	);
	$form['criteria']['actions']['delete'] = array(
		'#type' => 'submit',
		'#submit' => array('_delete_survey_criteria'),
		'#value' => t('Delete'),
		'#weight' => 10,
	);
}
  return $form;
}	

function _update_survey_criteria($form, &$form_state) {
 // print "<pre>"; print_r($form_state['values']);  exit;
  	$criteria_update = db_update('hero_criteria')
	   ->fields(array(	
		'criteria_name' => $form_state['values']['name'],							
	 ))
	 ->condition('criteria_id',$form_state['values']['criteria_id'],'=')
	 ->execute(); 
}

function _delete_survey_criteria($form, &$form_state) {



$query = db_select('hero_questions','hq')
     ->fields('hq')
     ->condition('hq.question_criteria_id', $form_state['values']['criteria_id'], '=')
     ->execute()
     ->fetchAll();
   //print "<pre>"; print_r($query); exit;  
if(!isset($query)) {
  	$hero_criteria_applicability = db_delete('hero_criteria_applicability')
          ->condition('criteria_id', $form_state['values']['criteria_id'])
          ->execute();
	$hero_criteria_department = db_delete('hero_criteria_department')
	  ->condition('criteria_id', $form_state['values']['criteria_id'])
	  ->execute();  
	$hero_criteria_frequency = db_delete('hero_criteria_frequency')
	  ->condition('criteria_id', $form_state['values']['criteria_id'])
	  ->execute(); 
   $hero_criteria = db_delete('hero_criteria')
	  ->condition('criteria_id', $form_state['values']['criteria_id'])
	  ->execute();     
   drupal_goto('question-admin-settings/criteria');	
}	
else {
  $mesage = '<b>First delete all following given below questions related to this criteria.</b>';	
  $mesage .= '<ul>';
  foreach($query as $key => $val) {
	  $mesage .= '<li>'.$val->question_name.'</li>'; 
  }
  $mesage .= '</ul>';	  
  drupal_set_message($mesage,'warning');
  drupal_goto('question-admin-settings/criteria');
}	     

}
