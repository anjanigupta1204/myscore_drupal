<?php

function dealer_score_monthwise_report_for_all() {

    $output = drupal_get_form('get_excel_report_dealer_avg_score_monthly');
    return $output;
}

//module_load_include('inc', 'survey_report_month', 'report_page_1');
module_load_include('inc', 'excel_report', 'report_page_3');

function get_excel_report_dealer_avg_score_monthly($form, &$form_state) {

    $form['#id'] = 'get_excel_report_dealer_avg_score_monthly';
    $form['#tree'] = TRUE;
    global $user;
    $employee = $_SESSION[$user->name];
    
    $multiRole = explode(',', $employee['emp_designation_type']);
    $role = $multiRole['0'];

    $list = array();
    $list['0'] = t('All');
    //echo $employee['emp_code'];exit;

    $year_list = array('2013' => '2013', '2014' => '2014', '2015' => '2015');
    $month_list = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
    $depts = get_depts();
    $form['dealer']['month'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Month:',
        '#options' => $month_list,
    );

    $form['dealer']['excel_year'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Select Year',
        '#options' => $year_list,
    );
if($role=='asmview' || $role=='zoview' || $role=='hoview'){
    $form['dealer']['dept'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Stream',
        '#options' => $depts,
    );
}
    $form['dealer']['excel_actions'] = array('#type' => 'actions');
    $form['dealer']['excel_actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Export'),
        '#submit' => array('report_dealer_avg_score_monthly_submit'),
    );

    return $form;
}

function report_dealer_avg_score_monthly_submit($form, &$form_state) {


    global $user;
    $selected_month = $form_state['values']['dealer']['month'];
    $selected_year = $form_state['values']['dealer']['excel_year'];
    $selected_dept = $form_state['values']['dealer']['dept'];

	
    if ($selected_dept == '1') {
        $dept = 'sales';
    } else {
        $dept = 'services';
    }

    $employee = $_SESSION[$user->name];
    $emp_dept = $employee['emp_department'];   
    if ($emp_dept == 'sales') {
        $emp_dept_code = '1';
    } else {
        $emp_dept_code = '2';
    }
    $emp_code = $employee['emp_code'];


    $multiRole = explode(',', $employee['emp_designation_type']);
    $dealer_code = "";
    if($multiRole['0'] == 'tsm' || $multiRole['0'] == 'tsmview'){
    }else{
	 module_invoke('hero_dashboard', 'get_allrole_survey_details');
	  $emp_array = implode(',',get_allrole_survey_details($employee['emp_code']));
	  }
//echo '<pre>';print_r($user); exit;
    if ($multiRole['0'] == 'tsm' || $multiRole['0'] == 'tsmview') {
      $dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_code = '$emp_code'";
//$dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$selected_dept' AND sbi.survey_status_id = '4' AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code = '$emp_code'";

        $dealer_in_qry_fire = db_query($dealer_in_qry);
        while ($row = $dealer_in_qry_fire->fetchAssoc()) {
            $dealer_code .= $row['dealer_code'] . ',';
        }
    } else if ($multiRole['0'] == 'asm') {
        $under_emps_qry = "SELECT emp_code  FROM hero_hmcl_staff WHERE emp_report_to_designation_code = '$emp_code'";

        $under_emps_qry_fire = db_query($under_emps_qry);
        while ($row1 = $under_emps_qry_fire->fetchAssoc()) {
            $emp_code = $row1['emp_code'];

          // $dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_code = '$emp_code'";
         $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$emp_dept_code' AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code IN ($emp_array)";  

            $dealer_in_qry_fire = db_query($dealer_in_qry);
            while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                // echo $row['dealer_code']."<br>";
                $dealer_code .= $row['dealer_code'] . ',';
            }
        }
    } else if ($multiRole['0'] == 'zo') {
        
          $under_asms_qry = "SELECT emp_code AS asm_code FROM hero_hmcl_staff WHERE emp_report_to_designation_code = '$emp_code'";
        
        $under_asms_qry_fire = db_query($under_asms_qry);
        while ($asms_row = $under_asms_qry_fire->fetchAssoc()) {
            $asm_code = $asms_row['asm_code'];
              //print_r($asm_code ); die;
            $under_emps_qry = "SELECT emp_code  FROM hero_hmcl_staff WHERE emp_report_to_designation_code = '$asm_code'";

            $under_emps_qry_fire = db_query($under_emps_qry);
            while ($row1 = $under_emps_qry_fire->fetchAssoc()) {
                $emp_code = $row1['emp_code'];

               // $dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_code = '$emp_code'";
                $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$emp_dept_code'  AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code IN ($emp_array)";
                // die($dealer_in_qry);
                $dealer_in_qry_fire = db_query($dealer_in_qry);
                while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                    // echo $row['dealer_code']."<br>";
                    $dealer = $row['dealer_code'];

                     $dealer_code .= $row['dealer_code'] . ',';
					
                }
            }
        }
    } else if ($multiRole['0'] == 'asmview') {

        $ofc_area_code = $employee['emp_area_office_code'];
        $tsms_id = get_tsms_for_areacode($ofc_area_code);
        
        foreach($tsms_id as $val) {
            
                $emp_code = $val;

                $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$selected_dept'  AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code IN ($emp_array)";
               
                $dealer_in_qry_fire = db_query($dealer_in_qry);
                while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                    // echo $row['dealer_code']."<br>";
                    $dealer = $row['dealer_code'];

                    $dealer_code .= $row['dealer_code'] . ',';
                }
            }
    }else if ($multiRole['0'] == 'zoview') {
        $tsms = get_tsms_for_zonecode($employee['emp_zone_code']);
        foreach($tsms as $val) {
            
                $emp_code = $val;

                $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$selected_dept'  AND sbi.survey_year = '$selected_year'";
               
                $dealer_in_qry_fire = db_query($dealer_in_qry);
                while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                    // echo $row['dealer_code']."<br>";
                    $dealer = $row['dealer_code'];

                    $dealer_code .= $row['dealer_code'] . ',';
                }
            }
    }else if($multiRole['0']=='hoview'){

  $dealer_in_qry = "SELECT DISTINCT asso_dealer_code FROM survey_basic_info  WHERE survey_year = '$selected_year' AND survey_month ='$selected_month' AND survey_for_dept_code = '$selected_dept'";
               
                $dealer_in_qry_fire = db_query($dealer_in_qry);
				$row = $dealer_in_qry_fire->fetchAll();
				
                foreach($row as $dealercode ) {
                    
                   //print_r($dealercode);
                    $dealer_code .= $dealercode->asso_dealer_code . ',';
                }
				//exit;
}
    
	
    
    $dealer_code = rtrim($dealer_code, ',');
    if($multiRole['0']=='tsm' || $multiRole['0']=='asm' || $multiRole['0']=='zo'){
    if($emp_dept=='sales'){
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($emp_dept=='services'){
        $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
    }}elseif($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){

     if($dept=='sales'){
       
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($dept=='services'){
        $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
      }
	}
	else if($multiRole['0']=='hoview'){
	
    if($dept=='sales'){
       
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($dept=='services'){
         $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
      }
 
}   
   

    $ques_query_fire = db_query($ques_query);
    $ques_query_data = $ques_query_fire->fetchAll();
    $emp_dept = $employee['emp_department'];   
    
	if ($dept == 'sales') {
	$emp_dept_code = '1';
	} else if($dept == 'services') {
	$emp_dept_code = '2';
	}
	if ($emp_dept == 'sales') {
        $emp_dept_code = '1';
    } else if($emp_dept == 'services'){
        $emp_dept_code = '2';
    }
//echo $dept; die;	
if($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){
    $dealer_qry = "SELECT dms.dealer_code, sbi.sid
                FROM hero_dealer_mapping_status AS dms
                JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code)
                WHERE sbi.asso_dealer_code IN ($dealer_code)
                AND sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year' AND dms.dealer_mapped_to_staff_person_department_code = '$dept'";
}else if($multiRole['0']=='tsm' || $multiRole['0']=='zo'||$multiRole['0']=='asm'){
   	
        $dealer_qry = "SELECT DISTINCT dms.dealer_code, sbi.sid
                FROM hero_dealer_mapping_status AS dms
                JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code)
               JOIN survey_question_list AS sq ON(sq.S_id = sbi.sid) WHERE sbi.asso_dealer_code IN ($dealer_code)
                AND sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year' AND sbi.survey_for_dept_code = '$emp_dept_code'"; 

				}else if($multiRole['0']=='hoview'){

     $dealer_qry = "SELECT dms.dealer_code, sbi.sid
                FROM hero_dealer_mapping_status AS dms
                JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code)
				WHERE sbi.asso_dealer_code IN ($dealer_code)
                AND sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year' AND sbi.survey_for_dept_code = '$emp_dept_code' order by dealer_code";   
}


	$dealer_query_fire = db_query($dealer_qry);
    $dealer_query_data = $dealer_query_fire->fetchAll();
    $dataset1 = db_query($dealer_qry);
  
    $header1[] = 'Criteria';
    $header1[] = 'Question';
	 
    while ($row = $dataset1->fetchAssoc()) {
	$sid = $row['sid'];
	$q_query_res1 = "SELECT distinct S_id FROM survey_question_list 
                        WHERE  S_id = '$sid'";
	$sidlist = db_query($q_query_res1);
	$sidn = $sidlist->fetchAll();
	
	
     $siddf[] = $sidn['0']->S_id;
		
     }
	$listid = array_unique($siddf);
	if($listid['0'] == '')
	{
	unset($listid['0']);
	}
	
	foreach($listid as $surveyid)
	{
	$q_query_res1 = "SELECT  asso_dealer_code FROM  survey_basic_info WHERE sid = '$surveyid'";
	$sidlist_new = db_query($q_query_res1);
	while ($rowlist = $sidlist_new->fetchAssoc()) {
			$header[] = $rowlist['asso_dealer_code'];
	  }
	}
	 //print_r($header);
	 //exit;
    foreach ($ques_query_data as $key => $value) {
        foreach ($dealer_query_data as $key1 => $value1) {
		
		//print_r($value);\
		//echo '<pre>';
		//print_r($value1); 
            $criteira[$value->question_criteria_id] = $value->criteria_name;
            $questions[$value->question_id] = $value->question_name;
            
		
	$sid = $value1->sid;
	$question_id = $value->question_id;
	$selectmonth = $selected_month;
	$selectyear = $selected_year;
	$deptment = $selected_dept;		
			
	if ($deptment == '1') {
	$dept = 'sales';
	} else {
	$dept = 'services';
	}

    $employee = $_SESSION[$user->name];
    $emp_dept = $employee['emp_department'];   
    if ($emp_dept == 'sales') {
        $emp_dept_code = '1';
    } else {
        $emp_dept_code = '2';
    }
    $emp_code = $employee['emp_code'];


   
    //echo '<pre>';
	 $q_query_res = "SELECT question_response FROM survey_question_list 
                        WHERE Q_id = '$question_id'
                        AND S_id = '$sid' order by S_id";
	//echo $q_query_res; exit;
	//$sid, $question_id

       $questions_respose_set = db_query($q_query_res);
	   $result = $questions_respose_set->rowCount();
	//echo $value1->dealer_code; exit;
	$header1[] = $value1->dealer_code; 
	
	if($result == 0 )
	{
	   
       $question_response[$value->question_id][$value1->dealer_code] = '';
       
	}else{
	

    while ($question_res = $questions_respose_set->fetchAssoc()) {
	   
     
       $question_response[$value->question_id][$value1->dealer_code] = $question_res['question_response'];
          
	  }
	}	
	
		
		
        }
    }

 
   
  // print_r($question_response); die;
     //print_r($header1); die;
  
    if($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){
        echo 'Year - '.$selected_year."\n";
        echo 'Month - '.$selected_month."\n";
        echo 'Department - '.$dept."\n";
    }
    else if($multiRole['0']=='tsm' || $multiRole['0']=='zo'||$multiRole['0']=='asm'){
        echo 'Year - '.$selected_year."\n";
        echo 'Month - '.$selected_month."\n";
        echo 'Department - '.$emp_dept;
	}
		echo "\t";
		echo "\t";
		echo "Zone";
		echo "\n";
    
    echo "\t";
    echo "\t";
    echo "Dealer Code";
	print("\n");
   
   // $header_count = sizeof($header);
   
   $headerl = array_unique($header1);
	
	foreach($headerl as  $hd){
	 echo $hd  . "\t";
     }
	
	
	
	
	//echo "\t";
    echo "Avg";
    print("\n");
	//echo "<pre>";
//print_r($question_response); die;
    foreach ($question_response as $key => $value) {

        $sum = array_sum($value);
                
       
        
        $query_q_c = "SELECT hc.criteria_name, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id) WHERE  hq.question_id = '$key'";
        $q_c_set = db_query($query_q_c);
        $q_c_data = $q_c_set->fetchAll();
		
        echo $q_c_data[0]->criteria_name . "\t";
        echo $q_c_data[0]->question_name . "\t";
        $count = 0;
		
        foreach ($value as $key1 => $value1) {
           
            echo $value1 . "\t";
            if (isset($value1) && $value1 !='') {
                $count++;
            }
        }
		//exit;
     //echo $sum;
     //echo $count; 
	$space_count =  sizeof($header)-(sizeof($value)+2);
		for ($i = 0; $i < $space_count; $i++) {
		   echo "\t";
		}
        $avg = $sum / $count;
        echo round($avg, 2);
        print("\n");
    }
    $date = date("d-m-Y");
    $filename = 'Dealers-' . $date . '.xls';


    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename");
    exit;
}

function get_question_response($sid, $question_id, $selectmonth, $selectyear, $deptment) {

 if ($deptment == '1') {
        $dept = 'sales';
    } else {
        $dept = 'services';
    }

    $employee = $_SESSION[$user->name];
    $emp_dept = $employee['emp_department'];   
    if ($emp_dept == 'sales') {
        $emp_dept_code = '1';
    } else {
        $emp_dept_code = '2';
    }
    $emp_code = $employee['emp_code'];


    $multiRole = explode(',', $employee['emp_designation_type']);
 
	if($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){
	 $q_query1 = "SELECT Distinct dms.dealer_code, sbi.sid FROM hero_dealer_mapping_status AS dms 
	LEFT JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code) 
	WHERE sbi.asso_dealer_code ='".$question_id."'
	AND sbi.survey_status_id = '4' AND sbi.survey_month = '".$selectmonth."' AND sbi.survey_year = '".$selectyear."' AND dms.dealer_mapped_to_staff_person_department_code = '$dept' ";
	}else if($multiRole['0']='tsm' || $multiRole['0']='zo'||$multiRole['0']='asm'){
    $q_query1 = "SELECT Distinct dms.dealer_code, sbi.sid FROM hero_dealer_mapping_status AS dms 
	LEFT JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code) 
	WHERE sbi.asso_dealer_code ='".$question_id."'
	AND sbi.survey_status_id = '4' AND sbi.survey_month = '".$selectmonth."' AND sbi.survey_year = '".$selectyear."' ";
	}
	 $questions_respose_set1 = db_query($q_query1);
	$question_res1 = $questions_respose_set1->fetchAssoc();
    
	$q_query_res = "SELECT question_response FROM survey_question_list 
                        WHERE Q_id = '$question_id'
                        AND S_id = '$sid'";
	//echo $q_query; exit;
	//$sid, $question_id
    $questions_respose_set = db_query($q_query_res);
    while ($question_res = $questions_respose_set->fetchAssoc()) {

        return $question_res['question_response'];
    }
}

// function to get the asmview email_ids based on area code
function get_tsms_for_areacode($ofc_area_code) {

    $qry = "SELECT emp_code FROM hero_hmcl_staff WHERE emp_designation_type = 'tsm' AND emp_area_office_code = '" . $ofc_area_code . "'";

    $data_for_asmview = db_query($qry);

    $asmview_email_data = $data_for_asmview->fetchAll();

    foreach ($asmview_email_data as $key => $value) {
        $tsms_ids[$value->emp_code] = $value->emp_code;
    }


    return $tsms_ids;
}
function get_tsms_for_zonecode($zone_code){
    $qry = "SELECT emp_code FROM hero_hmcl_staff WHERE emp_designation_type = 'tsm' AND emp_zone_code = '" . $zone_code . "'";
    
    $data_for_asmview = db_query($qry);

    $asmview_email_data = $data_for_asmview->fetchAll();

    foreach ($asmview_email_data as $key => $value) {
        $tsms_ids[$value->emp_code] = $value->emp_code;
    }


    return $tsms_ids;
    
}