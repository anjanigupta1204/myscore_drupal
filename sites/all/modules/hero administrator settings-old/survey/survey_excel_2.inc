<?php

function excel_export_for_survey() {

global $user;
 
//ob_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');
 require_once('Classes/PHPExcel.php');


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


 
	$employee = $_SESSION[$user->name];

	 $uri = $_SERVER['REQUEST_URI'];
   $urlparam = explode('/', $uri);
  //$date = date('Y-m-d');
  $date = date('d/m/Y');
   
	$dealer_code = $urlparam['3'];
  $dealer_detail = db_select('hero_dealer', 'hd')
	->fields('hd')
	->condition('dealer_code', $dealer_code,'=')
	->execute()
	->fetchAssoc();
	
$header = array('Criteria_id','Criteria_name','Question_id','Question_name','Response/available','Trained','action_plan','action_plan_date');
$header_count = count($header);
    for ($i = 0; $i < $header_count; $i++) {
	 echo $header[$i] . "\t";
		
    }
	
	
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Criteria_id');
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Criteria_name');
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Question_id');
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Question_name');
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Response');
   // $objPHPExcel->getActiveSheet()->setCellValue('F3', 'Trained');
    $objPHPExcel->getActiveSheet()->setCellValue('F3', 'action_plan');
    $objPHPExcel->getActiveSheet()->setCellValue('G3', 'action_plan_date');
 
	
	
	
	 //print("\n"); 
	 
	    if($employee['emp_department'] == 'sales') {
		$sales_norms_fields = array('gm' => 'General manager',
		           'Showroom_manager' => 'Showroom manager',
				   'network_manager' => 'Network manager',
		           'receptionist' => 'Receptionist',
				   'hostess' => 'Hostess',
		           'Sales_executive' => 'Sales Executive',
				   'goodlife_executive' => 'GoodLife Executive',
		           'marketing_executive' => 'Marketing Executive',
		           'rural_support_sales' => 'Rural Support - Sales',
				   'accountant' => 'Accountant',
				   'billing_executive' => 'Billing Executive',
				   'cashier' => 'Cashier',
				   'it_support_executive' => 'IT Support Executive',
		           'delivery_executive' => 'Delivery Executive',
				   'pdi_technician' => 'PDI Technician',
				   'rto_executive' => 'RTO Executive',
				   'network_executive' => 'Network Executive',
				   'housekeeping_staff' => 'Housekeeping Staff',
				   'pantry' => 'Pantry',
				   'security' => 'Security',
		);
		
		$get_sales_norms = db_select('hero_sales_manpower_norms', 'h')
		                     ->fields('h')
		                     ->execute();
		 
		$record = $get_sales_norms->fetchAll(); 
					   $dealer_sales_vol_by_12 = round($dealer_detail['dealer_sales_vol']/12);
$norms = array();

		foreach($record as $key => $val) {
			if(($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($dealer_sales_vol_by_12 <= $val->appliacbility_criteria_max)) {
				$norms = $record[$key];
			}elseif(($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($val->appliacbility_criteria_max == NULL)) {
				$norms = $record[$key];
			}
		}
		
		
		//$i=3;
		foreach($sales_norms_fields as $key => $val) {
		//$i++;
		    echo  '0' . "\t";
			echo 'NA' . "\t";
			echo  '0' . "\t";
			echo  $val . "\t";
			echo  $norms->$key . "\t";
			echo 'NA' . "\t";
			echo '' . "\t";
			echo $date . "\t";
			
			//$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '0');
			//$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'NA');
//$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '0');
			//$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val);
			//$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $norms->$key);
			//$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'NA');
			//$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '');
			//$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $date);
			
			
			
			
			print("\n");
		}
		
		}
		 elseif($employee['emp_department'] == 'services') {		   
 		  $services_norms = db_select('hero_service_manpower_norms','hsmn')
			   ->fields('hsmn')
			   ->execute()
			   ->fetchAll();
		foreach($services_norms as $key => $val) {
		// code for services norms	
		switch ($key):
		case 0:
			$format = ($dealer_detail['dealer_service_vol'] >= 1000 ? 1 : 0);
		case 1:
			$format = 1;
			break;
		case 2:
			$format = $dealer_detail['dealer_service_vol']/(26*40*12);
			break;
		case 3:
			$format = $dealer_detail['dealer_service_vol']/(26*20*12);
			break;
		case 4:
			$format = 1;
			break;
		case 5:
		// For Technician (no . of ramps + 1) 28-12-13
		    $format = $dealer_detail['no_of_ramps']+1 ; 
			//$format = ($dealer_detail['dealer_service_vol']/(26*10*12))+1;
			break;
		case 6:
		 //For Assistant Technician (no . of ramps + 1)/2 28-12-13 
		    $format = round(($dealer_detail['no_of_ramps']+1)/2);
			//$format = (($dealer_detail['dealer_service_vol']/(26*10*12))+1)/2;
			break;
		case 7:
			$format = $dealer_detail['dealer_service_vol']/(26*20*12);
			break;
		case 8:
			$format = 1;
			break;	
		case 9:
			$format = 1;
			break;	
		case 10:
			$format = $dealer_detail['dealer_service_vol']/(26*50*12);
			break;								
	endswitch;
			echo  '0' . "\t";
			echo 'NA' . "\t";
			echo  '0' . "\t";
			echo  $val->designation . "\t";
			echo  round($format) . "\t";
			echo 'NA' . "\t";
			echo '' . "\t";
			echo $date . "\t";
				
			print("\n");
						
		//	$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '0');
		//	$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'NA');
		//	$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '0');
		//	$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $val->designation);
		//	$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, round($format));
		//	$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'NA');
		//	$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '');
		//	$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $date);
		
	
	}
	
			   }
			   

		//print "<pre>"; print_r($norms_fields); exit; 	   
			   $current_month = get_survey_month();
	// Piyush 03-01-14 frequency_criteria
	$quarterly = array('03','06','09','12');
	$monthly = array('01','02','03','04','05','06','07','08','09','10','11','12');
	$halfYearly = array('03','09');
	$yearly = array('03');
	// select criteria for this month.
	      if( in_array($current_month, $quarterly) && in_array($current_month, $halfYearly) && in_array($current_month, $yearly) ){
	         $freq = 'All';
	       }else if( in_array($current_month, $quarterly) && in_array($current_month, $monthly) && in_array($current_month, $halfYearly) ){
	       $freq = 'quart_month_half';
	       }else if( in_array($current_month, $quarterly) && in_array($current_month, $monthly) ){
	       $freq = 'quart_month';
	       }else if( in_array($current_month, $quarterly) ){
	       $freq = 'Quarterly';
	       }else if( in_array($current_month, $monthly) ){
	       $freq = 'Monthly';
	       }else if( in_array($current_month, $halfYearly)){
	       $freq = 'Half-Yearly';
	       }else if( in_array($current_month, $yearly) ){
	       $freq = 'Yearly';
	       }
	// check 
	if($freq == 'All') {
		$query = db_select('hero_criteria_frequency','hcf')
					->condition('hcf.frequency_id', array('1','2','3','4'));
	}else if($freq == 'Quarterly') {
		$query = db_select('hero_criteria_frequency','hcf')
					->condition('hcf.frequency_id', array('1','4'));
	}else if($freq == 'Monthly'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('4'));
	 }else if($freq == 'Half-Yearly'){  
		$query = db_select('hero_criteria_frequency','hcf')
		           ->condition('hcf.frequency_id', array('3','4'));
	 }else if($freq == 'Yearly'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('2','4'));
	 }else if($freq == 'quart_month_half'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('1','3','4'));
	 }else if($freq == 'quart_month'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('1','4'));
	 }
  	
  // end frequency 03-01-14
    $query->join('hero_criteria_department', 'hcd', 'hcf.criteria_id = hcd.criteria_id');
    if($employee['emp_department'] == 'sales') {
        $query->condition('hcd.department_id', '1','=');
    }
    elseif($employee['emp_department'] == 'services') {
		$query->condition('hcd.department_id', '2','=');
	}
	 $query->join('hero_criteria_applicability', 'hca', 'hcd.criteria_id = hca.criteria_id');

   // if($dealer_detail['dealer_applicability'] == 'Vlead') {
   // Add For vlead (30-12-13) Piyush Rai
     if($dealer_detail['dealer_applicability'] == '1')
     {
		$and = db_and();
		$or = db_or();
		  // only for vlead questions 
		 $and->condition('hca.applicability_id', '1','=');
		 $or->condition('hca.applicability_id', '2','=');
		 $or->condition('hca.applicability_id', '3','=');
		 $or->condition('hca.applicability_id', '4','=');
		 $or->condition('hca.applicability_id', '5','=');
		 $or->condition('hca.applicability_id', '6','=');
		 $or->condition($and);
		 $query->condition($or);
	 }
	// Add non Vlead Applicability (30-Dec-13)- Piyush Rai
	else{
	    $or = db_or();
	    $or->condition('hca.applicability_id', '1','<>');
		$or->condition('hca.applicability_id', '2','=');
		$or->condition('hca.applicability_id', '3','=');
		$or->condition('hca.applicability_id', '4','=');
		$or->condition('hca.applicability_id', '5','=');
		$or->condition('hca.applicability_id', '6','=');
		$query->condition($or);
	 }
  // end non vlead
          $query->fields('hcd',array('criteria_id'));
          $query->orderBy('hcd.criteria_id','ASC');
         $result = $query->execute();
		 
		 if($employee['emp_department'] == 'sales') {
            $dept = 1;
	   }
	   elseif($employee['emp_department'] == 'services') {
            $dept = 2;
	   } 
	$i=3;   
	   //fetch all question related to staff member department
		$question_assoc_to_dept = db_select('hero_questions_department','hqd')
			   ->fields('hqd',array('question_id'))
			   ->condition('hqd.department_id', $dept,'=')
			   ->execute()
			   ->fetchAll();    
       $question_ids = array();
       foreach($question_assoc_to_dept as $key => $val) {
		   $question_ids[$val->question_id] = $val->question_id; 
		   
	   } 
	  
	   
	    foreach($result as $record) {
		 
        $output = db_select('hero_criteria','hc')
           ->fields('hc', array('criteria_name'))
	      ->condition('criteria_id',$record->criteria_id,'=')
		  ->groupBy('hc.criteria_name')
		  ->orderBy('criteria_id', 'ASC') 
	      ->execute()
	      ->fetchAssoc(); 
      
	  $prev_criteria =db_select('survey_criteria_list','scl')
           ->fields('scl', array('criteria_id'))
	      ->condition('criteria_name',$output['criteria_name'],'=')
	      ->condition('sid',$previous_survey['sid'],'=')
	      ->execute()
	      ->fetchAssoc(); 
	      
	   // print "<pre>"; print_r($output);  
      
		$form[$record->criteria_id] = array();
		$form['#tree'] = TRUE;	
		//print_r($question_ids); 
		  
			 
			
       foreach($question_ids as $key => $val) {
	   // print_r($val);
		 $survey_question = db_select('hero_questions','hq')
           ->fields('hq')
	       ->condition('question_id',$val,'=')
	       ->condition('question_criteria_id',$record->criteria_id,'=')
	       ->execute()
	       ->fetchAssoc();
		    if(!empty($survey_question)) {
		  $i++;
		   $q_id = $survey_question[question_id];
		   $q_ans = $survey_question[question_name];
		   echo  $record->criteria_id . "\t";
			
			$criid=$output['criteria_name'];
			
			$criid1=explode(",",$criid);
			$criid2=implode(" ",$criid1);
			
			//echo $output['criteria_name'] . "\t";
		  echo $criid2 . ",";

		  echo  $q_id . "\t";
			
		      $q_ans1=explode(",",$q_ans);
			$q_ans2=implode(" ",$q_ans1);
			
			echo  $q_ans2 . "\t";
			
			
			 echo  '' . "\t";
			echo 'NA' . "\t";
			echo '' . "\t";
			echo $date . "\t";
			$datefinal=explode('/',$date);
			$datechange=$datefinal['0'].'.'.$datefinal['1'].'.'.$datefinal['2'];
			
			//print("\n");
		
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $record->criteria_id);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $criid2);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $q_id);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $q_ans2 );
			
			
$objValidation = $objPHPExcel->getActiveSheet()->getCell('E'.$i, '')->getDataValidation();
$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
$objValidation->setAllowBlank(false);
$objValidation->setShowInputMessage(true);
$objValidation->setShowErrorMessage(true);
$objValidation->setShowDropDown(true);
$objValidation->setErrorTitle('Input error');
$objValidation->setError('Value is not in list.');
//$objValidation->setPromptTitle('Pick from list');
//$objValidation->setPrompt('Please pick a value from the drop-down list.');
$objValidation->setFormula1('"Yes,No"'); 




//$objValidation = $objPHPExcel->getActiveSheet()->getCell('G'.$i)->getDataValidation();

//$objValidation->setType( PHPExcel_Cell_DataValidation::TYPE_DATE);
//$objValidation->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_STOP );
//$objValidation->setAllowBlank(true);
//$objValidation->setShowInputMessage(true);
//$objValidation->setShowErrorMessage(true);
//$objValidation->setErrorTitle('Input error');
//$objValidation->setError('Only Date is permitted!');
//$objValidation->setPromptTitle('Allowed input');
//$objValidation->setPrompt('Only dates between 01/01/2011 and 31/03/2011 are allowed.');
//$objValidation->setFormula1(40544);
//$objValidation->setFormula2(40633);



			
			
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '');
		




   

		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '');

//bjPHPExcel->getActiveSheet()        
  //->getStyle('G'.$i)
   //>getNumberFormat()
   //>setFormatCode('dd/mm/yyyy');

//$objPHPExcel->getActiveSheet()        
 // ->getStyle('G'.$i)
  //->getNumberFormat()
  //->setFormatCode('dd/mm/yyyy');




   
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $datechange );
			
			
			
			 unset($question_ids[$key]);
			// break;
			}
		  
		   }
		   
		   
$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);




$objPHPExcel->getActiveSheet()->getProtection()->setAutoFilter(true);
$objPHPExcel->getActiveSheet()->getProtection()->setDeleteColumns(true);
$objPHPExcel->getActiveSheet()->getProtection()->setDeleteRows(true);
//bjPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
//$objPHPExcel->getActiveSheet()->getProtection()->setFormatColumns(true);
//$objPHPExcel->getActiveSheet()->getProtection()->setFormatRows(true);
$objPHPExcel->getActiveSheet()->getProtection()->setInsertColumns(true);
$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
$objPHPExcel->getActiveSheet()->getProtection()->setObjects(true);
$objPHPExcel->getActiveSheet()->getProtection()->setPivotTables(true);
$objPHPExcel->getActiveSheet()->getProtection()->setScenarios(true);
$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
$objPHPExcel->getActiveSheet()->getProtection()->setInsertHyperlinks(true);








//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
//$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);







$objPHPExcel->getActiveSheet()
	->getStyle('E4:E'.$i)
	->getProtection()->setLocked(
		PHPExcel_Style_Protection::PROTECTION_UNPROTECTED
	);

$objPHPExcel->getActiveSheet()
	->getStyle('F4:F'.$i)
	->getProtection()->setLocked(
		PHPExcel_Style_Protection::PROTECTION_UNPROTECTED
	);
	

$objPHPExcel->getActiveSheet()
	->getStyle('G4:G'.$i)
	->getProtection()->setLocked(
		PHPExcel_Style_Protection::PROTECTION_UNPROTECTED
	);	
	
	$objPHPExcel->setActiveSheetIndex(0);
	   
		  
			
	  }
	 // exit;
			// module_invoke('survey', 'theming_dealer_survey_form');
	
   $filename = $urlparam[3].'_'.$urlparam[4].'.xls';	
			   
	 //$filename = 'Dealers-' . $date . '.xls';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

ob_end_clean();

header("Content-Type: application/vnd.ms-excel");
//header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

header("Content-Disposition: attachment;filename=$filename");
header("Cache-Control: max-age=0");

$objWriter->save('php://output');

    exit;
}






?>