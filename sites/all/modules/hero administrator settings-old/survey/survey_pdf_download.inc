<?php

function get_dealer_code_from_url($file_link)
{
   $linkparam = explode('/', $file_link);
    $count = count($linkparam);
    $dealer_code = $linkparam[3];
	return $dealer_code;
}

/************Download pdf for Edit_survey and View_survey ******************/

function download_pdf_for_view_survey()
{
ob_start();
$survey_id =  get_last_segment($_SERVER['REQUEST_URI']);
 $survey_data = db_select('survey_basic_info','sbi')
            ->fields('sbi')
            ->condition('sid',$survey_id,'=')
            ->execute()
            ->fetchAssoc();	
	$dealer_code = $survey_data['asso_dealer_code'];
	$created_date = date("d-m-Y H:i:s",$survey_data['survey_created_date']);
	$visited_date = $survey_data['survey_visited_date'];
	$survey_month = $survey_data['survey_month'];
	if($survey_data['survey_for_dept_code'] == 1)
	{
	 $dept = 'Sale';
	 }else{
	 $dept = 'Service';
	 }
	 $employee_data = db_select('hero_hmcl_staff','hhs')
            ->fields('hhs')
            ->condition('emp_code',$survey_data['survey_owner_emp_code'],'=')
            ->execute()
            ->fetchAssoc();
	 $emp_name = $employee_data['emp_name'];
	$dealer_data = db_select('hero_dealer','hd')
            ->fields('hd')
            ->condition('dealer_code',$dealer_code,'=')
            ->execute()
            ->fetchAssoc();	
	$dealer_name = $dealer_data['dealer_name'];
	$score = $survey_data['survey_score_percent'];
   $html = <<<EOD
   <p >Score = $score %</p>
   <div class="import-notes">Guidelines.<ul>
	  <li>Response to all questions is mandatory.</li><li>Providing action plan with time line is mandatory in case selected option is "NO".  
	   </li><li>If equipments are operational then only marks to be given.</li>
	   <li>If the candidate has undergone the training mentioned in program norm only then he is to be considered as Trained.</li>
</ul></div>
<table cellspacing="0" cellpadding="1" border="1">
           <tr style="background-color:#000;color:#fff;">
           <th align="center"> Dealer Code</th>
           <th align="center">Dealer Name</th>
           <th align="center">Department</th>
		   <th align="center">Created By</th>
		   <th align="center">Visit Date</th>
		   <th align="center">Start Date</th>
       </tr>
	   <tr >
           <td align="center">$dealer_code</td>
           <td align="center">$dealer_name</td>
           <td align="center">$dept</td>
		   <td align="center">$emp_name</td>
		   <td align="center">$visited_date</td>
		   <td align="center">$created_date</td>
       </tr>

EOD;

// sale upload data 
$html .= <<<EOD
       </table>
         <p>$dept Data</p>	   
		  <table cellspacing="0" cellpadding="1" border="1">
           <tr>
           <th align="center">Business Parameters</th>
           <th align="center" colspan="2" >Last Year</th>
           <th align="center" colspan="6">Current Year</th>
		   
       </tr>
      <tr>
        <td align="center"></td>
		<td align="center"><b>Annual</b></td>
		<td align="center"><b>YTM</b></td>
		<td align="center"><b>Target</b></td>
		<td align="center"><b>YTM Target</b></td>
		<td align="center"><b>YTM Arch</b></td>
		<td align="center"><b>% Ach</b></td>
		<td align="center"><b>Current Month Target</b></td>
		<td align="center"><b>Remarks</b></td>
       </tr>
EOD;

			 $query = db_select('sale_product_info','spi');
			 $query->join('sale_product','sp','sp.sale_product_id = spi.sale_product_id');
			 $query->fields('spi')
		           ->fields('sp',array('sale_product_name'));
			 $query->condition('spi.dealer_code', $dealer_code);
             $query->condition('current_data_entry_month',$survey_month,'=');
           $dealer_sales_results = $query->execute();			 

	foreach($dealer_sales_results as $key => $val)
    {
 //echo '<pre>'; print_r($val->dealer_code); exit;
    $current_year_ytm_target = round($val->current_year_ytm_target);
 $current_year_ytm_achieved = round($val->current_year_ytm_achieved);
    $percent_achieved = round(($current_year_ytm_achieved/$current_year_ytm_target) * 100);
	$product_name = ucfirst($val->sale_product_name);
	$html .= <<<EOD
	<tr>
        <td align="center">$product_name</td>
		<td align="center">$val->last_year_annual_achieved</td>
		<td align="center">$val->last_year_ytm_achieved</td>
		<td align="center">$val->current_year_annual_target</td>
		<td align="center">$val->current_year_ytm_target</td>
		<td align="center">$val->current_year_ytm_achieved</td>
		<td align="center">$percent_achieved</td>
		<td align="center">$val->current_monnth_target</td>
		<td align="left">$val->sale_product_remark</td>
    </tr>
EOD;
			
 }
// end sale upload data
// Start data for Norms
 if($survey_data['survey_for_dept_code'] == 1)
{ 
$html .= <<<EOD
</table><p> Sale Norms</p>
   <table cellspacing="0" cellpadding="1" border="1">
           <tr style="background-color:#000;color:#fff;">
           <th >Designtion</th>
           <th align="center">Required as per Norms</th>
           <th align="center">Available(Nos)</th>
		   <th align="center">Action Plan(TSM)</th>
		   <th align="center">Action Plan(ASM)</th>
		   <th align="center">Action Plan(ZM)</th>
		   <th align="center">Target Date</th>
       </tr>
EOD;
 $norms_data = db_select('survey_norms','sn')
            ->fields('sn')
            ->condition('survey_id',$survey_id,'=')
            ->execute();
	foreach($norms_data as $norms)	
  {
    $sales_desig = $norms->designation;
    $sales_require_val = is_decimal($norms->require_value) ? round($norms->require_value) : $norms->require_value;
	$sales_actual_val = $norms->actual_value;
	$sales_action_plan = $norms->action_plan;
	$sales_action_plan_asm = $norms->action_plan_asm;
	$sales_action_plan_zm = $norms->action_plan_zm;
	$sales_target_date = $norms->target_date;
  
$html .= <<<EOD
       <tr>
        <td>$sales_desig</td>
		<td align="center">$sales_require_val</td>
		<td align="center">$sales_actual_val</td>
		<td align="center">$sales_action_plan</td>
		<td align="center">$sales_action_plan_asm</td>
		<td align="center">$sales_action_plan_zm</td>
		<td align="center">$sales_target_date</td>
       </tr>
EOD;
}
}
elseif($survey_data['survey_for_dept_code'] == 2)
{ 
$html .= <<<EOD
</table><p> Service Norms</p>
   <table cellspacing="0" cellpadding="1" border="1">
           <tr style="background-color:#000;color:#fff;">
                  <th align="center">Designation</th>
                  <th align="center">Required as per Norms</th>
                  <th align="center">Available(Nos)</th>
				  <th align="center">Program Norms</th>
				  <th align="center">Trained Avl.(Nos)</th>
				  <th align="center">Action Plan(TSM)</th>
				  <th align="center">Action Plan(ASM)</th>
				  <th align="center">Action Plan(ZM)</th>
				  <th align="center">Target Date</th>
                  </tr>
EOD;
 $norms_data = db_select('survey_norms','sn')
            ->fields('sn')
            ->condition('survey_id',$survey_id,'=')
            ->execute();
	foreach($norms_data as $norms)	
  {
    $service_desig = $norms->designation;
    $service_require_val = is_decimal($norms->require_value) ? round($norms->require_value) : $norms->require_value;
	$service_actual_val = $norms->actual_value;
	$program_norms = $norms->program_norms;
	$trained_value = $norms->trained_value;
	$service_action_plan = $norms->action_plan;
	$service_action_plan_asm = $norms->action_plan_asm;
	$service_action_plan_zm = $norms->action_plan_zm;
	$service_target_date = $norms->target_date;
  
$html .= <<<EOD
       <tr>
        <td>$service_desig</td>
		<td align="center">$service_require_val</td>
		<td align="center">$service_actual_val</td>
		<td align="center">$program_norms</td>
		<td align="center">$trained_value</td>
		<td align="center">$service_action_plan</td>
		<td align="center">$service_action_plan_asm</td>
		<td align="center">$service_action_plan_zm</td>
		<td align="center">$service_target_date</td>
       </tr>
EOD;
}
}
	
 // Criteria question
$criteria_data = db_select('survey_criteria_list','scl')
            ->fields('scl')
            ->condition('sid',$survey_id,'=')
            ->execute(); 
			//print_r($criteria_data);exit;
foreach($criteria_data as $criteria_detail) {
 $criteria_id = $criteria_detail->criteria_id;	 
$html .= <<<EOD
</table>
<p style="background-color:#d2d2d2;color:#000;font-size:11px;"><strong>$criteria_detail->criteria_name</strong></p>
<table cellspacing="0" cellpadding="1" border="1">
<tr style="background-color:#000;color:#fff;">
<th align="center">Parameter</th>
<th align="center">Response</th>
<th align="center">Action Plan(TM)</th>
<th align="center">Action Plan(ASM)</th>
<th align="center">Action Plan(ZM)</th>
<th align="center">Target Date</th>
</tr>
EOD;

$question_data = db_select('survey_question_list','s')
            ->fields('s')
            ->condition('criteria_id',$criteria_id,'=')
            ->execute(); 
foreach($question_data as $question){
 $question_name = $question->question_name; 
$responce_value = $question->question_response;
if(isset($question_name)){
if($responce_value == '0'){
$responce = 'No';
}elseif($responce_value == '5'){
$responce = 'Yes';
}elseif($responce_value == NULL){
$responce = '';
}
$action_plan = $question->question_action_plan;
$action_plan_asm = $question->question_action_plan_asm;
$action_plan_zm = $question->question_action_plan_zm;
$action_plan_date = $question->question_action_plan_date;
 $html .= <<<EOD
<tr>
<td align="left">$question_name</td>
<td align="center">$responce</td>
<td align="left">$action_plan</td>
<td align="left">$action_plan_asm</td>
<td align="left">$action_plan_zm</td>
<td align="center">$action_plan_date</td>
</tr>
 <!-----END sales departmant------> 
EOD;
} // end question condition
}
}
$remark = $survey_data['survey_remark'];
//$remark_asm = $survey_data['survey_remark_asm'];
//$remark_zm = $survey_data['survey_remark_zm'];
$html .= <<<EOD
</table>
<p>&nbsp; </p>
<table cellspacing="0" cellpadding="1" border="1">
<tr style="background-color:#000;color:#fff;"><th align="center">Remark</th></tr>
<tr><td align="left">$remark</td></tr>
</table>
EOD;
 
 require_once DRUPAL_ROOT ."/sites/all/modules/print/lib/tcpdf/tcpdf.php";
  $filename = 'myScore_'.$dealer_code.'.pdf';
  $pdf = new TCPDF();
  // write data
   $pdf->AddPage('P');
	$pdf->SetFont('helvetica', '', 8);
   $pdf->writeHTML($html,true, false, false, false, '');
   // end write
   //$pdf->Output($filename, 'D');
   die($pdf->Output($filename, 'I'));
 //} 
}	

/************Download pdf for Start_survey ******************/

function download_pdf_for_start_survey()
{
ob_start();
global $user;
$employee = $_SESSION[$user->name];

$dealer_code = get_dealer_code_from_url($_SERVER['REQUEST_URI']);
$dealer_detail = db_select('hero_dealer','hd')
                ->fields('hd')
				->condition('hd.dealer_code',$dealer_code, '=')
				->execute()
	            ->fetchAssoc();
 $query = db_select('hero_dealer_mapping_status','hdms');	
         $query->join('hero_hmcl_staff','hhs','hdms.dealer_mapped_to_staff_person_code = hhs.emp_code');
         $query->condition('dealer_code',$dealer_code,'=');
		 $query->fields('hdms', array('dealer_code','dealer_Name'));
		 $query->fields('hhs', array('emp_code','emp_name'));
		$dealer_mapping_details = $query->execute()->fetchAssoc();
		 ///
		//print_r($dealer_mapping_details);exit;			   

	if($employee['emp_department'] == 'sales') {
            $dept = 1;
			$department = 'Sale';
	   }
	   elseif($employee['emp_department'] == 'services') {
            $dept = 2;
			$department = 'Service';
	   } 
	   

		 
$dealer_name = $dealer_detail['dealer_name'];
$emp_name = $dealer_mapping_details['emp_name'];
$visited_date = date('d-m-Y', time());
$created_date = date("d-m-Y H:i:s",REQUEST_TIME);
		  $html = <<<EOD
   <div class="import-notes">Guidelines.<ul>
	  <li>Response to all questions is mandatory.</li><li>Providing action plan with time line is mandatory in case selected option is "NO".  
	   </li><li>If equipments are operational then only marks to be given.</li>
	   <li>If the candidate has undergone the training mentioned in program norm only then he is to be considered as Trained.</li>
</ul></div>
<table cellspacing="0" cellpadding="1" border="1">
           <tr style="background-color:#000;color:#fff;">
           <th align="center"> Dealer Code</th>
           <th align="center">Dealer Name</th>
           <th align="center">Department</th>
		   <th align="center">Created By</th>
		   <th align="center">Visit Date</th>
		   <th align="center">Start Date</th>
       </tr>
	   <tr >
           <td align="center">$dealer_code</td>
           <td align="center">$dealer_name</td>
           <td align="center">$department</td>
		   <td align="center">$emp_name</td>
		   <td align="center">$visited_date</td>
		   <td align="center">$created_date</td>
       </tr>

EOD;

// sale upload data 

$html .= <<<EOD
       </table> 
         <p>$department Data</p>	   
		  <table cellspacing="0" cellpadding="1" border="1">
           <tr style="">
           <th align="center">Business Parameters</th>
           <th align="center" colspan="2" >Last Year</th>
           <th align="center" colspan="6">Current Year</th>
		   
       </tr>
      <tr>
        <td align="center"></td>
		<td align="center"><b>Annual</b></td>
		<td align="center"><b>YTM</b></td>
		<td align="center"><b>Target</b></td>
		<td align="center"><b>YTM Target</b></td>
		<td align="center"><b>YTM Arch</b></td>
		<td align="center"><b>% Ach</b></td>
		<td align="center"><b>Current Month Target</b></td>
		<td align="center"><b>Remarks</b></td>
       </tr>
EOD;
$query = db_select('sale_business_parameter','sbp');
               $query->join('sale_product','sp','sp.sale_product_id = sbp.product_id');
			   $query->fields('sbp')
			         ->fields('sp');
			   $query->condition('sbp.dealer_code',$dealer_detail['dealer_code'],'=');
			   //$query->condition('sbp.product_id',1);
			   
			   $dealer_sales_results = $query->execute();				   
			    $form['business_sales_data'] = array();
        $form['#tree'] = TRUE;	   //its a type tree	
		// _business_parameter	   
	   if(!empty($dealer_sales_results)) {
               foreach($dealer_sales_results as $key => $val) {
			   //echo '<pre>';print_r($val);exit;
			   $last_year_annual = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept+$val->LY_oct
			                            +$val->LY_nov+$val->LY_dec+$val->LY_jan+$val->LY_fab+$val->LY_march);
			$target_year_annual = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug+$val->TY_sept+$val->TY_oct
			                            +$val->TY_nov+$val->TY_dec+$val->TY_jan+$val->TY_fab+$val->TY_march);
		switch (get_survey_month()):
		case 1:
		    $last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept+$val->LY_oct
			                            +$val->LY_nov+$val->LY_dec);
			$current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug+$val->TY_sept+$val->TY_oct
			                            +$val->TY_nov+$val->TY_dec);
            $current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july+$val->TY_achieved_aug+$val->TY_achieved_sept+$val->TY_achieved_oct+$val->TY_achieved_nov+$val->TY_achieved_dec);	
			                            
            $current_month_target	= 	$val->TY_jan;							
			break;
		case 2:
		 $last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept+$val->LY_oct
			                            +$val->LY_nov+$val->LY_dec+$val->LY_jan);
			$current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug+$val->TY_sept+$val->TY_oct
			                            +$val->TY_nov+$val->TY_dec+$val->TY_jan);
			$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july+$val->TY_achieved_aug+$val->TY_achieved_sept+$val->TY_achieved_oct+$val->TY_achieved_nov+$val->TY_achieved_dec+$val->TY_achieved_jan);
			                            
			$current_month_target	= 	$val->TY_fab;
			break;
		case 3:
		  $last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept+$val->LY_oct
			                            +$val->LY_nov+$val->LY_dec+$val->LY_jan+$val->LY_fab);
			$current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug+$val->TY_sept+$val->TY_oct
			                            +$val->TY_nov+$val->TY_dec+$val->TY_jan+$val->TY_fab);
			$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july+$val->TY_achieved_aug+$val->TY_achieved_sept+$val->TY_achieved_oct
			                            +$val->TY_achieved_nov+$val->TY_achieved_dec+$val->TY_achieved_jan+$val->TY_achieved_fab);
			$current_month_target	= 	$val->TY_march;
			break;
		case 4:
		$last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept+$val->LY_oct
			                            +$val->LY_nov+$val->LY_dec+$val->LY_jan+$val->LY_fab+$val->LY_march);
		 $current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug+$val->TY_sept+$val->TY_oct
			                            +$val->TY_nov+$val->TY_dec+$val->TY_jan+$val->TY_fab+$val->TY_march);	
			$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july+$val->TY_achieved_aug+$val->TY_achieved_sept+$val->TY_achieved_oct+$val->TY_achieved_nov+$val->TY_achieved_dec+$val->TY_achieved_jan+$val->TY_achieved_fab+$val->TY_achieved_march);
			                            
			$current_month_target	= 	$val->TY_april;
			break;
		case 5:
		$last_year_ytm = ($val->LY_april);
		$current_year_ytm_target = ($val->TY_april);
		$current_year_ytm_achieved = ($val->TY_achieved_april);
			$current_month_target	= 	$val->TY_may;
			break;
		case 6:
		$last_year_ytm = ($val->LY_april+$val->LY_may);
		$current_year_ytm_target = ($val->TY_april+$val->TY_may);
		$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may);
			$current_month_target	= 	$val->TY_june;
			break;
		case 7:
		$last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june);
		$current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june);
		$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june);
		  $current_month_target	= 	$val->TY_july;
		  break;
		case 8:
		$last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july);
		$current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july);
		$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july);
			$current_month_target	= 	$val->TY_aug;
			break;	
		case 9:
		$last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug);
		$current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug);
		$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july
		                             +$val->TY_achieved_aug);
			$current_month_target	= 	$val->TY_sept;
			break;	
		case 10:
		$last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept);
		 $current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug+$val->TY_sept);
		 $current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july
		                             +$val->TY_achieved_aug+$val->TY_achieved_sept);
			$current_month_target	= 	$val->TY_oct;
			break;
         case 11:
		  $last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept+$val->LY_oct);
		  $current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july
		                             +$val->TY_achieved_aug+$val->TY_achieved_sept+$val->TY_achieved_oct);
			$current_month_target	= 	$val->TY_nov;
			break;
         case 12:
		 $last_year_ytm = ($val->LY_april+$val->LY_may+$val->LY_june+$val->LY_july+$val->LY_aug+$val->LY_sept+$val->LY_oct
			                            +$val->LY_nov);
			$current_year_ytm_target = ($val->TY_april+$val->TY_may+$val->TY_june+$val->TY_july+$val->TY_aug+$val->TY_sept+$val->TY_oct
			                            +$val->TY_nov);
			$current_year_ytm_achieved = ($val->TY_achieved_april+$val->TY_achieved_may+$val->TY_achieved_june+$val->TY_achieved_july
		                             +$val->TY_achieved_aug+$val->TY_achieved_sept+$val->TY_achieved_oct+$val->TY_achieved_nov);							
			$current_month_target	= 	$val->TY_dec;
			break;			
	   endswitch;	

$percent_achieved = round(($current_year_ytm_achieved/$current_year_ytm_target) * 100); 

	$html .= <<<EOD
	<tr>
        <td align="center">$val->sale_product_name</td>
		<td align="center">$last_year_annual</td>
		<td align="center">$last_year_ytm</td>
		<td align="center">$target_year_annual</td>
		<td align="center">$current_year_ytm_target</td>
		<td align="center">$current_year_ytm_achieved</td>
		<td align="center">$percent_achieved</td>
		<td align="center">$current_month_target</td>
		<td align="left"> </td>
    </tr>
EOD;
}
}			
// end sale upload data
// Start data for Norms
 if($employee['emp_department'] == 'sales') {
$html .= <<<EOD
</table><p> Sale Norms</p>
   <table cellspacing="0" cellpadding="1" border="1">
           <tr style="background-color:#000;color:#fff;">
           <th >Designtion</th>
           <th align="center">Required as per Norms</th>
           <th align="center">Available(Nos)</th>
		   <th align="center">Action Plan(TSM)</th>
		   <th align="center">Action Plan(ASM)</th>
		   <th align="center">Action Plan(ZM)</th>
		   <th align="center">Target Date</th>
       </tr>
EOD;
 
 // fetch norms form table
		$sales_norms_fields = array('gm' => 'General manager',
		           'Showroom_manager' => 'Showroom manager',
				   'network_manager' => 'Network manager',
		           'receptionist' => 'Receptionist',
				   'hostess' => 'Hostess',
		           'Sales_executive' => 'Sales Executive',
				   'goodlife_executive' => 'GoodLife Executive',
		           'marketing_executive' => 'Marketing Executive',
		           'rural_support_sales' => 'Rural Support - Sales',
				   'accountant' => 'Accountant',
				   'billing_executive' => 'Billing Executive',
				   'cashier' => 'Cashier',
				   'it_support_executive' => 'IT Support Executive',
		           'delivery_executive' => 'Delivery Executive',
				   'pdi_technician' => 'PDI Technician',
				   'rto_executive' => 'RTO Executive',
				   'network_executive' => 'Network Executive',
				   'housekeeping_staff' => 'Housekeeping Staff',
				   'pantry' => 'Pantry',
				   'security' => 'Security',
		);
		
		$get_sales_norms = db_select('hero_sales_manpower_norms', 'h')
		                     ->fields('h')
		                     ->execute();
		 
		$record = $get_sales_norms->fetchAll(); 
		$norms = array();
		// piyush 20-02-14
		$dealer_sales_vol_by_12 = round($dealer_detail['dealer_sales_vol']/12);

		foreach($record as $key => $val) {
			if(($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($dealer_sales_vol_by_12 <= $val->appliacbility_criteria_max)) {
				$norms = $record[$key];
			}elseif(($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($val->appliacbility_criteria_max == NULL)) {
				$norms = $record[$key];
			}
		}
		//echo '<pre>'; print_r($norms);exit;
		// made by piyush 14-04-14 counter sale
		$dealer_counter_sales = ceil($dealer_detail['countersale']/75);
			 $norms->Sales_executive = $dealer_counter_sales;
	foreach($sales_norms_fields as $key => $val) {	 
           $sales_desig = $val;
		   if($key == 'network_manager' || $key == 'network_executive')
		  {
		   $sales_require_val = '';
		   }
		   else{
		   $sales_require_val = is_decimal($norms->$key) ?  round($norms->$key) : $norms->$key;
		   }
$html .= <<<EOD
       <tr>
        <td>$sales_desig</td>
		<td align="center">$sales_require_val</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
       </tr>
EOD;
}
}
 elseif($employee['emp_department'] == 'services') {		   
 		 
$html .= <<<EOD
</table><p> Service Norms</p>
   <table cellspacing="0" cellpadding="1" border="1">
           <tr style="background-color:#000;color:#fff;">
                  <th align="center">Designation</th>
                  <th align="center">Required as per Norms</th>
                  <th align="center">Available(Nos)</th>
				  <th align="center">Program Norms</th>
				  <th align="center">Trained Avl.(Nos)</th>
				  <th align="center">Action Plan(TSM)</th>
		          <th align="center">Action Plan(ASM)</th>
		          <th align="center">Action Plan(ZM)</th>
		          <th align="center">Target Date</th>
                  </tr>
EOD;
	 $services_norms = db_select('hero_service_manpower_norms','hsmn')
			   ->fields('hsmn')
			   ->execute()
			   ->fetchAll();
// Divided by 12 for Services Norms(Required as per Norm)->Piyush
		foreach($services_norms as $key => $val) {
		// code for services norms	
		switch ($key):
		case 0:
			$format = ($dealer_detail['dealer_service_vol'] >= 1000 ? 1 : 0);
		case 1:
			$format = 1;
			break;
		case 2:
			$format = $dealer_detail['dealer_service_vol']/(26*40*12);
			break;
		case 3:
			$format = $dealer_detail['dealer_service_vol']/(26*20*12);
			break;
		case 4:
			$format = 1;
			break;
		case 5:
		// For Technician (no . of ramps + 1) 28-12-13
		    $format = $dealer_detail['no_of_ramps']+1 ; 
			//$format = ($dealer_detail['dealer_service_vol']/(26*10*12))+1;
			break;
		case 6:
		 //For Assistant Technician (no . of ramps + 1)/2 28-12-13 
		    $format = round(($dealer_detail['no_of_ramps']+1)/2);
			//$format = (($dealer_detail['dealer_service_vol']/(26*10*12))+1)/2;
			break;
		case 7:
			$format = $dealer_detail['dealer_service_vol']/(26*20*12);
			break;
		case 8:
			$format = 1;
			break;	
		case 9:
			$format = 1;
			break;	
		case 10:
			$format = $dealer_detail['dealer_service_vol']/(26*50*12);
			break;								
	endswitch;	
 
   $service_desig = $val->designation;
  $service_require_val = round($format);
  $program_norms = $val->program_norms;
$html .= <<<EOD
       <tr>
        <td>$service_desig</td>
		<td align="center">$service_require_val</td>
		<td align="center"></td>
		<td align="center">$program_norms</td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
		<td align="center"></td>
       </tr>
EOD;
}
}
// Start criteria	

	 
 $current_month = get_survey_month(); 
	$quarterly = array('03','06','09','12');
	$monthly = array('01','02','03','04','05','06','07','08','09','10','11','12');
	$halfYearly = array('03','09');
	$yearly = array('03');
	// select criteria for this month.
	      if( in_array($current_month, $quarterly) && in_array($current_month, $halfYearly) && in_array($current_month, $yearly) ){
	         $freq = 'All';
	       }else if( in_array($current_month, $quarterly) && in_array($current_month, $monthly) && in_array($current_month, $halfYearly) ){
	       $freq = 'quart_month_half';
	       }else if( in_array($current_month, $quarterly) && in_array($current_month, $monthly) ){
	       $freq = 'quart_month';
	       }else if( in_array($current_month, $quarterly) ){
	       $freq = 'Quarterly';
	       }else if( in_array($current_month, $monthly) ){
	       $freq = 'Monthly';
	       }else if( in_array($current_month, $halfYearly)){
	       $freq = 'Half-Yearly';
	       }else if( in_array($current_month, $yearly) ){
	       $freq = 'Yearly';
	       }
	// check 
	if($freq == 'All') {
		$query = db_select('hero_criteria_frequency','hcf')
					->condition('hcf.frequency_id', array('1','2','3','4'));
	}else if($freq == 'Quarterly') {
		$query = db_select('hero_criteria_frequency','hcf')
					->condition('hcf.frequency_id', array('1','4'));
	}else if($freq == 'Monthly'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('4'));
	 }else if($freq == 'Half-Yearly'){  
		$query = db_select('hero_criteria_frequency','hcf')
		           ->condition('hcf.frequency_id', array('3','4'));
	 }else if($freq == 'Yearly'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('2','4'));
	 }else if($freq == 'quart_month_half'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('1','3','4'));
	 }else if($freq == 'quart_month'){  
		$query = db_select('hero_criteria_frequency','hcf')
		            ->condition('hcf.frequency_id', array('1','4'));
	 }
  	
  // end frequency 03-01-14
    $query->join('hero_criteria_department', 'hcd', 'hcf.criteria_id = hcd.criteria_id');
    if($employee['emp_department'] == 'sales') {
        $query->condition('hcd.department_id', '1','=');
    }
    elseif($employee['emp_department'] == 'services') {
		$query->condition('hcd.department_id', '2','=');
	}
	 $query->join('hero_criteria_applicability', 'hca', 'hcd.criteria_id = hca.criteria_id');

	 // print_r($dealer_detail);
   // echo $dealer_detail['dealer_applicability'];exit;
   // Add For vlead (30-12-13) Piyush Rai
     if($dealer_detail['dealer_applicability'] == '1')
     {
		$and = db_and();
		$or = db_or();
		  // only for vlead questions 
		 $and->condition('hca.applicability_id', '1','=');
		 $or->condition('hca.applicability_id', '2','=');
		 $or->condition('hca.applicability_id', '3','=');
		 $or->condition('hca.applicability_id', '4','=');
		 $or->condition('hca.applicability_id', '5','=');
		 $or->condition('hca.applicability_id', '6','=');
		 $or->condition($and);
		 $query->condition($or);
	 }
	else{
	    $or = db_or();
	    $or->condition('hca.applicability_id', '1','<>');
		$or->condition('hca.applicability_id', '2','=');
		$or->condition('hca.applicability_id', '3','=');
		$or->condition('hca.applicability_id', '4','=');
		$or->condition('hca.applicability_id', '5','=');
		$or->condition('hca.applicability_id', '6','=');
		$query->condition($or);
	 }
  // end non vlead
          $query->fields('hcd',array('criteria_id'));
		  $query->groupBy('hcd.criteria_id');
          $query->orderBy('hcd.criteria_id','ASC');
         $criteria_detail = $query->execute();

	
$criteria_name = array();
//fetch all question related to staff member department
		$question_assoc_to_dept = db_select('hero_questions_department','hqd')
			   ->fields('hqd',array('question_id'))
			   ->condition('hqd.department_id', $dept,'=')
			   ->execute()
			   ->fetchAll();    
       $question_ids = array();
       foreach($question_assoc_to_dept as $key => $val) {
		   $question_ids[$val->question_id] = $val->question_id; 
	   } 
foreach($criteria_detail as $record) {
        $output = db_select('hero_criteria','hc')
           ->fields('hc', array('criteria_name'))
	      ->condition('hc.criteria_id',$record->criteria_id,'=')
		  ->groupBy('hc.criteria_id')
		  ->orderBy('hc.criteria_id', 'ASC') 
	      ->execute()
	      ->fetchAssoc();
//echo $record->criteria_id.' - ';
 $criteria_name[$record->criteria_id] = $output['criteria_name']; 
    $criteriaName = $criteria_name[$record->criteria_id]; 
$html .= <<<EOD
</table>
<p style="background-color:#d2d2d2;color:#000;font-size:11px;"><strong>$criteriaName</strong></p>
<table cellspacing="0" cellpadding="1" border="1">
<tr style="background-color:#000;color:#fff;">
<th align="center">Parameter</th>
<th align="center">Response</th>
<th align="center">Action Plan</th>
<th align="center">Target Date</th>
</tr>
EOD;
        foreach($question_ids as  $val) {
		 $survey_question = db_select('hero_questions','hq')
           ->fields('hq')
		   ->condition('hq.question_id',$val,'=')
	       ->condition('hq.question_criteria_id',$record->criteria_id,'=')
		   ->groupBy('hq.question_id')
	       ->execute()
	       ->fetchAssoc();
		  //echo $record->criteria_id.'/'. $val.'-' ;
		 $question_name = $survey_question['question_name'];  
if(!empty($question_name))	{
 $html .= <<<EOD
<tr>
<td align="left">$question_name</td>
<td align="center"></td>
<td align="center"></td>
<td align="center"></td>
</tr>
EOD;
}
}
}
$html .= <<<EOD
</table>
<p>&nbsp; </p>
<table cellspacing="0" cellpadding="1" border="1">
<tr style="background-color:#000;color:#fff;"><th align="center">Remark</th></tr>
<tr><td align="center"></td></tr>
</table>
EOD;
 
 require_once DRUPAL_ROOT ."/sites/all/modules/print/lib/tcpdf/tcpdf.php";
  $filename = 'myScore_'.$dealer_code.'.pdf';
  $pdf = new TCPDF();
  // write data
   $pdf->AddPage('P');
	$pdf->SetFont('helvetica', '', 8);
   $pdf->writeHTML($html,true, false, false, false, '');
   // end write
   //$pdfpath = DRUPAL_ROOT.'/survey_pdf/'.$filename;
   //$pdf->Output($filename, 'D');
   die($pdf->Output($filename, 'I'));
 //} 
}	




?>