<?php

function dealer_score_monthwise_report_for_all() {

    $output = drupal_get_form('get_excel_report_dealer_avg_score_monthly');
    return $output;
}

//module_load_include('inc', 'survey_report_month', 'report_page_1');
module_load_include('inc', 'excel_report', 'report_page_3');

function get_excel_report_dealer_avg_score_monthly($form, &$form_state) {

    $form['#id'] = 'get_excel_report_dealer_avg_score_monthly';
    $form['#tree'] = TRUE;
    global $user;
    $employee = $_SESSION[$user->name];
    
    $multiRole = explode(',', $employee['emp_designation_type']);
    $role = $multiRole['0'];

    $list = array();
    $list['0'] = t('All');
    //echo $employee['emp_code'];exit;

    $year_list = array('2013' => '2013', '2014' => '2014');
    $month_list = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
    $depts = get_depts();
    $form['dealer']['month'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Month:',
        '#options' => $month_list,
    );

    $form['dealer']['excel_year'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Select Year',
        '#options' => $year_list,
    );
if($role=='asmview' || $role=='zoview'){
    $form['dealer']['dept'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Stream',
        '#options' => $depts,
    );
}
    $form['dealer']['excel_actions'] = array('#type' => 'actions');
    $form['dealer']['excel_actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Export'),
        '#submit' => array('report_dealer_avg_score_monthly_submit'),
    );

    return $form;
}

function report_dealer_avg_score_monthly_submit($form, &$form_state) {

    global $user;
    $selected_month = $form_state['values']['dealer']['month'];
    $selected_year = $form_state['values']['dealer']['excel_year'];
    $selected_dept = $form_state['values']['dealer']['dept'];

    if ($selected_dept == '1') {
        $dept = 'sales';
    } else {
        $dept = 'services';
    }

    $employee = $_SESSION[$user->name];
    $emp_dept = $employee['emp_department'];   
    if ($emp_dept == 'sales') {
        $emp_dept_code = '1';
    } else {
        $emp_dept_code = '2';
    }
    $emp_code = $employee['emp_code'];


    $multiRole = explode(',', $employee['emp_designation_type']);
    $dealer_code = "";
    if($multiRole['0'] == 'tsm' || $multiRole['0'] == 'tsmview'){
    }else{
	 module_invoke('hero_dashboard', 'get_allrole_survey_details');
	  $emp_array = implode(',',get_allrole_survey_details($employee['emp_code']));
	  }
//echo '<pre>';print_r($user); exit;
    if ($multiRole['0'] == 'tsm' || $multiRole['0'] == 'tsmview') {
      $dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_code = '$emp_code'";
//$dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$selected_dept' AND sbi.survey_status_id = '4' AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code = '$emp_code'";

        $dealer_in_qry_fire = db_query($dealer_in_qry);
        while ($row = $dealer_in_qry_fire->fetchAssoc()) {
            $dealer_code .= $row['dealer_code'] . ',';
        }
    } else if ($multiRole['0'] == 'asm') {
        $under_emps_qry = "SELECT emp_code  FROM hero_hmcl_staff WHERE emp_report_to_designation_code = '$emp_code'";

        $under_emps_qry_fire = db_query($under_emps_qry);
        while ($row1 = $under_emps_qry_fire->fetchAssoc()) {
            $emp_code = $row1['emp_code'];

          // $dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_code = '$emp_code'";
         $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$emp_dept_code' AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code IN ($emp_array)";  

            $dealer_in_qry_fire = db_query($dealer_in_qry);
            while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                // echo $row['dealer_code']."<br>";
                $dealer_code .= $row['dealer_code'] . ',';
            }
        }
    } else if ($multiRole['0'] == 'zo') {
        
        $under_asms_qry = "SELECT emp_code AS asm_code FROM hero_hmcl_staff WHERE emp_report_to_designation_code = '$emp_code'";

        $under_asms_qry_fire = db_query($under_asms_qry);
        while ($asms_row = $under_asms_qry_fire->fetchAssoc()) {
            $asm_code = $asms_row['asm_code'];

            $under_emps_qry = "SELECT emp_code  FROM hero_hmcl_staff WHERE emp_report_to_designation_code = '$asm_code'";

            $under_emps_qry_fire = db_query($under_emps_qry);
            while ($row1 = $under_emps_qry_fire->fetchAssoc()) {
                $emp_code = $row1['emp_code'];

               // $dealer_in_qry = "SELECT dealer_code FROM hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_code = '$emp_code'";
               $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$emp_dept_code'  AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code IN ($emp_array)";
                // die($dealer_in_qry);
                $dealer_in_qry_fire = db_query($dealer_in_qry);
                while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                    // echo $row['dealer_code']."<br>";
                    $dealer = $row['dealer_code'];

                    $dealer_code .= $row['dealer_code'] . ',';
                }
            }
        }
    } else if ($multiRole['0'] == 'asmview') {

        $ofc_area_code = $employee['emp_area_office_code'];
        $tsms_id = get_tsms_for_areacode($ofc_area_code);
        
        foreach($tsms_id as $val) {
            
                $emp_code = $val;

                $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$selected_dept'  AND sbi.survey_year = '$selected_year' AND dealer_mapped_to_staff_person_code IN ($emp_array)";
               
                $dealer_in_qry_fire = db_query($dealer_in_qry);
                while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                    // echo $row['dealer_code']."<br>";
                    $dealer = $row['dealer_code'];

                    $dealer_code .= $row['dealer_code'] . ',';
                }
            }
    }else if ($multiRole['0'] == 'zoview') {
        $tsms = get_tsms_for_zonecode($employee['emp_zone_code']);
        foreach($tsms as $val) {
            
                $emp_code = $val;

                $dealer_in_qry = "SELECT DISTINCT dealer_code FROM hero_dealer_mapping_status AS ms JOIN survey_basic_info AS sbi ON(ms.dealer_code = sbi.asso_dealer_code) WHERE sbi.survey_for_dept_code = '$selected_dept'  AND sbi.survey_year = '$selected_year'";
               
                $dealer_in_qry_fire = db_query($dealer_in_qry);
                while ($row = $dealer_in_qry_fire->fetchAssoc()) {
                    // echo $row['dealer_code']."<br>";
                    $dealer = $row['dealer_code'];

                    $dealer_code .= $row['dealer_code'] . ',';
                }
            }
    }

    
    $dealer_code = rtrim($dealer_code, ',');
    if($multiRole['0']=='tsm' || $multiRole['0']=='asm' || $multiRole['0']=='zo'){
    if($emp_dept=='sales'){
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($emp_dept=='services'){
        $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
    }}elseif($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){

     if($dept=='sales'){
       
    $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '1'";
    }elseif($dept=='services'){
        $ques_query = "SELECT hq.question_criteria_id, hc.criteria_name, hq.question_id, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id)
                        JOIN hero_questions_department AS qd ON(hq.question_id = qd.question_id)
                        WHERE qd.department_id = '2'";
    }   
    } 

    $ques_query_fire = db_query($ques_query);
    $ques_query_data = $ques_query_fire->fetchAll();
if($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){
    $dealer_qry = "SELECT dms.dealer_code, sbi.sid
                FROM hero_dealer_mapping_status AS dms
                JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code)
                WHERE sbi.asso_dealer_code IN ($dealer_code)
                AND sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year' AND dms.dealer_mapped_to_staff_person_department_code = '$dept'";
}else if($multiRole['0']='tsm' || $multiRole['0']='zo'||$multiRole['0']='asm'){
   	
   $dealer_qry = "SELECT DISTINCT dms.dealer_code, sbi.sid
                FROM hero_dealer_mapping_status AS dms
                JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code)
                WHERE sbi.asso_dealer_code IN ($dealer_code)
                AND sbi.survey_status_id = '4'
                AND sbi.survey_month = '$selected_month' AND sbi.survey_year = '$selected_year'"; 
}


	$dealer_query_fire = db_query($dealer_in_qry);
    $dealer_query_data = $dealer_query_fire->fetchAll();
    $dataset1 = db_query($dealer_in_qry);
   
    $header[] = 'Criteria';
    $header[] = 'Question';
    while ($row = $dataset1->fetchAssoc()) {
	
        $header[] = $row['dealer_code'];
	//print_r($row);
    }
	//die;
	
    foreach ($ques_query_data as $key => $value) {
        foreach ($dealer_query_data as $key1 => $value1) {
		
		
            $criteira[$value->question_criteria_id] = $value->criteria_name;
            $questions[$value->question_id] = $value->question_name;
            $question_response[$value->question_criteria_id . '-' . $value->question_id][$value1->dealer_code] 
			= get_question_response($value->question_id , $value1->dealer_code, $selected_month,$selected_year,$selected_dept);
			
        }
    }


    //print_r($criteira); die;
    //   print_r($questions); die;
   //print_r($question_response); die;
    if($multiRole['0']='asmview' || $multiRole['0']='zoview'){
        echo 'Year - '.$selected_year."\n";
        echo 'Month - '.$selected_month."\n";
        echo 'Department - '.$dept."\n";
    }
    else if($multiRole['0']='tsm' || $multiRole['0']='zo'||$multiRole['0']='asm'){
        echo 'Year - '.$selected_year."\n";
        echo 'Month - '.$selected_month."\n";
        echo 'Department - '.$emp_dept;
	}
		echo "\t";
		echo "\t";
		echo "Zone";
		echo "\n";
    
    echo "\t";
    echo "\t";
    echo "Dealer Code";
	print("\n");
    $header_count = sizeof($header);
    
	for ($i = 0; $i < $header_count; $i++) {
        echo $header[$i] . "\t";
    }
	
	
	echo "\t";
    echo "Avg";
    print("\n");
	//echo "<pre>";
//print_r($question_response); die;
    foreach ($question_response as $key => $value) {

        $sum = array_sum($value);



        $cri_q_arr = explode("-", $key);
        $selected_cri = $cri_q_arr[0];
        $selected_q = $cri_q_arr[1];
        $query_q_c = "SELECT hc.criteria_name, hq.question_name 
                        FROM hero_criteria AS hc
                        JOIN hero_questions AS hq ON(hc.criteria_id = hq.question_criteria_id) WHERE hq.question_criteria_id = '$selected_cri' AND hq.question_id = '$selected_q'";
        $q_c_set = db_query($query_q_c);
        $q_c_data = $q_c_set->fetchAll();
        echo $q_c_data[0]->criteria_name . "\t";
        echo $q_c_data[0]->question_name . "\t";
        $count = 0;
        foreach ($value as $key1 => $value1) {

            echo $value1 . "\t";
            if (isset($value1)) {
                $count++;
            }
        }

        $avg = $sum / $count;
        echo round($avg, 2);
        print("\n");
    }
    $date = date("d-m-Y");
    $filename = 'Dealers-' . $date . '.xls';


    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename");
    exit;
}

function get_question_response($sid, $question_id, $selectmonth, $selectyear, $deptment) {

 if ($deptment == '1') {
        $dept = 'sales';
    } else {
        $dept = 'services';
    }

    $employee = $_SESSION[$user->name];
    $emp_dept = $employee['emp_department'];   
    if ($emp_dept == 'sales') {
        $emp_dept_code = '1';
    } else {
        $emp_dept_code = '2';
    }
    $emp_code = $employee['emp_code'];


    $multiRole = explode(',', $employee['emp_designation_type']);
 
	if($multiRole['0']=='asmview' || $multiRole['0']=='zoview'){
	 $q_query1 = "SELECT Distinct dms.dealer_code, sbi.sid FROM hero_dealer_mapping_status AS dms 
	LEFT JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code) 
	WHERE sbi.asso_dealer_code ='".$question_id."'
	AND sbi.survey_status_id = '4' AND sbi.survey_month = '".$selectmonth."' AND sbi.survey_year = '".$selectyear."' AND dms.dealer_mapped_to_staff_person_department_code = '$dept' ";
	}else if($multiRole['0']='tsm' || $multiRole['0']='zo'||$multiRole['0']='asm'){
    $q_query1 = "SELECT Distinct dms.dealer_code, sbi.sid FROM hero_dealer_mapping_status AS dms 
	LEFT JOIN survey_basic_info AS sbi ON(dms.dealer_code = sbi.asso_dealer_code) 
	WHERE sbi.asso_dealer_code ='".$question_id."'
	AND sbi.survey_status_id = '4' AND sbi.survey_month = '".$selectmonth."' AND sbi.survey_year = '".$selectyear."' ";
	}
	 $questions_respose_set1 = db_query($q_query1);
	$question_res1 = $questions_respose_set1->fetchAssoc();
    $q_query = "SELECT question_response FROM survey_question_list 
                        WHERE Q_id = '$sid'
                        AND S_id = '$question_res1[sid]'";
	//echo $q_query; exit;
    $questions_respose_set = db_query($q_query);
    while ($question_res = $questions_respose_set->fetchAssoc()) {

        return $question_res['question_response'];
    }
}

// function to get the asmview email_ids based on area code
function get_tsms_for_areacode($ofc_area_code) {

    $qry = "SELECT emp_code FROM hero_hmcl_staff WHERE emp_designation_type = 'tsm' AND emp_area_office_code = '" . $ofc_area_code . "'";

    $data_for_asmview = db_query($qry);

    $asmview_email_data = $data_for_asmview->fetchAll();

    foreach ($asmview_email_data as $key => $value) {
        $tsms_ids[$value->emp_code] = $value->emp_code;
    }


    return $tsms_ids;
}
function get_tsms_for_zonecode($zone_code){
    $qry = "SELECT emp_code FROM hero_hmcl_staff WHERE emp_designation_type = 'tsm' AND emp_zone_code = '" . $zone_code . "'";
    
    $data_for_asmview = db_query($qry);

    $asmview_email_data = $data_for_asmview->fetchAll();

    foreach ($asmview_email_data as $key => $value) {
        $tsms_ids[$value->emp_code] = $value->emp_code;
    }


    return $tsms_ids;
    
}