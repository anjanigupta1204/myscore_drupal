<?php

function dealer_score_dept_wise() {
global $user;
    $employee = $_SESSION[$user->name];

    $emp_code = $employee['emp_code'];
    $multiRole = explode(',', $employee['emp_designation_type']);
    if ($multiRole['0'] == 'zo' || $multiRole['0'] == 'zoview' || $multiRole['0'] == 'asmview' || $multiRole['0'] == 'asm') {
    $output = drupal_get_form('get_dealer_score_dept_wise');
    return $output;
    } else {
        return "Requested page is only for ZO.";
    }
}


//module_load_include('inc', 'excel_report', 'report_page_3');
module_load_include('inc', 'excel_report', 'report_page_2');
module_load_include('inc', 'excel_report', 'report_page_3');

function get_dealer_score_dept_wise($form, &$form_state) {

    $form['#id'] = 'get_dealer_score_dept_wise';
    $form['#tree'] = TRUE;


    $list = array();
    $list['0'] = t('All');
    //echo $employee['emp_code'];exit;

    $year_list = array('2013' => '2013', '2014' => '2014');
    $month_list = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
    
    $form['dealer_dept']['from_month'] = array(
        '#type' => 'select',
        '#title' => 'From:',
        '#required' => true,
        '#options' => $month_list,
        '#ajax' => array(
            'event' => 'change',
            'callback' => 'get_ajax_month_for_deptwise',
            'wrapper' => 'tomonth-wrapper',
    ),
    );
$form['dealer_dept']['wrapper'] = array(
    '#prefix' => '<div id="tomonth-wrapper">',
    '#suffix' => '</div>',
  );

$options = array('Select');
  if (isset($form_state['values']['dealer_dept']['from_month'])) {
      
    // Pre-populate options for city dropdown list if province id is set
    $options = _load_tomonth($form_state['values']['dealer_dept']['from_month']);
   
  }

    $form['dealer_dept']['wrapper']['to_month'] = array(
        '#type' => 'select',
        '#title' => 'To:',
        '#required' => true,
        '#options' => $options,
    );
    $form['dealer_dept']['excel_year'] = array(
        '#type' => 'select',
        '#title' => 'Year:',
        '#required' => true,
        '#options' => $year_list,
    );


    $form['dealer_dept']['excel_actions'] = array('#type' => 'actions');
    $form['dealer_dept']['excel_actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Export'),
        '#submit' => array('report_dealer_dept_wise_submit'),
    );

    return $form;
}

function get_ajax_month_for_deptwise($form, $form_state) {

  // Return the dropdown list including the wrapper
  return $form['dealer_dept']['wrapper'];
}

function report_dealer_dept_wise_submit($form, &$form_state) {
    global $user;
    $selected_from_month = $form_state['values']['dealer_dept']['from_month'];
    $selected_to_month = $form_state['values']['dealer_dept']['wrapper']['to_month'];
    $selected_year = $form_state['values']['dealer_dept']['excel_year'];
    if ($selected_year == '0') {
        $selected_year = date('Y');
    }
    $header1 = get_months_for_header($selected_from_month, $selected_to_month);
    $header3 = get_months_for_header_itr($selected_from_month, $selected_to_month);

    $month_count = sizeof($header1);
    $employee = $_SESSION[$user->name];
    $emp_dept = $employee['emp_department'];
    $multiRole = explode(',', $employee['emp_designation_type']);

    echo 'Year - ' . $selected_year . "\n";
    echo 'Month From - ' . get_month_name($selected_from_month) . "\n";
    echo 'Month To - ' . get_month_name($selected_to_month) . "\n";
    echo "\t";
    if ($multiRole['0'] == 'zoview' || $multiRole['0'] == 'asmview') {
    for ($i = 0; $i < $month_count; $i++) {
        echo $header1[$i] . "\t\t\t";
    }
    } else {
        if ($emp_dept == 'sales') {
            for ($i = 0; $i < $month_count; $i++) {
                echo $header1[$i] . "\t";
            }
        } else {
            for ($i = 0; $i < $month_count; $i++) {
                echo $header1[$i] . "\t";
            }
        }
    }
    print("\n");
    echo "\t";
    
    if ($multiRole['0'] == 'zoview' || $multiRole['0'] == 'asmview') {
   
    for ($i = 0; $i < $month_count; $i++) {
            echo 'Sales' . "\t";
            echo 'Services' . "\t";
            echo 'Average Score' . "\t";
    }
    }
    print("\n");

    $multiRole = explode(',', $employee['emp_designation_type']);


    if ($multiRole['0'] == 'zoview') {
       $zone = $employee['emp_zone_code'];
       $area_office_codes = '';
       $area_ofc_qry = "SELECT DISTINCT emp_area_office_code FROM hero_hmcl_staff WHERE emp_zone_code ='$zone'";
       
    $area_ofc_qry_fire = db_query($area_ofc_qry);
    
    while ($area_ofc_row = $area_ofc_qry_fire->fetchAssoc()) {
            $area_office_codes .= $area_ofc_row['emp_area_office_code'] . ',';
    }
    $area_office_codes = rtrim($area_office_codes, ',');
    
    
    $dealer_qry = "SELECT dealer_code, dealer_name FROM  hero_dealer_mapping_status WHERE area_office_code IN($area_office_codes)";
    
    $dealer_qry_fire = db_query($dealer_qry);
    while ($dealer_qry_row = $dealer_qry_fire->fetchAssoc()) {
        $dealer_codes[] = $dealer_qry_row['dealer_code'];
    }
    } elseif ($multiRole['0'] == 'zo') {
       $zone = $employee['emp_zone_code'];
       $area_office_codes = '';
       $area_ofc_qry = "SELECT DISTINCT emp_area_office_code FROM hero_hmcl_staff WHERE emp_zone_code ='$zone'";
       
    $area_ofc_qry_fire = db_query($area_ofc_qry);
    
    while ($area_ofc_row = $area_ofc_qry_fire->fetchAssoc()) {
            $area_office_codes .= $area_ofc_row['emp_area_office_code'] . ',';
    }
    $area_office_codes = rtrim($area_office_codes, ',');
    
    
    $dealer_qry = "SELECT dealer_code, dealer_name FROM  hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_department_code = '$emp_dept' AND area_office_code IN($area_office_codes)";
    
    $dealer_qry_fire = db_query($dealer_qry);
    while ($dealer_qry_row = $dealer_qry_fire->fetchAssoc()) {
        $dealer_codes[] = $dealer_qry_row['dealer_code'];
    }
    } elseif ($multiRole['0'] == 'asmview') {
        $ao = $employee['emp_area_office_code']; 
        $dealer_qry = "SELECT dealer_code, dealer_name FROM  hero_dealer_mapping_status WHERE area_office_code IN($ao)";
    
    $dealer_qry_fire = db_query($dealer_qry);
    while ($dealer_qry_row = $dealer_qry_fire->fetchAssoc()) {
        $dealer_codes[] = $dealer_qry_row['dealer_code'];
    }
    } elseif ($multiRole['0'] == 'asm') {
        $ao = $employee['emp_area_office_code']; 
        $dealer_qry = "SELECT dealer_code, dealer_name FROM  hero_dealer_mapping_status WHERE dealer_mapped_to_staff_person_department_code = '$emp_dept' AND area_office_code IN($ao)";
    
    $dealer_qry_fire = db_query($dealer_qry);
    while ($dealer_qry_row = $dealer_qry_fire->fetchAssoc()) {
        $dealer_codes[] = $dealer_qry_row['dealer_code'];
    }
    }
   
    foreach ($dealer_codes as $value) {
    $dealer_code = $value;
        echo $dealer_code . "\t";
    foreach ($header3 as $key => $value) {
        
            if ($multiRole['0'] == 'asmview' || $multiRole['0'] == 'zoview') {
         
        $sales_res_qry = "select survey_score_percent sales_sum FROM survey_basic_info AS sbi
        WHERE sbi.asso_dealer_code = '$dealer_code' AND sbi.survey_status_id = '4' AND sbi.survey_for_dept_code = '1' AND sbi.survey_year = '$selected_year' AND sbi.survey_month = '$key'";
       
        $sales_res_qry_fire = db_query($sales_res_qry);
        $sales_res_row = $sales_res_qry_fire->fetchAll();
        //print_r($sales_res_row);
                if (!isset($sales_res_row['0']->sales_sum)) {
            $sales_sum = '0';
                } else {
            $sales_sum = $sales_res_row['0']->sales_sum;
        }
                echo $sales_sum . "\t";
        $services_res_qry = "select survey_score_percent services_sum FROM survey_basic_info AS sbi
        WHERE sbi.asso_dealer_code = '$dealer_code' AND sbi.survey_status_id = '4' AND sbi.survey_for_dept_code = '2' AND sbi.survey_year = '$selected_year' AND sbi.survey_month = '$key'";
        
        $services_res_qry_fire = db_query($services_res_qry);
        $services_res_row = $services_res_qry_fire->fetchAll();
        
                if (!isset($services_res_row['0']->services_sum)) {
            $services_sum = '0';
                } else {
            $services_sum = $services_res_row['0']->services_sum;
        }
                echo $services_sum . "\t";
                $score_sum = $sales_sum + $services_sum;
                $avg = $score_sum / 2;
                echo round($avg, 2) . "\t";
            } elseif ($multiRole['0'] == 'zo') {
        
                if ($emp_dept == 'services') {
        $services_res_qry = "select survey_score_percent services_sum FROM survey_basic_info AS sbi
        WHERE sbi.asso_dealer_code = '$dealer_code' AND sbi.survey_status_id = '4' AND sbi.survey_for_dept_code = '2' AND sbi.survey_year = '$selected_year' AND sbi.survey_month = '$key'";
        
        $services_res_qry_fire = db_query($services_res_qry);
        $services_res_row = $services_res_qry_fire->fetchAll();
                    echo $services_res_row['0']->services_sum . "\t";
                } else {
        $services_res_qry = "select survey_score_percent sales_sum FROM survey_basic_info AS sbi
        WHERE sbi.asso_dealer_code = '$dealer_code' AND sbi.survey_status_id = '4' AND sbi.survey_for_dept_code = '1' AND sbi.survey_year = '$selected_year' AND sbi.survey_month = '$key'";
        
        $services_res_qry_fire = db_query($services_res_qry);
        $services_res_row = $services_res_qry_fire->fetchAll();
        

                    echo $services_res_row['0']->sales_sum . "\t";
    }
            } elseif ($multiRole['0'] == 'asm') {
    
                if ($emp_dept == 'services') {
        $services_res_qry = "select survey_score_percent services_sum FROM survey_basic_info AS sbi
        WHERE sbi.asso_dealer_code = '$dealer_code' AND sbi.survey_status_id = '4' AND sbi.survey_for_dept_code = '2' AND sbi.survey_year = '$selected_year' AND sbi.survey_month = '$key'";
        
        $services_res_qry_fire = db_query($services_res_qry);
        $services_res_row = $services_res_qry_fire->fetchAll();

                    // echo "NA"."\t";
                    echo $services_res_row['0']->services_sum . "\t";

                    // echo $services_res_row['0']->services_sum."\t";
                } else {
        $services_res_qry = "select survey_score_percent sales_sum FROM survey_basic_info AS sbi
        WHERE sbi.asso_dealer_code = '$dealer_code' AND sbi.survey_status_id = '4' AND sbi.survey_for_dept_code = '1' AND sbi.survey_year = '$selected_year' AND sbi.survey_month = '$key'";
        
        $services_res_qry_fire = db_query($services_res_qry);
        $services_res_row = $services_res_qry_fire->fetchAll();
        

                    // echo $services_res_row['0']->sales_sum."\t";
                    // echo "NA"."\t";

                    echo $services_res_row['0']->sales_sum . "\t";
    }
    
    }   
    }
    print("\n");
    
     }
     
     
    $date = date("d-m-Y");
    $filename = 'AO_Dealers-' . $date . '.xls';


    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=$filename");
    exit;
}

function get_months_for_header($from_month, $to_month) {

    $months_year = array(
        '1' => 'Jan',
        '2' => 'Feb',
        '3' => 'Mar',
        '4' => 'Apr',
        '5' => 'May',
        '6' => 'Jun',
        '7' => 'Jul',
        '8' => 'Aug',
        '9' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
    );


    for ($from_month; $from_month <= $to_month; $from_month++) {

        $header_array[] = $months_year[$from_month];
    }
    return $header_array;
}

function get_months_for_header_itr($from_month, $to_month) {

    $months_year = array(
        '1' => 'Jan',
        '2' => 'Feb',
        '3' => 'Mar',
        '4' => 'Apr',
        '5' => 'May',
        '6' => 'Jun',
        '7' => 'Jul',
        '8' => 'Aug',
        '9' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
    );


    for ($from_month; $from_month <= $to_month; $from_month++) {

        $header_array[$from_month] = $months_year[$from_month];
    }
    return $header_array;
}
