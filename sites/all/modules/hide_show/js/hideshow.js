/*
 * Behavior for the automatic file upload
 */

(function ($) {
  Drupal.behaviors.autoUpload = {
    attach: function(context, settings) {
      if ($('body').hasClass('html')) {
		function hideRtSdBr(fadeSpeed) {
          $('#sidebar-first').fadeOut(fadeSpeed, function () {
            $('body').removeClass('sidebars').addClass('sidebar-left');
            $('#moreinfo span').text('');
            $('#moreinfo').attr('title', 'Click to view more information');
          }); //end $('#sidebar-right').fadeOut(fadeSpeed, function ()
        } //end function hideRtSdBr(fadeSpeed)
 
        //hide the side bar on page load
        //hideRtSdBr("fast");
        //setup info button for sidebar
        $('#main').prepend('<a id="moreinfo" title="Click to view more information" onClick="return false;" href="#" style="position:relative; float:right; width:32px; min-height:32px; top:0; right:0; z-index: 6; text-align:center; text-decoration:none;"><img src="/files/information_sm.png" alt="information icon" /><span style="font-size:.9em; line-height:.7em;"></span></a>');
 
        //set up moreinfo icon click function
        $('#moreinfo').click(function () {
          if ($('body').hasClass('sidebars')) {
            hideRtSdBr("fast");
          } else {
            $('body').removeClass('sidebar-left').addClass('sidebars');
            $('#sidebar-right').fadeIn("fast").css({'border': '1px solid #CCCCCC', 'width': '198px'});
            $('#moreinfo span').text('Hide');
            $('#moreinfo').attr('title', 'Click to hide more info.');
          } //end if hasclass sidebars
        }); // end $('#moreinfo').click(function ()
      }
    }
  };
})(jQuery);
