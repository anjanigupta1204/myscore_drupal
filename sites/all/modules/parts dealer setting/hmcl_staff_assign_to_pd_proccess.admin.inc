<?php
module_load_include('module', 'staff', 'staff');


function associated_dealers2($arg=NULL) {
	 $staff_member_code = $arg;  
	  $result = db_select('hero_dealer_mapping_status', 'n')
		->fields('n', array('dealer_Name','dealer_code'))
		->condition('dealer_mapped_to_staff_person_code', $staff_member_code,'=')
		->condition('dealer_mapped_to_staff_person_department_code', 'parts','=')
		->execute()
		->fetchAll();
	 $output = array();
	 foreach($result as $key => $val) {
		 $val = (array) $val;
		 $output[$val['dealer_code']] = $val['dealer_Name'];
	 }	 	
	if(empty($output)) {
	  $output = array('0' =>'No Dealer Found.');	
	}	    
  return $output;
}

function pd_hmcl_staff_member_description($form, &$form_state, $path = NULL) {
	
    $selected_emp = (isset($_SESSION['ord_status']['empsearch']) ? $_SESSION['ord_status']['empsearch'] : 'All');
  
   // We are going to output the results in a table with a nice header.
  $header = array(
    // The header gives the table the information it needs in order to make
    // the query calls for ordering. TableSort uses the field information
    // to know what database column to sort by.
	array('data' => t('Operation')),
    array('data' => t('Employee Code ')),
    array('data' => t('Employee Designation ')),
    array('data' => t('Employee name ')),
  
    array('data' => t('Department')),
    array('data' => t('Area Office')),
    array('data' => t('Active')),
    array('data' => t('Zone')),
    array('data' => t('Contact number')),
    array('data' => t('Report To')),
    array('data' => t('Reporting To (name) ')),
    array('data' => t('Personal Email id '))
    
  );
  
	//print_r($_SESSION); exit;
   // fetch all record
   
	if($_SESSION['ord_status']['empsearch']!='')
	{
 
   $result = db_select('hero_hmcl_staff', 'hs')
    ->fields('hs')
	->extend('PagerDefault')
	->condition('hs.emp_code',$_SESSION['ord_status']['empsearch'])
	->limit(5)    
    ->execute()
    ->fetchAll();
 
	} else {
	$result = db_select('hero_hmcl_staff', 'hs')
    ->fields('hs')
	->extend('PagerDefault')
	->limit(5)    
    ->execute()
    ->fetchAll();

	
   } 

 // print "<pre>"; print_r($result); exit;
 
 if(!empty($result)) {	 	 
  foreach($result as $key => $val) {
     if($path == 'admin') {
		 $link = l('assign',('parts-dealer/admin/staff/list/'.$val->staff_id.'/assign'));
	  }
	  else {
		$link = l('assign',('parts-dealer/hmcl/staff/'.$val->staff_id.'/assign')); 
	  }
	  $rows[] = array( 
	    array('data' => t($link)),
		array('data' => t($val->emp_code)),  
		array('data' => t($val->emp_designation_type)), 
		array('data' => t($val->emp_name)),
	
		array('data' => t($val->emp_department)), 
		array('data' => t($val->emp_area_office_name)),
		array('data' => t(($val->emp_status == '1') ? 'Active': 'Block')),  
		array('data' => t($val->emp_zone_name)),
		array('data' => t($val->emp_primary_contact_no)),  
		array('data' => t($val->emp_report_to_designation_code)), 
		array('data' => t($val->emp_report_to_name)),
		array('data' => t($val->emp_report_to_person_email_id)),
		   
	  ); 
  }

  $form['portal2'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('HMCL Staff List'), 
	  '#weight' => 5, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => FALSE,
  );
  $selecteddealer = pd_get_second_dropdown_options1_employee($selected_emp); 
    $form['portal2']['empsearch'] = array(
	   '#type' => 'select',
	   '#validated' => TRUE,
	   '#title' => t('Select Employee'),
	   '#options' => $selecteddealer,
	   
	   '#ajax' => array(
       'callback' => 'pd_get_second_dropdown_options1_employee',
        'wrapper' => 'dropdown-second-replace3',
       ),
	   '#default_value' => $selected_emp,
	  
   ); 

 $form['portal2']['search'] = array(
	  '#type' =>'submit', 
	  '#submit' => array('emp_submit'),
	  '#value' => t('Search'), 
	  '#weight' => 0,
   );   

  $form['portal2']['pager_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no date formats found in the db'),
  );

  // attach the pager theme
  $form['portal2']['pager_pager'] = array('#theme' => 'pager');   
}else {
  $form['portal2'] = array(
	  '#type' => 'fieldset', 
	  '#title' => t('Result'), 
	  '#weight' => 5, 
	  '#collapsible' => TRUE, 
	  '#collapsed' => FALSE,
  );
  // build the table for the nice output.
  $form['portal2']['tablesort_table'] = array(
    '#type'=> 'item',
    '#title'=> t(''),
    '#markup'=> t('No result found.'),
  );	

}

  return $form;
}


function pd_get_second_dropdown_options1_employee( $arg) {
     
	 $result = db_select('hero_hmcl_staff', 'hm')
	->fields('hm',array('emp_code','emp_name'))
	->condition('emp_status', 1)
	->orderBy('hm.emp_code', 'ASC')
	->execute()
	->fetchAll();
     
	   // print_r($result); 
     $arr = array(''=>'All');
	 $output= array(''=>$arr);
	 //print_r($output);exit;
	 foreach($result as $key => $val) {
		 $val = (array) $val;
		 
		 //print_r($val);
		 // for dealer name and code in STRING(OLD)
	  //  FOR DEALER NAME AND CODE IN ARRAY 

	$output[$val['emp_code']]= array($val['emp_code'] => $val['emp_name']);
	 
	 }	
//print_r($output);exit;	 
	if(empty($output)) {
	  $output = array('0' =>'No Dealer Found.');	
	}	    
    

	return $output;
 
    //return array(0 => t("All"));
 
}


function hmcl_profile_update2( $form, &$form_state) {
 
   $replacement = array(
    '@province_id' => $form_state['values']['emp_area_office_code'],
    '@city_id' => $form_state['values']['emp_area_office_name'],
  );
   
   $path = $_GET['q'];
   $pathn = explode('/', $path); 
   
  
   
   $emp_design = $form_state['values']['emp_design'];
   $emp_zone = $form_state['values']['emp_zone'];
   $emp_zone_code = $form_state['values']['emp_zone_code'];
   $emp_department = $form_state['values']['emp_department'];
   $emp_status = $form_state['values']['emp_status'];
   
 
	if($emp_design == 'asm')
	{
	
		$arr =array();
		//echo "SELECT `emp_code`,`emp_name` FROM `hero_hmcl_staff` WHERE `emp_designation_type`='zo' and  `emp_zone_code` ='1' and  `emp_status` = '1' and `emp_department` = 'sales'";
		$result = db_query("SELECT `emp_code`,`emp_name`,`emp_email_id` FROM 
		`hero_hmcl_staff` 
		WHERE `emp_designation_type`='zo' 
		and  `emp_zone_code` = '$emp_zone_code' and  `emp_status` = '$emp_status' 
		and `emp_department` = '$emp_department'");
		foreach ($result as $row) {
			$arr[] = $row;
		}	
		$vid = $arr;	
		$emp_report_to_designation = $vid[0]->emp_code;
		$emp_report_to_name = $vid[0]->emp_name;
		$emp_report_to_person_email_id = $vid[0]->emp_email_id; 
    }else{
		//print "<pre>"; print_r($form_state['values']); exit;
		$emp_report_to_designation = $form_state['values']['emp_report_to_designation_code'];
		$emp_report_to_name = $form_state['values']['emp_report_to_name'];
		$emp_report_to_person_email_id = $form_state['values']['emp_report_to_person_email_id'];
    }
	
	
	//print_r($pathn); exit;
	$emp_name =  $form_state['values']['emp_name'];
	$emp_login_id =  $form_state['values']['emp_login_id'];
	$emp_code = $form_state['values']['emp_code'];
	$emp_email_id = $form_state['values']['emp_email_id'];
	$emp_department = $form_state['values']['emp_department'];
	$emp_contact_number = $form_state['values']['emp_contact_number'];
	$emp_design = $form_state['values']['emp_design'];
	$emp_area_office = $form_state['values']['emp_area_office_name']; 
	$emp_area_office_code = $form_state['values']['emp_area_office_code'];
	
	
	
	
	
	$query = db_select('hero_hmcl_staff', 'hd');
	$query->fields('hd', array('emp_code'));
	$query->condition('hd.emp_code', $emp_code);
	// This will return the count of the fields found.
	
	$row_count = $query->countQuery()->execute()->fetchField();
      
	$listdesign = db_select('hero_hmcl_staff', 'h')
								 ->fields('h',array('emp_designation_type','emp_area_office_code'))
								 ->condition('emp_code', $emp_code,'=')
								 ->execute();
			$empdesinnation = $listdesign->fetchAll();
	  
	        $design = $empdesinnation['0']->emp_designation_type; 
	        $area_office_code = $empdesinnation['0']->emp_area_office_code; 
	        
			$listtsm = db_select('hero_hmcl_staff', 'h')
								 ->fields('h',array('emp_designation_type','emp_area_office_code','emp_code','emp_status','emp_department'))
								 ->condition('emp_code', $emp_code,'!=')
								 ->condition('emp_status', '1','=')
								 ->condition('emp_area_office_code', $area_office_code,'=')
								 ->condition('emp_designation_type', $design,'=')
								 ->condition('emp_department', $emp_department,'=')
								 ->execute();
			$emptsm = $listtsm->fetchAll();
	  
	        $emptsmfinal = $emptsm['0']->emp_code; 
	         
			
			$listdealer = db_select('hero_dealer_mapping_status', 'q')
							 ->fields('q',array('dealer_code','dealer_mapped_to_staff_person_department_code'))
							 ->condition('dealer_mapped_to_staff_person_code', $emp_code,'=')
							 ->execute();
			$alldealerlist = $listdealer->fetchAll();
			//print_r($alldealerlist);  exit;
			$list_dealer = array();
			$list_dept = array();
			foreach($alldealerlist as $dlist)
			{
					$list_dealer[]=$dlist->dealer_code;
					$list_dept[]=$dlist->dealer_mapped_to_staff_person_department_code;
			}
             if(!empty($list_dealer))
			 {
					if($design != $emp_design  || $area_office_code !=$emp_area_office_code)
					{ 
					  db_update('hero_dealer_mapping_status')
										->fields(array(
										 'is_dealer_mapped' => NULL,
										 'dealer_mapped_to_staff_person_code'=>$emptsmfinal,
										))
										->condition('dealer_code', $list_dealer,'IN')
										->condition('dealer_mapped_to_staff_person_department_code', $list_dept,'IN')
								 ->execute();
				   }
			 }
              

			
  
   if (isset($row_count) && ($row_count > 0)) {
	//echo $emp_design;
	//exit;
	//	echo 'tstasfd'; exit;
    if($emp_design=='zo')
	{
               
    $result= db_update('hero_hmcl_staff')
		   ->fields(array(
		
			'emp_name' => $emp_name,
			'emp_login_id' => $emp_login_id,
			'emp_designation_type' => $emp_design,
			'emp_email_id' => $emp_email_id,
			'emp_department' => $emp_department,
			'emp_primary_contact_no' => $emp_contact_number,
			'emp_report_to_designation_code' => $emp_report_to_designation,
			'emp_area_office_name' => $emp_area_office,
			'emp_area_office_code' => $emp_area_office_code,
			'emp_status' => $emp_status,
			'emp_zone_name' => $emp_zone,
			'emp_zone_code' => $emp_zone_code,
			'emp_report_to_name' => $emp_report_to_name,
			'emp_report_to_person_email_id' => $emp_report_to_person_email_id,
			
		 ))
		 ->condition('staff_id',$pathn[3],'=')
		 ->execute();
	 
	   db_update('hero_hmcl_staff')
		   ->fields(array(
		    'emp_report_to_designation_code' => $emp_code,
			'emp_report_to_name' => $emp_name,
			'emp_report_to_person_email_id' => $emp_email_id,
			
		 ))
		 ->condition('emp_designation_type','asm','=')
		 ->condition('emp_department',$emp_department,'=')
		 ->condition('emp_zone_code',$emp_zone_code,'=')
		 ->execute();
		 
    // form_set_error('emp_code', t('This Employee code <b>('.$emp_code.')</b> already exists,You can Update information of Current Employee'));
     drupal_set_message(t('Current Employee Record has been successfully updated...'));
        } else{		
				if($emp_design == 'tsm')
				{
				//echo $emp_design;
				//echo 'hello';
				//exit;
				$result = db_select('hero_hmcl_staff', 'hm')
				->fields('hm')
				->condition('emp_department', $emp_department,'=')
				->condition('emp_designation_type', 'asm','=')
				->condition('emp_area_office_code', $emp_area_office_code,'=')
				->condition('emp_status', '1','=')
				->execute()
				->fetchAll();
			//echo $emp_department.$emp_area_office_code;
			
			//print_R($result);
			//	exit;
				
				 $tsmemp_area_office_name   = $result[0]->emp_area_office_name; 
				 $tsmemp_report_to_designation_code   = $result[0]->emp_code; 
				 $tsmemp_report_to_name  = $result[0]->emp_name; 
				 $tsmemp_report_to_person_email_id =  $result[0]->emp_email_id; 

				$result= db_update('hero_hmcl_staff')
					   ->fields(array(
					
						'emp_name' => $emp_name,
						'emp_login_id' => $emp_login_id,
						'emp_designation_type' => $emp_design,
						'emp_email_id' => $emp_email_id,
						'emp_department' => $emp_department,
						'emp_primary_contact_no' => $emp_contact_number,
						'emp_report_to_designation_code' => $tsmemp_report_to_designation_code,
						'emp_area_office_name' => $tsmemp_area_office_name,
						'emp_area_office_code' => $emp_area_office_code,
						'emp_status' => $emp_status,
						'emp_zone_name' => $emp_zone,
						'emp_zone_code' => $emp_zone_code,
						'emp_report_to_name' => $tsmemp_report_to_name,
						'emp_report_to_person_email_id' => $tsmemp_report_to_person_email_id,
						
					 ))
					 ->condition('staff_id',$pathn[3],'=')
					 ->execute();
					 
			}else{
			  
				$result= db_update('hero_hmcl_staff')
					   ->fields(array(
					
						'emp_name' => $emp_name,
						'emp_login_id' => $emp_login_id,
						'emp_designation_type' => $emp_design,
						'emp_email_id' => $emp_email_id,
						'emp_department' => $emp_department,
						'emp_primary_contact_no' => $emp_contact_number,
						'emp_report_to_designation_code' => $emp_report_to_designation,
						'emp_area_office_name' => $emp_area_office,
						'emp_area_office_code' => $emp_area_office_code,
						'emp_status' => $emp_status,
						'emp_zone_name' => $emp_zone,
						'emp_zone_code' => $emp_zone_code,
						'emp_report_to_name' => $emp_report_to_name,
						'emp_report_to_person_email_id' => $emp_report_to_person_email_id,
						
					 ))
					 ->condition('staff_id',$pathn[3],'=')
					 ->execute();
					 
				   } 
				 //form_set_error('emp_code', t('This Employee code <b>('.$emp_code.')</b> already exists,You can Update information of Current Employee'));
					drupal_set_message(t('Current Employee Record has been successfully updated.'));					
				/* data saving done */
       }
     }else {
	 
	// echo 'tst1'; exit;
	 
	  $result= db_update('hero_hmcl_staff')
		   ->fields(array(
		
			'emp_name' => $emp_name,
			'emp_login_id' => $emp_login_id,
			'emp_code' => $emp_code,
			'emp_designation_type' => $emp_design,
			'emp_email_id' => $emp_email_id,
			'emp_department' => $emp_department,
			'emp_primary_contact_no' => $emp_contact_number,
			'emp_report_to_designation_code' => $emp_report_to_designation,
			'emp_area_office_name' => $emp_area_office,
			'emp_area_office_code' => $emp_area_office_code,
			'emp_status' => $emp_status,
			'emp_zone_name' => $emp_zone,
			'emp_zone_code' => $emp_zone_code,
			'emp_report_to_name' => $emp_report_to_name,
			'emp_report_to_person_email_id' => $emp_report_to_person_email_id,
			
		 ))
		 ->condition('staff_id',$pathn[3],'=')
		 ->execute();
 
     drupal_set_message(t('HMCL Employee Code and Record has been successfully updated.'));
   }

    	 
}	


function form_staff_detail2($form, $form_state, $arg = NULL) {
 //print_r($arg); exit;
 if(is_numeric($arg)) { 
	$result = db_select('hero_hmcl_staff', 'hs')
    ->fields('hs')
    ->condition('staff_id', $arg,'=')
    ->execute()
    ->fetchAssoc();
//print_r($result); exit;
	
$form['add_staff_member_update'] = array(
  '#type' => 'fieldset', 
  '#title' => t($result['emp_name'].' Profile'), 
  '#prefix' => '<div class="portal-form">',
  '#suffix' => '</div>',
  '#weight' => -55, 
  '#collapsible' => TRUE, 
  '#collapsed' => FALSE,
);	
$form['add_staff_member_update']['emp_code'] = array(
  '#type' => 'textfield', 
  '#title' => t('Employee Code '), 
  '#size' => 30, 
  '#disabled' => FALSE,
  '#default_value' => $result['emp_code'],
);
$form['add_staff_member_update']['emp_email_id'] = array(
  '#type' => 'textfield', 
  '#title' => t('Employee Email Id '), 
  '#size' => 30,
  '#disabled' => FALSE,
  '#default_value' => $result['emp_email_id'],   
);

$form['add_staff_member_update']['emp_design'] = array(
   '#type' => 'select',
   '#title' => t('Employee Designation'),
   '#options' => array(
	 '' => t('Select'),		
	 'tsm' => t('TSM'),
	 'asm' => t('ASM'),
	 'zo' => t('ZO'),
	 'zpm' => t('ZPM'),
	 'rpm' => t('RPM'),
	 'apm' => t('APM'),
	),
  '#disabled' => FALSE,
  '#default_value' => $result['emp_designation_type'],
);


$form['add_staff_member_update']['emp_name'] = array(
  '#type' => 'textfield', 
  '#title' => t('Employee name '), 
  '#size' => 30,
  '#disabled' => FALSE,
  '#default_value' => $result['emp_name'],   
);

$form['add_staff_member_update']['emp_login_id'] = array(
  '#type' => 'textfield', 
  '#title' => t('Employee Login Id '), 
  '#size' => 30,
  '#disabled' => FALSE,
  '#default_value' => $result['emp_login_id'],   
);


$form['add_staff_member_update']['emp_department'] = array(
	'#type' => 'select', 
	'#title' => t('Department'), 
	'#options' => array(	 
	 'parts' => t('Parts'),
	),
	'#disabled' => FALSE,
	'#default_value' => $result['emp_department'], 
);


$form['add_staff_member_update']['emp_zone'] = array(

  '#type' => 'select',
   '#title' => t('Zone'),
   '#options' => _get_zone_info_(),
   '#disabled' => FALSE,
   '#default_value' => $result['emp_zone_name'],
  
);

$form['add_staff_member_update']['emp_zone_code'] = array(
   '#type' => 'select',
   '#options' => _get_zone_code_(), 
   '#title' => t('Zone Code'),  
   '#required' => FALSE,
   '#default_value' => $result['emp_zone_code'],
   /**'#prefix' =>'<div id="dropdown-second-replace">',
   '#suffix' => '</div>',**/
);



/* test ************************************************/



$form['add_staff_member_update']['emp_area_office_code'] = array(
    '#title' => t('Area Office code'),
    '#type' => 'select',
    '#options' => _get_area_office_code__info_(),
    '#ajax' => array(
      'event'=>'change',
      'callback' =>'sandbox_ajax_dropdown_city_update',
      'wrapper' => 'dropdown_second_replace',
    ),
	'#default_value' => $result['emp_area_office_code'],
  );

  // Wrapper for city dropdown list
  $form['add_staff_member_update']['wrapper'] = array(
     '#prefix'  => '<div id="dropdown_second_replace">',
    '#suffix' => '</div>',
  );

  // Options for city dropdown list
  $options = array($result['emp_area_office_name']);
  if (isset($form_state['values']['emp_area_office_code'])) {
    // Pre-populate options for city dropdown list if emp_area_office_code id is set
    $options = _load_city($form_state['values']['emp_area_office_code']);

  }
  
  $options_2 = array($result['emp_report_to_designation_code']);
  if (isset($form_state['values']['emp_area_office_code'])) {
    // Pre-populate options for city dropdown list if emp_area_office_code id is set
    $options_2 = _load_emp_code_name_designation($form_state['values']['emp_area_office_code']);

  } 
  
  
  $options_3 = array($result['emp_report_to_person_email_id']);
  if (isset($form_state['values']['emp_area_office_code'])) {
    // Pre-populate options for city dropdown list if emp_area_office_code id is set
	
    $options_3 = _load_emp_email($form_state['values']['emp_area_office_code']);
	

  }
 
  $options_4 = array($result['emp_report_to_name']);
  if (isset($form_state['values']['emp_area_office_code'])) {
    // Pre-populate options for city dropdown list if emp_area_office_code id is set
    $options_4 = _load_emp_name($form_state['values']['emp_area_office_code']);

  }

  // emp_area_office_code dropdown list
  $form['add_staff_member_update']['wrapper']['emp_area_office_name'] = array(
    '#title' => t('Area Office Name'),
    '#type' => 'select',
    '#options' => $options,
	'#default_value' => $result['emp_area_office_name'],
  );
  
  $form['add_staff_member_update']['wrapper']['emp_report_to_designation_code'] = array(
     '#title' => t('Report To (Employee Code)'),
    '#type' => 'select',
    '#options' => $options_2,
	'#default_value' => $result['emp_report_to_designation_code'],
  );
  
  $form['add_staff_member_update']['wrapper']['emp_report_to_name'] = array(
	'#title' => t('Reporting To (Name)'), 
    '#type' => 'select',
    '#options' => $options_4,
	'#default_value' => $result['emp_report_to_name'],
  );
  
  $form['add_staff_member_update']['wrapper']['emp_report_to_person_email_id'] = array(
    '#title' => t('Reporting Manager email id '),
    '#type' => 'select',
    '#options' => $options_3,
	'#default_value' => $result['emp_report_to_person_email_id'],
  );
  
 
  
 
 /*   test*********************************************** */


$form['add_staff_member_update']['emp_status'] = array(
   '#type' => 'select',
   '#title' => t('Status'),
   '#options' => array(
	  1 => t('Active'),
	 0 => t('Block'),
   ),
   '#default_value' => $result['emp_status'],
   '#disabled' => FALSE,
);	

$form['add_staff_member_update']['emp_contact_number'] = array(
  '#type' => 'textfield', 
  '#title' => t('Contact number'), 
  '#size' => 30, 
  '#default_value' => $result['emp_primary_contact_no'], 
  '#disabled' => FALSE, 
);
$form['add_staff_member_update']['mapped'] = array(
  '#type' => 'select', 
  '#title' => 'Associated dealer',  
  '#options' => associated_dealers2($result['emp_code']), 
  '#multiple' => TRUE, 
  '#disabled' => FALSE,
);

$form['add_staff_member_update']['submit'] = array(
		'#type' => 'submit',
		'#submit' => array('hmcl_profile_update2'),
		'#value' => t('Update'),
		'#weight' => 5,
	);
  
 }  

return $form;
}



 