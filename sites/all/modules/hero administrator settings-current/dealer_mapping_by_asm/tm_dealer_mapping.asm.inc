<?php

function dealer_mapping_on_asm_level($theme = NULL) {
 // Fetch and sort dealer_mappings.
global $user;
$employee = $_SESSION[$user->name];
if($employee['emp_designation_type'] == 'asmview')
{
 return drupal_get_form('tsm_mapping_form_for_asmview');
  }
else{ 
  return drupal_get_form('tsm_dealer_mapping_form');
  }
}

/**************************************************
*  START TM-DEALER MAPPING FORM FOR READ ONLY ASM 
**************************************************/

function tsm_mapping_form_for_asmview($form, $form_state)
{
global $user,$theme_key;
$employee = $_SESSION[$user->name];
  drupal_theme_initialize();
  if (!isset($theme)) {
    // If theme is not specifically set, rehash for the current theme.
    $theme = $theme_key;
  }
if($employee['emp_designation_type'] == 'asmview')
{
   $assoEmpCode = array();
	$q = "SELECT e1.emp_code FROM hero_hmcl_staff e1 WHERE 
			e1.emp_report_to_designation_code IN(SELECT e2.emp_code FROM hero_hmcl_staff e2 WHERE 
			e2.emp_area_office_code = ".$employee['emp_area_office_code'].")";
	
      $all_code = db_query($q); 
        foreach($all_code as $key => $val){
		$assoEmpCode[] = $val->emp_code;
        }
	         $query = db_select('hero_dealer_mapping_status','d');
	         $query->join('hero_hmcl_staff','h','h.emp_code = d.dealer_mapped_to_staff_person_code');
			 $query->fields('d',array('dealer_code','dealer_Name','dealer_mapped_to_staff_person_code','dealer_mapped_to_staff_person_department_code'))
			      ->fields('h',array('emp_name','emp_report_to_name'));
			 $query->condition('d.dealer_mapped_to_staff_person_code',$assoEmpCode, 'IN');
			 //$query->condition('d.dealer_mapped_to_staff_person_department_code','services');
			 $query->orderBy('d.dealer_code','asc');
			 $query->groupBy('d.dealer_code');
			 $query = $query->extend('TableSort')->extend('PagerDefault')->limit(15);
			 $res = $query->execute();
			 
	   $all_asso_emp_name = array();
	   $sales_asso_emp_name = get_all_asso_staff_person_for_asmview($assoEmpCode,'sales');
	    $services_asso_emp_name = get_all_asso_staff_person_for_asmview($assoEmpCode,'services');
	  $asso_dealer_name = array();

	  
 /**$form['sale_mapping'] = array(
 '#type' => 'fieldset',
 '#title' => t('Sales Department'),
 '#title_display' => 'invisible',
  '#collapsible' => true, 
		  '#collapsed' => true, 
		 '#attributes' => array(
				  'class' => array('collapsible'),
				),
 );
 $form['services_mapping'] = array(
 '#type' => 'fieldset',
 '#title' => t('Services Department'),
 '#title_display' => 'invisible',
  '#collapsible' => true, 
		  '#collapsed' => true, 
		 '#attributes' => array(
				  'class' => array('collapsible'),
				),
 );**/
 
 
 $form['sale_mapping'] = array(); $form['services_mapping'] = array();
  $form['#tree'] = TRUE;
  $form['edited_theme'] = array(
    '#type' => 'value',
    '#value' => $theme,
    );
	

	/**************/
      //echo '<pre>'; print_r($res);exit;
	  
foreach($res as $key => $val){
   $asso_dealer_name[$val->dealer_code] = $val->dealer_Name;
  $key = $val->dealer_code;
  
   $form['sales_mapping'][$key]['sales_dealer_code'] = array(
      '#type' => 'markup',
      '#markup' => $val->dealer_code,
    );
  $form['sales_mapping'][$key]['sales_dealer_name'] = array(
      '#type' => 'markup',
      '#markup' => $val->dealer_Name,
    );
  $form['sales_mapping'][$key]['sales_asso_emp_code'] = array(
      '#type' => 'select',
	  '#default_value' => get_asso_employee($val->dealer_code,'sales'), 
      '#options' =>$sales_asso_emp_name,
    );
		
	$form['services_mapping'][$key]['services_dealer_code'] = array(
      '#type' => 'markup',
      '#markup' => $val->dealer_code,
    );
  $form['services_mapping'][$key]['services_dealer_name'] = array(
      '#type' => 'markup',
      '#markup' => $val->dealer_Name,
    );
	$form['services_mapping'][$key]['services_asso_emp_code'] = array(
      '#type' => 'select',
	  '#default_value' => get_asso_employee($val->dealer_code,'services'), 
      '#options' =>$services_asso_emp_name,
    );
	$form['sales_mapping'][$key]['sales_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save Sales Mapping'),
	'#submit' => array('save_sales_mapping_for_asmview'),
    );
	$form['services_mapping'][$key]['services_save'] = array(
    '#type' => 'submit',
    '#value' => t('Save Service Mapping'),
	'#submit' => array('save_services_mapping_for_asmview'),
    );
  }
}
 
 return $form;
}

function save_sales_mapping_for_asmview($form, &$form_state)
{
$update = $form_state['values']['sales_mapping'];
//echo '<pre>'; print_r($update);exit;
if(!empty($update)){
	foreach($update as $key => $value){	
	$dealer_code = $key ;
		 $update_mapping = db_update('hero_dealer_mapping_status') 
		  ->fields(array(
			'dealer_mapped_to_staff_person_code' => $value['sales_asso_emp_code'],
			'is_dealer_mapped' => '1',
		  ))
		   ->condition('dealer_code', $dealer_code, '=')
		   ->condition('dealer_mapped_to_staff_person_department_code', 'sales', '=')
		  ->execute();
  /****** Update survey to hand over to other TSM ******/		  
		    $dept = 1;
		 $update_survey =  db_update('survey_basic_info') 
		  ->fields(array(
			'survey_owner_emp_code' => $value['sales_asso_emp_code'],
		  ))
		   ->condition('asso_dealer_code', $dealer_code, '=')
		   ->condition('survey_for_dept_code', $dept, '=')
		  ->execute();	
/******END*****/			  
		}	
  drupal_set_message(t('Dealer-TSM(sales) mapping has been successfully Updated.'));

}else{
     drupal_set_message(t('Not updated. Please check again!'),'warning');
}


}

function save_services_mapping_for_asmview($form, &$form_state)
{
$update = $form_state['values']['services_mapping'];
//echo '<pre>'; print_r($update);exit;
if(!empty($update)){
	foreach($update as $key => $value){	
	$dealer_code = $key ;
		 $update_mapping = db_update('hero_dealer_mapping_status') 
		  ->fields(array(
			'dealer_mapped_to_staff_person_code' => $value['services_asso_emp_code'],
			'is_dealer_mapped' => '1',
		  ))
		   ->condition('dealer_code', $dealer_code, '=')
		   ->condition('dealer_mapped_to_staff_person_department_code', 'services', '=')
		  ->execute();
  /****** Update survey to hand over to other TSM ******/		  
		    $dept = 2;
		 $update_survey =  db_update('survey_basic_info') 
		  ->fields(array(
			'survey_owner_emp_code' => $value['services_asso_emp_code'],
		  ))
		   ->condition('asso_dealer_code', $dealer_code, '=')
		   ->condition('survey_for_dept_code', $dept, '=')
		  ->execute();	
/******END*****/			  
		}	
  drupal_set_message(t('Dealer-TSM(services) mapping has been successfully Updated.'));

}else{
     drupal_set_message(t('Not updated. Please check again!'),'warning');
}


}


/******* End mapping for READ ONLY ASM *******/

/*********************************************************************
*  Get only associate employee which is mapped to a perticular dealer
*********************************************************************/ 

function get_asso_employee($dealer_code,$dept)
{
     $query = db_select('hero_dealer_mapping_status','d');
	     $query->join('hero_hmcl_staff','h','h.emp_code = d.dealer_mapped_to_staff_person_code');
		 $query->fields('d',array('dealer_mapped_to_staff_person_code'))
		       ->fields('h',array('emp_name'));
		 $query->condition('d.dealer_code',$dealer_code, '=');
		 $query->condition('d.dealer_mapped_to_staff_person_department_code',$dept,'=');
		     $asso_emp = $query->execute()->fetchAssoc();
			// print_r($asso_emp);
			   
		if(!empty($asso_emp))
		{
		//print_r($asso_emp);
		  $asso_emp[$asso_emp['dealer_mapped_to_staff_person_code']] = $asso_emp['emp_name'].'('.$asso_emp['emp_code'].')';
	
		  }
		  else{
		  //echo "hello1";
		  
		    $asso_emp = array('No result found'=>'No result found');
		  }
		 //print_r($asso_emp);
	return $asso_emp;
}

/*********************************************************************
*  Get all associated employee which is mapped to dealer
*********************************************************************/ 

function get_all_asso_staff_person_for_asmview($assoEmpCode,$dept)
{

$asso_emp_name = array();
           $query = db_select('hero_dealer_mapping_status','d');
	         $query->join('hero_hmcl_staff','h','h.emp_code = d.dealer_mapped_to_staff_person_code');
			 $query->fields('d',array('dealer_code','dealer_Name','dealer_mapped_to_staff_person_code','dealer_mapped_to_staff_person_department_code'))
			      ->fields('h',array('emp_name','emp_report_to_name'));
			 $query->condition('d.dealer_mapped_to_staff_person_code',$assoEmpCode, 'IN');
			 $query->condition('d.dealer_mapped_to_staff_person_department_code',$dept,'=');
			 $query->orderBy('d.dealer_code','asc');
			 $res = $query->execute();

			 foreach($res as $key => $val){
		$asso_emp_name[$val->dealer_mapped_to_staff_person_code] = $val->emp_name.'('.$val->dealer_mapped_to_staff_person_code.')';
		}
		if(!empty($asso_emp_name))
		{
		print_r($asso_emp_name);
		  return $asso_emp_name;
		  }else{
		    return array('No result found'=>'No result found');
		  }
}

	function get_all_asso_staff_person($EmpCode,$dept) {
			
			
			 $asso_emp_name = array();
			 $query = db_select('hero_hmcl_staff','h');
			 $query->fields('h',array('emp_name','emp_code'));
			 $query->condition('h.emp_report_to_designation_code',$EmpCode, '=');
			 $query->condition('h.emp_department',$dept,'=');
			 
			 $res = $query->execute();

				foreach($res as $key => $val){

					$asso_emp_name[$val->emp_code] = $val->emp_name.'('.$val->emp_code.')';
				}
				if(!empty($asso_emp_name))
				{
					return $asso_emp_name;
				}else{
					return array('No  result found'=>'No result found');
				}
			
		}
/***************************
// tsm dealer mapping form
/**************************/
function tsm_dealer_mapping_form($form, &$form_state)
{
global $user,$theme_key;
$employee = $_SESSION[$user->name];
$multiRole = explode(',',$employee['emp_designation_type']);
  drupal_theme_initialize();
  if (!isset($theme)) {
    // If theme is not specifically set, rehash for the current theme.
    $theme = $theme_key;
  }
if(in_array('asm', $multiRole) && (!in_array('zo', $multiRole)) )
{

  //print_r($_SESSION); die;
   $assoEmpCode = array();
	$q = "SELECT h.emp_code FROM hero_hmcl_staff h WHERE h.emp_report_to_designation_code = ".$employee['emp_code'];
      $all_code = db_query($q); 
        foreach($all_code as $key => $val){
		$assoEmpCode[] = $val->emp_code;
        }
		if(in_array('tsm', $multiRole))
		{
		$assoEmpCode[] = $employee['emp_code'];
		}
	
	         $query = db_select('hero_dealer_mapping_status','d');
	         $query->join('hero_hmcl_staff','h','h.emp_code = d.dealer_mapped_to_staff_person_code');
			 $query->fields('d',array('dealer_code','dealer_Name','dealer_mapped_to_staff_person_code','dealer_mapped_to_staff_person_department_code'))
			      ->fields('h',array('emp_name','emp_report_to_name'));
			 $query->condition('d.dealer_mapped_to_staff_person_code',$assoEmpCode, 'IN');
			 $query->condition('d.dealer_mapped_to_staff_person_department_code',$employee['emp_department']);
			 $query->orderBy('d.dealer_code','asc');
			 $query = $query->extend('TableSort')->extend('PagerDefault')->limit(15);
			 $res = $query->execute();
			 
	   $all_asso_emp_name = array();
	   $all_asso_emp_name = get_all_asso_staff_person($employee['emp_code'],$employee['emp_department']);
	  $asso_dealer_name = array();
	 // print_r($all_asso_emp_name);exit;
	  
 $form['dealer_mappings'] = array();
  $form['#tree'] = TRUE;
  $form['edited_theme'] = array(
    '#type' => 'value',
    '#value' => $theme,
    );
	  /**************/
      //echo '<pre>'; print_r($res);exit;
	  
		foreach($res as $key => $val){
		
   $asso_dealer_name[$val->dealer_code] = $val->dealer_Name;
  $key = $val->dealer_code;
  
  
   $form['dealer_mappings'][$key]['dealer_code'] = array(
      '#type' => 'markup',
      '#markup' => $val->dealer_code,
    );
  $form['dealer_mappings'][$key]['dealer_name'] = array(
      '#type' => 'markup',
      '#markup' => $val->dealer_Name,
    );
  $form['dealer_mappings'][$key]['asso_emp_code'] = array(
      '#type' => 'select',
	  '#default_value' => get_asso_employee($val->dealer_code,$employee['emp_department']), 
      '#options' =>$all_asso_emp_name,
    );
	$form['dealer_mappings'][$key]['department'] = array(
      '#type' => 'hidden',
      '#value' => $val->dealer_mapped_to_staff_person_department_code,
    );
	$form['dealer_mappings'][$key]['theme'] = array(
      '#type' => 'hidden',
      '#value' => $theme,
    );
	
  }
  
  $form['dealer_mappings']['mapp'] = array(
    '#type' => 'submit',
    '#value' => t('Save Dealer Mappings'),
	'#submit' => array('tsm_dealer_mapping_form_submit')
  );
	/**********************/
	  
}// End ASM LEVEL MAPPING
 
 return $form;
}

function tsm_dealer_mapping_form_submit($form, &$form_state)
{
$update = $form_state['values']['dealer_mappings'];
//echo '<pre>'; print_r($update);exit;
if(!empty($update)){
	foreach($update as $key => $value){	
	$dealer_code = $key ;
		 $updated1 = db_update('hero_dealer_mapping_status') 
		  ->fields(array(
			'dealer_mapped_to_staff_person_code' => $value['asso_emp_code'],
			'is_dealer_mapped' => '1',
		  ))
		   ->condition('dealer_code', $dealer_code, '=')
		   ->condition('dealer_mapped_to_staff_person_department_code', trim($value['department']), '=')
		  ->execute();
  /****** Update survey to hand over to other TSM ******/		  
		  if(trim($value['department']) == 'sales')
		  {
		    $dept = 1;
		  }elseif(trim($value['department']) == 'services'){
		    $dept = 2;
		  }
		  
		 $updated2 =  db_update('survey_basic_info') 
		  ->fields(array(
			'survey_owner_emp_code' => $value['asso_emp_code'],
		  ))
		   ->condition('asso_dealer_code', $dealer_code, '=')
		   ->condition('survey_for_dept_code', $dept, '=')
		  ->execute();	
/******END*****/			  
		}	
  drupal_set_message(t('Dealer mapping has been successfully Updated.'));

}else{
     drupal_set_message(t('Not updated. Please check again!'),'warning');
}

}

function theme_tsm_dealer_mapping_form($variables) {
 $form = $variables['form'];
// print "<pre>"; print_r($form['dealer_mappings']); exit;
  $rows = array();
  foreach (element_children($form['dealer_mappings']) as $id) {
    $form['dealer_mappings'][$id]['weight']['#attributes']['class'] = array('dealer-mappings-weight');
    $rows[] = array(
      'data' => array(
        // Add our 'name' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_code']),
        // Add our 'description' column.
        drupal_render($form['dealer_mappings'][$id]['dealer_name']),
        // Add our 'staff person' column.
        drupal_render($form['dealer_mappings'][$id]['asso_emp_code']),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array(t('Dealer Code'), t('Dealer Name'),  t('Associated TM '));
  $table_id = 'dealer-mappings-table';
  //$output = drupal_render_children($form);
  // We can render our tabledrag table for output.
  $output = drupal_render($form['portal1']);
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
	'empty' => t('<b style="color:#ff0000;font-size:16px;">This is only for Area Manager.<b>'),
    'attributes' => array('id' => $table_id),
  ));
  $output .= theme('pager'); 
  $output .= drupal_render($form['actions']);
  $output .= drupal_render_children($form);
  return $output;
}  


function theme_tsm_mapping_form_for_asmview($variables)
{

$form = $variables['form'];
// print "<pre>"; print_r($form['dealer_mappings']); exit;
  $rows = array();
  foreach (element_children($form['sales_mapping']) as $id) {
    $form['mapping'][$id]['weight']['#attributes']['class'] = array('dealer-mappings-weight');
    $rows[] = array(
      'data' => array(
       // Add our 'name' column.
        drupal_render($form['sales_mapping'][$id]['sales_dealer_code']),
        // Add our 'description' column.
        drupal_render($form['sales_mapping'][$id]['sales_dealer_name']),
        // Add our 'staff person' column.
        drupal_render($form['sales_mapping'][$id]['sales_asso_emp_code']),
		// Add column for save mapping
		drupal_render($form['sales_mapping'][$id]['sales_save']),
      ),
      'class' => array('draggable'),
    );
	}
	foreach (element_children($form['services_mapping']) as $id) {
	$rows1[] = array(
      'data' => array(
       // Add our 'name' column.
        drupal_render($form['services_mapping'][$id]['services_dealer_code']),
        // Add our 'description' column.
        drupal_render($form['services_mapping'][$id]['services_dealer_name']),
        // Add our 'staff person' column.
        drupal_render($form['services_mapping'][$id]['services_asso_emp_code']),
		// Add column for save mapping
		drupal_render($form['services_mapping'][$id]['services_save']),
      ),
      'class' => array('draggable'),
    );
  }

  $header_sales = array(t('Dealer Code'), t('Dealer Name'),  t('Associated TM(Sales) '), t('Action'),);
   $header_services = array(t('Dealer Code'), t('Dealer Name'),  t('Associated TM(Services) '), t('Action'),);
  $table_id = 'dealer-mappings-table';
  $output = drupal_render($form['portal1']);
  // We can render our tabledrag table for output.
  $output .= '<p class ="depart">Sales Department</p>';
  $output .= theme('table', array(
    'header' => $header_sales,
    'rows' => $rows,
	'empty' => t('<b style="color:#ff0000;font-size:16px;">This is only for Area Manager.<b>'),
    'attributes' => array('id' => $table_id),
  ));
  $output .= '<p class ="depart">Services Department</p>';
  
  $output .= theme('table', array(
    'header' => $header_services,
    'rows' => $rows1,
	'empty' => t('<b style="color:#ff0000;font-size:16px;">This is only for Area Manager.<b>'),
    'attributes' => array('id' => $table_id),
  ));
  $output .= theme('pager'); 
  $output .= drupal_render($form['actions']);
  $output .= drupal_render_children($form);
  return $output;


}