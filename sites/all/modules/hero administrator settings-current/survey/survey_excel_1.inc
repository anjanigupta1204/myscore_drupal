
<?php

function excel_import_for_survey() {

    $output = drupal_get_form('Excel_import_for_survey_form');
    return $output;
}

function Excel_import_for_survey_form($form, &$form_state) {

    $form['notes'] = array(
        '#type' => 'markup',
        '#markup' => '<div class="import-notes" >A few notes when uploading. 
<ul>
<li>Step-1:Browse and Choose File in .xls format,Please do not change the format and name of downloaded file</li>
<li>Step-2:Click on the Upload button</li>
<li>Step-3:Click on the import button to complete the process</li>
</ul>
</div>
',
        '#upload_location' => 'public://tmp/',
    );
    $form['import'] = array(
        '#title' => t('Import'),
        '#type' => 'managed_file',
        //'#description' => t('The uploaded xls will be imported and temporarily saved.'),
        '#upload_location' => 'public://tmp/',
		'#required' => TRUE,
		'#upload_validators' => array(
            'file_validate_extensions' => array('xls'),
        ),
    );
	
	

	
    $form['submit'] = array(
        '#type' => 'submit',
		'#value' => t('Import'),
			
	
    );
	
	  $form['submit']['wrapper'] = array(
        '#prefix' => '<div id="city-wrapper">',
        '#suffix' => '</div>',
    );


    return $form;
	
	
	
}
function Excel_import_for_survey_form_validate($form, $form_state) {
//echo "hii"; exit;
$uri = $_SERVER['REQUEST_URI'];
   $urlparam = explode('/', $uri);
   $file_name = $urlparam[3].'_'.$urlparam[4].'.xls';
   $orginal_filename = $form['import']['#file']->filename;
  if(trim($file_name) != trim($orginal_filename)){
  
  form_set_error('filename', t('Please Upload Correct Dealer or Month for Survey'));
  return false;

}
}
//module_load_include('inc', 'excel_report', 'report_page_2');

function Excel_import_for_survey_form_submit($form, $form_state) {



 
    global $user;
    $dealer = arg(1);
    $my = arg(2);
    $dealer_name = get_dealer_name_for_excel($dealer);
    $max_sid = get_max_sid();
    $max_sid = $max_sid + rand(5,10);
    $survey_name = 'survey for ' . $dealer_name;

    //$survey_name = mysql_real_escape_string($survey_name);
    $survey_name = $survey_name;
    $my_array = (explode('-', $my));
    $month = $my_array['0'];
    $month_no = get_month_code($month);
    $survey_created_date = REQUEST_TIME;
    $employee = $_SESSION[$user->name];

    $emp_code = $employee['emp_code'];
    $dept = $employee['emp_department'];
	
	
    if ($dept == 'services') {
        $dept = '2';
    } else {
        $dept = '1';
    }
    $year = $my_array['1'];
  
    
	//echo $form_state['input']['import']['fid'];
	
	
    $uri = db_query("SELECT uri, filename FROM {file_managed} WHERE fid = :fid", array(
        ':fid' => $form_state['input']['import']['fid'],
            ))->fetchField();
			
    // $uri = 'public://tmp/hero_services.csv';
   // $uri = 'public://tmp/hero_sales_2.csv';

    
  

    $cri_arr = get_criteria();
	//print_r($uri);
//survey_offline_online =1
    $sql = "SELECT name FROM {system}  ";
     $table_name_system = db_query_temporary($sql, array());
     
	//print_r($table_name_system); exit;
	
    if (!empty($uri)) {
        
		$query = db_insert('survey_basic_info');
		$query->fields(array(
		  'sid' => $max_sid,
		  'survey_name' => $survey_name,
		  'survey_status_id' => 1,
		  'asso_dealer_code' => $dealer,
		  'survey_for_dept_code' => $dept,
		  'survey_owner_emp_code' => $emp_code,
		  'survey_created_date' => $survey_created_date,
		  'survey_visited_date' => date("Y-m-d"),
		  'survey_month' => $month_no,
		  'survey_year' => $year,
			));
	   $success = $query->execute();
	   $lastId = Database::getConnection()->lastInsertId(); 
		

	
$path=explode("/",$uri);
$pathfianl=$path[3];
//global $base_url;	 
//echo $npath = variable_get('file_public_path', conf_path() . '/files'); die;
module_load_include('inc', 'phpexcel');
  
  // The path to the excel file
  $path = variable_get('file_public_path', conf_path() . '/files')."/tmp/".$pathfianl; ;
  
  $result = phpexcel_import($path);
  $row_data =  $result[0]; 
 
 unset($row_data['0']);
 unset($row_data['1']);
  foreach ($cri_arr AS $k => $v) {
            $m_criteria = $v['criteria_id'];
			
            foreach ($row_data AS $fk => $fv) {
			     
			    $action_plan_date = format_date(strtotime($fv['6']) ,'custom','Y-m-d');
       
			   
                if ($m_criteria == $fv['0']) {
				
                    $res_array[$m_criteria][] = array($fv['2'], $fv['3'], $fv['4'], $fv['5'], $action_plan_date);
					// $res_norm['0'][] = array($fv['3'], $fv['4'], $fv['5'], $fv['5'], $action_plan_date);
                } elseif ($fv['0'] == '0') {
                    $res_norm['0'][] = array($fv['3'], $fv['4'], $fv['5'], $fv['5'], $action_plan_date);
                }
            }
			
        }
		
		
        if ($dept == '2') {
            for ($i = 0; $i <= 10; $i++) {
                $res['0'][] = $res_norm['0'][$i];
            }
        } else {
            for ($i = 0; $i <= 19; $i++) {
                $res['0'][] = $res_norm['0'][$i];
            }
        }

	
       // foreach ($res['0'] AS $nvalue) {
		$employee = $_SESSION[$user->name];
	 $uri = $_SERVER['REQUEST_URI'];
   $urlparam = explode('/', $uri);
  //$date = date('Y-m-d');
  $date = date('d/m/Y');
	$dealer_code = $urlparam['3'];
  $dealer_detail = db_select('hero_dealer', 'hd')
	->fields('hd')
	->condition('dealer_code', $dealer_code,'=')
	->execute()
	->fetchAssoc();
	
echo "<pre>";
	
	if($employee['emp_department'] == 'sales') {
		$sales_norms_fields = array('gm' => 'General manager',
		           'Showroom_manager' => 'Showroom manager',
				   'network_manager' => 'Network manager',
		           'receptionist' => 'Receptionist',
				   'hostess' => 'Hostess',
		           'Sales_executive' => 'Sales Executive',
				   'goodlife_executive' => 'GoodLife Executive',
		           'marketing_executive' => 'Marketing Executive',
		           'rural_support_sales' => 'Rural Support - Sales',
				   'accountant' => 'Accountant',
				   'billing_executive' => 'Billing Executive',
				   'cashier' => 'Cashier',
				   'it_support_executive' => 'IT Support Executive',
		           'delivery_executive' => 'Delivery Executive',
				   'pdi_technician' => 'PDI Technician',
				   'rto_executive' => 'RTO Executive',
				   'network_executive' => 'Network Executive',
				   'housekeeping_staff' => 'Housekeeping Staff',
				   'pantry' => 'Pantry',
				   'security' => 'Security',
		);
		
		$get_sales_norms = db_select('hero_sales_manpower_norms', 'h')
		                     ->fields('h')
		                     ->execute();
		 
		$record = $get_sales_norms->fetchAll(); 
					   $dealer_sales_vol_by_12 = round($dealer_detail['dealer_sales_vol']/12);
					   
$norms = array();

		foreach($record as $key => $val) {
			if(($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($dealer_sales_vol_by_12 <= $val->appliacbility_criteria_max)) {
				$norms = $record[$key];
				
			}elseif(($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($val->appliacbility_criteria_max == NULL)) {
				$norms = $record[$key];
				
			}
		}
		
		$dealer_counter_sales = ceil($dealer_detail['countersale']/75); 
		$norms->Sales_executive = $dealer_counter_sales;
		
		
		foreach($sales_norms_fields as $key => $val) {
		$req=$norms->$key;
	    //print_r($key);
		//print_r($req);
if($val!='')
{	
		db_insert('survey_norms') 
					->fields(array(
					  'survey_id' => $max_sid,
					  'designation' => $val,
					  'require_value' => $req,
					  'actual_value' => $val11,
					  'trained_value' => $trained,
					  'action_plan' => $action_plan,
					  'target_date' => $target_date,
				))
				->execute();

}			
			
			
			print("\n");
			
			
			
		
		}
		//exit;
		}
		elseif($employee['emp_department'] == 'services') {		   
 		  $services_norms = db_select('hero_service_manpower_norms','hsmn')
			   ->fields('hsmn')
			   ->execute()
			   ->fetchAll();
			  
		foreach($services_norms as $key => $val) {
		
		$pnorms = $val->program_norms; 
		// code for services norms	
		
		switch ($key):
		case 0:
			$format = ($dealer_detail['dealer_service_vol'] >= 1000 ? 1 : 0);
		case 1:
			$format = 1;
			break;
		case 2:
			$format = $dealer_detail['dealer_service_vol']/(26*40*12);
			break;
		case 3:
			$format = $dealer_detail['dealer_service_vol']/(26*20*12);
			break;
		case 4:
			$format = 1;
			break;
		case 5:
		// For Technician (no . of ramps + 1) 28-12-13
		    $format = $dealer_detail['no_of_ramps']+1 ; 
			//$format = ($dealer_detail['dealer_service_vol']/(26*10*12))+1;
			break;
		case 6:
		 //For Assistant Technician (no . of ramps + 1)/2 28-12-13 
		    $format = round(($dealer_detail['no_of_ramps']+1)/2);
			//$format = (($dealer_detail['dealer_service_vol']/(26*10*12))+1)/2;
			break;
		case 7:
			$format = $dealer_detail['dealer_service_vol']/(26*20*12);
			break;
		case 8:
			$format = 1;
			break;	
		case 9:
			$format = 1;
			break;	
		case 10:
			$format = $dealer_detail['dealer_service_vol']/(26*50*12);
			break;								
	endswitch;
					
					$val=$val->designation;
					
					$req=round($format);
		
		if($val!='')
{		
		db_insert('survey_norms') 
					->fields(array(
					  'survey_id' => $max_sid,
					  'designation' => $val,
					  'require_value' => $req,
					  'actual_value' => $val11,
					  'program_norms' => $pnorms,
					  'trained_value' => $trained,
					  'action_plan' => $action_plan,
					  'target_date' => $target_date,
				))
				->execute();
}
	
	}
	
			   }
	//echo "</pre>";
	
	//die;







           // $deg = mysql_real_escape_string($nvalue['0']);
	   //  $deg = $nvalue['0'];
		// $degt=mytest($deg);
          //  $available = $nvalue['1'];
           // $trained = $nvalue['2'];
            //$action_plan = $nvalue['3'];
            //$nres = mysql_real_escape_string($nvalue['4']);
            //$target_date = $nvalue['4'];
            //$format = format_designation($nvalue['0']);
            //$req = get_services_required_value_for_norms($dealer, $dept, trim($nvalue['0']));
           
                  //}
     //  exit;

       

    
        foreach ($res_array AS $key => $value) {
		
		
            $cri_name = get_criteria_name($key);
			db_insert('survey_criteria_list') 
					->fields(array(
					  'sid' => $max_sid,
					  'criteria_name' => $cri_name,
					  ))
				->execute();  
            $max_cri_id = get_max_criteria_id();
			
			//print_r($criteria_insert_qry);
		//	die;
			
            foreach ($value AS $fval) {
			   //print_r($fval); exit;
                $question_id = $fval['0'];
                $question_name = $fval['1'];
                $question_action_plan = $fval['3'];
                $question=mytest($question_name);
				$question_responsefinal = $fval['2'];
				 
               if($question_responsefinal=='Yes')
			   {
			   $record = db_select('hero_questions', 'n')
				->fields('n', array('question_weightage'))
				->condition('question_id', $question_id, '=')
				->execute()
				->fetchAll();
				 $question_weightage = $record[0]->question_weightage;
				
				$question_response=$question_weightage;
			   }
			   else
			   {
			   $question_response=0;
			   }
			   
			  $qdate=$fval['4'];
				//echo $qdate;
			
		    db_insert('survey_question_list') 
					->fields(array(
					  'criteria_id' => $max_cri_id,
					  'question_name' => $question_name,
					  'question_response' => $question_response,
					  'question_action_plan' => $question_action_plan,
					  'question_action_plan_date' => $qdate,
					  'Q_id' => $question_id,
					  'S_id' => $max_sid,
					 ))
				->execute();
		
		
		    }
			
		
			}

		//	die;
		}
		 $lastId = Database::getConnection()->lastInsertId();
        

			$get_basicinfo= db_select('survey_question_list', 'q')
								 ->fields('q',array('S_id'))
								 ->condition('question_id', $lastId,'=')
								 ->condition('flag', 0,'=')
								 ->execute();
			
			$flagresult = $get_basicinfo->fetchAll();
			if(!empty($flagresult))
			{
				foreach($flagresult as $sid);
				{
				
				 
				  $survey_sid = $sid->S_id;
				  db_update('survey_basic_info')
									->fields(array(
									 'flag' => 2,
									))
									->condition('sid', $survey_sid,'=')
					   ->execute();
				  drupal_set_message('Survey uploaded Successfully.');
								   
				     db_update('survey_norms')
									->fields(array(
									 'flag' => 2,
									))
									->condition('survey_id', $survey_sid,'=')
					   ->execute();
				      db_update('survey_question_list')
									->fields(array(
									 'flag' => 2,
									))
									->condition('S_id', $survey_sid,'=')
					   ->execute();
				    
				 
				 
				  
				}
            }
		
	}
   

 function mytest($question_name) {
 //$search = array("\x00", "\n", "\r", "\\", "'", "\"", "\x1a"); f
 //$replace = array("\\x00", "\\n", "\\r", "\\\\" ,"\'", "\\\"", "\\\x1a"); 
 //return str_replace($search, $replace, $question_name); 
 } 

function get_month_code($month) {

    $months_year = array(
        'January' => '1',
        'February' => '2',
        'March' => '3',
        'April' => '4',
        'May' => '5',
        'June' => '6',
        'July' => '7',
        'August' => '8',
        'September' => '9',
        'October' => '10',
        'November' => '11',
        'December' => '12',
    );

    foreach ($months_year as $key => $val) {
        if ($key == $month) {
            return $val;
        }
    }
}

function get_dealer_name_for_excel($dealer_code) {
    $dealers = db_select('hero_dealer', 'h')
            ->fields('h', array('dealer_name'))
            ->condition('h.dealer_code', $dealer_code, '=')
            ->execute()
            ->fetchAll();
    return $dealers['0']->dealer_name;
}

function get_max_sid() {

    $query = db_select('survey_basic_info');
    $query->addExpression('MAX(sid)');
    $max = $query->execute()->fetchField();
    return $max;
}

function get_max_criteria_id() {

    $query = db_select('survey_criteria_list');
    $query->addExpression('MAX(criteria_id)');
    $max = $query->execute()->fetchField();
    return $max;
}

function get_criteria() {

    $cri_qry = "SELECT DISTINCT criteria_id FROM hero_criteria";
    $cri_qry_fire = db_query($cri_qry);
    while ($cri_qry_fire_row = $cri_qry_fire->fetchAssoc()) {
        $cri[] = $cri_qry_fire_row;
    }
    return $cri;
}

function get_criteria_name($c_id) {

    $cri_qry = "SELECT DISTINCT criteria_name FROM hero_criteria WHERE criteria_id = '$c_id'";
    $cri_qry_fire = db_query($cri_qry);
    $k = $cri_qry_fire->fetchAll();
    return $k['0']->criteria_name;
}

function get_services_required_value_for_norms($dealer, $dept, $des) {

    $dealers = db_select('hero_dealer', 'h')
            ->fields('h', array('dealer_sales_vol', 'dealer_service_vol', 'no_of_ramps', 'countersale'))
            ->condition('h.dealer_code', $dealer, '=')
            ->execute()
            ->fetchAll();

    $sales_vol = $dealers['0']->dealer_sales_vol;
    $service_vol = $dealers['0']->dealer_service_vol;
    $no_of_ramps = $dealers['0']->no_of_ramps;
    $countersale = $dealers['0']->countersale;

    $services_norms = db_select('hero_service_manpower_norms', 'hsmn')
            ->fields('hsmn')
            ->execute()
            ->fetchAll();



    if ($dept == '2') {
        foreach ($services_norms as $key => $val) {
            // code for services norms	
            switch ($des):
                case 'Manager Customer Care':
                    $format = ($service_vol >= 1000 ? 1 : 0);
                case 'Customer Care Executive':
                    $format = 1;
                    break;
                case 'Floor Supervisor':

                    $format = round($service_vol / (26 * 40 * 12));
                    break;
                case 'Frontline Supervisor':
                    $format = round($service_vol / (26 * 20 * 12));
                    break;
                case 'Parts Incharge':
                    $format = 1;
                    break;
                case 'Technician':
                    // For Technician (no . of ramps + 1) 28-12-13
                    $format = $no_of_ramps + 1;
                    //$format = ($dealer_detail['dealer_service_vol']/(26*10*12))+1;
                    break;
                case 'Assistant Technician':
                    //For Assistant Technician (no . of ramps + 1)/2 28-12-13 
                    $format = round(($no_of_ramps + 1) / 2);
                    //$format = (($dealer_detail['dealer_service_vol']/(26*10*12))+1)/2;
                    break;
                case 'Washer Boys':
                    $format = round($service_vol / (26 * 20 * 12));
                    break;
                case 'Receptionist/Data Entry operator/ Clerks':
                    $format = 1;
                    break;
                case 'Good Life Executive':
                    $format = 1;
                    break;
                case 'Billing Executive':
                    $format = round($service_vol / (26 * 50 * 12));
                    break;
            endswitch;

            return $format;
        }
    }else {

        return get_sales_required_norms($sales_vol, $countersale, $des);
    }
}

function get_sales_required_norms($sales_vol, $countersale, $des) {


    $get_sales_norms = db_select('hero_sales_manpower_norms', 'h')
            ->fields('h')
            ->execute();

    $record = $get_sales_norms->fetchAll();

    $dealer_sales_vol_by_12 = round($sales_vol / 12);

    foreach ($record as $val) {
       
        if (($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($dealer_sales_vol_by_12 <= $val->appliacbility_criteria_max)) {

            switch ($des):
                case 'Genral manager':
                    $format = (is_decimal($val->gm) ? round($val->gm) : $val->gm);
                case 'Showroom manager':
                    $format = (is_decimal($val->Showroom_manager) ? round($val->Showroom_manager) : $val->Showroom_manager);
                    break;
                case 'Network manager':

                    $format = (is_decimal($val->network_manager) ? round($val->network_manager) : $val->network_manager);
                    break;
                case 'Receptionist':
                    $format = (is_decimal($val->receptionist) ? round($val->receptionist) : $val->receptionist);
                    break;
                case 'Hostess':
                    $format = (is_decimal($val->hostess) ? round($val->hostess) : $val->hostess);
                    break;
                case 'Sales Excecutive':
                     $format = (is_decimal($val->Sales_executive) ? round($val->Sales_executive) : $val->Sales_executive);
                    //$format = ($dealer_detail['dealer_service_vol']/(26*10*12))+1; 
                    break;
                case 'GoodLife Executive':
                    //For Assistant Technician (no . of ramps + 1)/2 28-12-13 
                    $format = (is_decimal($val->goodlife_executive) ? round($val->goodlife_executive) : $val->goodlife_executive);
                    //$format = (($dealer_detail['dealer_service_vol']/(26*10*12))+1)/2;
                    break;
                case 'Marketing Executive':
                    $format = (is_decimal($val->marketing_executive) ? round($val->marketing_executive) : $val->marketing_executive);
                    break;
                case 'Rural Support - Sales':
                    $format = (is_decimal($val->rural_support_sales) ? round($val->rural_support_sales) : $val->rural_support_sales);
                    break;
                case 'Accountant':
                    $format = (is_decimal($val->accountant) ? round($val->accountant) : $val->accountant);
                    break;
                case 'Billing Executive':
                    $format = (is_decimal($val->billing_executive) ? round($val->billing_executive) : $val->billing_executive);
                    break;
                case 'Cashier':
                    $format = (is_decimal($val->cashier) ? round($val->cashier) : $val->cashier);
                    break;
                case 'IT Support Executive':
                    $format = (is_decimal($val->it_support_executive) ? round($val->it_support_executive) : $val->it_support_executive);
                    break;
                case 'Delivery Executive':
                    $format = (is_decimal($val->delivery_executive) ? round($val->delivery_executive) : $val->delivery_executive);
                    break;
                case 'PDI Technician':
                    $format = (is_decimal($val->pdi_technician) ? round($val->pdi_technician) : $val->pdi_technician);
                    break;
                case 'RTO Executive':
                    $format = (is_decimal($val->rto_executive) ? round($val->rto_executive) : $val->rto_executive);
                    break;
                case 'Network Executive':
                    $format = (is_decimal($val->network_executive) ? round($val->network_executive) : $val->network_executive);
                    break;
                case 'Housekeeping Staff':
                    $format = (is_decimal($val->housekeeping_staff) ? round($val->housekeeping_staff) : $val->housekeeping_staff);
                    break;
                case 'Pantry':
                    $format = (is_decimal($val->pantry) ? round($val->pantry) : $val->pantry);
                    break;
                case 'Security':
                    $format = (is_decimal($val->security) ? round($val->security) : $val->security);
                    break;
            endswitch;
        }elseif (($dealer_sales_vol_by_12 >= $val->appliacbility_criteria) && ($val->appliacbility_criteria_max == NULL)) {

            switch ($des):
                case 'Genral manager':
                    $format = (is_decimal($val->gm) ? round($val->gm) : $val->gm);
                case 'Showroom manager':
                    $format = (is_decimal($val->Showroom_manager) ? round($val->Showroom_manager) : $val->Showroom_manager);
                    break;
                case 'Network manager':

                    $format = (is_decimal($val->network_manager) ? round($val->network_manager) : $val->network_manager);
                    break;
                case 'Receptionist':
                    $format = (is_decimal($val->receptionist) ? round($val->receptionist) : $val->receptionist);
                    break;
                case 'Hostess':
                    $format = (is_decimal($val->hostess) ? round($val->hostess) : $val->hostess);
                    break;
                case 'Sales Excecutive':
                    $format = (is_decimal($val->Sales_executive) ? round($val->Sales_executive) : $val->Sales_executive);
                    //$format = ($dealer_detail['dealer_service_vol']/(26*10*12))+1;
                    break;
                case 'GoodLife Executive':
                    //For Assistant Technician (no . of ramps + 1)/2 28-12-13 
                    $format = (is_decimal($val->goodlife_executive) ? round($val->goodlife_executive) : $val->goodlife_executive);
                    //$format = (($dealer_detail['dealer_service_vol']/(26*10*12))+1)/2;
                    break;
                case 'Marketing Executive':
                    $format = (is_decimal($val->marketing_executive) ? round($val->marketing_executive) : $val->marketing_executive);
                    break;
                case 'Rural Support - Sales':
                    $format = (is_decimal($val->rural_support_sales) ? round($val->rural_support_sales) : $val->rural_support_sales);
                    break;
                case 'Accountant':
                    $format = (is_decimal($val->accountant) ? round($val->accountant) : $val->accountant);
                    break;
                case 'Billing Executive':
                    $format = (is_decimal($val->billing_executive) ? round($val->billing_executive) : $val->billing_executive);
                    break;
                case 'Cashier':
                    $format = (is_decimal($val->cashier) ? round($val->cashier) : $val->cashier);
                    break;
                case 'IT Support Executive':
                    $format = (is_decimal($val->it_support_executive) ? round($val->it_support_executive) : $val->it_support_executive);
                    break;
                case 'Delivery Executive':
                    $format = (is_decimal($val->delivery_executive) ? round($val->delivery_executive) : $val->delivery_executive);
                    break;
                case 'PDI Technician':
                    $format = (is_decimal($val->pdi_technician) ? round($val->pdi_technician) : $val->pdi_technician);
                    break;
                case 'RTO Executive':
                    $format = (is_decimal($val->rto_executive) ? round($val->rto_executive) : $val->rto_executive);
                    break;
                case 'Network Executive':
                    $format = (is_decimal($val->network_executive) ? round($val->network_executive) : $val->network_executive);
                    break;
                case 'Housekeeping Staff':
                    $format = (is_decimal($val->housekeeping_staff) ? round($val->housekeeping_staff) : $val->housekeeping_staff);
                    break;
                case 'Pantry':
                    $format = (is_decimal($val->pantry) ? round($val->pantry) : $val->pantry);
                    break;
                case 'Security':
                    $format = (is_decimal($val->security) ? round($val->security) : $val->security);
                    break;
            endswitch;
        }
    }

    return $format;
}
