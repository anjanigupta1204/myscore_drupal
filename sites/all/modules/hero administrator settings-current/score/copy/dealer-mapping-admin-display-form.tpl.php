<?php
/**
 * @file
 * Default theme implementation to configure drag_drop.
 *
 * Available variables:
 * - $block_regions: An array of regions. Keyed by name with the title as value.
 * - $block_listing: An array of drag_drop keyed by region and then delta.
 * - $form_submit: Form submit button.
 *
 * Each $block_listing[$region] contains an array of drag_drop for that region.
 *
 * Each $data in $block_listing[$region] contains:
 * - $data->region_title: Region title for the listed block.
 * - $data->block_title: Block title.
 * - $data->region_select: Drop-down menu for assigning a region.
 * - $data->weight_select: Drop-down menu for setting weights.
 * - $data->configure_link: Block configuration link.
 * - $data->delete_link: For deleting user added drag_drop.
 *
 * @see template_preprocess_block_admin_display_form()
 * @see theme_block_admin_display()
 *
 * @ingroup themeable
 */
?>
<?php
  // Add table javascript.
  drupal_add_js('misc/tableheader.js');
  foreach ($block_regions as $region => $title) {
    drupal_add_tabledrag('dealer_mapping', 'match', 'sibling', 'dealer-mapping-region-select', 'dealer-mapping-region-' . $region, NULL, FALSE);
    drupal_add_tabledrag('dealer_mapping', 'order', 'sibling', 'dealer-mapping-weight', 'dealer-mapping-weight-' . $region);
  }
?>
<table id="drag_drop" class="sticky-enabled">
  <thead>
    <tr>
      <th><?php print t('Staff person'); ?></th>
      <th><?php print t('Area office'); ?></th>
      <th><?php print t('Weight'); ?></th>
      <th colspan="2"><?php print t('Operations'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php $row = 0; ?>
    <?php foreach ($block_regions as $region => $title): ?>
      <tr class="region-title region-title-<?php print $region?>">
        <td colspan="5"><?php print $title; ?></td>
      </tr>
      <tr class="region-message region-<?php print $region?>-message <?php print empty($block_listing[$region]) ? 'region-empty' : 'region-populated'; ?>">
        <td colspan="5"><em><?php print t('No drag_drop in this region'); ?></em></td>
      </tr>
      <?php foreach ($block_listing[$region] as $delta => $data): ?>
      <tr class="draggable <?php print $row % 2 == 0 ? 'odd' : 'even'; ?><?php print $data->row_class ? ' ' . $data->row_class : ''; ?>">
        <td class="block"><?php print $data->block_title; ?></td>
      </tr>
      <?php $row++; ?>
      <?php endforeach; ?>
    <?php endforeach; ?>
  </tbody>
</table>

<?php print $form_submit; ?>
