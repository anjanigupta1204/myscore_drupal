<?php
class OM_ResponsiveMobileMenu_Helper_Data extends Mage_Core_Helper_Abstract
{
	/*
	* @function retunrs system configurations
	* @param field code
	*/
	protected function getConfigData($key)
	{
		return Mage::getStoreConfig('rmm/settings/' . $key);
	}
	/*
	* @function check if module is enabled
	*/
	public function checkIfEnabled()
	{
		if($this->getConfigData('enable') == 1){
			return true;
		}
	}

	public function getBreakdownWidth()
	{
	 	return $this->getConfigData('width');
	}

	public function getBackgroundColor()
	{
	 	return $this->getConfigData('background');
	}

	public function getBorderColor()
	{
		return $this->getConfigData('border');
	}
	public function getTextColor()
	{
		return $this->getConfigData('textcolor');
	}
	public function getTargetNav()
	{
		return $this->getConfigData('target');
	}	
}
