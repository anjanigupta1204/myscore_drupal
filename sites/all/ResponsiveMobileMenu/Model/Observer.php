<?php
class OM_ResponsiveMobileMenu_Model_Observer
{
  public function removeTopMenu()
  {
    $website_id = Mage::app()->getWebsite()->getId();
    $layout = Mage::getSingleton('core/layout');
    $helper= Mage::helper('responsivemobilemenu');
    /*if($helper->checkIfEnabled()){
        $layout->getUpdate()->addUpdate('<remove name="catalog.topnav" />');
    }*/

    $layout->getUpdate()->load();
    $layout->generateXml();
    $layout->generateBlocks();

  }
}
