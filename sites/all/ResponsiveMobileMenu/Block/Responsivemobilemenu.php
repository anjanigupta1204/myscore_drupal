<?php
class OM_ResponsiveMobileMenu_Block_Responsivemobilemenu extends Mage_Core_Block_Template{
  public function getMobileMenu($parentId, $isChild)
  {
    $categories = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('id')
    ->addAttributeToSelect('name')
    ->addAttributeToSelect('url_key')
    ->addAttributeToSelect('url')
    ->addAttributeToSelect('is_active')
    ->addAttributeToFilter('is_active','1')
    ->addAttributeToFilter('parent_id',array('eq' => $parentId))
    ->addAttributeToFilter('include_in_menu','1');

    $html= '';
    $class = ($isChild) ? "sub-cat-list" : "cat-list";
    $html .= '<ul class="'.$class.'">';

    foreach($categories as $category)
    {
      $html .= '<li><a href="'.$category->getUrl().'" title="'.$category->getName().'"><span>'.$category->getName().'</a></span>';
      $subcats = $category->getChildren();
       if($subcats != ''){
           $html .= $this->getMobileMenu($category->getId(), true);
       }
       $html .= '</li>';
    }
    $html .= '</ul>';
    return $html;
  }

  public function getMobileMenuCollection()
  {
    return $this->getMobileMenu(2,false);
  } 
}
