<?php

/* ************************************************** */
/* Parts delear Evaluation Question Add/Delete/Update */
/* ************************************************** */

function parts_dealer_question(){
	
	$output = drupal_render(drupal_get_form('pd_add_new_question'));	
	$output .= drupal_render(drupal_get_form('pd_search_question'));	
	
	$output .= drupal_render(drupal_get_form('parts_dealer_question_list'));
	$output .= '<script>
			  function pde_delete_question(){
				  if(confirm("Are you sure want ot delete this question?")){
					  return true;
				}
				  return false;
			  }
			  jQuery(document).ready(function($){
				  if($("#edit-has-sub-question:checked").length){
					  $("div.form-item-sub-question").show();
				  }
				  $("#edit-has-sub-question").click(function(){
					  $("div.form-item-sub-question").slideToggle();
				  });
				  
				  
				  
			  });
			  </script>';
	
	parts_dealer_question_action();
	
	return $output;
}

function parts_dealer_question_action(){
	global $base_url; 
	$path = menu_get_item();

	if(isset($path['map'][2]) && isset($path['map'][3])){

		$action = $path['map'][2];
		$id = $path['map'][3];
		if($action == 'delete'){
			/* checking question is in use before detele */			
			$rs = db_query("SELECT question_id FROM pd_evalution_score_questions c WHERE question_id = $id limit 1");			
			if($rs->rowCount()){
				
				form_set_error('delete',t('This question is in use.'));
				drupal_goto($base_url.'/parts-dealer/evaluation-question');
				return;
			}else{
				db_delete('pd_question')  
				->condition('id',  $id)	
				->execute();	
			  drupal_set_message('Question deleted successfully.');
			  drupal_goto($base_url.'/parts-dealer/evaluation-question');

			}
				
		}

	}

}

function parts_dealer_question_list(){
	
	global $base_url; 
	$build = array();
	$rows = array();
		
	
			
		$query = db_select('pd_question')
			->extend('PagerDefault')
			->extend('TableSort');
			
	    $query->fields('pd_question', array('id', 'question','category_id','min_achieve','status', 'max_mark','question_type','quidelines','sort_order'))
		
	    ->condition('status', array(3), 'NOT IN');
		
		if(isset($_SESSION['pd_question']['search_str'])){
			$query->condition('question', '%'.$_SESSION['pd_question']['search_str'].'%', 'LIKE');							
		}	
		if(isset($_SESSION['pd_question']['category_id']) && $_SESSION['pd_question']['category_id'] != 0 ){			
			$categories = db_select('pd_question_category')->fields('pd_question_category',array('id'))->condition('parent_id',$_SESSION['pd_question']['category_id'])->execute();
			$subcat = array_keys($categories->fetchAllKeyed());
			if(!empty($subcat)){
				$query->condition('category_id',$subcat, 'IN');
			}else{
				$query->condition('category_id',$_SESSION['pd_question']['category_id']);
			}
			
		}
		
		$query->orderBy('id', 'DESC')->limit(100);
		
	$results = $query->execute();
	
	
	$categories = pd_qcategories();
	
	$questionType = array(1 => 'Yes/No', 2 => 'Input', 3 => 'Pre-fill' );
	$i = 1;

	
		
	$header = array('S.N','Question','Category','Score','Min Ach','Question Type','Status','Sort Order','Action');
	
	
	
	foreach ($results as $row) {
		$rows[] = array(			
			 $i++,		
			 $row->question,
			
			 $categories[$row->category_id],		 
			 $row->max_mark,			
			 $row->min_achieve,			
			 $questionType[$row->question_type],			
			($row->status == 1 ? 'Enabled': 'Disabled'),
			 '<input type="text" size="4" value="'. $row->sort_order.'" name="question['.$row->id.']" />',
			'<a href="'.$base_url.'/parts-dealer/evaluation-question/edit/'.$row->id.'" >Edit</a> | <a onclick="return pde_delete_question();" href="'.$base_url.'/parts-dealer/evaluation-question/delete/'.$row->id.'">Delete</a> ',
		);
	}
	
	$variables = array('header' => $header, 'rows' => $rows);
	$content = theme('table', $variables, array());
	
	$build['content'] = array(		
		'total' => array(
			'#markup' => '<h4>Total ('.$results->rowCount().')</h4>',
		),
		
		'questions' => array(
			'#markup' => $content,
		),	
	);
	
	$build['pager'] = array(
		'#theme' => 'pager',
		'#weight' => 5,
	);
	$build['submit'] = array (
		'#type' => 'submit',
		'#value' => 'Save Changes',
		'#submit' => array('pd_submit_question_list_form_submit')
	);
	
  return $build;
}

function pd_submit_question_list_form_submit($form, &$form_state){
	

	if(!empty($_POST)){
		foreach($_POST['question']  as $qid => $sort_order ){
			db_update('pd_question')->fields(array(
			  'sort_order' => $sort_order,  		
			  ))  
			->condition('id', $qid)	
			->execute(); 
		}
		drupal_set_message('Questions updated successfully.');		
	}
}



function pd_search_question($form, &$form_state){
	
	global $base_url; 
	$path = menu_get_item();
	
	$form['fieldset'] = array(
		'#type' => 'fieldset',
		'#title' =>  t('Search Question'),			
		'#collapsible' => TRUE,	
	);
	
	$form['fieldset']['question'] = array(		
		'#type' => 'textfield',	
		'#title' => 'Question',	 
		'#default_value' => (isset($_SESSION['pd_question']['search_str']) ? $_SESSION['pd_question']['search_str'] : ''),	
	); 	
			
	$form['fieldset']['category_id'] = array(
		'#title' => 'Category',				
		'#default_value' => (isset($_SESSION['pd_question']['category_id']) ? $_SESSION['pd_question']['category_id'] : ''),
		'#options'=> pd_qcategories(),		
		'#type' => 'select',
		
	); 
	$form['fieldset']['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Search Question',	
		'#submit' => array('pd_search_question_submit')
	); 
	$form['fieldset']['reset_submit'] = array(
		'#type' => 'submit',
		'#value' => 'Reset Search Filter',	
		'#submit' => array('pd_search_question_submit_reset')
	); 
	
	return $form;
}
function pd_search_question_submit($form, &$form_state){	
	
	$_SESSION['pd_question']['search_str'] = trim($form_state['values']['question']);
	
	$_SESSION['pd_question']['category_id'] = $form_state['values']['category_id'];
}
function pd_search_question_submit_reset($form, &$form_state){	
	unset($_SESSION['pd_question']);
}

function pd_add_new_question( $form, &$form_state){
	global $base_url; 
	$path = menu_get_item();
	
	$edit_question_data = array('id'=>'','question'=>'','sub_question'=>'','category_id'=>'','status'=>'','max_mark'=>'','question_type'=>'','quidelines'=>'','obiee_view_id'=>'','min_achieve'=> '','value_type'=>'');
	 
	 /* edit question */
	if(isset($path['map'][2]) && isset($path['map'][3])){		
		$action = $path['map'][2];
		$id = $path['map'][3];
		if($action == 'edit'){			
			$edit_question_data = db_query("SELECT * FROM pd_question c WHERE id = '$id'")->fetchAssoc();			
		}

	}
	
	$form['fieldset'] = array(
		'#type' => 'fieldset',
		'#title' =>  ($path['map'][3] != '' ? t('Update Question') : t('Add New Question')),			
		'#collapsible' => (isset($path['map'][3]) != '' ? FALSE : TRUE),
		'#collapsed' => (isset($path['map'][3]) != '' ? FALSE : TRUE), 
	
	);
	
	
	$form['fieldset']['question_id'] = array(
		'#type' => 'hidden',
		'#name' => 'question_id',
		'#default_value' => $edit_question_data['id'],
	); 	
	
	$form['fieldset']['question'] = array(		
		'#type' => 'textfield',	
		'#title' => 'Question',				
		'#required' => true,
		'#maxlength' => '256',
		'#default_value' => $edit_question_data['question'],
		
	); 	
		
	
	$form['fieldset']['category_id'] = array(
		'#title' => 'Category',		
		'#attributes' => array('class' => array('chosen-select')),
		'#default_value' => $edit_question_data['category_id'],
		'#options'=> pd_qcategories(),
		'#required' => true,
		'#type' => 'select',
		
	); 
	
	$form['fieldset']['max_mark'] = array(		
		'#type' => 'textfield',	
		'#title' => 'Score',				
		'#required' => true,
		'#default_value' => $edit_question_data['max_mark'],
		
	); 		
	
	$form['fieldset']['min_achieve'] = array(		
		'#type' => 'textfield',	
		'#title' => 'Min Achievement',				
		'#required' => true,
		'#default_value' => $edit_question_data['min_achieve'],
		
	); 	
	
	$form['fieldset']['question_type'] = array(
		'#title' => 'Question Type',		
		'#attributes' => array('class' => array('chosen-select')),
		'#default_value' => $edit_question_data['question_type'],
		'#options'=> array(1 => 'Yes/No', 2 => 'Input', 3 => 'Pre-fill' ),
		'#required' => true,
		'#type' => 'select',		
	); 
	$form['fieldset']['value_type'] = array(
		'#title' => 'Value In',		
		'#attributes' => array('class' => array('chosen-select')),
		'#default_value' => $edit_question_data['value_type'],
		'#options'=> array(0 => 'Select' , 1 => 'Parcentage', 2 => 'Actual' ),
		'#required' => true,
		'#type' => 'select',		
	); 
	
	$form['fieldset']['obiee_view_id'] = array(
		'#title' => 'KPI Data',		
		'#attributes' => array('class' => array('chosen-select')),
		'#default_value' => $edit_question_data['obiee_view_id'],
		'#options'=> pd_obiee_view_id(),
		'#required' => true,
		'#type' => 'select',		
	);
	
	$form['fieldset']['quidelines'] = array(
		'#title' => 'Quide Lines',		
		'#type' => 'textarea',
		 '#rows' => "4" ,
		 '#cols'=> "30",
		'#default_value' => $edit_question_data['quidelines'],
		
	); 
	$form['fieldset']['status'] = array(
		'#attributes' => array('class' => array('chosen-select')),
		'#default_value' => $edit_question_data['status'],
		'#options'=> array( 1 => 'Enbled',0 => 'Disabled' ),
		'#type' => 'select',
		
	); 
	
	$form['fieldset']['submit'] = array(
		'#type' => 'submit',
		'#value' => ($path['map'][3] != '' ? 'Update Question' : 'Add New Question'),	
		'#submit' => array('pd_add_new_question_save')
	); 
	
	return $form;
}

function pd_add_new_question_save($form, &$form_state){		
		
		
		if(!is_numeric($form_state['values']['max_mark'])){
			form_set_error($form_state['values']['max_mark'],t('Score must be a numeric value.'));
		}else{		
		if($form_state['values']['question_id'] != '' ){
			db_update('pd_question')->fields(array(
			  'question' => $form_state['values']['question'],			 
			  'status' =>  $form_state['values']['status'], 
			  'quidelines' => $form_state['values']['quidelines'],  			  
			  'category_id' => $form_state['values']['category_id'],  			  
			  	  
			  'value_type' => $form_state['values']['value_type'],
			  'max_mark' => (int)$form_state['values']['max_mark'],
			  'min_achieve' => $form_state['values']['min_achieve'],
			  'obiee_view_id' => $form_state['values']['obiee_view_id'],
			  'question_type' => $form_state['values']['question_type'],  		
			  ))  
			->condition('id', $form_state['values']['question_id'])	
			->execute();			
			drupal_set_message('Question updeted successfully.');

			drupal_goto($base_url.'/parts-dealer/evaluation-question');	
			
		}else{
			db_insert('pd_question')->fields(array(      
			  'question' => $form_state['values']['question'],			 
			  'status' =>  $form_state['values']['status'], 
			  'quidelines' => $form_state['values']['quidelines'],  			  
			  'category_id' => $form_state['values']['category_id'],  			  
			  'value_type' => $form_state['values']['value_type'],  			  
						  
			  'max_mark' => (int)$form_state['values']['max_mark'],  			  
			  'min_achieve' => $form_state['values']['min_achieve'],  			  
			  'obiee_view_id' => $form_state['values']['obiee_view_id'],  			  
			  'question_type' => $form_state['values']['question_type'],  			  
			  'sort_order' => '0',			  
			))->execute();
			drupal_set_message('Question added successfully.');		
		}
	}
}

function pd_qcategories($parent_id = 0 ){
	
	$result = db_query("SELECT * FROM pd_question_category WHERE parent_id = $parent_id");	
	$category[] = 'ALL';
	while($record = $result->fetchAssoc()) {		
		$category[$record['id']] = $record['name'];
		
		$sub_result = db_query("SELECT * FROM pd_question_category WHERE parent_id =".$record['id']);
		if($sub_result->rowCount())
			while($srecord = $sub_result->fetchAssoc()){
				$category[$srecord['id']] = '-'.$srecord['name'];
			}
		
	}
	
	return $category;
}

function pd_obiee_view_id(){
	$result = db_select('pd_obiee_views', 'v')->fields('v', array('id', 'view_name'))->execute()->fetchAll(PDO::FETCH_ASSOC);
	$data_source[0] = 'Select';
	foreach($result as $views){
			$data_source[$views['id']] = $views['view_name'];
	}
	
	return $data_source;
}