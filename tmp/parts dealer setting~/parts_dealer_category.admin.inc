<?php

/* ************************************************** */
/* Parts delear Evaluation Category Add/Delete/Update */
/* ************************************************** */

function parts_dealer_category(){
	
	$output = drupal_render(drupal_get_form('pd_add_new_category'));
	$output .= drupal_render(drupal_get_form('parts_dealer_category_list'));
	$output .= '<script>
			  function pde_delete_cat(){
				  if(confirm("Are you sure want ot delete this category?")){
					  return true;
				  }
				  return false;
			  }
			  </script>';
	
	parts_dealer_category_action();
	
	return $output;
}
/* delete category */
function parts_dealer_category_action(){
	global $base_url; 
	$path = menu_get_item();

	if(isset($path['map'][2]) && isset($path['map'][3])){

		$action = $path['map'][2];
		$id 	= (int)$path['map'][3];
		/* set status 3 for detele category */
			if($action == 'delete'){	
			db_delete('pd_question_category')  
			->condition('id',  $id)	
			->execute();	
			
		  drupal_set_message('Category deleted successfully.');
		  drupal_goto($base_url.'/parts-dealer/evaluation-category');
		}

	}

}

function parts_dealer_category_list(){
	global $base_url; 
	$build = array();
	$rows = array();
	
	$header = array('Name','Status','Questions','Sort Order','Action');
	
	$query = db_select('pd_question_category','c')
		->extend('PagerDefault')
		->extend('TableSort');
		  $query->fields('c', array('id', 'name','alias','parent_id','sort_order', 'status',))
		  ->condition('status', array(3), 'NOT IN')
		  ->condition('parent_id', 0)
		->orderBy('sort_order', 'ASC')		
		->limit(20)
		;
		
	$results = $query->execute();
	
	foreach ($results as $row) {
		
		$q = db_select('pd_question', 'q')
		->fields('q',array('id'))
		->condition('id', $row->id)
		->execute();
		
		/* main categories */
		$rows[] = array(			
				
			'<b>'.$row->name.'</b>',					
			($row->status == 1 ? 'Enabled': 'Disabled'),
			$q->rowCount(),
			'<input type="text" size="4" value="'. $row->sort_order.'" name="category['.$row->id.']" />',
		
			'<a href="'.$base_url.'/parts-dealer/evaluation-category/edit/'.$row->id.'" >Edit</a> | <a onclick="return pde_delete_cat();" href="'.$base_url.'/parts-dealer/evaluation-category/delete/'.$row->id.'">Delete</a> ',
		
		);
		
		
		$sub_query = db_select('pd_question_category','c');		
		  $sub_query->fields('c', array('id', 'name','alias','parent_id','sort_order', 'status',))
		  ->condition('status', array(3), 'NOT IN')
		  ->condition('parent_id', $row->id)
		->orderBy('sort_order', 'ASC')	
		;
		
		$sub_results = $sub_query->execute();
		
		foreach ($sub_results as $row) {
			
			$qq = db_select('pd_question', 'q')
			->fields('q',array('id'))
			->condition('category_id', $row->id)
			->execute();
			
			/* main categories */
			$rows[] = array(				
				($row->parent_id != '0' ? '-'.$row->name: $row->name),					
				($row->status == 1 ? 'Enabled': 'Disabled'),
				 $qq->rowCount(),
				'<input type="text" size="4" value="'. $row->sort_order.'" name="category['.$row->id.']" />',
				
				'<a href="'.$base_url.'/parts-dealer/evaluation-category/edit/'.$row->id.'" >Edit</a> | <a onclick="return pde_delete_cat();" href="'.$base_url.'/parts-dealer/evaluation-category/delete/'.$row->id.'">Delete</a> ',
				
				
			);
			
		}
				
		
		
	}
	
	$variables = array('header' => $header, 'rows' => $rows);
	$content = theme('table', $variables, array());
	
	$build['content'] = array(
		'categories' => array(
			'#markup' => $content,
		),
		
	);
	$build['submit'] = array (
		'#type' => 'submit',
		'#value' => 'Save Changes',
		'#submit' => array('pd_submit_category_list_form_submit')
	);
	$build['pager'] = array(
		'#theme' => 'pager',
		'#weight' => 5,
	);

  return $build;
}

function pd_submit_category_list_form_submit($form, &$form_state){	
	if(!empty($_POST)){
		foreach($_POST['category']  as $cid => $sort_order ){
			db_update('pd_question_category')->fields(array(
			  'sort_order' => $sort_order,  		
			  ))  
			->condition('id', $cid)	
			->execute(); 
		}
		drupal_set_message('Categories updated successfully.');	
	}
}

/* add new categories */
function pd_add_new_category( $form, &$form_state){
	
	

	global $base_url; 
	$path = menu_get_item();
	
	
	$edit_cat_data = array('id'=>'','name'=> '','alias'=>'','status'=>'','parent_id'=>'');
	
	if(isset($path['map'][2]) && isset($path['map'][3])){	
		
		$action = $path['map'][2];
		$id = $path['map'][3];
		if($action == 'edit'){			
			$edit_cat_data = db_query("SELECT * FROM pd_question_category c WHERE id = '$id'")->fetchAssoc();			
		}

	}
		
	$form = array();	
	$query =  "SELECT * FROM pd_question_category c WHERE 1";		
	$result = db_query($query);	
	$options = array();
	
	$options[0] = 'None';
	while($record = $result->fetchAssoc()) {		
		$options[$record['id']] = $record['name'];
	}
	
	
	$form['fieldset'] = array(
		'#type' => 'fieldset',
		'#title' => t('Add New Category'),			
		'#collapsible' => ($path['map'][3] != '' ? FALSE : TRUE),
		'#collapsed' => ($path['map'][3] != '' ? FALSE : TRUE), 
	
	);
	
	
	$form['fieldset']['id'] = array(
		'#type' => 'hidden',
		'#name' => 'category_id',
		'#default_value' => $edit_cat_data['id'],
	); 	
	
	$form['fieldset']['name'] = array(
		'#title' => 'Name',		
		'#type' => 'textfield',		
		'#required' => true,
		'#default_value' => $edit_cat_data['name'],
		
	); 	
	
	$form['fieldset']['alias'] = array(
		'#title' => 'Alias',		
		'#type' => 'textfield',		
		'#required' => true,
		'#default_value' => $edit_cat_data['alias'],
	); 

	$form['fieldset']['parent'] = array(
		'#title' => t('Parent'),
		'#attributes' => array('class' => array('chosen-select')),
		'#options'=> $options,
		'#type' => 'select',
		'#default_value' => $edit_cat_data['parent_id'],
		
	); 
	
	$form['fieldset']['status'] = array(
		'#attributes' => array('class' => array('chosen-select')),
		'#default_value' => $edit_cat_data['status'],
		'#options'=> array( 1 => 'Enbled',0 => 'Disabled' ),
		'#type' => 'select',
		
	); 
	
	$form['fieldset']['submit'] = array(
		'#type' => 'submit',
		'#value' => (isset($path['map'][3]) != '' ? 'Update Category' : 'Add New Category'),	
		'#submit' => array('pd_add_new_category_save')
	); 
	
	return $form;
}
/* save/update categories */
function pd_add_new_category_save($form, &$form_state){		
		
		if($form_state['values']['category_id'] != '' ){
			db_update('pd_question_category')->fields(array(
			  'name' => $form_state['values']['name'],
			  'alias' =>  $form_state['values']['alias'],   
			  'parent_id' =>  $form_state['values']['parent'],   
			  'status' =>  $form_state['values']['status']))
			->condition('id', $form_state['values']['category_id'])	
			->execute();			
			drupal_set_message('Category updeted successfully.');			
		}else{
			db_insert('pd_question_category')->fields(array(      
			  'name' => $form_state['values']['name'],
			  'alias' =>  $form_state['values']['alias'],   
			  'parent_id' =>  $form_state['values']['parent'],   
			  'status' =>  $form_state['values']['status'],   			  
			  'sort_order' => '0',			  
			))->execute();
			drupal_set_message('Category added successfully.');		
		}
			
}