<?php

function pd_evaluation_score_pdf($pd_code,$pd_name,$evaluation_by_staff,$evaluation_by_staff_name,$evaluation_month) { 

if((int)$pd_code && (int)$evaluation_by_staff && $evaluation_month != '' ){	
		
	$score_query = db_select('pd_evalution_score', 'score')
		  ->fields('score')	  
		  ->condition('score.pd_code', $pd_code)		  
		  ->condition('score.evalution_by_staff', $evaluation_by_staff)		  
		  ->condition('score.evalution_month', $evaluation_month)		  
		  ->execute();
		  
	
	$rowCount = $score_query->rowCount();

	if($rowCount){
		
		$finalscore = 0;
		
		$score_result 		= $score_query->fetchAssoc();			
		
		$score_questions = db_query("SELECT sq.*,
				q.category_id,q.question,q.question_type,
				(SELECT name from pd_question_category WHERE id = q.category_id) category_name,
				(SELECT parent_id from pd_question_category WHERE id = q.category_id) parent_category_id,
				(SELECT name from pd_question_category WHERE id = (SELECT parent_id from pd_question_category WHERE id = q.category_id)) parent_category_name 
				FROM `pd_evalution_score_questions`sq LEFT JOIN pd_question q on sq.question_id = q.id WHERE pd_score_id = {$score_result[id]} order by parent_category_name DESC,category_name ASC")->fetchAllAssoc('question_id');
			/* 	
			echo '<pre>';
			print_R($score_questions);			
			die; */
			
			
			$finalscore = db_query("SELECT SUM(q.max_mark) overall_score,SUM(sq.score) obtain_score,((SUM(sq.score)*100)/SUM(q.max_mark)) parcent  FROM `pd_evalution_score_questions`sq LEFT JOIN pd_question q on sq.question_id = q.id WHERE sq.pd_score_id = {$score_result[id]}")->fetchAssoc();
			//print_r($finalscore);
			//die;
			$html = '';
			
			$html .= '<div>';
			$html .= '<h3>Score '.round($finalscore['parcent']).'%</h3>';
			
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';
			$html .= '<tr style="background-color:#000;color:#fff">';
			$html .= '<td align="center" >Distributor Code</td>';
			$html .= '<td align="center" >Distributor Name</td>';			
			$html .= '<td align="center" >Created By</td>';
			$html .= '<td align="center" >Start Date</td>';
			$html .= '<td align="center" >Evalution Month</td>';
			$html .= '</tr>';			
			$html .= '<tr>';
			$html .= '<td align="center"   >'.$score_result['pd_code'].'</td>';
			$html .= '<td align="center"   >'.$pd_name.'</td>';			
			$html .= '<td align="center"   >'.$evaluation_by_staff_name.'</td>';
			$html .= '<td align="center"   >'.date('d-m-Y',strtotime($score_result['start_date'])).'</td>';
			$html .= '<td align="center"   >'.$score_result['evalution_month'].'</td>';
			$html .= '</tr>';
			$html .= '</table>';
			
			$html .= '<table style="border-collapse: collapse;" width="100%" cellpadding="1" border="1">';			
			$category = '';
			
			foreach($score_questions as $row){
				
				if($row->parent_category_name != $category){
				
					$html .= '<tr> ';							
					$html .= '<td colspan="8">&nbsp;</td>';						
					$html .= '</tr>';
					$html .= '<tr> ';							
					$html .= '<td colspan="8"><h2>'.$row->parent_category_name.'</h2></td>';				
					$html .= '</tr>';					
					$html .= '<tr style="background-color:#000;color:#fff">';
					$html .= '<th width="10%">Category</th>';		
					$html .= '<th width="30%">Parameter</th>';		
					$html .= '<th align="center" width="10%">Target</th>';		
					$html .= '<th align="center" width="10%">Actual</th>';		
					$html .= '<th align="center" width="10%" >% Ach</th>';		
					$html .= '<th align="center" width="10%" >Score</th>';		
					$html .= '<th align="center" width="10%" >Remarks/<br>Action Plan</th>';		
					$html .= '<th align="center" width="10%" >Target Date</th>';		
					$html .= '</tr>';
					
					$category = $row->parent_category_name;
					
				}				
				
								
				$html .= '<tr>';
				$html .= '<td>'.$row->category_name.'</td>';	
				$html .= '<td>'.$row->question.'</td>';	
				if($row->question_type == 1){ 
					// Yes/no
					if($row->target == 1){
						$desc = 'Yes';
					}else if($row->target == 2){
						$desc = 'No';
					}					
					$html .= '<td align="center" colspan="3">'.$desc.'</td>';	
					
				}else{
					$html .= '<td align="center" >'.$row->target.'</td>';		
					$html .= '<td align="center" >'.$row->actual.'</td>';	
					$html .= '<td align="center" >'.$row->actual_par.'</td>';		
				}
				
				$html .= '<td align="center" >'.$row->score.'</td>';							
				$html .= '<td align="center" >'.$row->action_plan.'</td>';							
				$html .= '<td align="center" >'.$row->target_date.'</td>';							
				$html .= '</tr>';
					
			}
			
			$html .= '</table>';
			
			$html .= '<table style="border-collapse: collapse;" cellpadding="1" border="1">';					
			$html .= '<tr> <td colspan="8"  ><h2 style="margin-top: 20px; margin-bottom: 0px;">TSM Remark</h2></td></tr>';				
			$html .= '<tr> <td colspan="8"   >'.$score_result['tsm_feedback'].'</td></tr>';
			
			$html .= '<tr><td colspan="8"  ><h2 style="margin-top: 20px; margin-bottom: 0px;">ASM Remark</h2></td></tr>';				
			$html .= '<tr><td colspan="8"   >'.$score_result['asm_feedback'].'</td></tr>';		
			$html .= '</table>';
			$html .= '</div>';
			
			
		//echo $html;
		
		require_once DRUPAL_ROOT ."/sites/all/modules/print/lib/tcpdf/tcpdf.php";
		
		$filename = 'Distributor_Evaluation_'.$pd_code.'-'.time().'.pdf';
		
		$pdf = new TCPDF();
		// write data
		$pdf->AddPage('P');
		$pdf->SetFont('helvetica', '', 8);
		$pdf->writeHTML($html,true, false, false, false, '');
		// end write
		$pdfpath = DRUPAL_ROOT.'/evaluation_pdf/'.$filename;
		//$pdf->Output($filename, 'F');
		//$pdf->Output($filename, 'D');
		$content = $pdf->Output($pdfpath,'F');
		
		return  $pdfpath;
		//die($pdf->Output($filename, 'I'));
		
		}		
	}

}