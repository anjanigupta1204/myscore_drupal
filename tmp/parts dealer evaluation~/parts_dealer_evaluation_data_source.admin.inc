<?php
function pd_question_kpi_data_source($field,$obiee_view_id,$evaluation_month,$dealer_code){
	
	
	 //echo $field,'::',$obiee_view_id,'::',$evaluation_month,'::',$dealer_code.'<br>'; 

	if($obiee_view_id == 0){ return; }

	if(empty($_SESSION['obiee_data'][$dealer_code][$evaluation_month])){
		if($obiee_view_id != 0){
			$question_score_query = db_select('pd_obiee_data', 'ob')
					->fields('ob',array('pd_code','month_year','target','achievement','ach_parcent','obiee_view_id'))	  
					->condition('ob.pd_code',$dealer_code)		  
					//->condition('ob.obiee_view_id',$obiee_view_id)		  
					->condition('ob.month_year',date('mY',strtotime($evaluation_month)))		  
					->execute()
					->fetchAllAssoc('obiee_view_id')
					//->fetchAssoc()
					;

			//echo '<pre>';
			$_SESSION['obiee_data'][$dealer_code][$evaluation_month] = $question_score_query;
			//echo '</pre>';
			//die;
		}
	}
	
	
	//print_r($_SESSION['obiee_data'][$dealer_code][$evaluation_month]);
	//die;
	
	
	if($_SESSION['obiee_data'][$dealer_code][$evaluation_month][$obiee_view_id]->$field != ''){
		return $_SESSION['obiee_data'][$dealer_code][$evaluation_month][$obiee_view_id]->$field;
	}
	//return 0;
	
	
	//if($field == 'target' && $obiee_view_id == 0)
	//return '98';
	//return '94';
}

