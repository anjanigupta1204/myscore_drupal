<?php
function previous_month_remark_and_target_date(){
 
global $user, $base_url; 	
$path = menu_get_item();
//print_R($path);
/* 
[map] => Array
(
	[0] => parts-dealer-evaluation
	[1] => prev
	[2] => remark-and-target-date
	[3] => 30000
	[4] => Aug-2016
	[5] => 9479
	[6] => 4
) */
$result = '';
if((int)$path['map'][3] && $path['map'][4] != '' && (int)$path['map'][5] && (int)$path['map'][6]){
	
	$previous_months = array(
		date('M-Y',strtotime($path['map'][4].' -1 month')), 
		date('M-Y',strtotime($path['map'][4].' -2 month')),
		date('M-Y',strtotime($path['map'][4].' -3 month'))
	);
	
	/* getting previous month action plan and target date */
		
	$prev_months_remarks = db_select('pd_evalution_score', 'score')
		->fields('score',array('id','evalution_month'))	  
		->condition('score.evalution_by_staff',(int)$path['map'][5])		  
		->condition('score.evalution_month',$previous_months,'IN')		  
		->condition('score.pd_code',(int)$path['map'][3])	
		->orderBy('score.evalution_month','DESC')		
		->execute()->fetchAllAssoc('id');
	
	$score_ids = array_keys($prev_months_remarks);		
	
	if(!empty($score_ids)){		
		$query = db_select('pd_evalution_score_questions', 'score')
		->fields('score',array('pd_score_id','action_plan','target_date','question_id'))  
		->condition('score.pd_score_id',$score_ids, 'IN')		  
		->condition('score.question_id',(int)$path['map'][6])		  
		->execute();
			
		/* echo '<pre>';
		print_r($query);	
		 */
		$result = '';
		if($query->rowCount()){
			$result = '<table border="1">';
			$result .= '<tr><td colspan="3"><h2>Previous Data<h2></td></tr>';			
			$result .= '<tr><th>Month</th><th>Remark/Action Plan</th><th>Target Date</th></tr>';			
			foreach($query->fetchAll() as $row){
					$result .= '<tr><td>'.$prev_months_remarks[$row->pd_score_id]->evalution_month.'</td><td>'.($row->action_plan != '' ? $row->action_plan : 'None' ).'</td><td>'.($row->target_date != '' ? $row->target_date : 'None' ).'</td></tr>';
			}
			$result .= '</table>';
			
		}else{
			 $result = '<div style="text-align:center;">There are no previous data found!!</div>';
		}		
	}else{ 
		 $result = '<div style="text-align:center;">There are no previous data found!!</div>';
	}	
}
echo $result;
exit;
}