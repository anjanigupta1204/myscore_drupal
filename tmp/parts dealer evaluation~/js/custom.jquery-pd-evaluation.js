/*
* Company : Orange Mantra
* Author : Kumar.shiv@orangemantra.in
* Package : Myscore
* Module : Distribute Evaluation
* Creation Date : July , 2016
*/
var sk = jQuery.noConflict();
var pd_form = 'pd-evaluation-display-form';
sk(document).ready(function() {
	
	 var currentRequest = errors_in_criteria = null;
/* set as save or draft on click button */	 

	/* submit form */
	sk( '#'+pd_form +' #save_draft, #'+pd_form +' #save').click(function(e){
		 e.preventDefault();
		
		if(sk(this).attr('id') == 'save'){
			sk("#save_as").val(1);				
			pd_validate_required_fields();
			
			/* count error in form */
			if(pd_count_validation_error()){
				pd_set_validation_error();
				sk.colorbox({html:errors_in_criteria});
				return false;
			}			 			/* save data */
			pd_autosave_data2();
			
		} else if(sk(this).attr('id') == 'save_draft'){
			sk("#save_as").val(0);	
			/* save data on submit */
			pd_autosave_data2();
		}
		
	});
	/* autosave on click criteria title*/
	sk( '#'+pd_form +' fieldset a.fieldset-title').click(function(){	
		pd_autosave_data();		
	});
		
	/* calculating parcentage for actual values */
	
	sk(document).on('keyup change','#'+pd_form+' input[id^=edit-question-target-]:not([readonly="readonly"])',function(e){
				pd_number_validation(this);
				pd_calculate_percentages(this);
				
	});
	
	sk(document).on('keyup change','#'+pd_form+' input[id^=edit-question-actual-]:not([readonly="readonly"])',function(e){
		 pd_number_validation(this);
		 pd_calculate_percentages(this);
	});
	
/* previous action plan and date */
sk('.prev-month-remark').click(function(e){
		e.preventDefault();	
		var q = sk(this).closest('tr').find('[id^=edit-question-q-]').text();
		sk.colorbox({		
			title: function(){		
			  return '<br><br><p><b style="color:green">Previous 3 months "Action Plan" and "Target Date" for <i>'+q+'</i></b></p>';
			},
			href:sk(this).attr('href'),
			innerWidth:'460',
			height:'320',
			
		});		
});

function pd_validate_required_fields(){
	var field_value = '';
	sk('#'+pd_form+' input[id^=edit-question-target-]:not([readonly="readonly"])').each(function(){				
			field_value = sk(this).val();
			if(field_value == ''){
				sk(this).addClass('validation_error');
			}else{
				sk(this).removeClass('validation_error');
			}
	});
	sk('#'+pd_form+' input[id^=edit-question-actual-]:not([readonly="readonly"])').each(function(){
			field_value = sk(this).val();
			if(field_value == ''){
				sk(this).addClass('validation_error');
			}else{
				sk(this).removeClass('validation_error');
			}			
	});
	
	/* validate yes/no questions */
	 sk('#'+pd_form+' fieldset tr').each(function(){		  

		var current_tr = sk(this);		
		var have_radio = current_tr.find('[type="radio"]').length;
		//console.log(have_radio);
		/* actual value validation */		
		if(have_radio){
			var radio_checked = current_tr.find('[type="radio"]:checked');						console.log(radio_checked.length);			
			if(radio_checked.length == 1){
				current_tr.removeClass('validation_error');
			}else{
				current_tr.addClass('validation_error');
			}
		}
	 });
	/* validate radio buttons */	
}

function pd_count_validation_error(){
	return sk('.validation_error').length;
}

function pd_set_validation_error(){
	if(pd_count_validation_error()){
		var errors_criteria = [];
		errors_in_criteria = '<p style="font-size:15px"><b>There are <span style="color:#ff0000">'+pd_count_validation_error()+ '</span> error(s) in the form. Please correct all the errors.</b></p>';
		
		errors_in_criteria += '<p style="font-size:15px"><i>The errors are in the following criteria(categories).</i></p>';
		
		sk('.validation_error').each(function(){
			var criteria = sk(this).closest('fieldset.collapsible');						var id 	= criteria.data('criteriaid');			var cat = criteria.data('criteria');
			errors_criteria[id] = cat;
		});		//console.log(errors_criteria);
		errors_criteria = errors_criteria.filter(function( element ) {
			   return !!element;
		});		console.log(errors_criteria);
		errors_in_criteria += "<ul style=' color: red; list-style: inside disc;margin-left:-30px;margin-top: 0;'><li>"+errors_criteria.join('<li>','</li>')+"</li></ul>"; 
	}
}

/* validate number */	
function pd_number_validation(_this){
  var numbers = /^[0-9.]+$/; 
  var replaceregx = /[^0-9.]/g;			 
  var thisval = sk(_this).val();  
  if(!thisval.match(numbers)) 
  { 
	sk(_this).val(thisval.replace(replaceregx,''));		
	return true;
  }
}

/* auto save data */
function pd_autosave_data(){		if(sk('#is_submited').val() !=0 ){ return false;}	
	currentRequest = sk.ajax({
	  url: Drupal.settings.pd_evaluation.hostname+"/parts-dealer-evaluation/autosave",
	  beforeSend: function() {   
		  if(currentRequest != null) { 
			currentRequest.abort();						
		  }
		
		sk(pd_form).find('[type="submit"]').attr('disabled','disabled');
	},
	  method: "POST",		 
	  data: sk('#'+pd_form).serializeArray(),
	  dataType: "html",
	  
	}).done(function( data ) {
		sk('#'+pd_form).find('[type="submit"]').removeAttr('disabled');	
		 
	});
}

/*  save/draft */
function pd_autosave_data2(){
	if(sk('#is_submited').val() != 0){ return false;}
	currentRequest = sk.ajax({
	  url: Drupal.settings.pd_evaluation.hostname+"/parts-dealer-evaluation/autosave",
	  beforeSend: function() {   
		if(currentRequest != null) { 
			currentRequest.abort();  
			sk.colorbox({html:"Please wait data is saving ....."});
		}
			sk('#'+pd_form).find('[type="submit"]').attr('disabled','disabled');
			
	},
	  method: "POST",		 
	  data: sk('#'+pd_form).serializeArray(),
	  dataType: "html",
	  
	}).done(function( data ) {
		sk('#'+pd_form).find('[type="submit"]').removeAttr('disabled');
		sk("#"+pd_form).unbind('submit').submit();
	});
}


/* calculating parcentage for actual values */
function pd_calculate_percentages(_this){
	var title = sk(_this).attr('title');
	if(title == 'Actual Value'){
		var target = sk(_this).closest('tr').find('[id^=edit-question-target-]').val();
		var actual = sk(_this).closest('tr').find('[id^=edit-question-actual-]:not([readonly="readonly"])').val();
		var actual_par = sk(_this).closest('tr').find('[id^=edit-question-actual-par-]');
		//console.log(target);
		//console.log(actual);
		if((parseInt(actual) != 0 && actual != '') && (parseInt(target) != 0  && target != '')){
			var per = (parseInt(actual)/parseInt(target))*100;
			actual_par.val(per);
		}else{
			actual_par.val(0);
		}
		//console.log(actual_par);
	}
}

/* make readonly fields after submit */
if(sk('#is_submited').val() != 0){
	jQuery('[id^=edit-question-target-],textarea').attr('disabled',true);	
}
/* make readonly fields after submit */

/* add percentage symbol for percentage values */
sk('input[title="% Value"]').after('<span class="percentage">%</span>');

/* open calender */
sk('[data-setdate=""],[data-setdate="0"]').datepicker({ maxDate: 0,
		beforeShow: function(input, inst){
		inst.dpDiv.css({marginLeft: -input.offsetWidth + 'px'});
},
	dateFormat: 'dd/mm/yy',
}); 	

/* end ready function */
});/** Company 	: Orange Mantra* Author 	: Kumar.shiv@orangemantra.in* Package 	: Myscore* Module 	: Distribute Evaluation* Creation Date : July , 2016*/